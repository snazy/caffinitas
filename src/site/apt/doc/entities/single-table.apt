  ------------------------------------------------------
  Single Table Inheritance
  ------------------------------------------------------


  The root class of a <single-table> entity inheritance structure must be annotated with both
{{{../../apidocs/index.html?org/caffinitas/mapper/annotations/CEntity.html}<<<@CEntity>>>}} and
{{{../../apidocs/index.html?org/caffinitas/mapper/annotations/CInheritance.html}<<<@CInheritance>>>}}<<<(type = >>>{{{../../apidocs/org/caffinitas/mapper/annotations/InheritanceType.html#SINGLE_TABLE}<<<InheritanceType.SINGLE_TABLE>>>}}<<<)>>>.

  You may want to override the default behaviour (column name and type) of the discriminator columns using
{{{../../apidocs/index.html?org/caffinitas/mapper/annotations/CDiscriminatorColumn.html}<<<@CDiscriminatorColumn>>>}} on the root entity and
{{{../../apidocs/index.html?org/caffinitas/mapper/annotations/CDiscriminatorValue.html}<<<@CDiscriminatorValue>>>}} annotation(s) on the root
and inherited entities. The default discriminator column name is <<<discr>>> of type <<<TEXT>>>. The default discriminator value is the
simple class name of the entity passed through the name mapper.

  All operations (read and write) are performed against the only table of the inheritance structure.

  Example code:

------------------------------------------------------------------------------------------------------------------------------
 @CEntity(partitionKey = "commonId")
 @CInheritance(type = InheritanceType.SINGLE_TABLE)
 @CDiscriminatorColumn(column = @CColumn(columnName = "discr", type = DataTypeName.TEXT))
 @CDiscriminatorValue("root")
 public class StRootEntity {
   @CColumn private UUID commonId;
 }

 @CEntity
 @CDiscriminatorValue("one")
 public class StEntityOne extends StRootEntity {
   // some attributes
 }

 @CEntity
 @CDiscriminatorValue("two")
 public class StEntityTwo extends StRootEntity {
   // some attributes
 }

 @CEntity
 @CDiscriminatorValue("one_one")
 public class StEntityOneOne extends StEntityOne {
   // some attributes
 }
------------------------------------------------------------------------------------------------------------------------------

