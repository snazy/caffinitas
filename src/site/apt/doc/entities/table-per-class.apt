  ------------------------------------------------------
  Table-Per-Class Inheritance
  ------------------------------------------------------

  The root class of a <table-per-class> entity inheritance structure must be annotated with both
{{{../../apidocs/index.html?org/caffinitas/mapper/annotations/CEntity.html}<<<@CEntity>>>}} and
{{{../../apidocs/index.html?org/caffinitas/mapper/annotations/CInheritance.html}<<<@CInheritance>>>}}<<<(type = >>>{{{../../apidocs/org/caffinitas/mapper/annotations/InheritanceType.html#TABLE_PER_CLASS}<<<InheritanceType.TABLE_PER_CLASS>>>}}<<<)>>>.

  Write operations like insert/update/delete directly go to the correct table.

  Read operations are performed against a set of tables of the inheritance structure at once.
The implementation will start with the entity type you pass to the <<<load*>>> methods of
{{{../../apidocs/org/caffinitas/mapper/core/PersistenceSession.html}<<<PersistenceSession>>>}}. If you pass the root entity of the example below
(<<<TpcRootEntity>>>), it will query the tables for <<<TpcRootEntity>>>, <<<TpcEntityOne>>>, <<<TpcEntityTwo>>>, <<<TpcEntityOneOne>>>.
If you pass the entity type <<<TpcEntityOne>>>, it will only query the tables for <<<TpcEntityOne>>> and <<<TpcEntityOneOne>>>.

  Primary key (CQL partition and clustering key) is the same on all inherited entities. It must be declared in the root entity and it is
illegal to declare the primary key in inherited entities.



  Example code:

------------------------------------------------------------------------------------------------------------------------------
 @CEntity(partitionKey = "commonId")
 @CInheritance(type = InheritanceType.TABLE_PER_CLASS)
 public class TpcRootEntity {
   @CColumn private UUID commonId;
 }

 @CEntity
 public class TpcEntityOne extends TpcRootEntity {
   // some attributes
 }

 @CEntity
 public class TpcEntityTwo extends TpcRootEntity {
   // some attributes
 }

 @CEntity
 public class TpcEntityOneOne extends TpcEntityOne {
   // some attributes
 }
------------------------------------------------------------------------------------------------------------------------------
