  ------------------------------------------------------
  Persisting
  ------------------------------------------------------

  <<Important>> Keep in mind that Apache Cassandra does not <really> differ between an <<<INSERT>>> and an <<<UPDATE>>>.
It is completly valid to issue a CQL <<<INSERT>>> on a row whose primary key already exists - the row will be updated.
And it is valid to issue a CQL <<<UPDATE>>> on a row that does not exist - the row will be created (if the primary key is fully specified
in the <<<UPDATE>>> statement).

  Caffinitas Mapper allows you to issue DML operations using the following methods:

  CQL <<<INSERT>>> synchronous and asynchronous:

  {{{../../apidocs/org/caffinitas/mapper/core/PersistenceSession.html#insert-T-org.caffinitas.mapper.core.PersistOption...-}<<<insert(Object entityInstance, PersistOption... persistOption)>>>}}

  {{{../../apidocs/org/caffinitas/mapper/core/PersistenceSession.html#insertAsync-T-org.caffinitas.mapper.core.PersistOption...-}<<<insertAsync(Object entityInstance, PersistOption... persistOption)>>>}}

  CQL <<<UPDATE>>> synchronous and asynchronous - note that an UPDATE on an entity with only primary key columns will result in a CQL INSERT:

  {{{../../apidocs/org/caffinitas/mapper/core/PersistenceSession.html#update-T-org.caffinitas.mapper.core.PersistOption...-}<<<update(Object entityInstance, PersistOption... persistOption)>>>}}

  {{{../../apidocs/org/caffinitas/mapper/core/PersistenceSession.html#updateAsync-T-org.caffinitas.mapper.core.PersistOption...-}<<<updateAsync(Object entityInstance, PersistOption... persistOption)>>>}}

  CQL <<<DELETE>>> synchronous and asynchronous:

  {{{../../apidocs/org/caffinitas/mapper/core/PersistenceSession.html#delete-T-org.caffinitas.mapper.core.PersistOption...-}<<<delete(Object entityInstance, PersistOption... persistOption)>>>}}

  {{{../../apidocs/org/caffinitas/mapper/core/PersistenceSession.html#deleteAsync-T-org.caffinitas.mapper.core.PersistOption...-}<<<deleteAsync(Object entityInstance, PersistOption... persistOption)>>>}}
