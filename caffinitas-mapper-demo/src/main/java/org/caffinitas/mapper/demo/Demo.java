/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.demo;

import com.datastax.driver.core.Cluster;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.Uninterruptibles;
import org.caffinitas.mapper.core.CqlStatementList;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.ModifyFuture;
import org.caffinitas.mapper.core.PersistenceManager;
import org.caffinitas.mapper.core.PersistenceManagerBuilder;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.SchemaGenerator;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.demo.entity.AggregatorModuleEntity;
import org.caffinitas.mapper.demo.entity.CodeModuleEntity;
import org.caffinitas.mapper.demo.entity.ModuleEntity;
import org.caffinitas.mapper.demo.entity.ProjectEntity;
import org.caffinitas.mapper.demo.entity.UserEntity;
import org.caffinitas.mapper.demo.types.AddressKind;
import org.caffinitas.mapper.demo.types.AddressType;
import org.caffinitas.mapper.demo.types.Language;
import org.caffinitas.mapper.demo.types.License;

import java.net.URL;
import java.util.List;
import java.util.UUID;

public class Demo
{

    public static void main(String args[]) throws Exception
    {
        String contact = "127.0.0.1";
        String clusterName = "test";

        if (args.length > 0)
        {
            contact = args[0];
            if (args.length > 1)
            {
                clusterName = args[1];
            }
        }

        Cluster cluster = Cluster.builder().
            addContactPoint(contact).
            withClusterName(clusterName).
            build();
        try
        {
            new Demo().runDemo(cluster);
        }
        finally
        {
            cluster.close();
        }
    }

    public void runDemo(Cluster cluster) throws Exception
    {

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(Demo.class.getPackage().getName());
        DataModel model = modelScanner.scan();

        PersistenceManager persistenceManager = PersistenceManagerBuilder.newBuilder().
            withModel(model).
            withCluster(cluster).
            withDefaultKeyspace("default_ks").
            withMappedKeyspace("lookup_KS_NAME", "demo").
            build();
        try
        {

            SchemaGenerator schemaGenerator = persistenceManager.createSchemaGenerator();

            System.out.println("Executing CQL DDL statements...");
            System.out.println();
            for (String stmt : schemaGenerator.generateLiveAlterDDL().getStatements())
            {
                System.out.println(stmt);
            }

            CqlStatementList cqlDDL = schemaGenerator.generateLiveAlterDDL();
            cqlDDL.execute(persistenceManager.driverSession());

            System.out.println("Wait until all schema objects are available...");
            System.out.println();
            persistenceManager.refreshSchema(); // This is necessary until schema changes are propagated from the Java Driver to Caffinitas Mappe automatically!
            schemaGenerator.forSchemaObjectsAvailable().awaitUninterruptibly();

            PersistenceSession session = persistenceManager.createSession();
            try
            {

                // create a user object and persist it

                UserEntity snazy = new UserEntity();
                snazy.setUsername("snazy");
                AddressType mainAddress = new AddressType(null, null, "Koeln", "Germany");
                snazy.setMainAddress(mainAddress);
                snazy.getOtherAddresses().put(AddressKind.HOME, mainAddress);
                session.insert(snazy);

                // now load the user object

                UserEntity loaded = session.loadOne(UserEntity.class, "snazy");
                System.out.printf("got user record for %s %n", loaded.getUsername());

                //
                //
                //

                // Setup a project...

                ProjectEntity project = new ProjectEntity();
                project.setId(UUID.randomUUID());
                project.setName("caffinitas");
                project.setLicense(License.APACHE_V2);
                project.getAuthors().add("snazy");
                project.getContributors().add("snazy");
                project.setProjectURL(new URL("http://caffinitas.org/"));

                AggregatorModuleEntity parentModule = new AggregatorModuleEntity();
                parentModule.setProjectId(project.getId());
                parentModule.setName("caffinitas-parent");

                CodeModuleEntity moduleCore = new CodeModuleEntity();
                moduleCore.setProjectId(project.getId());
                moduleCore.setName("caffinitas-core");
                moduleCore.setLanguage(Language.JAVA);

                CodeModuleEntity moduleDemo = new CodeModuleEntity();
                moduleDemo.setProjectId(project.getId());
                moduleDemo.setName("caffinitas-demo");
                moduleDemo.setLanguage(Language.JAVA);

                CodeModuleEntity moduleTest = new CodeModuleEntity();
                moduleTest.setProjectId(project.getId());
                moduleTest.setName("caffinitas-test");
                moduleTest.setLanguage(Language.JAVA);

                parentModule.getModules().add("caffinitas-core");
                parentModule.getModules().add("caffinitas-demo");
                parentModule.getModules().add("caffinitas-test");

                ModifyFuture<ProjectEntity> projectPersistFuture = session.insertAsync(project);
                ModifyFuture<AggregatorModuleEntity> parentModulePersistFuture = session.insertAsync(parentModule);
                ModifyFuture<CodeModuleEntity> moduleCorePersistFuture = session.insertAsync(moduleCore);
                ModifyFuture<CodeModuleEntity> moduleDemoPersistFuture = session.insertAsync(moduleDemo);
                ModifyFuture<CodeModuleEntity> moduleTestPersistFuture = session.insertAsync(moduleTest);

                ListenableFuture<List<Object>> combinedFuture =
                    Futures.allAsList(projectPersistFuture, parentModulePersistFuture,
                        moduleCorePersistFuture, moduleDemoPersistFuture, moduleTestPersistFuture);
                Uninterruptibles.getUninterruptibly(combinedFuture);

                //

                project = session.loadOne(ProjectEntity.class, project.getId());

                // load all modules (since projectId is the partition key in ModuleEntity we can load it with one SELECT)
                List<ModuleEntity> modules = session.loadMultiple(ModuleEntity.class, project.getId());

                for (ModuleEntity module : modules)
                {
                    System.out.println("  " + module.getName());
                }
            }
            finally
            {
                session.close();
            }

        }
        finally
        {
            persistenceManager.close();
        }
    }

}
