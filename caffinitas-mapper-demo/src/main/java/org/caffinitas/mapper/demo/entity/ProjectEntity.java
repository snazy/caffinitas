/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.demo.entity;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.annotations.DataTypeName;
import org.caffinitas.mapper.core.api.converter.URLStringConverter;
import org.caffinitas.mapper.demo.types.License;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@CEntity(partitionKey = "id")
public class ProjectEntity {
  @CColumn private UUID id;
  @CColumn private License license;
  @CColumn private String name;
  @CColumn private Set<String> authors;
  @CColumn private Set<String> contributors;
  @CColumn(converter = URLStringConverter.class, type = DataTypeName.TEXT) private URL projectURL;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public License getLicense() {
    return license;
  }

  public void setLicense(License license) {
    this.license = license;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<String> getAuthors() {
    if (authors==null) authors = new HashSet<String>();
    return authors;
  }

  public void setAuthors(Set<String> authors) {
    this.authors = authors;
  }

  public Set<String> getContributors() {
    if (contributors==null) contributors = new HashSet<String>();
    return contributors;
  }

  public void setContributors(Set<String> contributors) {
    this.contributors = contributors;
  }

  public URL getProjectURL() {
    return projectURL;
  }

  public void setProjectURL(URL projectURL) {
    this.projectURL = projectURL;
  }
}
