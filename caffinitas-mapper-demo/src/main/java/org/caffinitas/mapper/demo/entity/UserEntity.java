/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.demo.entity;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.demo.types.AddressKind;
import org.caffinitas.mapper.demo.types.AddressType;

import java.util.EnumMap;
import java.util.Map;

@CEntity(partitionKey = "username")
public class UserEntity {
  @CColumn private String username;

  @CColumn private AddressType mainAddress;
  @CColumn private Map<AddressKind, AddressType> otherAddresses;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public AddressType getMainAddress() {
    return mainAddress;
  }

  public void setMainAddress(AddressType mainAddress) {
    this.mainAddress = mainAddress;
  }

  public Map<AddressKind, AddressType> getOtherAddresses() {
    if (otherAddresses == null) { otherAddresses = new EnumMap<AddressKind, AddressType>(AddressKind.class); }
    return otherAddresses;
  }

  public void setOtherAddresses(Map<AddressKind, AddressType> otherAddresses) {
    this.otherAddresses = otherAddresses;
  }
}
