Caffinitas Mapper UUID Generator
================================

High throughput UUID generator with high probability to not ever generate duplicate UUIDs.

Background: Version 1 UUIDs (time based UUIDs) contain the node address of the node that genererates the UUID.
Additionally such UUIDs contain the time when the UUID was generated (with 100 ns precisition) and a clock value.

A good UUID generator (as any ID generator) should ensure that no UUID is ever generated twice. The node address
is usually the host's MAC address (Ethernet address) of either network adapter. This is a good choice - but if
the clock moves backwards (administrator intervention, clock skew etc), the probability for duplicate IDs is high.
The workaround is to choose a random clock value.

Caffinitas Mapper UUID Generator uses a simple Apache Cassandra table to store used *node address* plus *clock value*
combinations and holds them for 5 days (TTL for the row). With ``INSERT INTO ... IF NOT EXISTS`` CQL, Apache Cassandra
ensures that no key combination is used again (within the TTL of 5 days).

Additionally the UUID generator uses *slicing* to allow higher UUID generation rate than possible by definition -
the spec allows 10000 UUIDs per millisecond. The other benefit of using such slicing is a more constant thoughput
rate and elimination of locking regarding UUID generation.

Overhead occurs when a UUID generator slice needs to re-setup itself if either system clock went backwards or
the livetime of a *node address* plus *clock value* combination has expired.
