/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.uuidgen.test;

import org.caffinitas.mapper.uuidgen.SimpleUUIDGeneratorConfigurer;
import org.caffinitas.mapper.uuidgen.UUIDGenerator;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class UUIDTest {
  static UUIDGenerator uuidGenerator = new UUIDGenerator(4, SimpleUUIDGeneratorConfigurer.INSTANCE);

  static List<Set<UUID>> uniqueness = new ArrayList<Set<UUID>>();

  /**
   * Add 2 million generated UUIDs to a set - check for uniqueness in {@link #uniquenessCheck()}
   */
  @Test(threadPoolSize = 16, invocationCount = 16)
  public void uniqueness() {
    System.out.println("starting concurrent UUID generator uniqueness test thread ... takes time and consumes CPU");
    Set<UUID> set = new HashSet<UUID>(524288);
    for (int i = 0; i < 50000; i++) { set.add(uuidGenerator.generate()); }
    synchronized (uniqueness) {
      uniqueness.add(set);
    }
  }

  /**
   * Check that UUIDs generated using concurrent {@link #uniqueness()} are unique.
   */
  @Test(dependsOnMethods = "uniqueness")
  public void uniquenessCheck() {
    System.out.println("checking UUID generator uniqueness ... takes time ...");
    Set<UUID> chk = new HashSet<UUID>(4194304);
    for (Iterator<Set<UUID>> iter = uniqueness.iterator(); iter.hasNext(); ) {
      Set<UUID> uniquenes = iter.next();
      iter.remove();
      for (UUID uuid : uniquenes) {
        Assert.assertTrue(chk.add(uuid), "UUIDs not unique - duplicate: " + uuid);
      }
    }
  }

  @Test(threadPoolSize = 8, invocationCount = 8)
  public void timeUUID() {
    long t0 = System.currentTimeMillis();
    for (int i = 0; i < 200000; i++) { uuidGenerator.generate(); }
    long t = System.currentTimeMillis() - t0;
    System.out.println("t for 2.000.000 UUIDs = " + t + "ms");
    System.out.println("spins: " + uuidGenerator.get1msSpins());
    System.out.println(uuidGenerator.generate());
  }
}
