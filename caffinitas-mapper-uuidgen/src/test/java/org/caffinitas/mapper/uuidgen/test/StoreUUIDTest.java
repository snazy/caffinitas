/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.uuidgen.test;

import com.datastax.driver.core.VersionNumber;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.caffinitas.mapper.uuidgen.StoreUUIDGeneratorConfigurer;
import org.caffinitas.mapper.uuidgen.SystemMacAddresses;
import org.caffinitas.mapper.uuidgen.UUIDGenerator;
import org.caffinitas.mapper.uuidgen.UUIDUniqueness;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;

public class StoreUUIDTest extends CassandraTestBase {
  private static DataModel model;
  private UUIDGenerator generator;

  @BeforeClass
  public static void setup() {
    recreateKeyspace("junit");

    DataModelScanner modelScanner = new DataModelScanner();
    modelScanner.withCaffinitasPersistenceXml();
    modelScanner.getFilterBuilder().includePackage(UUIDUniqueness.class.getPackage().getName());
    model = modelScanner.scan();
  }

  @BeforeClass
  public static void setupPersistenceManager() {
    if (VersionNumber.parse("2.0.0").compareTo(VersionNumber.parse(configuredCassandraVersion)) >= 0) {
      throw new SkipException("user type test not applicable for cassandra version " + configuredCassandraVersion);
    }
    if (Integer.parseInt(configuredNodeCount) < 2) {
      throw new SkipException("options test not applicable for node count " + configuredNodeCount);
    }

    Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
    persistenceManager = withPersistenceManager(model, "caffinitas-uuidgen", "junit");

      createSchemaDo(Collections.<Class<?>>singletonList(UUIDUniqueness.class));
  }

  @AfterClass
  public static void closePersistenceManager() {
    persistenceManager.close();
  }

  @Test
  public void timeUUIDPrepare() throws IOException {
    PersistenceSession persistenceSession = persistenceManager.createSession();
    Set<Long> adrs = SystemMacAddresses.inquireMacAddresses();
    StoreUUIDGeneratorConfigurer config = new StoreUUIDGeneratorConfigurer(adrs, persistenceSession);
    generator = new UUIDGenerator(4, config);
  }

  @Test(threadPoolSize = 16, invocationCount = 16, dependsOnMethods = "timeUUIDPrepare")
  public void timeUUID() throws IOException {
    long t0 = System.currentTimeMillis();
    for (int i = 0; i < 2000000; i++) { generator.generate(); }
    long t = System.currentTimeMillis() - t0;
    System.out.println("t for 2.000.000 UUIDs = " + t + "ms");
    System.out.println(generator.generate());
    System.out.println("# of 1ms spins = " + generator.get1msSpins());
    System.out.println("generator stripe setup count = " + generator.getSetupCount());
    System.out.println("generator stripe total setup time = " + generator.getSetupTimeMillis() + "ms");
    System.out.println("failed immediate generator stripe acquires = " + generator.getStripeAcquireFailed());
  }
}
