/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.uuidgen;

import org.caffinitas.mapper.core.CasNotAppliedException;
import org.caffinitas.mapper.core.PersistOption;
import org.caffinitas.mapper.core.PersistenceSession;

import java.util.Set;

/**
 * UUID generator configurer that uses Apache Cassandra to ensure <i>node address</i> plus <i>clock value</i> uniqueness.
 */
public class StoreUUIDGeneratorConfigurer implements UUIDGeneratorConfigurer {

  private final long[] macs;
  private final PersistenceSession persistenceSession;

  public StoreUUIDGeneratorConfigurer(Set<Long> uuidGeneratorMacs, PersistenceSession persistenceSession) {
    if (uuidGeneratorMacs == null || uuidGeneratorMacs.isEmpty()) { throw new IllegalArgumentException("invalid uuidGeneratorMacs parameter"); }
    int i = 0;
    this.macs = new long[uuidGeneratorMacs.size()];
    for (Long mac : uuidGeneratorMacs) { macs[i++] = mac; }
    this.persistenceSession = persistenceSession;
  }

  @Override public int clockSequenceForMac(long macAddress, long validUntil) {
    UUIDUniqueness uni = new UUIDUniqueness(macAddress, randomClock(), validUntil);
    while (true) {
      try {
        persistenceSession.insert(uni, PersistOption.ifNotExists());
        return uni.getClockSequence();
      } catch (CasNotAppliedException ignore) {
        uni.setClockSequence(randomClock());
      }
    }
  }

  private int randomClock() {
      return Rand.random() & CLOCK_MASK;
  }

  @Override public long macAddress() {
    return macs[Rand.random(macs.length)];
  }
}
