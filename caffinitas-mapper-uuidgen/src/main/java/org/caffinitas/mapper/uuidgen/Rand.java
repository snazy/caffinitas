/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.uuidgen;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;

final class Rand {

    // This class should be refactored to use Java 7 ThreadLocalRandom when Java 7 is a minimum requirement.

    @SuppressWarnings("unchecked") static final AtomicReference<Random>[] RANDOMS = new AtomicReference[5];
    static {
        for (int i=0;i<RANDOMS.length;i++) {
            RANDOMS[i] = new AtomicReference<Random>(new SecureRandom());
        }
    }

    static int random(int bound) {
        while (true) {
            for (AtomicReference<Random> random : RANDOMS) {
                Random r = random.getAndSet(null);
                if (r!=null) {
                    try {
                        return r.nextInt(bound);
                    } finally {
                        random.set(r);
                    }
                }
            }
        }

        // Java 7 variant:
        //return ThreadLocalRandom.current().nextInt(bound);
    }

    static int random() {
        while (true) {
            for (AtomicReference<Random> random : RANDOMS) {
                Random r = random.getAndSet(null);
                if (r!=null) {
                    try {
                        return r.nextInt();
                    } finally {
                        random.set(r);
                    }
                }
            }
        }

        // Java 7 variant:
        //return ThreadLocalRandom.current().nextInt();
    }
}
