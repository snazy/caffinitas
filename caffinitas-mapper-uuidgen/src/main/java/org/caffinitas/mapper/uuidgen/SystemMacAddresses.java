/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.uuidgen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

/**
 * Helper class to retrieve all hardware addresses of a node.
 */
public final class SystemMacAddresses {
  private static final Logger LOGGER = LoggerFactory.getLogger(SystemMacAddresses.class);

  private SystemMacAddresses() {}

  public static Set<Long> inquireMacAddresses() throws SocketException {
    Set<Long> r = new HashSet<Long>();
    if (LOGGER.isDebugEnabled()) { LOGGER.debug("getting all hardware addresses of all network interfaces"); }
    collect(NetworkInterface.getNetworkInterfaces(), r);
    return r;
  }

  private static void collect(Enumeration<NetworkInterface> niEnum, Set<Long> r) throws SocketException {
    if (niEnum != null) {
      while (niEnum.hasMoreElements()) {
        NetworkInterface ni = niEnum.nextElement();
        if (ni.isLoopback() || ni.isVirtual()) { continue; }
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("getting hardware addresses of network interface {} \"{}\" with ifadrs {}",
              ni.getName(), ni.getDisplayName(),
              ni.getInterfaceAddresses());
        }
        byte[] hwadr = ni.getHardwareAddress();
        addHwAdr(hwadr, r);

        collect(ni.getSubInterfaces(), r);
      }
    }
  }

  private static void addHwAdr(byte[] hwadr, Set<Long> r) {
    if (hwadr == null) { return; }
    long adr = 0L;
    for (byte b : hwadr) {
      adr <<= 8;
      adr |= b & 0xff;
    }
    r.add(adr);
  }
}
