/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * High throughput UUID generator with high probability to not ever generate duplicate UUIDs.
 *
 * Usage:
 * Map the keyspace replacer {@code caffinitas-uuidgen} to the keyspace you want to use for UUID generation.
 * For example:
 * <code><br>
 * &nbsp;&nbsp;&nbsp;&nbsp;PersistenceManagerBuilder builder = PersistenceManagerBuilder.newBuilder().<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;withModel(model).<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;withCluster(cluster).<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;withMappedKeyspace("caffinitas-uuidgen", "replace-with-keyspace-for-uuid-generation");}<br>
 * </code>
 * Then use a <i>singleton</i> {@link org.caffinitas.mapper.uuidgen.UUIDGenerator}:
 * <code><br>
 * &nbsp;&nbsp;&nbsp;&nbsp;PersistenceSession persistenceSession = persistenceManager.createSession();<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;Set&lt;Long&gt; adrs = SystemMacAddresses.inquireMacAddresses();<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;StoreUUIDGeneratorConfigurer config = new StoreUUIDGeneratorConfigurer(adrs, persistenceSession);<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;UUIDGenerator generator = new UUIDGenerator(4, config);<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;// store generator where it is globally accessible...<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;// do not close the persistenceSession or persistenceManager while the UUID generator is in use<br>
 * </code>
 * <p></p>
 * Background: Version 1 UUIDs (time based UUIDs) contain the node address of the node that genererates the UUID.
 * Additionally such UUIDs contain the time when the UUID was generated (with 100 ns precisition) and a clock value.
 * <p></p>
 * A good UUID generator (as any ID generator) should ensure that no UUID is ever generated twice. The node address
 * is usually the host's MAC address (Ethernet address) of either network adapter. This is a good choice - but if
 * the clock moves backwards (administrator intervention, clock skew etc), the probability for duplicate IDs is high.
 * The workaround is to choose a random clock value.
 * <p></p>
 * Caffinitas Mapper UUID Generator uses a simple Apache Cassandra table to store used <i>node address</i> plus <i>clock value</i>
 * combinations and holds them for 5 days (TTL for the row). With {@code INSERT INTO ... IF NOT EXISTS} CQL, Apache Cassandra
 * ensures that no key combination is used again (within the TTL of 5 days).
 * <p></p>
 * Additionally the UUID generator uses <i>slicing</i> to allow higher UUID generation rate than possible by definition -
 * the spec allows 10000 UUIDs per millisecond. The other benefit of using such slicing is a more constant thoughput
 * rate and elimination of locking regarding UUID generation.
 * <p></p>
 * Overhead occurs when a UUID generator slice needs to re-setup itself if either system clock went backwards or
 * the livetime of a <i>node address</i> plus <i>clock value</i> combination has expired.
 */
@CKeyspace(keyspace = "@caffinitas-uuidgen") package org.caffinitas.mapper.uuidgen;

import org.caffinitas.mapper.annotations.CKeyspace;

