/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.uuidgen;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CEntity;

@CEntity(partitionKey = {"macAddress", "clockSequence"}, table = "uuiduni", defaultTTL = 432000)
public class UUIDUniqueness {
  @CColumn(columnName = "mac")
  private long macAddress;
  @CColumn(columnName = "clock")
  private int clockSequence;
  @CColumn(columnName = "expire")
  private long validUntil;

  public UUIDUniqueness() {
  }

  public UUIDUniqueness(long macAddress, int clockSequence, long validUntil) {
    this.macAddress = macAddress;
    this.clockSequence = clockSequence;
    this.validUntil = validUntil;
  }

  public long getMacAddress() {
    return macAddress;
  }

  public void setMacAddress(long macAddress) {
    this.macAddress = macAddress;
  }

  public int getClockSequence() {
    return clockSequence;
  }

  public void setClockSequence(int clockSequence) {
    this.clockSequence = clockSequence;
  }

  public long getValidUntil() {
    return validUntil;
  }

  public void setValidUntil(long validUntil) {
    this.validUntil = validUntil;
  }
}
