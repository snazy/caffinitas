/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.uuidgen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * UUID generator using multiple stripes to reduce contention and higher throughput.
 * Each stripe has its own <i>node address</i> plus <i>clock value</i> combination.
 * The implementation is non-syncronized / lock-free ; minor (rare) busy waits may occur.
 */
public final class UUIDGenerator {
  private static final Logger LOGGER = LoggerFactory.getLogger(UUIDGenerator.class);

  private final UUIDGeneratorConfigurer configurer;

  private final AtomicReference<Stripe>[] generators;
  private final AtomicInteger nextIdx = new AtomicInteger();

  private final AtomicLong spins = new AtomicLong();
  private final AtomicLong setupTime = new AtomicLong();
  private final AtomicLong setupCount = new AtomicLong();
  private final AtomicLong stripeAcquireFailed = new AtomicLong();

  /**
   * Create a new UUID generator instance.
   *
   * @param stripes    number of stripes - 2 to 4 stripes is usually sufficient
   * @param configurer generator configurer used to retrieve <i>node address</i> plus <i>clock value</i> values
   */
  @SuppressWarnings("unchecked") public UUIDGenerator(int stripes, UUIDGeneratorConfigurer configurer) {
    if (stripes <= 0) { throw new IllegalArgumentException(); }
    if (configurer == null) { throw new NullPointerException(); }
    this.configurer = configurer;
    generators = new AtomicReference[stripes];
    for (int i = 0; i < stripes; i++) { generators[i] = new AtomicReference<Stripe>(new Stripe()); }
  }

  public static long makeLSB(long mac, int clk) {
    long lsb = clk & 0x3fff;
    lsb <<= 48;
    lsb |= mac & 0x0000ffffffffffffL;
    lsb |= 0x8000000000000000L;
    return lsb;
  }

  public static long makeMSB(long time) {
    time &= 0x0fffffffffffffffL;
    // add version
    time |= 0x1000000000000000L;

    return (time << 32) |
        (time >> 16 & 0xffff0000L) |
        (time >> 48);
  }

  /**
   * Offset in 100ns intervals between "start of time" in v1 UUIDs and "start of time" of Java/UNIX timestamp.
   */
  public static final long UUID_UNIX_TIME_OFFSET = 0x01B21DD213814000L;

  static long javaTimeToUUIDTime(long javaTime, long cnt) {
    javaTime *= 10000L;
    javaTime += cnt;
    return UUID_UNIX_TIME_OFFSET + javaTime;
  }

  /**
   * Get Java timestamp from version 1 UUID.
   */
  public static long timeFromTimeUUID(UUID uuid) {
    long t = uuid.timestamp();
    long hundredNanos = t - UUID_UNIX_TIME_OFFSET;
    return hundredNanos / 10000;
  }

  /**
   * Performance indicator: number of 1 millisecond spins.
   * A "1ms spin" occurs if a stripe produced 10000 UUIDs in one milliseconds and needs to slow down.
   */
  public long get1msSpins() {
    return spins.get();
  }

  /**
   * Performance indicator: number of UUID generator stripe setups.
   */
  public long getSetupCount() {
    return setupCount.get();
  }

  /**
   * Performance indicator: number of milliseconds spent in UUID generator stripe setup.
   */
  public long getSetupTimeMillis() {
    return setupTime.get();
  }

  /**
   * Performance indicator: number of failed stripe acquires that resulted in a <i>busy wait</i>.
   */
  public long getStripeAcquireFailed() {
    return stripeAcquireFailed.get();
  }

  /**
   * Uses a generator stripe to generate a UUID. Uses a lock-free implementation to acquire and release a generator stripe.
   * If a generator could not be acquired immediately, the implementation sleeps for one millisecond - this is less expensive
   * than using a synchronied/lock-based implementation.
   */
  @SuppressWarnings({"ForLoopReplaceableByForEach", "BusyWait"}) public UUID generate() {
    AtomicReference<Stripe>[] gens = generators;
    int gl = gens.length;
    AtomicInteger ni = nextIdx;
    while (true) {
      for (int i = 0; i < gl; i++) {
        AtomicReference<Stripe> gen = gens[ni.getAndIncrement() % gl];
        Stripe g = gen.get();
        if (g != null) {
          if (gen.compareAndSet(g, null)) {
            try {
              return g.generate();
            } finally {
              gen.set(g);
            }
          }
        }
      }

      // this busy loop is less expensive than using synchronized-wait-notify or lock-condition-await-signal
      try {
        stripeAcquireFailed.incrementAndGet();
        Thread.sleep(1L);
      } catch (InterruptedException e) {
        Thread.interrupted();
      }
    }
  }

  private class Stripe {
    private static final long ONE_DAYS = 24L * 60L * 60L * 1000L;
    private volatile long lsb;

    private volatile long tAlloc;
    private volatile long fill;
    private volatile long validUntil;

    private Stripe() {
      if (LOGGER.isDebugEnabled()) { LOGGER.debug("generator stripe initializing - acquiring clock value"); }
      setup();
    }

    private void setup() {
      setupCount.incrementAndGet();
      long t0 = System.currentTimeMillis();
      long mac = configurer.macAddress();
      validUntil = System.currentTimeMillis() + ONE_DAYS;
      while (true) {
        int clk = configurer.clockSequenceForMac(mac, validUntil);
        if (clk != -1) {
          this.lsb = makeLSB(mac, clk);
          setupTime.addAndGet(System.currentTimeMillis() - t0);
          return;
        }
      }
    }

    @SuppressWarnings("BusyWait") UUID generate() {
      long t;
      long ta = tAlloc;
      long c = fill;
      while (true) {
        t = System.currentTimeMillis();

        if (t >= validUntil) {
          if (LOGGER.isDebugEnabled()) { LOGGER.debug("generator clock lease expired - acquiring new clock value"); }
          setup();
          t = System.currentTimeMillis();
        }

        long d = ta - t;
        if (d > 0) {
          if (d > 500) {
            if (LOGGER.isDebugEnabled()) { LOGGER.debug("system clock went back - acquiring new clock value"); }
            // high system time diff - perform setup() again
            setup();
          }
          else {
            try {
              Thread.sleep(ta - t);
            } catch (InterruptedException e) {
              Thread.currentThread().interrupt();
            }
          }
          continue;
        }
        else if (d < 0) {
          tAlloc = t;
          c = fill = 0;
        }
        else {
          if (c == 10000) {
            try {
              spins.incrementAndGet();
              Thread.sleep(1L);
            } catch (InterruptedException e) {
              Thread.currentThread().interrupt();
            }
            continue;
          }
        }
        break;
      }

      fill = c + 1;
      long tUuid = javaTimeToUUIDTime(t, c);
      long msb = makeMSB(tUuid);
      return new UUID(msb, lsb);
    }
  }
}
