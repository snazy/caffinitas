/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.uuidgen;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Very simple UUID generator configurer without any backend and just using local heap.
 */
public class SimpleUUIDGeneratorConfigurer implements UUIDGeneratorConfigurer {

  public static final SimpleUUIDGeneratorConfigurer INSTANCE = new SimpleUUIDGeneratorConfigurer();

  private final ConcurrentMap<GenCfg, GenCfg> existing = new ConcurrentHashMap<GenCfg, GenCfg>();

  private SimpleUUIDGeneratorConfigurer() {}

  @Override public long macAddress() {
    return 0L;
  }

  @Override public int clockSequenceForMac(long macAddress, long validUntil) {
    for (int i = 0; i < MAX_CLK_PER_MAC_CHECK; i++) {
      int clk = Rand.random() & CLOCK_MASK;
      GenCfg cfg = new GenCfg(macAddress, clk);
      GenCfg prev = existing.putIfAbsent(cfg, cfg);
      if (prev == null) { return clk; }
    }
    return -1;
  }

  private class GenCfg {
    private final long mac;
    private final int clk;

    private GenCfg(long mac, int clk) {
      this.mac = mac;
      this.clk = clk;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) { return true; }
      if (o == null || getClass() != o.getClass()) { return false; }

      GenCfg genCfg = (GenCfg) o;

      return clk == genCfg.clk && mac == genCfg.mac;
    }

    @Override
    public int hashCode() {
      int result = (int) (mac ^ (mac >>> 32));
      result = 31 * result + clk;
      return result;
    }
  }
}
