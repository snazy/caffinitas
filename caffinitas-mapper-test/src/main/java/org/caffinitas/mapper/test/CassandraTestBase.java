/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.VersionNumber;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import org.caffinitas.mapper.core.CqlStatementList;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceManager;
import org.caffinitas.mapper.core.PersistenceManagerBuilder;
import org.caffinitas.mapper.core.PersistenceSession;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class CassandraTestBase {
    public static final String CASSANDRA_VERSION = "2.1.0-rc4";
    public static final String NODE_COUNT = "1";
    public static final String CLUSTER_NAME = "test";
    public static final String SEED_HOSTS = "127.0.0.1";

    protected static PersistenceManager persistenceManager;

    public static Cluster cluster;

    public static final String configuredNodeCount = System.getProperty("nodeCount", NODE_COUNT);
    public static final String configuredCassandraVersion = System.getProperty("cassandraVersion", CASSANDRA_VERSION);

    @SuppressWarnings("BusyWait") static int exec(String... args) {
        try {
            System.out.println("Executing " + Arrays.toString(args));

            Process proc = Runtime.getRuntime().exec(args);
            InputStream in = proc.getInputStream();
            try  {
                InputStream err = proc.getErrorStream();
                try {
                    byte[] arr = new byte[256];
                    while (true) {
                        boolean any = false;

                        int av = in.available();
                        if (av > 0) {
                            int rd = in.read(arr, 0, Math.min(av, 256));
                            System.out.write(arr, 0, rd);
                            any = true;
                        }

                        av = err.available();
                        if (av > 0) {
                            int rd = err.read(arr, 0, Math.min(av, 256));
                            System.err.write(arr, 0, rd);
                            any = true;
                        }

                        try {
                            int ec = proc.exitValue();
                            if (!any) {
                                System.out.println("... returned with exit code " + ec);
                                return ec;
                            }
                        } catch (IllegalThreadStateException ignore) {
                        }
                        if (!any) {
                            Thread.sleep(200);
                        }
                    }
                } finally { err.close(); }
            } finally { in.close(); }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static VersionNumber clusterVersion(Cluster c) {
        VersionNumber minVersion = null;
        Set<Host> hosts = c.getMetadata().getAllHosts();
        for (Host host : hosts) {
            VersionNumber ver = host.getCassandraVersion();
            if (minVersion == null || minVersion.compareTo(ver) > 0) {
                minVersion = ver;
            }
        }
        return minVersion;
    }

    @SuppressWarnings({"BusyWait", "ClassExplicitlyExtendsThread"}) @BeforeClass
    public static void createCluster() {
        if (cluster != null)
            return;

        Cluster.Builder builder = Cluster.builder().
            addContactPoint(SEED_HOSTS).
            withClusterName(CLUSTER_NAME);

        VersionNumber expect = VersionNumber.parse(configuredCassandraVersion);
        boolean remove = true;
        boolean create = false;
        Cluster c = builder.build();
        try {
            VersionNumber ver = clusterVersion(c);
            if (ver.getMajor() == expect.getMajor() && ver.getMinor() == expect.getMinor()
                && c.getMetadata().getAllHosts().size() == Integer.parseInt(configuredNodeCount)) {
                System.out.println("Reusing previous Apache Cassandra cluster ...");
                remove = false;
            } else {
                create = true;
            }
        } catch (Throwable ignore) {
            create = true;
        } finally { c.close(); }

        if (remove) {
            System.out.println("Removing previous Apache Cassandra cluster test...");
            exec("ccm", "remove", "test");
        }

        if (create) {
            System.out.println("Creating Apache Cassandra cluster test...");
            if (exec("ccm", "create", "-v", configuredCassandraVersion, "-n", configuredNodeCount, "-s", "--vnodes", CLUSTER_NAME) != 0) {
                Assert.fail("Apache Cassandra cluster creation failed");
            }
            if (!System.getProperties().containsKey("keepCluster")) {
                Runtime.getRuntime().addShutdownHook(new Thread()
                {
                    @Override public void run()
                    {
                        try {
                            cluster.close();
                        } catch (Throwable ignore) {}
                        System.out.println("Removing previous Apache Cassandra cluster test (if exists)...");
                        exec("ccm", "remove", "test");
                    }
                });
            }
        }

        try {
            for (int i = 0; ; i++) {
                System.out.println("Building Cluster instance...");
                cluster = builder.build();

                try {
                    Session ignored = cluster.connect();
                    ignored.close();
                    break;
                } catch (Exception e) {
                    try {
                        cluster.close();
                    } catch (Throwable ignore) {
                    }
                    if (i == 5) {
                        throw new RuntimeException(e);
                    }
                    Thread.sleep(250L);
                }
            }
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    protected static PersistenceManagerBuilder withPersistenceManagerBuilder(DataModel model, String... ksMap) {
        PersistenceManagerBuilder builder = PersistenceManagerBuilder.newBuilder().
            withModel(model).
            withCluster(cluster).
            withDefaultKeyspace("default_ks").
            withMappedKeyspace("lookup", "lookup_ks");
        for (int i = 0; i < ksMap.length; i += 2) {
            builder.withMappedKeyspace(ksMap[i], ksMap[i + 1]);
        }
        return builder;
    }

    protected static PersistenceManager withPersistenceManager(DataModel model, String... ksMap) {
        PersistenceManagerBuilder builder = withPersistenceManagerBuilder(model, ksMap);
        return builder.build();
    }

    static boolean keyspaceExists(Session session, String keyspaceName) {
        for (Row row : session.execute("SELECT keyspace_name FROM system.schema_keyspaces;").all()) {
            if (keyspaceName.equals(row.getString(0))) {
                return true;
            }
        }
        return false;
    }

    @SuppressWarnings({"BusyWait", "CallToPrintStackTrace"}) protected static void recreateKeyspace(String keyspaceName) {
        System.out.println("Creating keyspace " + keyspaceName + " ...");
        for (int i = 0; ; i++) {
            Session session = null;
            try {
                session = cluster.connect();

                if (keyspaceExists(session, keyspaceName)) {
                    System.out.println("Dropping keyspace " + keyspaceName);
                    session.execute("DROP KEYSPACE " + keyspaceName).one();
                    for (int j = 0; j < 100; j++) {
                        if (!keyspaceExists(session, keyspaceName)) {
                            break;
                        }
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (keyspaceExists(session, keyspaceName)) {
                        Assert.fail("could not drop keyspace " + keyspaceName);
                    }
                }

                System.out.println("Creating keyspace " + keyspaceName);
                session
                    .execute("CREATE KEYSPACE " + keyspaceName + " WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 2 }")
                    .one();
                for (int j = 0; j < 100; j++) {
                    if (keyspaceExists(session, keyspaceName)) {
                        break;
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (!keyspaceExists(session, keyspaceName)) {
                    Assert.fail("could not drop keyspace " + keyspaceName);
                }
                return;
            } catch (NoHostAvailableException noHost) {
                try {
                    Thread.sleep(250L);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                if (i == 5) {
                    throw noHost;
                }
            } finally { if (session!=null) session.close(); }
        }
    }

    protected final void waitEntityAvailable(MappedSchemaObject entity) throws InterruptedException {
        persistenceManager.refreshSchema(); // This is necessary until schema changes are propagated from the Java Driver to Caffinitas Mappe automatically!
        if (!entity.forSchemaObjectExistsAndValid().await(2, TimeUnit.SECONDS)) {
            Assert.fail("entity for " + entity.getType() + " is not available: exists:" + entity.getCqlTable().isExists() + " valid:" +
                entity.getCqlTable().isValid());
        }
    }

    protected final void createIfNecessary(MappedSchemaObject... schemaObjects) {
        for (MappedSchemaObject schemaObject : schemaObjects) {
            CqlStatementList ddl = schemaObject.getAlterDDL(cluster);
            System.out.println(ddl);
            ddl.execute(persistenceManager.driverSession());
        }
        persistenceManager.refreshSchema(); // This is necessary until schema changes are propagated from the Java Driver to Caffinitas Mappe automatically!
        long nanos = 2L * schemaObjects.length * 1000000000L;
        for (MappedSchemaObject entity : schemaObjects) {
            try {
                nanos = entity.forSchemaObjectExistsAndValid().awaitNanos(nanos);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            if (nanos <= 0) {
                StringBuilder sb = new StringBuilder("Timeout waiting for entities to be available\n");
                for (MappedSchemaObject mappedClassObject : schemaObjects) {
                    sb.append("  ").append(mappedClassObject.getClass().getSimpleName()).append(" for ").append(mappedClassObject.getType()).
                        append(" exists:").append(mappedClassObject.getCqlTable().isExists()).
                        append(" valid:").append(mappedClassObject.getCqlTable().isValid()).append('\n');
                }
                Assert.fail(sb.toString());
            }
        }
    }

    protected static void createSchemaDo(List<Class<?>> entityClasses) {
        List<MappedSchemaObject> mappedClassObjects = new ArrayList<MappedSchemaObject>();
        for (Class<?> cls : entityClasses) {
            MappedSchemaObject e = persistenceManager.getEntity(cls);
            if (e != null) {
                mappedClassObjects.add(e);
            }
            MappedSchemaObject c = persistenceManager.getComposite(cls);
            if (c != null) {
                mappedClassObjects.add(c);
            }
        }
        for (MappedSchemaObject entity : mappedClassObjects) {
            CqlStatementList ddl = entity.getAlterDDL(persistenceManager.driverSession().getCluster());
            Assert.assertFalse(ddl.hasErrors(), ddl.toString());
            System.out.println(ddl);
            ddl.execute(persistenceManager.driverSession());
        }
        persistenceManager.refreshSchema(); // This is necessary until schema changes are propagated from the Java Driver to Caffinitas Mappe automatically!
        long nanos = 2L * mappedClassObjects.size() * 1000000000L;
        for (MappedSchemaObject entity : mappedClassObjects) {
            try {
                nanos = entity.forSchemaObjectExistsAndValid().awaitNanos(nanos);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            if (nanos <= 0) {
                StringBuilder sb = new StringBuilder("Timeout waiting for entities to be available\n");
                for (MappedSchemaObject mappedClassObject : mappedClassObjects) {
                    sb.append("  ").append(mappedClassObject.getClass().getSimpleName()).append(" for ").append(mappedClassObject.getType()).
                        append(" exists:").append(mappedClassObject.getCqlTable().isExists()).
                        append(" valid:").append(mappedClassObject.getCqlTable().isValid()).append('\n');
                }
                Assert.fail(sb.toString());
            }
        }
    }

    @AfterMethod
    public final void afterTest() throws InterruptedException {
        System.out.flush();
        System.err.flush();
        Thread.sleep(50);
    }

    @SuppressWarnings({"unchecked", "BusyWait"}) protected <T> void insertUpdateDelete(PersistenceSession persistenceSession, Class<T> type, T inst,
                                                                                       UpdateCallback<T> updater, Object... pk) throws Exception {

        T loaded = persistenceSession.loadOne(type, pk);
        Assert.assertNotNull(loaded);
        Assert.assertSame(loaded.getClass(), inst.getClass());

        updater.modify(loaded);

        persistenceSession.update(loaded);

        loaded = persistenceSession.loadOne(type, pk);
        Assert.assertNotNull(loaded);
        Assert.assertSame(loaded.getClass(), inst.getClass());

        updater.check(loaded);

        persistenceSession.delete(loaded);

        loaded = persistenceSession.loadOne(type, pk);
        Assert.assertNull(loaded);
    }
}
