/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.model;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.annotations.CMap;

import java.net.InetAddress;
import java.util.Map;

@CEntity(partitionKey = "id")
public class MapEntity extends Callbacks {
    @CColumn
    private int id;

    @CMap
    private Map<String, InetAddress> stringInetMap;

    @CMap
    private Map<Integer, SomeEnum> intEnumMap;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Map<String, InetAddress> getStringInetMap() {
        return stringInetMap;
    }

    public void setStringInetMap(Map<String, InetAddress> stringInetMap) {
        this.stringInetMap = stringInetMap;
    }

    public Map<Integer, SomeEnum> getIntEnumMap() {
        return intEnumMap;
    }

    public void setIntEnumMap(Map<Integer, SomeEnum> intEnumMap) {
        this.intEnumMap = intEnumMap;
    }
}
