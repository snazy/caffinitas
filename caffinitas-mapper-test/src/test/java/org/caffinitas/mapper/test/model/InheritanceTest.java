/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.model;

import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.caffinitas.mapper.test.UpdateCallback;
import org.caffinitas.mapper.test.model.inherit.st.StBaseEntity;
import org.caffinitas.mapper.test.model.inherit.st.StInheritA;
import org.caffinitas.mapper.test.model.inherit.st.StInheritB;
import org.caffinitas.mapper.test.model.inherit.st.StInheritB2;
import org.caffinitas.mapper.test.model.inherit.st.StInheritC;
import org.caffinitas.mapper.test.model.inherit.tpc.TpcBaseEntity;
import org.caffinitas.mapper.test.model.inherit.tpc.TpcInheritA;
import org.caffinitas.mapper.test.model.inherit.tpc.TpcInheritB;
import org.caffinitas.mapper.test.model.inherit.tpc.TpcInheritB2;
import org.caffinitas.mapper.test.model.inherit.tpc.TpcInheritC;
import org.caffinitas.mapper.test.model.inherit.tpc2.Tpc2BaseEntity;
import org.caffinitas.mapper.test.model.inherit.tpc2.Tpc2InheritA;
import org.caffinitas.mapper.test.model.inherit.tpc2.Tpc2InheritB;
import org.caffinitas.mapper.test.model.inherit.tpc2.Tpc2InheritB2;
import org.caffinitas.mapper.test.model.inherit.tpc2.Tpc2InheritB3;
import org.caffinitas.mapper.test.model.inherit.tpc2.Tpc2InheritC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("ConstantConditions") public class InheritanceTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("junit");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(InheritanceTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() {
        persistenceManager.close();
    }

    @Test
    public void createSchema() {
        createSchemaDo(Arrays.<Class<?>>asList(
            StBaseEntity.class,
            TpcBaseEntity.class, TpcInheritA.class, TpcInheritB.class, TpcInheritB2.class, TpcInheritC.class,
            Tpc2InheritA.class, Tpc2InheritB.class, Tpc2InheritB2.class, Tpc2InheritB3.class, Tpc2InheritC.class));
    }

    @Test(dependsOnMethods = "createSchema")
    public void inheritanceSingleTable() throws Exception {

        MappedSchemaObject entityBase = persistenceManager.getEntity(StBaseEntity.class);
        MappedSchemaObject entityA = persistenceManager.getEntity(StInheritA.class);
        MappedSchemaObject entityB = persistenceManager.getEntity(StInheritB.class);
        MappedSchemaObject entityB2 = persistenceManager.getEntity(StInheritB2.class);
        MappedSchemaObject entityC = persistenceManager.getEntity(StInheritC.class);
        Assert.assertEquals(entityBase.getAttributeNames().size(), 3);
        Assert.assertEquals(entityA.getAttributeNames().size(), 4);
        Assert.assertEquals(entityB.getAttributeNames().size(), 4);
        Assert.assertEquals(entityB2.getAttributeNames().size(), 5);
        Assert.assertEquals(entityC.getAttributeNames().size(), 4);
        Assert.assertEquals(entityBase.getAllColumns().length, 8);
        Assert.assertEquals(entityA.getAllColumns().length, 8);
        Assert.assertEquals(entityB.getAllColumns().length, 8);
        Assert.assertEquals(entityB2.getAllColumns().length, 8);
        Assert.assertEquals(entityC.getAllColumns().length, 8);

        waitEntityAvailable(entityBase);

        PersistenceSession session = persistenceManager.createSession();
        try {

            StBaseEntity instBase = new StBaseEntity();
            instBase.setId(1);
            instBase.setCid(1);
            instBase.setVal("val1");
            session.insert(instBase);

            StInheritA instA = new StInheritA();
            instA.setId(2);
            instA.setCid(1);
            instA.setVal("val2");
            instA.setInhA("inhA");
            session.insert(instA);

            StInheritB instB = new StInheritB();
            instB.setId(3);
            instB.setCid(1);
            instB.setVal("val3");
            instB.setInhB("inhB");
            session.insert(instB);

            StInheritB2 instB2 = new StInheritB2();
            instB2.setId(4);
            instB2.setCid(1);
            instB2.setVal("val4");
            instB2.setInhB("inhB");
            instB2.setInhB2("inhB2");
            session.insert(instB2);

            StInheritC instC = new StInheritC();
            instC.setId(5);
            instC.setCid(1);
            instC.setVal("val5");
            instC.setInhC("inhC");
            session.insert(instC);

            StBaseEntity loadedBase = session.loadOne(StBaseEntity.class, 1);
            Assert.assertNotNull(loadedBase);
            Assert.assertEquals(loadedBase.getId(), 1);
            Assert.assertEquals(loadedBase.getVal(), "val1");

            insertUpdateDelete(session, StBaseEntity.class, instBase, new UpdateCallback<StBaseEntity>() {
                @Override public void modify(StBaseEntity inst) {
                    //
                }

                @Override public void check(StBaseEntity inst) {
                    Assert.assertEquals(inst.getVal(), "val1");
                }
            }, 1);
            session.insert(loadedBase);

            StBaseEntity loadedA = session.loadOne(StBaseEntity.class, 2);
            Assert.assertNotNull(loadedA);
            Assert.assertEquals(loadedA.getId(), 2);
            Assert.assertEquals(loadedA.getVal(), "val2");
            Assert.assertTrue(loadedA instanceof StInheritA);
            Assert.assertEquals(((StInheritA) loadedA).getInhA(), "inhA");

            insertUpdateDelete(session, StBaseEntity.class, loadedA, new UpdateCallback<StBaseEntity>() {
                @Override public void modify(StBaseEntity inst) {
                    Assert.assertTrue(inst instanceof StInheritA);
                }

                @Override public void check(StBaseEntity inst) {
                    Assert.assertTrue(inst instanceof StInheritA);
                    Assert.assertEquals(inst.getVal(), "val2");
                }
            }, 2);
            session.insert(loadedA);

            StBaseEntity loadedB = session.loadOne(StBaseEntity.class, 3);
            Assert.assertNotNull(loadedB);
            Assert.assertEquals(loadedB.getId(), 3);
            Assert.assertEquals(loadedB.getVal(), "val3");
            Assert.assertTrue(loadedB instanceof StInheritB);
            Assert.assertEquals(((StInheritB) loadedB).getInhB(), "inhB");

            insertUpdateDelete(session, StBaseEntity.class, loadedB, new UpdateCallback<StBaseEntity>() {
                @Override public void modify(StBaseEntity inst) {
                    Assert.assertTrue(inst instanceof StInheritB);
                }

                @Override public void check(StBaseEntity inst) {
                    Assert.assertTrue(inst instanceof StInheritB);
                    Assert.assertEquals(inst.getVal(), "val3");
                }
            }, 3);
            session.insert(loadedB);

            StBaseEntity loadedB2 = session.loadOne(StBaseEntity.class, 4);
            Assert.assertNotNull(loadedB2);
            Assert.assertEquals(loadedB2.getId(), 4);
            Assert.assertEquals(loadedB2.getVal(), "val4");
            Assert.assertTrue(loadedB2 instanceof StInheritB2);
            Assert.assertEquals(((StInheritB) loadedB2).getInhB(), "inhB");
            Assert.assertEquals(((StInheritB2) loadedB2).getInhB2(), "inhB2");

            insertUpdateDelete(session, StBaseEntity.class, loadedB2, new UpdateCallback<StBaseEntity>() {
                @Override public void modify(StBaseEntity inst) {
                    Assert.assertTrue(inst instanceof StInheritB2);
                }

                @Override public void check(StBaseEntity inst) {
                    Assert.assertTrue(inst instanceof StInheritB2);
                    Assert.assertEquals(inst.getVal(), "val4");
                }
            }, 4);
            session.insert(loadedB2);

            StBaseEntity loadedC = session.loadOne(StBaseEntity.class, 5);
            Assert.assertNotNull(loadedC);
            Assert.assertEquals(loadedC.getId(), 5);
            Assert.assertEquals(loadedC.getVal(), "val5");
            Assert.assertTrue(loadedC instanceof StInheritC);
            Assert.assertEquals(((StInheritC) loadedC).getInhC(), "inhC");

            insertUpdateDelete(session, StBaseEntity.class, loadedC, new UpdateCallback<StBaseEntity>() {
                @Override public void modify(StBaseEntity inst) {
                    Assert.assertTrue(inst instanceof StInheritC);
                }

                @Override public void check(StBaseEntity inst) {
                    Assert.assertTrue(inst instanceof StInheritC);
                    Assert.assertEquals(inst.getVal(), "val5");
                }
            }, 5);
            session.insert(loadedC);

            loadedA = session.loadOne(StInheritA.class, 2);
            Assert.assertNotNull(loadedA);
            Assert.assertEquals(loadedA.getId(), 2);
            Assert.assertEquals(loadedA.getVal(), "val2");
            Assert.assertTrue(loadedA instanceof StInheritA);
            Assert.assertEquals(((StInheritA) loadedA).getInhA(), "inhA");

            insertUpdateDelete(session, StInheritA.class, (StInheritA) loadedA, new UpdateCallback<StInheritA>() {
                @Override public void modify(StInheritA inst) {
                    Assert.assertTrue(inst instanceof StInheritA);
                }

                @Override public void check(StInheritA inst) {
                    Assert.assertEquals(inst.getVal(), "val2");
                }
            }, 2);

            loadedB = session.loadOne(StBaseEntity.class, 3);
            Assert.assertNotNull(loadedB);
            Assert.assertEquals(loadedB.getId(), 3);
            Assert.assertEquals(loadedB.getVal(), "val3");
            Assert.assertTrue(loadedB instanceof StInheritB);
            Assert.assertEquals(((StInheritB) loadedB).getInhB(), "inhB");

            insertUpdateDelete(session, StBaseEntity.class, loadedB, new UpdateCallback<StBaseEntity>() {
                @Override public void modify(StBaseEntity inst) {
                    Assert.assertTrue(inst instanceof StInheritB);
                }

                @Override public void check(StBaseEntity inst) {
                    Assert.assertEquals(inst.getVal(), "val3");
                }
            }, 3);

            loadedB2 = session.loadOne(StInheritB.class, 4);
            Assert.assertNotNull(loadedB2);
            Assert.assertEquals(loadedB2.getId(), 4);
            Assert.assertEquals(loadedB2.getVal(), "val4");
            Assert.assertTrue(loadedB2 instanceof StInheritB2);
            Assert.assertEquals(((StInheritB) loadedB2).getInhB(), "inhB");
            Assert.assertEquals(((StInheritB2) loadedB2).getInhB2(), "inhB2");

            insertUpdateDelete(session, StInheritB.class, (StInheritB) loadedB2, new UpdateCallback<StInheritB>() {
                @Override public void modify(StInheritB inst) {
                    Assert.assertTrue(inst instanceof StInheritB2);
                }

                @Override public void check(StInheritB inst) {
                    Assert.assertEquals(inst.getVal(), "val4");
                }
            }, 4);
            session.insert(loadedB2);

            loadedB2 = session.loadOne(StInheritB2.class, 4);
            Assert.assertNotNull(loadedB2);
            Assert.assertEquals(loadedB2.getId(), 4);
            Assert.assertEquals(loadedB2.getVal(), "val4");
            Assert.assertTrue(loadedB2 instanceof StInheritB2);
            Assert.assertEquals(((StInheritB) loadedB2).getInhB(), "inhB");
            Assert.assertEquals(((StInheritB2) loadedB2).getInhB2(), "inhB2");

            insertUpdateDelete(session, StInheritB2.class, (StInheritB2) loadedB2, new UpdateCallback<StInheritB2>() {
                @Override public void modify(StInheritB2 inst) {
                    Assert.assertTrue(inst instanceof StInheritB2);
                }

                @Override public void check(StInheritB2 inst) {
                    Assert.assertEquals(inst.getVal(), "val4");
                }
            }, 4);

            loadedC = session.loadOne(StInheritC.class, 5);
            Assert.assertNotNull(loadedC);
            Assert.assertEquals(loadedC.getId(), 5);
            Assert.assertEquals(loadedC.getVal(), "val5");
            Assert.assertTrue(loadedC instanceof StInheritC);
            Assert.assertEquals(((StInheritC) loadedC).getInhC(), "inhC");

            insertUpdateDelete(session, StInheritC.class, (StInheritC) loadedC, new UpdateCallback<StInheritC>() {
                @Override public void modify(StInheritC inst) {
                    Assert.assertTrue(inst instanceof StInheritC);
                }

                @Override public void check(StInheritC inst) {
                    Assert.assertEquals(inst.getVal(), "val5");
                }
            }, 5);

            //
            //
            //

            instBase = new StBaseEntity();
            instBase.setId(1);
            instBase.setCid(1);
            instBase.setVal("val1");
            session.insert(instBase);

            instA = new StInheritA();
            instA.setId(1);
            instA.setCid(2);
            instA.setVal("val2");
            instA.setInhA("inhA");
            session.insert(instA);

            instB = new StInheritB();
            instB.setId(1);
            instB.setCid(3);
            instB.setVal("val3");
            instB.setInhB("inhB");
            session.insert(instB);

            instB2 = new StInheritB2();
            instB2.setId(1);
            instB2.setCid(4);
            instB2.setVal("val4");
            instB2.setInhB("inhB");
            instB2.setInhB2("inhB2");
            session.insert(instB2);

            instC = new StInheritC();
            instC.setId(1);
            instC.setCid(5);
            instC.setVal("val5");
            instC.setInhC("inhC");
            session.insert(instC);

            List<StBaseEntity> multiple = session.loadMultiple(StBaseEntity.class, 1);
            Assert.assertNotNull(multiple);
            Assert.assertEquals(multiple.size(), 5);

            List<StBaseEntity> none = session.loadMultiple(StBaseEntity.class, 0);
            Assert.assertNotNull(none);
            Assert.assertEquals(none.size(), 0);

        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void inheritanceTablePerClass() throws Exception {

        MappedSchemaObject entityBase = persistenceManager.getEntity(TpcBaseEntity.class);
        MappedSchemaObject entityA = persistenceManager.getEntity(TpcInheritA.class);
        MappedSchemaObject entityB = persistenceManager.getEntity(TpcInheritB.class);
        MappedSchemaObject entityB2 = persistenceManager.getEntity(TpcInheritB2.class);
        MappedSchemaObject entityC = persistenceManager.getEntity(TpcInheritC.class);
        Assert.assertEquals(entityBase.getAttributeNames().size(), 3);
        Assert.assertEquals(entityBase.getAllColumns().length, 3);
        Assert.assertEquals(entityA.getAttributeNames().size(), 4);
        Assert.assertEquals(entityA.getAllColumns().length, 4);
        Assert.assertEquals(entityB.getAttributeNames().size(), 4);
        Assert.assertEquals(entityB.getAllColumns().length, 4);
        Assert.assertEquals(entityB2.getAttributeNames().size(), 5);
        Assert.assertEquals(entityB2.getAllColumns().length, 5);
        Assert.assertEquals(entityC.getAttributeNames().size(), 4);
        Assert.assertEquals(entityC.getAllColumns().length, 4);

        waitEntityAvailable(entityBase);
        waitEntityAvailable(entityA);
        waitEntityAvailable(entityB);
        waitEntityAvailable(entityB2);
        waitEntityAvailable(entityC);

        PersistenceSession session = persistenceManager.createSession();
        try {

            TpcBaseEntity instBase = new TpcBaseEntity();
            instBase.setId(1);
            instBase.setCid(1);
            instBase.setVal("val1");
            session.insert(instBase);

            TpcInheritA instA = new TpcInheritA();
            instA.setId(2);
            instA.setCid(1);
            instA.setVal("val2");
            instA.setInhA("inhA");
            session.insert(instA);

            TpcInheritB instB = new TpcInheritB();
            instB.setId(3);
            instB.setCid(1);
            instB.setVal("val3");
            instB.setInhB("inhB");
            session.insert(instB);

            TpcInheritB2 instB2 = new TpcInheritB2();
            instB2.setId(4);
            instB2.setCid(1);
            instB2.setVal("val4");
            instB2.setInhB("inhB");
            instB2.setInhB2("inhB2");
            session.insert(instB2);

            TpcInheritC instC = new TpcInheritC();
            instC.setId(5);
            instC.setCid(1);
            instC.setVal("val5");
            instC.setInhC("inhC");
            session.insert(instC);

            TpcBaseEntity loadedBase = session.loadOne(TpcBaseEntity.class, 1);
            Assert.assertNotNull(loadedBase);
            Assert.assertEquals(loadedBase.getId(), 1);
            Assert.assertEquals(loadedBase.getVal(), "val1");

            TpcBaseEntity loadedA = session.loadOne(TpcInheritA.class, 2);
            Assert.assertNotNull(loadedA);
            Assert.assertEquals(loadedA.getId(), 2);
            Assert.assertEquals(loadedA.getVal(), "val2");
            Assert.assertTrue(loadedA instanceof TpcInheritA);
            Assert.assertEquals(((TpcInheritA) loadedA).getInhA(), "inhA");

            insertUpdateDelete(session, TpcInheritA.class, (TpcInheritA) loadedA, new UpdateCallback<TpcInheritA>() {
                @Override public void modify(TpcInheritA inst) {
                    Assert.assertTrue(inst instanceof TpcInheritA);
                }

                @Override public void check(TpcInheritA inst) {
                    Assert.assertTrue(inst instanceof TpcInheritA);
                    Assert.assertEquals(inst.getVal(), "val2");
                }
            }, 2);
            session.insert(loadedA);

            TpcBaseEntity loadedB = session.loadOne(TpcInheritB.class, 3);
            Assert.assertNotNull(loadedB);
            Assert.assertEquals(loadedB.getId(), 3);
            Assert.assertEquals(loadedB.getVal(), "val3");
            Assert.assertTrue(loadedB instanceof TpcInheritB);
            Assert.assertEquals(((TpcInheritB) loadedB).getInhB(), "inhB");

            insertUpdateDelete(session, TpcInheritB.class, (TpcInheritB) loadedB, new UpdateCallback<TpcInheritB>() {
                @Override public void modify(TpcInheritB inst) {
                    Assert.assertTrue(inst instanceof TpcInheritB);
                }

                @Override public void check(TpcInheritB inst) {
                    Assert.assertTrue(inst instanceof TpcInheritB);
                    Assert.assertEquals(inst.getVal(), "val3");
                }
            }, 3);
            session.insert(loadedB);

            TpcBaseEntity loadedB2 = session.loadOne(TpcInheritB2.class, 4);
            Assert.assertNotNull(loadedB2);
            Assert.assertEquals(loadedB2.getId(), 4);
            Assert.assertEquals(loadedB2.getVal(), "val4");
            Assert.assertTrue(loadedB2 instanceof TpcInheritB2);
            Assert.assertEquals(((TpcInheritB) loadedB2).getInhB(), "inhB");
            Assert.assertEquals(((TpcInheritB2) loadedB2).getInhB2(), "inhB2");

            insertUpdateDelete(session, TpcInheritB2.class, (TpcInheritB2) loadedB2, new UpdateCallback<TpcInheritB2>() {
                @Override public void modify(TpcInheritB2 inst) {
                    Assert.assertTrue(inst instanceof TpcInheritB2);
                }

                @Override public void check(TpcInheritB2 inst) {
                    Assert.assertTrue(inst instanceof TpcInheritB2);
                    Assert.assertEquals(inst.getVal(), "val4");
                }
            }, 4);
            session.insert(loadedB2);

            TpcBaseEntity loadedC = session.loadOne(TpcInheritC.class, 5);
            Assert.assertNotNull(loadedC);
            Assert.assertEquals(loadedC.getId(), 5);
            Assert.assertEquals(loadedC.getVal(), "val5");
            Assert.assertTrue(loadedC instanceof TpcInheritC);
            Assert.assertEquals(((TpcInheritC) loadedC).getInhC(), "inhC");

            insertUpdateDelete(session, TpcInheritC.class, (TpcInheritC) loadedC, new UpdateCallback<TpcInheritC>() {
                @Override public void modify(TpcInheritC inst) {
                    Assert.assertTrue(inst instanceof TpcInheritC);
                }

                @Override public void check(TpcInheritC inst) {
                    Assert.assertEquals(inst.getVal(), "val5");
                }
            }, 5);
            session.insert(loadedC);

            loadedB2 = session.loadOne(TpcInheritB.class, 4);
            Assert.assertNotNull(loadedB2);
            Assert.assertEquals(loadedB2.getId(), 4);
            Assert.assertEquals(loadedB2.getVal(), "val4");
            Assert.assertTrue(loadedB2 instanceof TpcInheritB2);
            Assert.assertEquals(((TpcInheritB) loadedB2).getInhB(), "inhB");
            Assert.assertEquals(((TpcInheritB2) loadedB2).getInhB2(), "inhB2");

            loadedA = session.loadOne(TpcBaseEntity.class, 2);
            Assert.assertNotNull(loadedA);
            Assert.assertEquals(loadedA.getId(), 2);
            Assert.assertEquals(loadedA.getVal(), "val2");
            Assert.assertTrue(loadedA instanceof TpcInheritA);
            Assert.assertEquals(((TpcInheritA) loadedA).getInhA(), "inhA");

            insertUpdateDelete(session, TpcBaseEntity.class, loadedA, new UpdateCallback<TpcBaseEntity>() {
                @Override public void modify(TpcBaseEntity inst) {
                    Assert.assertTrue(inst instanceof TpcInheritA);
                }

                @Override public void check(TpcBaseEntity inst) {
                    Assert.assertTrue(inst instanceof TpcInheritA);
                    Assert.assertEquals(inst.getVal(), "val2");
                }
            }, 2);

            loadedB = session.loadOne(TpcBaseEntity.class, 3);
            Assert.assertNotNull(loadedB);
            Assert.assertEquals(loadedB.getId(), 3);
            Assert.assertEquals(loadedB.getVal(), "val3");
            Assert.assertTrue(loadedB instanceof TpcInheritB);
            Assert.assertEquals(((TpcInheritB) loadedB).getInhB(), "inhB");

            insertUpdateDelete(session, TpcBaseEntity.class, loadedB, new UpdateCallback<TpcBaseEntity>() {
                @Override public void modify(TpcBaseEntity inst) {
                    Assert.assertTrue(inst instanceof TpcInheritB);
                }

                @Override public void check(TpcBaseEntity inst) {
                    Assert.assertTrue(inst instanceof TpcInheritB);
                    Assert.assertEquals(inst.getVal(), "val3");
                }
            }, 3);

            loadedB2 = session.loadOne(TpcBaseEntity.class, 4);
            Assert.assertNotNull(loadedB2);
            Assert.assertEquals(loadedB2.getId(), 4);
            Assert.assertEquals(loadedB2.getVal(), "val4");
            Assert.assertTrue(loadedB2 instanceof TpcInheritB2);
            Assert.assertEquals(((TpcInheritB) loadedB2).getInhB(), "inhB");
            Assert.assertEquals(((TpcInheritB2) loadedB2).getInhB2(), "inhB2");

            insertUpdateDelete(session, TpcBaseEntity.class, loadedB2, new UpdateCallback<TpcBaseEntity>() {
                @Override public void modify(TpcBaseEntity inst) {
                    Assert.assertTrue(inst instanceof TpcInheritB2);
                }

                @Override public void check(TpcBaseEntity inst) {
                    Assert.assertTrue(inst instanceof TpcInheritB2);
                    Assert.assertEquals(inst.getVal(), "val4");
                }
            }, 4);

            loadedC = session.loadOne(TpcBaseEntity.class, 5);
            Assert.assertNotNull(loadedC);
            Assert.assertEquals(loadedC.getId(), 5);
            Assert.assertEquals(loadedC.getVal(), "val5");
            Assert.assertTrue(loadedC instanceof TpcInheritC);
            Assert.assertEquals(((TpcInheritC) loadedC).getInhC(), "inhC");

            insertUpdateDelete(session, TpcBaseEntity.class, loadedC, new UpdateCallback<TpcBaseEntity>() {
                @Override public void modify(TpcBaseEntity inst) {
                    Assert.assertTrue(inst instanceof TpcInheritC);
                }

                @Override public void check(TpcBaseEntity inst) {
                    Assert.assertTrue(inst instanceof TpcInheritC);
                    Assert.assertEquals(inst.getVal(), "val5");
                }
            }, 5);

            //
            //
            //

            instBase = new TpcBaseEntity();
            instBase.setId(1);
            instBase.setCid(1);
            instBase.setVal("val1");
            session.insert(instBase);

            instA = new TpcInheritA();
            instA.setId(1);
            instA.setCid(2);
            instA.setVal("val2");
            instA.setInhA("inhA");
            session.insert(instA);

            instB = new TpcInheritB();
            instB.setId(1);
            instB.setCid(3);
            instB.setVal("val3");
            instB.setInhB("inhB");
            session.insert(instB);

            instB2 = new TpcInheritB2();
            instB2.setId(1);
            instB2.setCid(4);
            instB2.setVal("val4");
            instB2.setInhB("inhB");
            instB2.setInhB2("inhB2");
            session.insert(instB2);

            instC = new TpcInheritC();
            instC.setId(1);
            instC.setCid(5);
            instC.setVal("val5");
            instC.setInhC("inhC");
            session.insert(instC);

            List<TpcBaseEntity> multiple = session.loadMultiple(TpcBaseEntity.class, 1);
            Assert.assertNotNull(multiple);
            Assert.assertEquals(multiple.size(), 5);

            List<TpcBaseEntity> none = session.loadMultiple(TpcBaseEntity.class, 0);
            Assert.assertNotNull(none);
            Assert.assertEquals(none.size(), 0);

        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void inheritanceAbstractTablePerClass() throws Exception {

        MappedSchemaObject entityBase = persistenceManager.getEntity(Tpc2BaseEntity.class);
        MappedSchemaObject entityA = persistenceManager.getEntity(Tpc2InheritA.class);
        MappedSchemaObject entityB = persistenceManager.getEntity(Tpc2InheritB.class);
        MappedSchemaObject entityB2 = persistenceManager.getEntity(Tpc2InheritB2.class);
        MappedSchemaObject entityB3 = persistenceManager.getEntity(Tpc2InheritB3.class);
        MappedSchemaObject entityC = persistenceManager.getEntity(Tpc2InheritC.class);
        Assert.assertEquals(entityBase.getAttributeNames().size(), 3);
        Assert.assertEquals(entityBase.getAllColumns().length, 3);
        Assert.assertEquals(entityA.getAttributeNames().size(), 4);
        Assert.assertEquals(entityA.getAllColumns().length, 4);
        Assert.assertEquals(entityB.getAttributeNames().size(), 4);
        Assert.assertEquals(entityB.getAllColumns().length, 4);
        Assert.assertEquals(entityB2.getAttributeNames().size(), 5);
        Assert.assertEquals(entityB2.getAllColumns().length, 5);
        Assert.assertEquals(entityC.getAttributeNames().size(), 4);
        Assert.assertEquals(entityC.getAllColumns().length, 4);

        waitEntityAvailable(entityBase);
        waitEntityAvailable(entityA);
        waitEntityAvailable(entityB);
        waitEntityAvailable(entityB2);
        waitEntityAvailable(entityB3);
        waitEntityAvailable(entityC);

        PersistenceSession session = persistenceManager.createSession();
        try {

            Tpc2InheritA instA = new Tpc2InheritA();
            instA.setId(2);
            instA.setCid(1);
            instA.setVal("val2");
            instA.setInhA("inhA");
            session.insert(instA);

            Tpc2InheritB2 instB2 = new Tpc2InheritB2();
            instB2.setId(4);
            instB2.setCid(1);
            instB2.setVal("val4");
            instB2.setInhB("inhB");
            instB2.setInhB2("inhB2");
            session.insert(instB2);

            Tpc2InheritB3 instB3 = new Tpc2InheritB3();
            instB3.setId(6);
            instB3.setCid(1);
            instB3.setVal("val4");
            instB3.setInhB("inhB");
            instB3.setInhB2("inhB2");
            session.insert(instB2);

            Tpc2InheritC instC = new Tpc2InheritC();
            instC.setId(5);
            instC.setCid(1);
            instC.setVal("val5");
            instC.setInhC("inhC");
            session.insert(instC);

            Tpc2BaseEntity loadedA = session.loadOne(Tpc2InheritA.class, 2);
            Assert.assertNotNull(loadedA);
            Assert.assertEquals(loadedA.getId(), 2);
            Assert.assertEquals(loadedA.getVal(), "val2");
            Assert.assertTrue(loadedA instanceof Tpc2InheritA);
            Assert.assertEquals(((Tpc2InheritA) loadedA).getInhA(), "inhA");

            insertUpdateDelete(session, Tpc2InheritA.class, (Tpc2InheritA) loadedA, new UpdateCallback<Tpc2InheritA>() {
                @Override public void modify(Tpc2InheritA inst) {
                    Assert.assertTrue(inst instanceof Tpc2InheritA);
                }

                @Override public void check(Tpc2InheritA inst) {
                    Assert.assertTrue(inst instanceof Tpc2InheritA);
                    Assert.assertEquals(inst.getVal(), "val2");
                }
            }, 2);
            session.insert(loadedA);

            Tpc2BaseEntity loadedB2 = session.loadOne(Tpc2InheritB2.class, 4);
            Assert.assertNotNull(loadedB2);
            Assert.assertEquals(loadedB2.getId(), 4);
            Assert.assertEquals(loadedB2.getVal(), "val4");
            Assert.assertTrue(loadedB2 instanceof Tpc2InheritB2);
            Assert.assertEquals(((Tpc2InheritB) loadedB2).getInhB(), "inhB");
            Assert.assertEquals(((Tpc2InheritB2) loadedB2).getInhB2(), "inhB2");

            insertUpdateDelete(session, Tpc2InheritB2.class, (Tpc2InheritB2) loadedB2, new UpdateCallback<Tpc2InheritB2>() {
                @Override public void modify(Tpc2InheritB2 inst) {
                    Assert.assertTrue(inst instanceof Tpc2InheritB2);
                }

                @Override public void check(Tpc2InheritB2 inst) {
                    Assert.assertTrue(inst instanceof Tpc2InheritB2);
                    Assert.assertEquals(inst.getVal(), "val4");
                }
            }, 4);
            session.insert(loadedB2);

            Tpc2BaseEntity loadedC = session.loadOne(Tpc2InheritC.class, 5);
            Assert.assertNotNull(loadedC);
            Assert.assertEquals(loadedC.getId(), 5);
            Assert.assertEquals(loadedC.getVal(), "val5");
            Assert.assertTrue(loadedC instanceof Tpc2InheritC);
            Assert.assertEquals(((Tpc2InheritC) loadedC).getInhC(), "inhC");

            insertUpdateDelete(session, Tpc2InheritC.class, (Tpc2InheritC) loadedC, new UpdateCallback<Tpc2InheritC>() {
                @Override public void modify(Tpc2InheritC inst) {
                    //inst.setStr("422");
                }

                @Override public void check(Tpc2InheritC inst) {
                    Assert.assertEquals(inst.getVal(), "val5");
                }
            }, 5);
            session.insert(loadedC);

            loadedB2 = session.loadOne(Tpc2InheritB.class, 4);
            Assert.assertNotNull(loadedB2);
            Assert.assertEquals(loadedB2.getId(), 4);
            Assert.assertEquals(loadedB2.getVal(), "val4");
            Assert.assertTrue(loadedB2 instanceof Tpc2InheritB2);
            Assert.assertEquals(((Tpc2InheritB) loadedB2).getInhB(), "inhB");
            Assert.assertEquals(((Tpc2InheritB2) loadedB2).getInhB2(), "inhB2");

            loadedA = session.loadOne(Tpc2BaseEntity.class, 2);
            Assert.assertNotNull(loadedA);
            Assert.assertEquals(loadedA.getId(), 2);
            Assert.assertEquals(loadedA.getVal(), "val2");
            Assert.assertTrue(loadedA instanceof Tpc2InheritA);
            Assert.assertEquals(((Tpc2InheritA) loadedA).getInhA(), "inhA");

            insertUpdateDelete(session, Tpc2BaseEntity.class, loadedA, new UpdateCallback<Tpc2BaseEntity>() {
                @Override public void modify(Tpc2BaseEntity inst) {
                    Assert.assertTrue(inst instanceof Tpc2InheritA);
                }

                @Override public void check(Tpc2BaseEntity inst) {
                    Assert.assertTrue(inst instanceof Tpc2InheritA);
                    Assert.assertEquals(inst.getVal(), "val2");
                }
            }, 2);

            loadedB2 = session.loadOne(Tpc2BaseEntity.class, 4);
            Assert.assertNotNull(loadedB2);
            Assert.assertEquals(loadedB2.getId(), 4);
            Assert.assertEquals(loadedB2.getVal(), "val4");
            Assert.assertTrue(loadedB2 instanceof Tpc2InheritB2);
            Assert.assertEquals(((Tpc2InheritB) loadedB2).getInhB(), "inhB");
            Assert.assertEquals(((Tpc2InheritB2) loadedB2).getInhB2(), "inhB2");

            insertUpdateDelete(session, Tpc2BaseEntity.class, loadedB2, new UpdateCallback<Tpc2BaseEntity>() {
                @Override public void modify(Tpc2BaseEntity inst) {
                    Assert.assertTrue(inst instanceof Tpc2InheritB2);
                }

                @Override public void check(Tpc2BaseEntity inst) {
                    Assert.assertTrue(inst instanceof Tpc2InheritB2);
                    Assert.assertEquals(inst.getVal(), "val4");
                }
            }, 4);

            loadedC = session.loadOne(Tpc2BaseEntity.class, 5);
            Assert.assertNotNull(loadedC);
            Assert.assertEquals(loadedC.getId(), 5);
            Assert.assertEquals(loadedC.getVal(), "val5");
            Assert.assertTrue(loadedC instanceof Tpc2InheritC);
            Assert.assertEquals(((Tpc2InheritC) loadedC).getInhC(), "inhC");

            insertUpdateDelete(session, Tpc2BaseEntity.class, loadedC, new UpdateCallback<Tpc2BaseEntity>() {
                @Override public void modify(Tpc2BaseEntity inst) {
                    Assert.assertTrue(inst instanceof Tpc2InheritC);
                }

                @Override public void check(Tpc2BaseEntity inst) {
                    Assert.assertTrue(inst instanceof Tpc2InheritC);
                    Assert.assertEquals(inst.getVal(), "val5");
                }
            }, 5);

            //
            //
            //

            instA = new Tpc2InheritA();
            instA.setId(1);
            instA.setCid(2);
            instA.setVal("val2");
            instA.setInhA("inhA");
            session.insert(instA);

            instB2 = new Tpc2InheritB2();
            instB2.setId(1);
            instB2.setCid(4);
            instB2.setVal("val4");
            instB2.setInhB("inhB");
            instB2.setInhB2("inhB2");
            session.insert(instB2);

            instC = new Tpc2InheritC();
            instC.setId(1);
            instC.setCid(5);
            instC.setVal("val5");
            instC.setInhC("inhC");
            session.insert(instC);

            List<Tpc2BaseEntity> multiple = session.loadMultiple(Tpc2BaseEntity.class, 1);
            Assert.assertNotNull(multiple);
            Assert.assertEquals(multiple.size(), 3);

            List<Tpc2BaseEntity> none = session.loadMultiple(Tpc2BaseEntity.class, 0);
            Assert.assertNotNull(none);
            Assert.assertEquals(none.size(), 0);

        } finally {session.close();}
    }

}
