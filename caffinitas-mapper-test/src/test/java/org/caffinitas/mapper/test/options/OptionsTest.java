/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.options;

import com.datastax.driver.core.VersionNumber;
import org.caffinitas.mapper.core.CasNotAppliedException;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.PersistOption;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class OptionsTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("options");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(OptionsTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        if (VersionNumber.parse("2.0.0").compareTo(VersionNumber.parse(configuredCassandraVersion)) >= 0) {
            throw new SkipException("options test not applicable for Apache Cassandra version " + configuredCassandraVersion);
        }
        if (Integer.parseInt(configuredNodeCount) < 2) {
            throw new SkipException("options test not applicable for node count " + configuredNodeCount);
        }

        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() {
        persistenceManager.close();
    }

    @Test
    public void createSchema() throws Exception {
        persistenceManager.createSchemaGenerator().generateLiveAlterDDL().execute(persistenceManager.driverSession());
    }

    @Test(dependsOnMethods = "createSchema")
    public void insert_with_ttl() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            IfNotExistsEntity inst = new IfNotExistsEntity();
            inst.setPk(2);
            inst.setCk(1);
            inst.setValue("2-1");
            session.insert(inst, new PersistOption.TTLOption(86400));
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void insert_if_not_exists_first() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            IfNotExistsEntity inst = new IfNotExistsEntity();
            inst.setPk(1);
            inst.setCk(1);
            inst.setValue("1-1");
            session.insert(inst, PersistOption.IfNotExistsOption.INSTANCE);
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "insert_if_not_exists_first", expectedExceptions = CasNotAppliedException.class)
    public void insert_if_not_exists_fail() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            IfNotExistsEntity inst = new IfNotExistsEntity();
            inst.setPk(1);
            inst.setCk(1);
            inst.setValue("1-1");
            session.insert(inst, PersistOption.IfNotExistsOption.INSTANCE);
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void update_if() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            IfNotExistsEntity inst = new IfNotExistsEntity();
            inst.setPk(3);
            inst.setCk(5);
            inst.setValue("oldVal");
            session.insert(inst);

            inst = new IfNotExistsEntity();
            inst.setPk(3);
            inst.setCk(5);
            inst.setValue("1-1-update");
            session.update(inst, PersistOption.ifValue("value", "oldVal"));

            IfNotExistsEntity loaded = session.loadOne(IfNotExistsEntity.class, 3, 5);
            Assert.assertEquals(loaded.getValue(), "1-1-update");
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "insert_if_not_exists_fail")
    public void delete_if_exists_success() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            IfNotExistsEntity inst = new IfNotExistsEntity();
            inst.setPk(1);
            inst.setCk(1);
            inst.setValue("1-1-update");
            session.delete(inst, PersistOption.IfExistsOption.INSTANCE);
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "delete_if_exists_success", expectedExceptions = CasNotAppliedException.class)
    public void delete_if_exists_fail() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            IfNotExistsEntity inst = new IfNotExistsEntity();
            inst.setPk(99);
            inst.setCk(99);
            inst.setValue("fail");
            session.delete(inst, PersistOption.IfExistsOption.INSTANCE);
        } finally {session.close();}
    }

}
