/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.usertype;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CComposite;
import org.caffinitas.mapper.annotations.CompositeType;

import java.util.Date;

@CComposite(compositeType = CompositeType.USER_TYPE)
public class NextUserTypeComposite {
    @CColumn private double valA;

    @CColumn private Date ts;

    public double getValA() {
        return valA;
    }

    public void setValA(double valA) {
        this.valA = valA;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NextUserTypeComposite that = (NextUserTypeComposite) o;

        if (Double.compare(that.valA, valA) != 0) {
            return false;
        }
        if (ts != null ? !ts.equals(that.ts) : that.ts != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(valA);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (ts != null ? ts.hashCode() : 0);
        return result;
    }
}
