/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.lazy;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CComposite;
import org.caffinitas.mapper.annotations.CLazy;
import org.caffinitas.mapper.annotations.CompositeType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@CComposite(compositeType = CompositeType.COLUMNS)
public class LazyComposite {

    @CColumn private List<String> strList;

    @CColumn @CLazy private List<String> lazyStrList;

    @CColumn private Set<String> strSet;

    @CColumn @CLazy private Set<String> lazyStrSet;

    @CColumn private Map<String, String> strMap;

    @CColumn @CLazy private Map<String, String> lazyStrMap;

    public List<String> getStrList() {
        if (strList == null) {
            strList = new ArrayList<String>();
        }
        return strList;
    }

    public void setStrList(List<String> strList) {
        this.strList = strList;
    }

    public List<String> getLazyStrList() {
        if (lazyStrList == null) {
            lazyStrList = new ArrayList<String>();
        }
        return lazyStrList;
    }

    public void setLazyStrList(List<String> lazyStrList) {
        this.lazyStrList = lazyStrList;
    }

    public Set<String> getStrSet() {
        if (strSet == null) {
            strSet = new HashSet<String>();
        }
        return strSet;
    }

    public void setStrSet(Set<String> strSet) {
        this.strSet = strSet;
    }

    public Set<String> getLazyStrSet() {
        if (lazyStrSet == null) {
            lazyStrSet = new HashSet<String>();
        }
        return lazyStrSet;
    }

    public void setLazyStrSet(Set<String> lazyStrSet) {
        this.lazyStrSet = lazyStrSet;
    }

    public Map<String, String> getStrMap() {
        if (strMap == null) {
            strMap = new HashMap<String, String>();
        }
        return strMap;
    }

    public void setStrMap(Map<String, String> strMap) {
        this.strMap = strMap;
    }

    public Map<String, String> getLazyStrMap() {
        if (lazyStrMap == null) {
            lazyStrMap = new HashMap<String, String>();
        }
        return lazyStrMap;
    }

    public void setLazyStrMap(Map<String, String> lazyStrMap) {
        this.lazyStrMap = lazyStrMap;
    }
}
