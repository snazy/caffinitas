/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.tracking;

import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.ModifyFuture;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;

public class TrackingSessionTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("tracking");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(TrackingSessionTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() throws InterruptedException {
        persistenceManager.close();

        System.out.flush();
        System.err.flush();
        Thread.sleep(500);
    }

    @Test
    public void createSchema() {
        createSchemaDo(Arrays.<Class<?>>asList(TrackingEntity.class));
    }

    @Test(dependsOnMethods = "createSchema")
    public void tracking_insertAndLoad() throws Exception {

        PersistenceSession session = persistenceManager.createTrackingSession();
        try {
            TrackingEntity inst = new TrackingEntity();
            inst.setId(11);
            inst.setStr("str");
            inst.setValue("val");
            session.insert(inst);

            TrackingEntity loaded = session.loadOne(TrackingEntity.class, 11);
            Assert.assertSame(loaded, inst);
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "tracking_insertAndLoad")
    public void tracking_insertUpdateAndLoad() throws Exception {

        PersistenceSession session = persistenceManager.createTrackingSession();
        try {
            TrackingEntity loaded = session.loadOne(TrackingEntity.class, 11);

            loaded.setStr("updated");
            session.update(loaded);

            TrackingEntity loaded2 = session.loadOne(TrackingEntity.class, 11);
            Assert.assertSame(loaded2, loaded);
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "tracking_insertAndLoad")
    public void tracking_insertUpdateNoChange() throws Exception {

        PersistenceSession session = persistenceManager.createTrackingSession();
        try {
            TrackingEntity loaded = session.loadOne(TrackingEntity.class, 11);

            ModifyFuture<TrackingEntity> f = session.updateAsync(loaded);
            Assert.assertTrue(f.getClass().getName().toLowerCase().contains("immediate"));
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void tracking_flushOnClose() throws Exception {

        PersistenceSession session = persistenceManager.createTrackingSession();
        try {
            TrackingEntity inst = new TrackingEntity();
            inst.setId(21);
            inst.setStr("str");
            inst.setValue("val");
            session.insert(inst);

            inst.setStr("modified");
        } finally {session.close();}

        session = persistenceManager.createTrackingSession();
        try {
            TrackingEntity loaded = session.loadOne(TrackingEntity.class, 21);
            Assert.assertEquals(loaded.getStr(), "modified");
        } finally {session.close();}
    }
}
