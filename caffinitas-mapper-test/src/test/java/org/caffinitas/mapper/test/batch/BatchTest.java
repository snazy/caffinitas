/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.batch;

import com.datastax.driver.core.VersionNumber;
import org.caffinitas.mapper.core.Batch;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

public class BatchTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("junit");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(BatchTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        if (VersionNumber.parse("2.0.0").compareTo(VersionNumber.parse(configuredCassandraVersion)) >= 0) {
            throw new SkipException("options test not applicable for Apache Cassandra version " + configuredCassandraVersion);
        }

        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() {
        persistenceManager.close();
    }

    @Test
    public void createSchema() throws Exception {
        createSchemaDo(Collections.<Class<?>>singletonList(BatchEntity.class));
    }

    @Test(dependsOnMethods = "createSchema")
    public void batch_simple() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            Batch batch = session.startBatch();
            try {

                BatchEntity inst = new BatchEntity();
                inst.setId(1);
                inst.setVal("1");
                batch.insert(inst);

                inst = new BatchEntity();
                inst.setId(2);
                inst.setVal("2");
                batch.insert(inst);

                inst = new BatchEntity();
                inst.setId(3);
                inst.setVal("3");
                batch.insert(inst);
            } finally { batch.close(); } // Batch.close() implicitly calls Batch.submitBatch()

            BatchEntity loaded = session.loadOne(BatchEntity.class, 1);
            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getVal(), "1");
            loaded = session.loadOne(BatchEntity.class, 2);
            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getVal(), "2");
            loaded = session.loadOne(BatchEntity.class, 3);
            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getVal(), "3");
        } finally {session.close();}
    }

}
