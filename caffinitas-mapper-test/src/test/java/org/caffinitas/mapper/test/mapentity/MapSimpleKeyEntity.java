/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.mapentity;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CMapEntity;
import org.caffinitas.mapper.annotations.DataTypeName;
import org.caffinitas.mapper.core.MapEntity;

@CMapEntity(table = "me_simple_key",
    partitionKey = {"pkInt", "pkText"}, clusteringKey = {},
    clusteringType = String.class, clusteringDataType = DataTypeName.TEXT,
    valueType = MapSimpleKeyValue.class)
public class MapSimpleKeyEntity extends MapEntity<String, MapSimpleKeyValue> {
    @CColumn private int pkInt;
    @CColumn private String pkText;

    public int getPkInt() {
        return pkInt;
    }

    public void setPkInt(int pkInt) {
        this.pkInt = pkInt;
    }

    public String getPkText() {
        return pkText;
    }

    public void setPkText(String pkText) {
        this.pkText = pkText;
    }
}
