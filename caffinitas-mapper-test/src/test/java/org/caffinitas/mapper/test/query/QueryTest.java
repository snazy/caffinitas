/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.query;

import com.datastax.driver.core.VersionNumber;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.QueryBinder;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.caffinitas.mapper.test.query.st.StBaseEntity;
import org.caffinitas.mapper.test.query.st.StInheritB;
import org.caffinitas.mapper.test.query.tpc.TpcBaseEntity;
import org.caffinitas.mapper.test.query.tpc.TpcInheritA;
import org.caffinitas.mapper.test.query.tpc.TpcInheritB;
import org.caffinitas.mapper.test.query.tpc.TpcInheritB2;
import org.caffinitas.mapper.test.query.tpc.TpcInheritC;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("ConstantConditions") public class QueryTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("query");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(QueryTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        if (VersionNumber.parse("2.0.0").compareTo(VersionNumber.parse(configuredCassandraVersion)) >= 0) {
            throw new SkipException("user type test not applicable for Apache Cassandra version " + configuredCassandraVersion);
        }

        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() {
        persistenceManager.close();
    }

    @Test
    public void createSchema() throws Exception {
        createSchemaDo(Arrays.asList(StBaseEntity.class,
            TpcBaseEntity.class, TpcInheritA.class, TpcInheritB.class, TpcInheritB2.class, TpcInheritC.class,
            SimpleEntity.class));
    }

    @Test(dependsOnMethods = "createSchema")
    public void tablePerClass_root_sameCondition() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            QueryBinder<TpcBaseEntity> queryBinder = session.createQueryBinder(TpcBaseEntity.class, null, "id in :id", null);
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            List<TpcBaseEntity> result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // as named query

            queryBinder = session.createNamedQueryBinder(TpcBaseEntity.class, null, "byIds");
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // insert data

            TpcBaseEntity inst = new TpcBaseEntity();
            inst.setId(11);
            inst.setVal("one");
            session.insert(inst);
            inst.setId(12);
            inst.setVal("two");
            session.insert(inst);
            inst.setId(13);
            inst.setVal("three");
            session.insert(inst);

            // again

            queryBinder = session.createQueryBinder(TpcBaseEntity.class, null, "id in :id", null);
            queryBinder.setList("id", Arrays.asList(11, 12, 13));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 3);

            // as named query

            queryBinder = session.createNamedQueryBinder(TpcBaseEntity.class, null, "byIds");
            queryBinder.setList("id", Arrays.asList(11, 12, 13));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 3);
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void tablePerClass_root_differentConditions() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            Map<Class<? extends TpcBaseEntity>, String> conditionMap = new HashMap<Class<? extends TpcBaseEntity>, String>();
            conditionMap.put(TpcBaseEntity.class, "id = :id1");
            conditionMap.put(TpcInheritA.class, "id = :id2");
            conditionMap.put(TpcInheritB.class, "id = :id3");
            conditionMap.put(TpcInheritB2.class, "id = :id4");
            conditionMap.put(TpcInheritC.class, "id = :id5");
            QueryBinder<TpcBaseEntity> queryBinder = session.createQueryBinder(TpcBaseEntity.class, null, null, conditionMap);
            queryBinder.setInt("id1", 31);
            queryBinder.setInt("id2", 32);
            queryBinder.setInt("id3", 33);
            queryBinder.setInt("id4", 34);
            queryBinder.setInt("id5", 35);
            List<TpcBaseEntity> result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // as named query

            queryBinder = session.createNamedQueryBinder(TpcBaseEntity.class, null, "byOther");
            queryBinder.setInt("id1", 31);
            queryBinder.setInt("id2", 32);
            queryBinder.setInt("id3", 33);
            queryBinder.setInt("id4", 34);
            queryBinder.setInt("id5", 35);
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // insert data

            TpcBaseEntity inst = new TpcBaseEntity();
            inst.setId(31);
            inst.setVal("one");
            session.insert(inst);
            inst = new TpcInheritA();
            inst.setId(32);
            inst.setVal("two");
            session.insert(inst);
            inst = new TpcInheritB();
            inst.setId(33);
            inst.setVal("three");
            session.insert(inst);
            inst = new TpcInheritB2();
            inst.setId(34);
            inst.setVal("three");
            session.insert(inst);
            inst = new TpcInheritC();
            inst.setId(35);
            inst.setVal("three");
            session.insert(inst);

            // again

            queryBinder = session.createQueryBinder(TpcBaseEntity.class, null, null, conditionMap);
            queryBinder.setInt("id1", 31);
            queryBinder.setInt("id2", 32);
            queryBinder.setInt("id3", 33);
            queryBinder.setInt("id4", 34);
            queryBinder.setInt("id5", 35);
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 5);

            // as named query

            queryBinder = session.createNamedQueryBinder(TpcBaseEntity.class, null, "byOther");
            queryBinder.setInt("id1", 31);
            queryBinder.setInt("id2", 32);
            queryBinder.setInt("id3", 33);
            queryBinder.setInt("id4", 34);
            queryBinder.setInt("id5", 35);
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 5);
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "tablePerClass_root_sameCondition")
    public void tablePerClass_atB_sameCondition() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            QueryBinder<TpcInheritB> queryBinder = session.createQueryBinder(TpcInheritB.class, null, "id in :id", null);
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            List<TpcInheritB> result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // as named query

            queryBinder = session.createNamedQueryBinder(TpcInheritB.class, null, "byIds");
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // insert data

            TpcInheritB inst = new TpcInheritB();
            inst.setId(21);
            inst.setVal("one");
            session.insert(inst);
            inst.setId(22);
            inst.setVal("two");
            session.insert(inst);
            inst.setId(23);
            inst.setVal("three");
            session.insert(inst);

            // again

            queryBinder = session.createQueryBinder(TpcInheritB.class, null, "id in :id", null);
            queryBinder.setList("id", Arrays.asList(11, 12, 13, 21, 22, 23));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 3);

            // as named query

            queryBinder = session.createNamedQueryBinder(TpcInheritB.class, null, "byIds");
            queryBinder.setList("id", Arrays.asList(11, 12, 13, 21, 22, 23));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 3);
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void tablePerClass_atB_differentConditions() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            Map<Class<? extends TpcInheritB>, String> conditionMap = new HashMap<Class<? extends TpcInheritB>, String>();
            conditionMap.put(TpcInheritB.class, "id = :id3");
            conditionMap.put(TpcInheritB2.class, "id = :id4");
            QueryBinder<TpcInheritB> queryBinder = session.createQueryBinder(TpcInheritB.class, null, null, conditionMap);
            queryBinder.setInt("id1", 41);
            queryBinder.setInt("id2", 42);
            queryBinder.setInt("id3", 43);
            queryBinder.setInt("id4", 44);
            queryBinder.setInt("id5", 45);
            List<TpcInheritB> result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // as named query

            queryBinder = session.createNamedQueryBinder(TpcInheritB.class, null, "byOther");
            queryBinder.setInt("id1", 41);
            queryBinder.setInt("id2", 42);
            queryBinder.setInt("id3", 43);
            queryBinder.setInt("id4", 44);
            queryBinder.setInt("id5", 45);
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // insert data

            TpcBaseEntity inst = new TpcBaseEntity();
            inst.setId(41);
            inst.setVal("one");
            session.insert(inst);
            inst = new TpcInheritA();
            inst.setId(42);
            inst.setVal("two");
            session.insert(inst);
            inst = new TpcInheritB();
            inst.setId(43);
            inst.setVal("three");
            session.insert(inst);
            inst = new TpcInheritB2();
            inst.setId(44);
            inst.setVal("three");
            session.insert(inst);
            inst = new TpcInheritC();
            inst.setId(45);
            inst.setVal("three");
            session.insert(inst);

            // again

            queryBinder = session.createQueryBinder(TpcInheritB.class, null, null, conditionMap);
            queryBinder.setInt("id1", 41);
            queryBinder.setInt("id2", 42);
            queryBinder.setInt("id3", 43);
            queryBinder.setInt("id4", 44);
            queryBinder.setInt("id5", 45);
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 2);

            // as named query

            queryBinder = session.createNamedQueryBinder(TpcInheritB.class, null, "byOther");
            queryBinder.setInt("id1", 41);
            queryBinder.setInt("id2", 42);
            queryBinder.setInt("id3", 43);
            queryBinder.setInt("id4", 44);
            queryBinder.setInt("id5", 45);
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 2);
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void singleTable_root() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            QueryBinder<StBaseEntity> queryBinder = session.createQueryBinder(StBaseEntity.class, null, "id in :id", null);
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            List<StBaseEntity> result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // as named query

            queryBinder = session.createNamedQueryBinder(StBaseEntity.class, null, "byIds");
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // insert data

            StBaseEntity inst = new StBaseEntity();
            inst.setId(11);
            inst.setVal("one");
            session.insert(inst);
            inst.setId(12);
            inst.setVal("two");
            session.insert(inst);
            inst.setId(13);
            inst.setVal("three");
            session.insert(inst);

            // again

            queryBinder = session.createQueryBinder(StBaseEntity.class, null, "id in :id", null);
            queryBinder.setList("id", Arrays.asList(11, 12, 13));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 3);

            // as named query

            queryBinder = session.createNamedQueryBinder(StBaseEntity.class, null, "byIds");
            queryBinder.setList("id", Arrays.asList(11, 12, 13));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 3);
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "singleTable_root")
    public void singleTable_atB() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            QueryBinder<StInheritB> queryBinder = session.createQueryBinder(StInheritB.class, null, "id in :id", null);
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            List<StInheritB> result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // as named query

            queryBinder = session.createNamedQueryBinder(StInheritB.class, null, "byIds");
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // insert data

            StInheritB inst = new StInheritB();
            inst.setId(21);
            inst.setVal("one");
            session.insert(inst);
            inst.setId(22);
            inst.setVal("two");
            session.insert(inst);
            inst.setId(23);
            inst.setVal("three");
            session.insert(inst);

            // again

            queryBinder = session.createQueryBinder(StInheritB.class, null, "id in :id", null);
            queryBinder.setList("id", Arrays.asList(11, 12, 13, 21, 22, 23));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 3);

            // as named query

            queryBinder = session.createNamedQueryBinder(StInheritB.class, null, "byIds");
            queryBinder.setList("id", Arrays.asList(11, 12, 13, 21, 22, 23));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 3);
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void simple() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            QueryBinder<SimpleEntity> queryBinder = session.createQueryBinder(SimpleEntity.class, null, "id in :id", null);
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            List<SimpleEntity> result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // as named query

            queryBinder = session.createNamedQueryBinder(SimpleEntity.class, null, "byIds");
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertTrue(result.isEmpty());

            // insert data

            SimpleEntity inst = new SimpleEntity();
            inst.setId(1);
            inst.setStr("one");
            session.insert(inst);
            inst.setId(2);
            inst.setStr("two");
            session.insert(inst);
            inst.setId(3);
            inst.setStr("three");
            session.insert(inst);

            // again

            queryBinder = session.createQueryBinder(SimpleEntity.class, null, "id in :id", null);
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 3);

            // as named query

            queryBinder = session.createNamedQueryBinder(SimpleEntity.class, null, "byIds");
            queryBinder.setList("id", Arrays.asList(1, 2, 3));
            result = session.executeQuery(queryBinder);

            Assert.assertNotNull(result);
            Assert.assertEquals(result.size(), 3);
        } finally {session.close();}
    }
}
