/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.model;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.CqlStatementList;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceRuntimeException;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.SchemaGenerator;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.caffinitas.mapper.test.UpdateCallback;
import org.caffinitas.mapper.test.model.ksname.mapped.MappedKeyspaceNameEntity;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.InetAddress;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("ConstantConditions") public class RealEntityTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("junit");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(RealEntityTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() {
        persistenceManager.close();
    }

    @Test
    public void noDataColumnsEntity() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(NoDataColumnsEntity.class);
        Assert.assertEquals(entity.getAttributeNames().size(), 2);
        createSchemaDo(Collections.<Class<?>>singletonList(NoDataColumnsEntity.class));

        NoDataColumnsEntity inst = new NoDataColumnsEntity();
        inst.setId(11);
        inst.setStr("foo");

        PersistenceSession session = persistenceManager.createSession();
        try {
            session.insert(inst);

            NoDataColumnsEntity loaded = session.loadOne(NoDataColumnsEntity.class, 11, "foo");

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 11);
            Assert.assertEquals(loaded.getStr(), "foo");

            insertUpdateDelete(session, NoDataColumnsEntity.class, inst, new UpdateCallback<NoDataColumnsEntity>() {
                @Override public void modify(NoDataColumnsEntity inst) {
                    // nop
                }

                @Override public void check(NoDataColumnsEntity inst) {
                    Assert.assertEquals(inst.getStr(), "foo");
                }
            }, 11);

            session.insert(inst);
            inst.setStr("bar");
            session.insert(inst);

            List<NoDataColumnsEntity> many = session.loadMultiple(NoDataColumnsEntity.class, 11);
            Assert.assertNotNull(many);
            Assert.assertEquals(many.size(), 2);
            Assert.assertEquals(many.get(0).getStr(), "bar");
            Assert.assertEquals(many.get(1).getStr(), "foo");
        } finally { session.close(); }
    }

    @Test
    public void collEntity() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(CollEntity.class);
        Assert.assertEquals(entity.getAttributeNames().size(), 4);
        createSchemaDo(Collections.<Class<?>>singletonList(CollEntity.class));

        CollEntity inst = new CollEntity();
        inst.setId(11);
        inst.setStringList(Arrays.asList("one", "two", "three"));
        inst.setStringSet(new HashSet<String>(inst.getStringList()));

        PersistenceSession session = persistenceManager.createSession();
        try {
            session.insert(inst);

            CollEntity loaded = session.loadOne(CollEntity.class, 11);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 11);
            Assert.assertNotNull(loaded.getStringList());
            Assert.assertNotNull(loaded.getStringSet());
            Assert.assertEquals(loaded.getStringList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getStringSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));

            insertUpdateDelete(session, CollEntity.class, inst, new UpdateCallback<CollEntity>() {
                @Override public void modify(CollEntity inst) {
                    inst.setStringList(Arrays.asList("eins", "zwei", "drei"));
                }

                @Override public void check(CollEntity inst) {
                    Assert.assertEquals(inst.getStringList(), Arrays.asList("eins", "zwei", "drei"));
                }
            }, 11);
        } finally {session.close();}
    }

    @Test
    public void mapEntity() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(MapEntity.class);
        Assert.assertEquals(entity.getAttributeNames().size(), 3);
        createSchemaDo(Collections.<Class<?>>singletonList(MapEntity.class));

        MapEntity inst = new MapEntity();
        inst.setId(11);
        inst.setIntEnumMap(new HashMap<Integer, SomeEnum>());
        inst.setStringInetMap(new HashMap<String, InetAddress>());
        inst.getIntEnumMap().put(1, SomeEnum.ONE);
        inst.getIntEnumMap().put(2, SomeEnum.TWO);
        inst.getIntEnumMap().put(3, SomeEnum.THREE);
        inst.getStringInetMap().put("localhost", InetAddress.getLocalHost());
        inst.getStringInetMap().put("loopback", InetAddress.getByName("localhost"));

        PersistenceSession session = persistenceManager.createSession();
        try {
            session.insert(inst);

            MapEntity loaded = session.loadOne(MapEntity.class, 11);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 11);
            Assert.assertNotNull(loaded.getIntEnumMap());
            Assert.assertNotNull(loaded.getStringInetMap());
            Assert.assertEquals(loaded.getIntEnumMap().size(), 3);
            Assert.assertEquals(loaded.getStringInetMap().size(), 2);
            Assert.assertEquals(loaded.getIntEnumMap().get(1), SomeEnum.ONE);
            Assert.assertEquals(loaded.getIntEnumMap().get(2), SomeEnum.TWO);
            Assert.assertEquals(loaded.getIntEnumMap().get(3), SomeEnum.THREE);
            Assert.assertEquals(loaded.getStringInetMap().get("localhost"), InetAddress.getLocalHost());
            Assert.assertEquals(loaded.getStringInetMap().get("loopback"), InetAddress.getByName("localhost"));

            insertUpdateDelete(session, MapEntity.class, inst, new UpdateCallback<MapEntity>() {
                @Override public void modify(MapEntity inst) throws Exception {
                    inst.getIntEnumMap().put(4, SomeEnum.ONE);
                    inst.getIntEnumMap().put(5, SomeEnum.TWO);
                    inst.getIntEnumMap().put(6, SomeEnum.THREE);
                    inst.getStringInetMap().put("A", InetAddress.getLocalHost());
                    inst.getStringInetMap().put("B", InetAddress.getByName("localhost"));
                }

                @Override public void check(MapEntity inst) throws Exception {
                    Assert.assertEquals(inst.getId(), 11);
                    Assert.assertNotNull(inst.getIntEnumMap());
                    Assert.assertNotNull(inst.getStringInetMap());
                    Assert.assertEquals(inst.getIntEnumMap().size(), 6);
                    Assert.assertEquals(inst.getStringInetMap().size(), 4);
                    Assert.assertEquals(inst.getIntEnumMap().get(1), SomeEnum.ONE);
                    Assert.assertEquals(inst.getIntEnumMap().get(2), SomeEnum.TWO);
                    Assert.assertEquals(inst.getIntEnumMap().get(3), SomeEnum.THREE);
                    Assert.assertEquals(inst.getStringInetMap().get("localhost"), InetAddress.getLocalHost());
                    Assert.assertEquals(inst.getStringInetMap().get("loopback"), InetAddress.getByName("localhost"));
                    Assert.assertEquals(inst.getIntEnumMap().get(4), SomeEnum.ONE);
                    Assert.assertEquals(inst.getIntEnumMap().get(5), SomeEnum.TWO);
                    Assert.assertEquals(inst.getIntEnumMap().get(6), SomeEnum.THREE);
                    Assert.assertEquals(inst.getStringInetMap().get("A"), InetAddress.getLocalHost());
                    Assert.assertEquals(inst.getStringInetMap().get("B"), InetAddress.getByName("localhost"));
                }
            }, 11);
        } finally { session.close(); }
    }

    @Test
    public void flatCompEntity() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(FlatCompEntity.class);
        Assert.assertEquals(entity.getAttributeNames().size(), 2);
        createSchemaDo(Collections.<Class<?>>singletonList(FlatCompEntity.class));

        FlatCompEntity inst = new FlatCompEntity();
        inst.setId(11);
        FlatComposite flatComposite = new FlatComposite();
        NextFlatComposite next = new NextFlatComposite();
        Date date = new Date();
        next.setTs(date);
        next.setValA(7.51973d);
        flatComposite.setNext(next);
        flatComposite.setStr("some string value");
        flatComposite.setVint(42);
        inst.setFlatComposite(flatComposite);

        PersistenceSession session = persistenceManager.createSession();
        try {
            session.insert(inst);

            FlatCompEntity loaded = session.loadOne(FlatCompEntity.class, 11);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 11);

            flatComposite = loaded.getFlatComposite();
            Assert.assertNotNull(flatComposite);
            Assert.assertEquals(flatComposite.getStr(), "some string value");
            Assert.assertEquals(flatComposite.getVint(), 42);

            next = flatComposite.getNext();
            Assert.assertNotNull(next);
            Assert.assertEquals(next.getValA(), 7.51973d, .01d);
            Assert.assertEquals(next.getTs(), date);

            insertUpdateDelete(session, FlatCompEntity.class, inst, new UpdateCallback<FlatCompEntity>() {
                @Override public void modify(FlatCompEntity inst) {
                    inst.getFlatComposite().setStr("other string value");
                }

                @Override public void check(FlatCompEntity inst) {
                    Assert.assertEquals(inst.getFlatComposite().getStr(), "other string value");
                }
            }, 11);
        } finally { session.close(); }
    }

    @Test
    public void reference() throws Exception {

        MappedSchemaObject otherSimple = persistenceManager.getEntity(RefBySimplePKEntity.class);
        MappedSchemaObject otherFull = persistenceManager.getEntity(RefByFullPKEntity.class);
        MappedSchemaObject entity = persistenceManager.getEntity(ReferenceEntity.class);
        createSchemaDo(Arrays.asList(RefBySimplePKEntity.class, RefByFullPKEntity.class, ReferenceEntity.class));

        Assert.assertEquals(entity.getAttributeNames().size(), 4);
        Assert.assertEquals(otherSimple.getAttributeNames().size(), 3);
        Assert.assertEquals(otherFull.getAttributeNames().size(), 7);
        Assert.assertEquals(entity.getAllColumns().length, 8);
        Assert.assertEquals(otherSimple.getAllColumns().length, 3);
        Assert.assertEquals(otherFull.getAllColumns().length, 7);

        PersistenceSession session = persistenceManager.createSession();
        try {

            RefBySimplePKEntity refSimple = new RefBySimplePKEntity();
            refSimple.setId("idSimple");
            refSimple.setNum(42);
            refSimple.setValue("valSimple");
            session.insert(refSimple);

            final RefBySimplePKEntity refSimple2 = new RefBySimplePKEntity();
            refSimple2.setId("otherId");
            refSimple2.setNum(422);
            refSimple2.setValue("valSimple2");
            session.insert(refSimple2);

            RefByFullPKEntity refFull = new RefByFullPKEntity();
            refFull.setPk1("pk1");
            refFull.setPk2(2);
            refFull.setCk1(3);
            refFull.setCk2("ck2");
            refFull.setCk3(new Date());
            refFull.setValue("valFull");
            refFull.setNum(43);
            session.insert(refFull);

            ReferenceEntity inst = new ReferenceEntity();
            inst.setId(99);
            inst.setStr("str");
            session.insert(inst);

            inst = new ReferenceEntity();
            inst.setId(98);
            inst.setStr("str");
            inst.setOtherFull(refFull);
            inst.setOtherSimple(refSimple);
            session.insert(inst);

            ReferenceEntity loaded = session.loadOne(ReferenceEntity.class, 99);
            Assert.assertEquals(loaded.getId(), 99);
            Assert.assertEquals(loaded.getStr(), "str");
            Assert.assertNull(loaded.getOtherFull());
            Assert.assertNull(loaded.getOtherSimple());

            loaded = session.loadOne(ReferenceEntity.class, 98);
            Assert.assertEquals(loaded.getId(), 98);
            Assert.assertEquals(loaded.getStr(), "str");

            Assert.assertNotNull(loaded.getOtherSimple());
            Assert.assertEquals(loaded.getOtherSimple().getId(), "idSimple");
            Assert.assertEquals(loaded.getOtherSimple().getNum(), 42);
            Assert.assertEquals(loaded.getOtherSimple().getValue(), "valSimple");

            Assert.assertNotNull(loaded.getOtherFull());
            Assert.assertEquals(loaded.getOtherFull().getPk1(), "pk1");
            Assert.assertEquals(loaded.getOtherFull().getPk2(), 2);
            Assert.assertEquals(loaded.getOtherFull().getCk1(), 3);
            Assert.assertEquals(loaded.getOtherFull().getCk2(), "ck2");
            Assert.assertEquals(loaded.getOtherFull().getCk3(), refFull.getCk3());
            Assert.assertEquals(loaded.getOtherFull().getValue(), "valFull");
            Assert.assertEquals(loaded.getOtherFull().getNum(), 43);

            insertUpdateDelete(session, ReferenceEntity.class, inst, new UpdateCallback<ReferenceEntity>() {
                @Override public void modify(ReferenceEntity inst) {
                    inst.setOtherSimple(refSimple2);
                }

                @Override public void check(ReferenceEntity inst) {
                    Assert.assertEquals(inst.getOtherSimple().getId(), "otherId");
                    Assert.assertEquals(inst.getOtherSimple().getNum(), 422);
                    Assert.assertEquals(inst.getOtherSimple().getValue(), "valSimple2");
                }
            }, 98);
        } finally { session.close(); }
    }

    @Test
    public void denormalized() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(DenormEntity.class);
        Assert.assertEquals(entity.getAttributeNames().size(), 2);
        createSchemaDo(Collections.<Class<?>>singletonList(DenormEntity.class));

        DenormEntity inst = new DenormEntity();
        inst.setId(11);
        DenormInnerEntity inner = new DenormInnerEntity();
        NextFlatComposite next = new NextFlatComposite();
        Date date = new Date();
        next.setTs(date);
        next.setValA(7.51973d);
        inner.setNext(next);
        inner.setStr("some string value");
        inner.setVint(42);
        inst.setInner(inner);

        PersistenceSession session = persistenceManager.createSession();
        try {
            session.insert(inst);

            DenormEntity loaded = session.loadOne(DenormEntity.class, 11);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 11);

            inner = loaded.getInner();
            Assert.assertNotNull(inner);
            Assert.assertEquals(inner.getStr(), "some string value");
            Assert.assertEquals(inner.getVint(), 42);

            next = inner.getNext();
            Assert.assertNotNull(next);
            Assert.assertEquals(next.getValA(), 7.51973d, .01d);
            Assert.assertEquals(next.getTs(), date);

            insertUpdateDelete(session, DenormEntity.class, inst, new UpdateCallback<DenormEntity>() {
                @Override public void modify(DenormEntity inst) {
                    DenormInnerEntity inner = new DenormInnerEntity();
                    inner.setStr("other string value");
                    inner.setVint(422);
                    inst.setInner(inner);
                }

                @Override public void check(DenormEntity inst) {
                    Assert.assertEquals(inst.getInner().getVint(), 422);
                    Assert.assertEquals(inst.getInner().getStr(), "other string value");
                }
            }, 11);
        } finally { session.close(); }
    }

    @Test
    public void meta() throws Exception {
        MappedSchemaObject entity = persistenceManager.getEntity(MetaEntity.class);
        createSchemaDo(Collections.<Class<?>>singletonList(MetaEntity.class));

        Assert.assertEquals(14, entity.getAttributeNames().size());
        Assert.assertEquals(14, entity.getAllColumns().length);
        Assert.assertEquals(14, entity.getReadColumns().length);
        Assert.assertEquals(8, entity.getWriteColumns().length);
        Assert.assertEquals(13, entity.getReadDataColumns().length);

        PersistenceSession session = persistenceManager.createSession();
        try {
            MetaEntity inst = new MetaEntity();
            inst.setId(11);
            inst.setStr("some str");
            inst.setIntObj(1701);
            inst.setIntPrim(42);
            session.insert(inst);

            MetaEntity loaded = session.loadOne(MetaEntity.class, 11);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 11);
            Assert.assertEquals(loaded.getStr(), "some str");
            Assert.assertEquals(loaded.getIntObj(), Integer.valueOf(1701));
            Assert.assertEquals(loaded.getIntPrim(), 42);

            Date intObjWritetime = loaded.getIntObjWritetime();
            Date strWritetime = loaded.getStrWritetime();
            Date someEnum1Writetime = loaded.getSomeEnum1Writetime();

            Assert.assertNull(loaded.getIntObjTTL());
            Assert.assertNotNull(intObjWritetime);
            Assert.assertNull(loaded.getStrTTL());
            Assert.assertNotNull(strWritetime);
            Assert.assertNull(loaded.getSomeEnum1TTL());
            Assert.assertNull(someEnum1Writetime);

            Thread.sleep(100L);

            inst.setStr("other");
            inst.setIntObj(1702);
            inst.setIntPrim(43);
            session.insert(inst);

            loaded = session.loadOne(MetaEntity.class, 11);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 11);
            Assert.assertEquals(loaded.getStr(), "other");
            Assert.assertEquals(loaded.getIntObj(), Integer.valueOf(1702));
            Assert.assertEquals(loaded.getIntPrim(), 43);

            final Date intObjWritetime2 = loaded.getIntObjWritetime();
            final Date strWritetime2 = loaded.getStrWritetime();
            Date someEnum1Writetime2 = loaded.getSomeEnum1Writetime();

            Assert.assertNull(loaded.getIntObjTTL());
            Assert.assertNotNull(intObjWritetime2);
            Assert.assertNull(loaded.getStrTTL());
            Assert.assertNotNull(strWritetime2);
            Assert.assertNull(loaded.getSomeEnum1TTL());
            Assert.assertNull(someEnum1Writetime2);

            Assert.assertTrue(intObjWritetime2.getTime() > intObjWritetime.getTime());
            Assert.assertTrue(strWritetime2.getTime() > strWritetime.getTime());

            Thread.sleep(100L);

            insertUpdateDelete(session, MetaEntity.class, inst, new UpdateCallback<MetaEntity>() {
                @Override public void modify(MetaEntity inst) {
                    inst.setStr("other");
                    inst.setIntObj(1702);
                    inst.setIntPrim(43);
                }

                @Override public void check(MetaEntity inst) {

                    Date intObjWritetime3 = inst.getIntObjWritetime();
                    Date strWritetime3 = inst.getStrWritetime();
                    Date someEnum1Writetime3 = inst.getSomeEnum1Writetime();

                    Assert.assertNull(inst.getIntObjTTL());
                    Assert.assertNotNull(intObjWritetime3);
                    Assert.assertNull(inst.getStrTTL());
                    Assert.assertNotNull(strWritetime3);
                    Assert.assertNull(inst.getSomeEnum1TTL());
                    Assert.assertNull(someEnum1Writetime3);

                    Assert.assertTrue(intObjWritetime3.getTime() > intObjWritetime2.getTime());
                    Assert.assertTrue(strWritetime3.getTime() > strWritetime2.getTime());
                }
            }, 11);
        } finally { session.close(); }
    }

    @Test
    public void clustKeyEntity() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(ClustKeyEntity.class);
        Assert.assertEquals(entity.getAttributeNames().size(), 5);
        createSchemaDo(Collections.<Class<?>>singletonList(ClustKeyEntity.class));

        UUID uuid = UUID.fromString("61979120-e116-11e3-8b68-0800200c9a66");
        Date date = new Date();

        ClustKeyEntity inst = new ClustKeyEntity();
        inst.setPk1(uuid);
        inst.setPk2("part key part");
        inst.setCk1(date);
        inst.setCk2("clustering part");
        inst.setValue(42);

        PersistenceSession session = persistenceManager.createSession();
        try {
            session.insert(inst);

            ClustKeyEntity loaded = session.loadOne(ClustKeyEntity.class, uuid, "part key part", date, "clustering part");

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getPk1(), uuid);
            Assert.assertEquals(loaded.getPk2(), "part key part");
            Assert.assertEquals(loaded.getCk1(), date);
            Assert.assertEquals(loaded.getCk2(), "clustering part");
            Assert.assertEquals(loaded.getValue(), 42);

            insertUpdateDelete(session, ClustKeyEntity.class, inst, new UpdateCallback<ClustKeyEntity>() {
                @Override public void modify(ClustKeyEntity inst) {
                    inst.setValue(422);
                }

                @Override public void check(ClustKeyEntity inst) {
                    Assert.assertEquals(inst.getValue(), 422);
                }
            }, uuid, "part key part", date, "clustering part");
        } finally { session.close(); }
    }

    @SuppressWarnings("unchecked") @Test
    public void missingColumns_and_completeColumns() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(IncompleteEntity.class);
        Assert.assertEquals(entity.getAttributeNames().size(), 9);
        String ddl = entity.getCreateDDL();

        // REMOVE SOME COLUMNS
        Pattern pattern = Pattern.compile("^  (int_prim|some_enum1|some_enum3|str) .+$", Pattern.MULTILINE);
        Matcher m = pattern.matcher(ddl);
        String ddlShort = m.replaceAll("");

        System.out.println(ddlShort);
        persistenceManager.driverSession().execute(ddlShort);

        waitEntityAvailable(entity);

        PersistenceSession session = persistenceManager.createSession();
        try {

            IncompleteEntity inst = new IncompleteEntity();
            inst.setId(11);
            inst.setIntObj(42);
            inst.setIntPrim(43);
            inst.setSomeEnum1(SomeEnum.ONE);
            inst.setSomeEnum2(SomeEnum.TWO);
            inst.setSomeEnum3(SomeEnum.THREE);
            inst.setStr("str");
            inst.setVal(44);

            session.insert(inst);

            IncompleteEntity loaded = session.loadOne(IncompleteEntity.class, 11);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 11);
            Assert.assertEquals(loaded.getIntObj(), Integer.valueOf(42));
            Assert.assertEquals(loaded.getIntPrim(), 0);
            Assert.assertNull(loaded.getSomeEnum1());
            Assert.assertSame(loaded.getSomeEnum2(), SomeEnum.TWO);
            Assert.assertNull(loaded.getSomeEnum3());
            Assert.assertNull(loaded.getSomeEnum4());
            Assert.assertNull(loaded.getStr());
            Assert.assertEquals(loaded.getVal(), 44);

            //

            CqlStatementList alterDDL = entity.getAlterDDL(cluster);
            System.out.println(alterDDL);

            persistenceManager.alterEntitySchemaObject(entity);

            waitColumnAvailable(entity.getColumnsByAttributePath("str"));

            //

            inst = new IncompleteEntity();
            inst.setId(11);
            inst.setIntObj(42);
            inst.setIntPrim(43);
            inst.setSomeEnum1(SomeEnum.ONE);
            inst.setSomeEnum2(SomeEnum.TWO);
            inst.setSomeEnum3(SomeEnum.THREE);
            inst.setStr("str");
            inst.setVal(44);

            session.insert(inst);

            loaded = session.loadOne(IncompleteEntity.class, 11);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 11);
            Assert.assertEquals(loaded.getIntObj(), Integer.valueOf(42));
            Assert.assertEquals(loaded.getIntPrim(), 43);
            Assert.assertSame(loaded.getSomeEnum1(), SomeEnum.ONE);
            Assert.assertSame(loaded.getSomeEnum2(), SomeEnum.TWO);
            Assert.assertSame(loaded.getSomeEnum3(), SomeEnum.THREE);
            Assert.assertNull(loaded.getSomeEnum4());
            Assert.assertEquals(loaded.getStr(), "str");
            Assert.assertEquals(loaded.getVal(), 44);
        } finally { session.close(); }
    }

    @Test(expectedExceptions = PersistenceRuntimeException.class)
    public void missingColumns_fail() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(IncompleteFailEntity.class);
        Assert.assertEquals(entity.getAttributeNames().size(), 9);
        String ddl = entity.getCreateDDL();

        // REMOVE SOME COLUMNS
        Pattern pattern = Pattern.compile("^  (int_prim|some_enum1|some_enum3|str) .+$", Pattern.MULTILINE);
        Matcher m = pattern.matcher(ddl);
        String ddlShort = m.replaceAll("");

        System.out.println(ddlShort);
        persistenceManager.driverSession().execute(ddlShort);

        waitEntityExists(entity);

        PersistenceSession session = persistenceManager.createSession();
        try {

            IncompleteFailEntity inst = new IncompleteFailEntity();
            inst.setId(11);
            inst.setIntObj(42);
            inst.setIntPrim(43);
            inst.setSomeEnum1(SomeEnum.ONE);
            inst.setSomeEnum2(SomeEnum.TWO);
            inst.setSomeEnum3(SomeEnum.THREE);
            inst.setStr("str");
            inst.setVal(44);

            session.insert(inst);
        } finally { session.close(); }
    }

    @Test
    public void mappedKeyspace() throws Exception {
        MappedSchemaObject entity = persistenceManager.getEntity(MappedKeyspaceNameEntity.class);
        Assert.assertEquals(entity.getCqlTable().getKeyspace(), "lookup_ks");
    }

    @Test(expectedExceptions = PersistenceRuntimeException.class)
    public void notExists_fail() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(NotExistsFailEntity.class);
        Assert.assertEquals(entity.getAttributeNames().size(), 9);
        String ddl = entity.getCreateDDL();

        // REMOVE SOME COLUMNS
        Pattern pattern = Pattern.compile("^  (int_prim|some_enum1|some_enum3|str) .+$", Pattern.MULTILINE);
        Matcher m = pattern.matcher(ddl);
        String ddlShort = m.replaceAll("");

        System.out.println(ddlShort);
        persistenceManager.driverSession().execute(ddlShort);

        waitEntityExists(entity);

        PersistenceSession session = persistenceManager.createSession();
        try {

            NotExistsFailEntity inst = new NotExistsFailEntity();
            inst.setId(11);
            inst.setIntObj(42);
            inst.setIntPrim(43);
            inst.setSomeEnum1(SomeEnum.ONE);
            inst.setSomeEnum2(SomeEnum.TWO);
            inst.setSomeEnum3(SomeEnum.THREE);
            inst.setStr("str");
            inst.setVal(44);

            session.insert(inst);
        } finally { session.close(); }
    }

    @Test(expectedExceptions = PersistenceRuntimeException.class)
    public void notExists_ignore() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(NotExistsEntity.class);
        Assert.assertEquals(entity.getAttributeNames().size(), 9);
        String ddl = entity.getCreateDDL();

        // REMOVE SOME COLUMNS
        Pattern pattern = Pattern.compile("^  (int_prim|some_enum1|some_enum3|str) .+$", Pattern.MULTILINE);
        Matcher m = pattern.matcher(ddl);
        String ddlShort = m.replaceAll("");

        System.out.println(ddlShort);
        persistenceManager.driverSession().execute(ddlShort);

        waitEntityExists(entity);

        PersistenceSession session = persistenceManager.createSession();
        try {

            NotExistsEntity inst = new NotExistsEntity();
            inst.setId(11);
            inst.setIntObj(42);
            inst.setIntPrim(43);
            inst.setSomeEnum1(SomeEnum.ONE);
            inst.setSomeEnum2(SomeEnum.TWO);
            inst.setSomeEnum3(SomeEnum.THREE);
            inst.setStr("str");
            inst.setVal(44);

            session.insert(inst);

            NotExistsEntity loaded = session.loadOne(NotExistsEntity.class, 11);

            Assert.assertNull(loaded);
        } finally { session.close(); }
    }

    @Test
    public void simpleEntity() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(SimpleEntity.class);
        Assert.assertEquals(entity.getAttributeNames().size(), 8);
        createSchemaDo(Collections.<Class<?>>singletonList(SimpleEntity.class));

        SimpleEntity inst = new SimpleEntity();
        inst.setId(11);
        inst.setSomeEnum1(SomeEnum.ONE);
        inst.setSomeEnum2(SomeEnum.TWO);
        inst.setSomeEnum3(SomeEnum.THREE);

        PersistenceSession session = persistenceManager.createSession();
        try {
            session.insert(inst);

            SimpleEntity loaded = session.loadOne(SimpleEntity.class, 11);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 11);
            Assert.assertSame(loaded.getSomeEnum1(), SomeEnum.ONE);
            Assert.assertSame(loaded.getSomeEnum2(), SomeEnum.TWO);
            Assert.assertSame(loaded.getSomeEnum3(), SomeEnum.THREE);
            Assert.assertNull(loaded.getSomeEnum4());
            Assert.assertNull(loaded.getStr());
            Assert.assertNull(loaded.getIntObj());
            Assert.assertEquals(loaded.getIntPrim(), 0);

            insertUpdateDelete(session, SimpleEntity.class, inst, new UpdateCallback<SimpleEntity>() {
                @Override public void modify(SimpleEntity inst) {
                    inst.setStr("422");
                }

                @Override public void check(SimpleEntity inst) {
                    Assert.assertEquals(inst.getStr(), "422");
                }
            }, 11);
        } finally { session.close(); }
    }

    @Test(invocationCount = 1, invocationTimeOut = 1000L)
    public void generateSchema() throws Exception {
        SchemaGenerator schemaGenerator = persistenceManager.createSchemaGenerator();
        System.out.println();
        System.out.println("-- CREATE DDL statements");
        System.out.println();
        System.out.println(schemaGenerator.generateOfflineCreateDDL());
        System.out.println();
        System.out.println("-- DROP DDL statements");
        System.out.println();
        System.out.println(schemaGenerator.generateOfflineDropDDL());
        System.out.println();
        System.out.println("-- ALTER DDL statements");
        System.out.println();
        System.out.println(schemaGenerator.generateLiveAlterDDL());
    }

    @SuppressWarnings("BusyWait") protected final void waitEntityExists(MappedSchemaObject entity) throws InterruptedException {
        persistenceManager.refreshSchema();
        for (int i = 0; i < 200; i++) {
            if (entity.getCqlTable().isExists()) {
                return;
            }
            Thread.sleep(10L);
        }
    }

    @SuppressWarnings("BusyWait") protected final void waitColumnAvailable(List<CqlColumn> columns) throws InterruptedException {
        persistenceManager.refreshSchema();
        for (int i = 0; i < 200; i++) {
            boolean any = false;
            for (CqlColumn column : columns) {
                if (!column.isExists()) {
                    any = true;
                }
            }
            if (!any) {
                // note: give some time to prepare statements...
                Thread.sleep(250);
                return;
            }
            Thread.sleep(10L);
        }
    }

}
