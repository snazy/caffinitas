/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.mapentity;

import com.datastax.driver.core.VersionNumber;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class MapEntityTest extends CassandraTestBase {
    public static final String KEYSPACE_NAME = "mapentity";
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace(KEYSPACE_NAME);

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(MapEntityTest.class.getPackage().getName());
        model = modelScanner.scan();

        Assert.assertTrue(!model.getEntityClasses().isEmpty() || !model.getMapEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterClass
    public static void teardown() {
        persistenceManager.close();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        if (VersionNumber.parse("2.0.0").compareTo(VersionNumber.parse(configuredCassandraVersion)) >= 0) {
            throw new SkipException("options test not applicable for Apache Cassandra version " + configuredCassandraVersion);
        }
    }

    @AfterMethod
    public void flush() throws InterruptedException {
        System.out.flush();
        System.err.flush();
        Thread.sleep(500);
    }

    @Test
    public void createSchema() {
        createSchemaDo(Arrays.<Class<?>>asList(MapOneEntity.class, MapSimpleEntity.class, MapSimpleKeyEntity.class, MapSimpleValueEntity.class));
    }

    @Test(dependsOnMethods = "createSchema")
    public void mapEntity_one() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            MapOneEntity inst = new MapOneEntity();
            inst.setPkInt(11);
            inst.setPkText("pk");
            MapOneKey k1 = new MapOneKey();
            k1.setCkAscii("key");
            k1.setCkLong(1L);
            MapOneValue v = new MapOneValue();
            v.setStr("hello world");
            inst.put(k1, v);
            MapOneKey k2 = new MapOneKey();
            k2.setCkAscii("dog");
            k2.setCkLong(1L);
            v = new MapOneValue();
            v.setStr("Ursus");
            inst.put(k2, v);
            session.insert(inst);

            MapOneEntity loaded = session.loadOne(MapOneEntity.class, 11, "pk");
            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getPkInt(), 11);
            Assert.assertEquals(loaded.getPkText(), "pk");
            Assert.assertEquals(loaded.size(), 2);
            v = loaded.get(k1);
            Assert.assertNotNull(v);
            Assert.assertEquals(v.getStr(), "hello world");
            v = loaded.get(k2);
            Assert.assertNotNull(v);
            Assert.assertEquals(v.getStr(), "Ursus");
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void mapEntity_simple() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            MapSimpleEntity inst = new MapSimpleEntity();
            inst.setPkInt(11);
            inst.setPkText("pk");
            inst.put("key", ByteBuffer.wrap("hello world".getBytes()));
            inst.put("dog", ByteBuffer.wrap("Ursus".getBytes()));
            session.insert(inst);

            MapSimpleEntity loaded = session.loadOne(MapSimpleEntity.class, 11, "pk");
            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getPkInt(), 11);
            Assert.assertEquals(loaded.getPkText(), "pk");
            Assert.assertEquals(loaded.size(), 2);
            ByteBuffer v = loaded.get("key");
            Assert.assertNotNull(v);
            Assert.assertEquals(v, ByteBuffer.wrap("hello world".getBytes()));
            v = loaded.get("dog");
            Assert.assertNotNull(v);
            Assert.assertEquals(v, ByteBuffer.wrap("Ursus".getBytes()));
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void mapEntity_simpleKey() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            MapSimpleKeyEntity inst = new MapSimpleKeyEntity();
            inst.setPkInt(11);
            inst.setPkText("pk");
            MapSimpleKeyValue v = new MapSimpleKeyValue();
            v.setValue("hello world");
            inst.put("key", v);
            v = new MapSimpleKeyValue();
            v.setValue("Ursus");
            inst.put("dog", v);
            session.insert(inst);

            MapSimpleKeyEntity loaded = session.loadOne(MapSimpleKeyEntity.class, 11, "pk");
            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getPkInt(), 11);
            Assert.assertEquals(loaded.getPkText(), "pk");
            Assert.assertEquals(loaded.size(), 2);
            v = loaded.get("key");
            Assert.assertNotNull(v);
            Assert.assertEquals(v.getValue(), "hello world");
            v = loaded.get("dog");
            Assert.assertNotNull(v);
            Assert.assertEquals(v.getValue(), "Ursus");
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void mapEntity_simpleValue() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            MapSimpleValueEntity inst = new MapSimpleValueEntity();
            inst.setPkInt(11);
            inst.setPkText("pk");
            MapSimpleValueKey k1 = new MapSimpleValueKey();
            k1.setCkAscii("key");
            k1.setCkLong(0L);
            inst.put(k1, "hello world");
            MapSimpleValueKey k2 = new MapSimpleValueKey();
            k2.setCkAscii("dog");
            k2.setCkLong(0L);
            inst.put(k2, "Ursus");
            session.insert(inst);

            MapSimpleValueEntity loaded = session.loadOne(MapSimpleValueEntity.class, 11, "pk");
            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getPkInt(), 11);
            Assert.assertEquals(loaded.getPkText(), "pk");
            Assert.assertEquals(loaded.size(), 2);
            Assert.assertEquals(loaded.get(k1), "hello world");
            Assert.assertEquals(loaded.get(k2), "Ursus");
        } finally {session.close();}
    }
}
