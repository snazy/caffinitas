/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.mapentity;

import org.caffinitas.mapper.annotations.CColumn;

public class MapOneKey {
    @CColumn private String ckAscii;
    @CColumn private long ckLong;

    public String getCkAscii() {
        return ckAscii;
    }

    public void setCkAscii(String ckAscii) {
        this.ckAscii = ckAscii;
    }

    public long getCkLong() {
        return ckLong;
    }

    public void setCkLong(long ckLong) {
        this.ckLong = ckLong;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MapOneKey mapOneKey = (MapOneKey) o;

        if (ckLong != mapOneKey.ckLong) {
            return false;
        }
        if (ckAscii != null ? !ckAscii.equals(mapOneKey.ckAscii) : mapOneKey.ckAscii != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = ckAscii != null ? ckAscii.hashCode() : 0;
        result = 31 * result + (int) (ckLong ^ (ckLong >>> 32));
        return result;
    }
}
