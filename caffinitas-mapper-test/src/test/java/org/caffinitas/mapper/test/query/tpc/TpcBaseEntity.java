/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.query.tpc;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CDiscriminatorColumn;
import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.annotations.CInheritance;
import org.caffinitas.mapper.annotations.CNamedQuery;
import org.caffinitas.mapper.annotations.InheritanceType;
import org.caffinitas.mapper.test.model.Callbacks;

@CEntity(partitionKey = "id", clusteringKey = "cid",
    namedQueries = {
        @CNamedQuery(name = "byIds", condition = "id in :id"),
        @CNamedQuery(name = "byOther", condition = "id = :id1")
    })
@CInheritance(type = InheritanceType.TABLE_PER_CLASS)
@CDiscriminatorColumn(column = @CColumn)
public class TpcBaseEntity extends Callbacks {
    @CColumn private int id;
    @CColumn private int cid;
    @CColumn private String val;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
