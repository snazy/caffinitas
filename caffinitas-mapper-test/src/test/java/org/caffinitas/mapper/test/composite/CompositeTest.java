package org.caffinitas.mapper.test.composite;

import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

public class CompositeTest extends CassandraTestBase {
    public static final String KEYSPACE_NAME = "compo";
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace(KEYSPACE_NAME);

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(CompositeTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() throws InterruptedException {
        persistenceManager.close();

        System.out.flush();
        System.err.flush();
        Thread.sleep(500);
    }

    @Test
    public void createSchema() {
        createSchemaDo(Collections.<Class<?>>singletonList(CompEntity.class));
    }

    @Test(dependsOnMethods = "createSchema")
    public void composite_insertAndLoad() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            CompEntity inst = new CompEntity();
            inst.setId(11);
            CompComposite comp = new CompComposite();
            comp.setStr("string");
            comp.setNum(42);
            inst.setComp(comp);
            session.insert(inst);

            CompEntity loaded = session.loadOne(CompEntity.class, 11);
            Assert.assertNotNull(loaded);
            Assert.assertNotNull(loaded.getComp());
            Assert.assertEquals(loaded.getComp().getNum(), 42);
            Assert.assertEquals(loaded.getComp().getStr(), "string");
        } finally {session.close();}
    }
}
