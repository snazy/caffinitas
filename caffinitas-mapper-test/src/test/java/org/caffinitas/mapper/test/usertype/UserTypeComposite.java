/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.usertype;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CComposite;
import org.caffinitas.mapper.annotations.CompositeType;
import org.caffinitas.mapper.test.model.Callbacks;

@CComposite(compositeType = CompositeType.USER_TYPE)
public class UserTypeComposite extends Callbacks {
    @CColumn private int vint;

    @CColumn private String str;

    @CColumn private NextUserTypeComposite next;

    public int getVint() {
        return vint;
    }

    public void setVint(int vint) {
        this.vint = vint;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public NextUserTypeComposite getNext() {
        return next;
    }

    public void setNext(NextUserTypeComposite next) {
        this.next = next;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserTypeComposite that = (UserTypeComposite) o;

        if (vint != that.vint) {
            return false;
        }
        if (next != null ? !next.equals(that.next) : that.next != null) {
            return false;
        }
        if (str != null ? !str.equals(that.str) : that.str != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = vint;
        result = 31 * result + (str != null ? str.hashCode() : 0);
        result = 31 * result + (next != null ? next.hashCode() : 0);
        return result;
    }
}
