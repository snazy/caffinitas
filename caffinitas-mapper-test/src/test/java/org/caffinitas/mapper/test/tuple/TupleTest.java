package org.caffinitas.mapper.test.tuple;

import com.datastax.driver.core.VersionNumber;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

public class TupleTest extends CassandraTestBase {
    public static final String KEYSPACE_NAME = "tuplet";
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        if (VersionNumber.parse("2.1.0-rc1").compareTo(VersionNumber.parse(configuredCassandraVersion)) >= 0) {
            throw new SkipException("options test not applicable for cassandra version " + configuredCassandraVersion);
        }

        recreateKeyspace(KEYSPACE_NAME);

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(TupleTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() throws InterruptedException {
        persistenceManager.close();

        System.out.flush();
        System.err.flush();
        Thread.sleep(500);
    }

    @Test
    public void createSchema() {
        createSchemaDo(Collections.<Class<?>>singletonList(TupleEntity.class));
    }

    @Test(dependsOnMethods = "createSchema")
    public void composite_insertAndLoad() throws Exception {

        PersistenceSession session = persistenceManager.createSession();
        try {
            TupleEntity inst = new TupleEntity();
            inst.setId(11);
            TupleComposite comp = new TupleComposite();
            comp.setStr("string");
            comp.setNum(42);
            inst.setComp(comp);
            session.insert(inst);

            TupleEntity loaded = session.loadOne(TupleEntity.class, 11);
            Assert.assertNotNull(loaded);
            Assert.assertNotNull(loaded.getComp());
            Assert.assertEquals(loaded.getComp().getNum(), 42);
            Assert.assertEquals(loaded.getComp().getStr(), "string");
        } finally {session.close();}
    }
}
