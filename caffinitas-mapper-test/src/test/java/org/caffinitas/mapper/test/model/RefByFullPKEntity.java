/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.model;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CEntity;

import java.util.Date;

@CEntity(partitionKey = {"pk1", "pk2"}, clusteringKey = {"ck1", "ck2", "ck3"})
public class RefByFullPKEntity {
    @CColumn private String pk1;
    @CColumn private int pk2;
    @CColumn private int ck1;
    @CColumn private String ck2;
    @CColumn private Date ck3;

    @CColumn private String value;

    @CColumn private int num;

    public String getPk1() {
        return pk1;
    }

    public void setPk1(String pk1) {
        this.pk1 = pk1;
    }

    public int getPk2() {
        return pk2;
    }

    public void setPk2(int pk2) {
        this.pk2 = pk2;
    }

    public int getCk1() {
        return ck1;
    }

    public void setCk1(int ck1) {
        this.ck1 = ck1;
    }

    public String getCk2() {
        return ck2;
    }

    public void setCk2(String ck2) {
        this.ck2 = ck2;
    }

    public Date getCk3() {
        return ck3;
    }

    public void setCk3(Date ck3) {
        this.ck3 = ck3;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
