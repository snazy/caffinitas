/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.attrover;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class AttrOverrideTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("attrover");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(AttrOverrideTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() {
        persistenceManager.close();
    }

    @Test
    public void createSchema() throws Exception {
        createSchemaDo(Arrays.<Class<?>>asList(ReferencingEntity.class, ReferencedEntity.class));
    }

    @Test(dependsOnMethods = "createSchema")
    public void ref_schema() throws Exception {
        MappedSchemaObject<?> so = persistenceManager.getEntity(ReferencingEntity.class);
        List<CqlColumn> cols;
        cols = so.getColumnsByAttributePath("id");
        Assert.assertEquals(cols.size(), 1);
        Assert.assertEquals(cols.get(0).getName(), "id");
        cols = so.getColumnsByAttributePath("val");
        Assert.assertEquals(cols.size(), 1);
        Assert.assertEquals(cols.get(0).getName(), "val");

        cols = so.getColumnsByAttributePath("referencedEntity.id");
        Assert.assertEquals(cols.size(), 1);
        Assert.assertEquals(cols.get(0).getName(), "r_id");
        cols = so.getColumnsByAttributePath("referencedEntity.val");
        Assert.assertEquals(cols.size(), 1);
        Assert.assertEquals(cols.get(0).getName(), "r_val");

        cols = so.getColumnsByAttributePath("someComposite.vint");
        Assert.assertEquals(cols.get(0).getName(), "some_composite_vint");
        Assert.assertEquals(cols.size(), 1);
        cols = so.getColumnsByAttributePath("someComposite.str");
        Assert.assertEquals(cols.get(0).getName(), "some_composite_str");
        Assert.assertEquals(cols.size(), 1);
        cols = so.getColumnsByAttributePath("someComposite.next.valA");
        Assert.assertEquals(cols.size(), 1);
        Assert.assertEquals(cols.get(0).getName(), "next_a");
        cols = so.getColumnsByAttributePath("someComposite.next.ts");
        Assert.assertEquals(cols.size(), 1);
        Assert.assertEquals(cols.get(0).getName(), "some_composite_next_ts");

        cols = so.getColumnsByAttributePath("compB.vint");
        Assert.assertEquals(cols.size(), 1);
        Assert.assertEquals(cols.get(0).getName(), "vi");
        cols = so.getColumnsByAttributePath("compB.str");
        Assert.assertEquals(cols.size(), 1);
        Assert.assertEquals(cols.get(0).getName(), "comp_b_str");
        cols = so.getColumnsByAttributePath("compB.next.valA");
        Assert.assertEquals(cols.size(), 1);
        Assert.assertEquals(cols.get(0).getName(), "comp_b_next_val_a");
        cols = so.getColumnsByAttributePath("compB.next.ts");
        Assert.assertEquals(cols.size(), 1);
        Assert.assertEquals(cols.get(0).getName(), "nt");
    }

    @Test(dependsOnMethods = "createSchema")
    public void ref_simple() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            ReferencedEntity inst = new ReferencedEntity();
            inst.setId(1);
            inst.setVal("1");
            session.insert(inst);

            ReferencedEntity loaded = session.loadOne(ReferencedEntity.class, 1);
            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getVal(), "1");

            NextFlatComposite compNext = new NextFlatComposite();
            compNext.setTs(new Date());
            compNext.setValA(.42d);

            SomeComposite comp = new SomeComposite();
            comp.setStr("cstr");
            comp.setVint(42);
            comp.setNext(compNext);

            SomeComposite compB = new SomeComposite();
            compB.setStr("zyx");
            compB.setVint(88);
            compB.setNext(compNext);

            ReferencingEntity ref = new ReferencingEntity();
            ref.setId(2);
            ref.setVal("v2");
            ref.setReferencedEntity(loaded);
            ref.setSomeComposite(comp);
            ref.setCompB(compB);

            session.insert(ref);

            ReferencingEntity l = session.loadOne(ReferencingEntity.class, 2);
            Assert.assertEquals(l.getId(), 2);
            Assert.assertEquals(l.getVal(), "v2");
            Assert.assertNotNull(l.getReferencedEntity());
            Assert.assertEquals(l.getReferencedEntity().getId(), 1);
            Assert.assertEquals(l.getReferencedEntity().getVal(), "1");
            Assert.assertNotNull(l.getSomeComposite());
            Assert.assertEquals(l.getSomeComposite().getStr(), "cstr");
            Assert.assertEquals(l.getSomeComposite().getVint(), 42);
            Assert.assertNotNull(l.getSomeComposite().getNext());
            Assert.assertEquals(l.getSomeComposite().getNext().getTs(), compNext.getTs());
            Assert.assertEquals(l.getSomeComposite().getNext().getValA(), .42d);
            Assert.assertNotNull(l.getCompB());
            Assert.assertEquals(l.getCompB().getStr(), "zyx");
            Assert.assertEquals(l.getCompB().getVint(), 88);
            Assert.assertNotNull(l.getCompB().getNext());
            Assert.assertEquals(l.getCompB().getNext().getTs(), compNext.getTs());
            Assert.assertEquals(l.getCompB().getNext().getValA(), .42d);

        } finally {
            session.close();
        }
    }

}
