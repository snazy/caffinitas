/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.model;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.annotations.CReference;

@CEntity(partitionKey = "id")
public class ReferenceEntity {
    @CColumn private int id;

    @CReference private RefBySimplePKEntity otherSimple;

    @CReference private RefByFullPKEntity otherFull;

    @CColumn private String str;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RefBySimplePKEntity getOtherSimple() {
        return otherSimple;
    }

    public void setOtherSimple(RefBySimplePKEntity otherSimple) {
        this.otherSimple = otherSimple;
    }

    public RefByFullPKEntity getOtherFull() {
        return otherFull;
    }

    public void setOtherFull(RefByFullPKEntity otherFull) {
        this.otherFull = otherFull;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
