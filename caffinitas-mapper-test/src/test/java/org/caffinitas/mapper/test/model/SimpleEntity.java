/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.model;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.annotations.DataTypeName;

@CEntity(partitionKey = {"id"})
public class SimpleEntity extends Callbacks {
    @CColumn
    private int id;

    @CColumn
    private SomeEnum someEnum1;

    @CColumn(type = DataTypeName.ENUM_AS_NAME)
    private SomeEnum someEnum2;

    @CColumn(type = DataTypeName.ENUM_AS_ORDINAL)
    private SomeEnum someEnum3;

    @CColumn
    private SomeEnum someEnum4;

    @CColumn
    private String str;

    @CColumn
    private Integer intObj;

    @CColumn
    private int intPrim;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SomeEnum getSomeEnum1() {
        return someEnum1;
    }

    public void setSomeEnum1(SomeEnum someEnum1) {
        this.someEnum1 = someEnum1;
    }

    public SomeEnum getSomeEnum2() {
        return someEnum2;
    }

    public void setSomeEnum2(SomeEnum someEnum2) {
        this.someEnum2 = someEnum2;
    }

    public SomeEnum getSomeEnum3() {
        return someEnum3;
    }

    public void setSomeEnum3(SomeEnum someEnum3) {
        this.someEnum3 = someEnum3;
    }

    public SomeEnum getSomeEnum4() {
        return someEnum4;
    }

    public void setSomeEnum4(SomeEnum someEnum4) {
        this.someEnum4 = someEnum4;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public Integer getIntObj() {
        return intObj;
    }

    public void setIntObj(Integer intObj) {
        this.intObj = intObj;
    }

    public int getIntPrim() {
        return intPrim;
    }

    public void setIntPrim(int intPrim) {
        this.intPrim = intPrim;
    }
}
