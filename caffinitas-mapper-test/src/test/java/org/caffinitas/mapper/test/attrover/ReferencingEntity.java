/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.attrover;

import org.caffinitas.mapper.annotations.CAttributeOverride;
import org.caffinitas.mapper.annotations.CAttributeOverrides;
import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CDenormalized;
import org.caffinitas.mapper.annotations.CEntity;

@CEntity(partitionKey = "id")
public class ReferencingEntity {
    @CColumn private int id;
    @CColumn private String val;

    @CAttributeOverrides(
        {
            @CAttributeOverride(attributePath = "id", column = "r_id"),
            @CAttributeOverride(attributePath = "val", column = "r_val")
        }
    )
    @CDenormalized private ReferencedEntity referencedEntity;

    @CAttributeOverride(attributePath = "next.valA", column = "next_a")
    @CColumn private SomeComposite someComposite;

    @CAttributeOverrides(
        {
            @CAttributeOverride(attributePath = "vint", column = "vi"),
            @CAttributeOverride(attributePath = "next.ts", column = "nt")
        }
    )
    @CColumn private SomeComposite compB;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public ReferencedEntity getReferencedEntity() {
        return referencedEntity;
    }

    public void setReferencedEntity(ReferencedEntity referencedEntity) {
        this.referencedEntity = referencedEntity;
    }

    public SomeComposite getSomeComposite() {
        return someComposite;
    }

    public void setSomeComposite(SomeComposite someComposite) {
        this.someComposite = someComposite;
    }

    public SomeComposite getCompB() {
        return compB;
    }

    public void setCompB(SomeComposite compB) {
        this.compB = compB;
    }
}
