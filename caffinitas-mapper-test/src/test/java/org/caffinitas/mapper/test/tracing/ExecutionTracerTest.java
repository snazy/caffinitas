/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.tracing;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.VersionNumber;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.core.tracing.AbstractDelegateExecutionTracer;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.caffinitas.mapper.core.tracing.ExecutionTracerFactory;
import org.caffinitas.mapper.core.tracing.LoggingExecutionTracer;
import org.caffinitas.mapper.core.tracing.SingletonExecutionTracerFactory;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;

public class ExecutionTracerTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("tracing");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(ExecutionTracerTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        if (VersionNumber.parse("2.0.0").compareTo(VersionNumber.parse(configuredCassandraVersion)) >= 0) {
            throw new SkipException("execution tracer test not applicable for Apache Cassandra version " + configuredCassandraVersion);
        }
    }

    @AfterMethod
    public void closePersistenceManager() {
        persistenceManager.close();
    }

    @Test
    public void createSchema() throws Exception {
        persistenceManager = withPersistenceManagerBuilder(model).build();

        createSchemaDo(Collections.<Class<?>>singletonList(SimpleEntity.class));
    }

    @Test(dependsOnMethods = "createSchema")
    public void tablePerClass_root_sameCondition() throws Exception {
        final AtomicBoolean beginModify = new AtomicBoolean();
        final AtomicBoolean beginQuery = new AtomicBoolean();
        final AtomicBoolean readResultSetBegin = new AtomicBoolean();
        final AtomicBoolean readResultSetRow = new AtomicBoolean();
        final AtomicBoolean readResultSetEnd = new AtomicBoolean();
        final AtomicBoolean modifyWrapResultSet = new AtomicBoolean();

        final ExecutionTracer loggingTracer = new LoggingExecutionTracer(ExecutionTracerTest.class, true);

        ExecutionTracer executionTracer = new AbstractDelegateExecutionTracer() {
            @Override protected ExecutionTracer getDelegate() {
                return loggingTracer;
            }

            @Override public void onBeginModify(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, PersistMode mode,
                                                Statement statement) {
                beginModify.set(true);
                super.onBeginModify(persistenceSession, entity, mode, statement);
            }

            @Override public void onBeginQuery(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, Statement statement) {
                beginQuery.set(true);
                super.onBeginQuery(persistenceSession, entity, statement);
            }

            @Override public void onReadResultSetBegin(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet,
                                                       CqlColumn[] columns) {
                readResultSetBegin.set(true);
                super.onReadResultSetBegin(persistenceSession, entity, resultSet, columns);
            }

            @Override public void onReadResultSetRow(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet,
                                                     CqlColumn[] columns, Row row) {
                readResultSetRow.set(true);
                super.onReadResultSetRow(persistenceSession, entity, resultSet, columns, row);
            }

            @Override public void onReadResultSetEnd(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet,
                                                     CqlColumn[] columns) {
                readResultSetEnd.set(true);
                super.onReadResultSetEnd(persistenceSession, entity, resultSet, columns);
            }

            @Override public void onModifyWrapResultSet(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, PersistMode mode,
                                                        ResultSet resultSet) {
                modifyWrapResultSet.set(true);
                super.onModifyWrapResultSet(persistenceSession, entity, mode, resultSet);
            }
        };

        ExecutionTracerFactory executionTracerFactory = new SingletonExecutionTracerFactory(executionTracer);

        persistenceManager = withPersistenceManagerBuilder(model).withExecutionTracerFactory(executionTracerFactory).build();

        PersistenceSession session = persistenceManager.createSession();
        try {
            session.setTracing(true);

            SimpleEntity inst = new SimpleEntity();
            inst.setId(1);

            session.insert(inst);

            Assert.assertTrue(beginModify.get());
            Assert.assertFalse(beginQuery.get());
            Assert.assertFalse(readResultSetBegin.get());
            Assert.assertFalse(readResultSetRow.get());
            Assert.assertFalse(readResultSetEnd.get());
            Assert.assertTrue(modifyWrapResultSet.get());

            beginModify.set(false);
            modifyWrapResultSet.set(false);

            session.loadOne(SimpleEntity.class, 1);

            Assert.assertFalse(beginModify.get());
            Assert.assertTrue(beginQuery.get());
            Assert.assertTrue(readResultSetBegin.get());
            Assert.assertTrue(readResultSetRow.get());
            Assert.assertTrue(readResultSetEnd.get());
            Assert.assertFalse(modifyWrapResultSet.get());

        } finally {
            session.close();
        }
    }

}
