/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.usertype;

import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.UserType;
import com.datastax.driver.core.VersionNumber;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RealUserTypeTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("udttest");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(RealUserTypeTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        if (VersionNumber.parse("2.1.0-rc1").compareTo(VersionNumber.parse(configuredCassandraVersion)) >= 0) {
            throw new SkipException("user type test not applicable for cassandra version " + configuredCassandraVersion);
        }

        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() {
        persistenceManager.close();
    }

    @Test
    public void createSchema() throws Exception {
        persistenceManager.createSchemaGenerator().generateLiveAlterDDL().execute(persistenceManager.driverSession());
    }

    @Test(dependsOnMethods = "createSchema")
    public void userTypeCompEntity() throws Exception {

        MappedSchemaObject entity = persistenceManager.getEntity(UserTypeCompEntity.class);
        MappedSchemaObject composite = persistenceManager.getComposite(UserTypeComposite.class);
        MappedSchemaObject compositeNext = persistenceManager.getComposite(NextUserTypeComposite.class);
        createIfNecessary(entity, composite, compositeNext);
        Assert.assertEquals(entity.getAttributeNames().size(), 2);

        UserTypeCompEntity inst = new UserTypeCompEntity();
        inst.setId(11);
        UserTypeComposite utComposite = buildComposite("some string value", 42);
        Date date = utComposite.getNext().getTs();
        inst.setUserTypeComposite(utComposite);

        PersistenceSession session = persistenceManager.createSession();
        try {
            session.insert(inst);

            UserTypeCompEntity loaded = session.loadOne(UserTypeCompEntity.class, 11);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 11);

            utComposite = loaded.getUserTypeComposite();
            assertUT(utComposite, "some string value", 42);

            NextUserTypeComposite next = utComposite.getNext();
            Assert.assertNotNull(next);
            Assert.assertEquals(next.getValA(), 7.51973d, .01d);
            Assert.assertEquals(next.getTs(), date);
        } finally {session.close();}
    }

    @SuppressWarnings("unchecked") @Test(dependsOnMethods = "createSchema")
    public void userTypeComposites_list() throws Exception {
        MappedSchemaObject entity = persistenceManager.getEntity(UserTypeCompInListEntity.class);
        MappedSchemaObject composite = persistenceManager.getComposite(UserTypeComposite.class);
        MappedSchemaObject compositeNext = persistenceManager.getComposite(NextUserTypeComposite.class);
        createIfNecessary(entity, composite, compositeNext);

        Assert.assertNotNull(entity);

        Assert.assertNotNull(entity.getAttributeByPath("id"));
        Assert.assertNotNull(entity.getAttributeByPath("listCompA"));
        Assert.assertNotNull(entity.getAttributeByPath("compB"));
        Assert.assertNull(entity.getAttributeByPath("listCompA.innerA"));
        Assert.assertNull(entity.getAttributeByPath("listCompA.innerA.txtCompIA"));
        Assert.assertNull(entity.getAttributeByPath("listCompA.innerA.valCompIA"));

        Assert.assertEquals(entity.getAttributeNames().size(), 3);
        Assert.assertEquals(entity.getAllColumns().length, 3);

        PersistenceSession session = persistenceManager.createSession();
        try {
            UserTypeCompInListEntity inst = new UserTypeCompInListEntity();
            inst.setId(11);

            UserTypeComposite comp = buildComposite("some string value", 42);
            inst.setCompB(comp);

            inst.setListCompA(Arrays.asList(
                buildComposite("array#0", 0),
                buildComposite("array#1", 1),
                buildComposite("array#2", 2),
                buildComposite("array#3", 3),
                buildComposite("array#4", 4)
            ));

            session.insert(inst);

            UserTypeCompInListEntity loaded = session.loadOne(UserTypeCompInListEntity.class, 11);
            Assert.assertNotNull(loaded);
            comp = loaded.getCompB();
            assertUT(comp, "some string value", 42);

            List<UserTypeComposite> actual = inst.getListCompA();
            List<UserTypeComposite> expected = loaded.getListCompA();
            Assert.assertNotNull(actual);
            Assert.assertEquals(actual.size(), expected.size());
            Assert.assertEquals(actual, expected);
        } finally {session.close();}
    }

    @SuppressWarnings("unchecked") @Test(dependsOnMethods = "createSchema")
    public void userTypeComposites_set() throws Exception {
        MappedSchemaObject entity = persistenceManager.getEntity(UserTypeCompInSetEntity.class);
        MappedSchemaObject composite = persistenceManager.getComposite(UserTypeComposite.class);
        MappedSchemaObject compositeNext = persistenceManager.getComposite(NextUserTypeComposite.class);
        createIfNecessary(entity, composite, compositeNext);

        Assert.assertNotNull(entity);

        Assert.assertNotNull(entity.getAttributeByPath("id"));
        Assert.assertNotNull(entity.getAttributeByPath("setCompA"));
        Assert.assertNotNull(entity.getAttributeByPath("compB"));
        Assert.assertNull(entity.getAttributeByPath("setCompA.innerA"));
        Assert.assertNull(entity.getAttributeByPath("setCompA.innerA.txtCompIA"));
        Assert.assertNull(entity.getAttributeByPath("setCompA.innerA.valCompIA"));

        Assert.assertEquals(entity.getAttributeNames().size(), 3);
        Assert.assertEquals(entity.getAllColumns().length, 3);

        PersistenceSession session = persistenceManager.createSession();
        try {
            UserTypeCompInSetEntity inst = new UserTypeCompInSetEntity();
            inst.setId(11);

            UserTypeComposite comp = buildComposite("some string value", 42);
            inst.setCompB(comp);

            inst.setSetCompA(new HashSet<UserTypeComposite>(Arrays.asList(
                buildComposite("array#0", 0),
                buildComposite("array#1", 1),
                buildComposite("array#2", 2),
                buildComposite("array#3", 3),
                buildComposite("array#4", 4)
            )));

            session.insert(inst);

            UserTypeCompInSetEntity loaded = session.loadOne(UserTypeCompInSetEntity.class, 11);
            Assert.assertNotNull(loaded);
            comp = loaded.getCompB();
            assertUT(comp, "some string value", 42);

            Set<UserTypeComposite> actual = inst.getSetCompA();
            Set<UserTypeComposite> expected = loaded.getSetCompA();
            Assert.assertNotNull(actual);
            Assert.assertEquals(actual.size(), expected.size());
            Assert.assertEquals(actual, expected);
        } finally {session.close();}
    }

    @SuppressWarnings("unchecked") @Test(dependsOnMethods = "createSchema")
    public void userTypeComposites_mapKey() throws Exception {
        MappedSchemaObject entity = persistenceManager.getEntity(UserTypeCompInMapKeyEntity.class);
        MappedSchemaObject composite = persistenceManager.getComposite(UserTypeComposite.class);
        MappedSchemaObject compositeNext = persistenceManager.getComposite(NextUserTypeComposite.class);
        createIfNecessary(entity, composite, compositeNext);

        Assert.assertNotNull(entity);

        Assert.assertNotNull(entity.getAttributeByPath("id"));
        Assert.assertNotNull(entity.getAttributeByPath("mapCompA"));
        Assert.assertNotNull(entity.getAttributeByPath("compB"));
        Assert.assertNull(entity.getAttributeByPath("mapCompA.innerA"));
        Assert.assertNull(entity.getAttributeByPath("mapCompA.innerA.txtCompIA"));
        Assert.assertNull(entity.getAttributeByPath("mapCompA.innerA.valCompIA"));

        Assert.assertEquals(entity.getAttributeNames().size(), 3);
        Assert.assertEquals(entity.getAllColumns().length, 3);

        PersistenceSession session = persistenceManager.createSession();
        try {
            UserTypeCompInMapKeyEntity inst = new UserTypeCompInMapKeyEntity();
            inst.setId(11);

            UserTypeComposite comp = buildComposite("some string value", 42);
            inst.setCompB(comp);

            inst.setMapCompA(new HashMap<UserTypeComposite, String>());
            inst.getMapCompA().put(buildComposite("array#0", 0), "str#0");
            inst.getMapCompA().put(buildComposite("array#1", 1), "str#1");
            inst.getMapCompA().put(buildComposite("array#2", 2), "str#2");
            inst.getMapCompA().put(buildComposite("array#3", 3), "str#3");
            inst.getMapCompA().put(buildComposite("array#4", 4), "str#4");

            session.insert(inst);

            UserTypeCompInMapKeyEntity loaded = session.loadOne(UserTypeCompInMapKeyEntity.class, 11);
            Assert.assertNotNull(loaded);
            comp = loaded.getCompB();
            assertUT(comp, "some string value", 42);

            assertMapUT(inst.getMapCompA(), loaded.getMapCompA());
        } finally {session.close();}
    }

    @SuppressWarnings("unchecked") @Test(dependsOnMethods = "createSchema")
    public void userTypeComposites_mapValue() throws Exception {
        MappedSchemaObject entity = persistenceManager.getEntity(UserTypeCompInMapValueEntity.class);
        MappedSchemaObject composite = persistenceManager.getComposite(UserTypeComposite.class);
        MappedSchemaObject compositeNext = persistenceManager.getComposite(NextUserTypeComposite.class);
        createIfNecessary(entity, composite, compositeNext);

        Assert.assertNotNull(entity);

        Assert.assertNotNull(entity.getAttributeByPath("id"));
        Assert.assertNotNull(entity.getAttributeByPath("mapCompA"));
        Assert.assertNotNull(entity.getAttributeByPath("compB"));
        Assert.assertNull(entity.getAttributeByPath("mapCompA.innerA"));
        Assert.assertNull(entity.getAttributeByPath("mapCompA.innerA.txtCompIA"));
        Assert.assertNull(entity.getAttributeByPath("mapCompA.innerA.valCompIA"));

        Assert.assertEquals(entity.getAttributeNames().size(), 3);
        Assert.assertEquals(entity.getAllColumns().length, 3);

        PersistenceSession session = persistenceManager.createSession();
        try {
            UserTypeCompInMapValueEntity inst = new UserTypeCompInMapValueEntity();
            inst.setId(11);

            UserTypeComposite comp = buildComposite("some string value", 42);
            inst.setCompB(comp);

            inst.setMapCompA(new HashMap<String, UserTypeComposite>());
            inst.getMapCompA().put("str#0", buildComposite("array#0", 0));
            inst.getMapCompA().put("str#1", buildComposite("array#1", 1));
            inst.getMapCompA().put("str#2", buildComposite("array#2", 2));
            inst.getMapCompA().put("str#3", buildComposite("array#3", 3));
            inst.getMapCompA().put("str#4", buildComposite("array#4", 4));

            session.insert(inst);

            UserTypeCompInMapValueEntity loaded = session.loadOne(UserTypeCompInMapValueEntity.class, 11);
            Assert.assertNotNull(loaded);
            comp = loaded.getCompB();
            assertUT(comp, "some string value", 42);

            assertMapUT(inst.getMapCompA(), loaded.getMapCompA());
        } finally {session.close();}
    }

    @SuppressWarnings("unchecked") @Test(dependsOnMethods = "createSchema")
    public void userTypeComposites_map() throws Exception {
        MappedSchemaObject entity = persistenceManager.getEntity(UserTypeCompInMapEntity.class);
        MappedSchemaObject composite = persistenceManager.getComposite(UserTypeComposite.class);
        MappedSchemaObject compositeNext = persistenceManager.getComposite(NextUserTypeComposite.class);
        createIfNecessary(entity, composite, compositeNext);

        Assert.assertNotNull(entity);

        Assert.assertNotNull(entity.getAttributeByPath("id"));
        Assert.assertNotNull(entity.getAttributeByPath("mapCompA"));
        Assert.assertNotNull(entity.getAttributeByPath("compB"));
        Assert.assertNull(entity.getAttributeByPath("mapCompA.innerA"));
        Assert.assertNull(entity.getAttributeByPath("mapCompA.innerA.txtCompIA"));
        Assert.assertNull(entity.getAttributeByPath("mapCompA.innerA.valCompIA"));

        Assert.assertEquals(entity.getAttributeNames().size(), 3);
        Assert.assertEquals(entity.getAllColumns().length, 3);

        PersistenceSession session = persistenceManager.createSession();
        try {
            UserTypeCompInMapEntity inst = new UserTypeCompInMapEntity();
            inst.setId(11);

            UserTypeComposite comp = buildComposite("some string value", 42);
            inst.setCompB(comp);

            inst.setMapCompA(new HashMap<UserTypeComposite, UserTypeComposite>());
            inst.getMapCompA().put(buildComposite("key#0", 0), buildComposite("val#0", 0));
            inst.getMapCompA().put(buildComposite("key#1", 1), buildComposite("val#1", 1));
            inst.getMapCompA().put(buildComposite("key#2", 2), buildComposite("val#2", 2));
            inst.getMapCompA().put(buildComposite("key#3", 3), buildComposite("val#3", 3));
            inst.getMapCompA().put(buildComposite("key#4", 4), buildComposite("val#4", 4));

            session.insert(inst);

            UserTypeCompInMapEntity loaded = session.loadOne(UserTypeCompInMapEntity.class, 11);
            Assert.assertNotNull(loaded);
            comp = loaded.getCompB();
            assertUT(comp, "some string value", 42);

            assertMapUT(inst.getMapCompA(), loaded.getMapCompA());
        } finally {session.close();}
    }

    private void assertMapUT(Map<?, ?> expected, Map<?, ?> actual) {
        Assert.assertNotNull(actual);
        Assert.assertEquals(actual.size(), expected.size());
        Assert.assertEquals(actual, expected);
    }

    private void assertUT(UserTypeComposite comp, String str, int vint) {
        Assert.assertNotNull(comp);
        Assert.assertEquals(comp.getStr(), str);
        Assert.assertEquals(comp.getVint(), vint);
    }

    private UserTypeComposite buildComposite(String str, int vint) {
        UserTypeComposite comp = new UserTypeComposite();
        NextUserTypeComposite next = new NextUserTypeComposite();
        next.setTs(new Date());
        next.setValA(7.51973d);
        comp.setNext(next);
        comp.setStr(str);
        comp.setVint(vint);
        return comp;
    }

    @Test
    public void userTypeMeta() throws InterruptedException {
        Session session = cluster.connect();
        try {
            session.execute("CREATE TYPE udttest.phone ( cntry varchar, city varchar, num varchar )");
            session.execute("CREATE TYPE udttest.address ( street varchar, city varchar, country varchar, ph_num phone )");

            Thread.sleep(500L);

            KeyspaceMetadata ksm = cluster.getMetadata().getKeyspace("udttest");
            Collection<UserType> uts = ksm.getUserTypes();

            Assert.assertTrue(uts.size() >= 2);

            UserType utPhone = ksm.getUserType("phone");
            UserType utAddress = ksm.getUserType("address");

            Assert.assertNotNull(utPhone);
            Assert.assertNotNull(utAddress);
        } finally {session.close();}
    }
}
