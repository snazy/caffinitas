/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.usertype;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.annotations.CMap;

import java.util.Map;

@CEntity(partitionKey = "id")
public class UserTypeCompInMapEntity {
    @CColumn private int id;

    @CMap private Map<UserTypeComposite, UserTypeComposite> mapCompA;

    @CColumn private UserTypeComposite compB;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Map<UserTypeComposite, UserTypeComposite> getMapCompA() {
        return mapCompA;
    }

    public void setMapCompA(Map<UserTypeComposite, UserTypeComposite> mapCompA) {
        this.mapCompA = mapCompA;
    }

    public UserTypeComposite getCompB() {
        return compB;
    }

    public void setCompB(UserTypeComposite compB) {
        this.compB = compB;
    }
}
