/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.demo;

import com.datastax.driver.core.VersionNumber;
import org.caffinitas.mapper.demo.Demo;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemoTest extends CassandraTestBase {

    @BeforeClass
    public static void setup() {
        recreateKeyspace("demo");
    }

    @BeforeMethod
    public void checkForCassandraVersion() {
        if (VersionNumber.parse("2.1.0-rc1").compareTo(VersionNumber.parse(configuredCassandraVersion)) >= 0) {
            throw new SkipException("user type test not applicable for Apache Cassandra version " + configuredCassandraVersion);
        }
    }

    @Test
    public void demo() throws Exception {
        new Demo().runDemo(cluster);
    }
}
