package org.caffinitas.mapper.test.dynamic;

import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.DynamicComposite;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

public class DynamicCompositeTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("dyna");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(DynamicCompositeTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() throws InterruptedException {
        persistenceManager.close();

        System.out.flush();
        System.err.flush();
        Thread.sleep(500);
    }

    @Test
    public void createSchema() {
        createSchemaDo(Collections.<Class<?>>singletonList(DynamicCompEntity.class));
    }

    @Test(dependsOnMethods = "createSchema")
    public void dynamic_insertAndLoad() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            DynamicCompEntity inst = new DynamicCompEntity();
            inst.setId(11);
            DynamicComposite dyn = new DynamicComposite();
            dyn.addBool(true);
            dyn.addUtf8String("Ursus");
            dyn.addInt(42);
            inst.setDyn(dyn);
            session.insert(inst);

            DynamicCompEntity loaded = session.loadOne(DynamicCompEntity.class, 11);
            Assert.assertNotNull(loaded);
            dyn = loaded.getDyn();
            Assert.assertNotNull(dyn);
            Assert.assertEquals(dyn.size(), 3);
            Assert.assertEquals(dyn.getType(0), DataType.cboolean());
            Assert.assertEquals(dyn.getType(1), DataType.text());
            Assert.assertEquals(dyn.getType(2), DataType.cint());
            Assert.assertEquals(dyn.getBool(0), true);
            Assert.assertEquals(dyn.getString(1), "Ursus");
            Assert.assertEquals(dyn.getInt(2), 42);
        } finally {session.close();}
    }

}
