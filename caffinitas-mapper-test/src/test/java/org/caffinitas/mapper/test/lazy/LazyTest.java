/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.test.lazy;

import org.caffinitas.mapper.core.AbstractLazy;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.PersistOption;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.lazy.LazyList;
import org.caffinitas.mapper.core.lazy.LazyMap;
import org.caffinitas.mapper.core.lazy.LazySet;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.caffinitas.mapper.test.lazy.st.StBaseEntity;
import org.caffinitas.mapper.test.lazy.st.StInheritA;
import org.caffinitas.mapper.test.lazy.st.StInheritB;
import org.caffinitas.mapper.test.lazy.st.StInheritB2;
import org.caffinitas.mapper.test.lazy.st.StInheritC;
import org.caffinitas.mapper.test.lazy.tpc.TpcBaseEntity;
import org.caffinitas.mapper.test.lazy.tpc.TpcInheritA;
import org.caffinitas.mapper.test.lazy.tpc.TpcInheritB;
import org.caffinitas.mapper.test.lazy.tpc.TpcInheritB2;
import org.caffinitas.mapper.test.lazy.tpc.TpcInheritC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class LazyTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("junit");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(LazyTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() {
        persistenceManager.close();
    }

    @Test
    public void createSchema() throws Exception {
        createSchemaDo(Arrays.asList(LazySimpleEntity.class, LazyCompEntity.class,
            StBaseEntity.class, TpcBaseEntity.class, TpcInheritA.class, TpcInheritB.class, TpcInheritB2.class, TpcInheritC.class));
    }

    @Test(dependsOnMethods = "createSchema")
    public void lazySimple() throws Exception {

        PersistenceSession session = persistenceManager.createSession();
        try {

            LazySimpleEntity inst = new LazySimpleEntity();
            inst.setId(1);
            inst.setStrList(Arrays.asList("one", "two", "three"));
            inst.setLazyStrList(Arrays.asList("one", "two", "three"));
            inst.setStrSet(new HashSet<String>(inst.getStrList()));
            inst.setLazyStrSet(new HashSet<String>(inst.getStrList()));
            inst.getStrMap().put("one", "1");
            inst.getStrMap().put("two", "2");
            inst.getStrMap().put("three", "3");
            inst.getLazyStrMap().put("one", "1");
            inst.getLazyStrMap().put("two", "2");
            inst.getLazyStrMap().put("three", "3");

            session.insert(inst);

            LazySimpleEntity loaded = session.loadOneWithOptions(LazySimpleEntity.class, PersistOption.single(PersistOption.loadEager()), 1);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 1);
            Assert.assertNotNull(loaded.getStrList());
            Assert.assertNotNull(loaded.getStrSet());
            Assert.assertNotNull(loaded.getStrMap());
            Assert.assertNotNull(loaded.getLazyStrList());
            Assert.assertNotNull(loaded.getLazyStrSet());
            Assert.assertNotNull(loaded.getLazyStrMap());
            Assert.assertTrue(loaded.getStrList() instanceof ArrayList);
            Assert.assertTrue(loaded.getStrSet() instanceof HashSet);
            Assert.assertTrue(loaded.getStrMap() instanceof HashMap);
            Assert.assertFalse(loaded.getLazyStrList() instanceof LazyList);
            Assert.assertFalse(loaded.getLazyStrSet() instanceof LazySet);
            Assert.assertFalse(loaded.getLazyStrMap() instanceof LazyMap);
            Assert.assertEquals(loaded.getStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getLazyStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getLazyStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getLazyStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));

            loaded = session.loadOne(LazySimpleEntity.class, 1);

            session.update(loaded);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 1);
            Assert.assertNotNull(loaded.getStrList());
            Assert.assertNotNull(loaded.getStrSet());
            Assert.assertNotNull(loaded.getStrMap());
            Assert.assertNotNull(loaded.getLazyStrList());
            Assert.assertNotNull(loaded.getLazyStrSet());
            Assert.assertNotNull(loaded.getLazyStrMap());
            Assert.assertTrue(loaded.getStrList() instanceof ArrayList);
            Assert.assertTrue(loaded.getStrSet() instanceof HashSet);
            Assert.assertTrue(loaded.getStrMap() instanceof HashMap);
            Assert.assertTrue(loaded.getLazyStrList() instanceof LazyList);
            Assert.assertTrue(loaded.getLazyStrSet() instanceof LazySet);
            Assert.assertTrue(loaded.getLazyStrMap() instanceof LazyMap);
            Assert.assertFalse(((AbstractLazy) loaded.getLazyStrList()).isLoaded());
            Assert.assertFalse(((AbstractLazy) loaded.getLazyStrSet()).isLoaded());
            Assert.assertFalse(((AbstractLazy) loaded.getLazyStrMap()).isLoaded());
            Assert.assertEquals(loaded.getStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getLazyStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getLazyStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getLazyStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertFalse(loaded.getLazyStrList() instanceof LazyList);
            Assert.assertFalse(loaded.getLazyStrSet() instanceof LazySet);
            Assert.assertFalse(loaded.getLazyStrMap() instanceof LazyMap);

            loaded = session.loadOne(LazySimpleEntity.class, 1);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 1);
            Assert.assertNotNull(loaded.getStrList());
            Assert.assertNotNull(loaded.getStrSet());
            Assert.assertNotNull(loaded.getStrMap());
            Assert.assertNotNull(loaded.getLazyStrList());
            Assert.assertNotNull(loaded.getLazyStrSet());
            Assert.assertNotNull(loaded.getLazyStrMap());
            Assert.assertTrue(loaded.getStrList() instanceof ArrayList);
            Assert.assertTrue(loaded.getStrSet() instanceof HashSet);
            Assert.assertTrue(loaded.getStrMap() instanceof HashMap);
            Assert.assertTrue(loaded.getLazyStrList() instanceof LazyList);
            Assert.assertTrue(loaded.getLazyStrSet() instanceof LazySet);
            Assert.assertTrue(loaded.getLazyStrMap() instanceof LazyMap);
            Assert.assertFalse(((AbstractLazy) loaded.getLazyStrList()).isLoaded());
            Assert.assertFalse(((AbstractLazy) loaded.getLazyStrSet()).isLoaded());
            Assert.assertFalse(((AbstractLazy) loaded.getLazyStrMap()).isLoaded());
            session.loadLazy(loaded);
            Assert.assertFalse(loaded.getLazyStrList() instanceof LazyList);
            Assert.assertFalse(loaded.getLazyStrSet() instanceof LazySet);
            Assert.assertFalse(loaded.getLazyStrMap() instanceof LazyMap);
            Assert.assertEquals(loaded.getStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getLazyStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getLazyStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getLazyStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));

        }finally {session.close();}

    }

    @Test
    public void lazyComposite() throws Exception {

        PersistenceSession session = persistenceManager.createSession();
        try {

            LazyCompEntity inst = new LazyCompEntity();
            inst.setId(1);
            inst.setComposite(new LazyComposite());
            inst.getComposite().setStrList(Arrays.asList("one", "two", "three"));
            inst.getComposite().setLazyStrList(Arrays.asList("one", "two", "three"));
            inst.getComposite().setStrSet(new HashSet<String>(inst.getComposite().getStrList()));
            inst.getComposite().setLazyStrSet(new HashSet<String>(inst.getComposite().getStrList()));
            inst.getComposite().getStrMap().put("one", "1");
            inst.getComposite().getStrMap().put("two", "2");
            inst.getComposite().getStrMap().put("three", "3");
            inst.getComposite().getLazyStrMap().put("one", "1");
            inst.getComposite().getLazyStrMap().put("two", "2");
            inst.getComposite().getLazyStrMap().put("three", "3");

            session.insert(inst);

            LazyCompEntity loaded = session.loadOneWithOptions(LazyCompEntity.class, PersistOption.single(PersistOption.loadEager()), 1);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 1);
            Assert.assertNotNull(loaded.getComposite().getStrList());
            Assert.assertNotNull(loaded.getComposite().getStrSet());
            Assert.assertNotNull(loaded.getComposite().getStrMap());
            Assert.assertNotNull(loaded.getComposite().getLazyStrList());
            Assert.assertNotNull(loaded.getComposite().getLazyStrSet());
            Assert.assertNotNull(loaded.getComposite().getLazyStrMap());
            Assert.assertTrue(loaded.getComposite().getStrList() instanceof ArrayList);
            Assert.assertTrue(loaded.getComposite().getStrSet() instanceof HashSet);
            Assert.assertTrue(loaded.getComposite().getStrMap() instanceof HashMap);
            Assert.assertFalse(loaded.getComposite().getLazyStrList() instanceof LazyList);
            Assert.assertFalse(loaded.getComposite().getLazyStrSet() instanceof LazySet);
            Assert.assertFalse(loaded.getComposite().getLazyStrMap() instanceof LazyMap);
            Assert.assertEquals(loaded.getComposite().getStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getComposite().getLazyStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getComposite().getStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getComposite().getLazyStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getComposite().getStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getComposite().getLazyStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));

            loaded = session.loadOne(LazyCompEntity.class, 1);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 1);
            Assert.assertNotNull(loaded.getComposite().getStrList());
            Assert.assertNotNull(loaded.getComposite().getStrSet());
            Assert.assertNotNull(loaded.getComposite().getStrMap());
            Assert.assertNotNull(loaded.getComposite().getLazyStrList());
            Assert.assertNotNull(loaded.getComposite().getLazyStrSet());
            Assert.assertNotNull(loaded.getComposite().getLazyStrMap());
            Assert.assertTrue(loaded.getComposite().getStrList() instanceof ArrayList);
            Assert.assertTrue(loaded.getComposite().getStrSet() instanceof HashSet);
            Assert.assertTrue(loaded.getComposite().getStrMap() instanceof HashMap);
            Assert.assertTrue(loaded.getComposite().getLazyStrList() instanceof LazyList);
            Assert.assertTrue(loaded.getComposite().getLazyStrSet() instanceof LazySet);
            Assert.assertTrue(loaded.getComposite().getLazyStrMap() instanceof LazyMap);
            Assert.assertFalse(((AbstractLazy) loaded.getComposite().getLazyStrList()).isLoaded());
            Assert.assertFalse(((AbstractLazy) loaded.getComposite().getLazyStrSet()).isLoaded());
            Assert.assertFalse(((AbstractLazy) loaded.getComposite().getLazyStrMap()).isLoaded());
            Assert.assertEquals(loaded.getComposite().getStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getComposite().getLazyStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getComposite().getStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getComposite().getLazyStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getComposite().getStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getComposite().getLazyStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertFalse(loaded.getComposite().getLazyStrList() instanceof LazyList);
            Assert.assertFalse(loaded.getComposite().getLazyStrSet() instanceof LazySet);
            Assert.assertFalse(loaded.getComposite().getLazyStrMap() instanceof LazyMap);

            loaded = session.loadOne(LazyCompEntity.class, 1);

            Assert.assertNotNull(loaded);
            Assert.assertEquals(loaded.getId(), 1);
            Assert.assertNotNull(loaded.getComposite().getStrList());
            Assert.assertNotNull(loaded.getComposite().getStrSet());
            Assert.assertNotNull(loaded.getComposite().getStrMap());
            Assert.assertNotNull(loaded.getComposite().getLazyStrList());
            Assert.assertNotNull(loaded.getComposite().getLazyStrSet());
            Assert.assertNotNull(loaded.getComposite().getLazyStrMap());
            Assert.assertTrue(loaded.getComposite().getStrList() instanceof ArrayList);
            Assert.assertTrue(loaded.getComposite().getStrSet() instanceof HashSet);
            Assert.assertTrue(loaded.getComposite().getStrMap() instanceof HashMap);
            Assert.assertTrue(loaded.getComposite().getLazyStrList() instanceof LazyList);
            Assert.assertTrue(loaded.getComposite().getLazyStrSet() instanceof LazySet);
            Assert.assertTrue(loaded.getComposite().getLazyStrMap() instanceof LazyMap);
            Assert.assertFalse(((AbstractLazy) loaded.getComposite().getLazyStrList()).isLoaded());
            Assert.assertFalse(((AbstractLazy) loaded.getComposite().getLazyStrSet()).isLoaded());
            Assert.assertFalse(((AbstractLazy) loaded.getComposite().getLazyStrMap()).isLoaded());
            session.loadLazy(loaded);
            Assert.assertFalse(loaded.getComposite().getLazyStrList() instanceof LazyList);
            Assert.assertFalse(loaded.getComposite().getLazyStrSet() instanceof LazySet);
            Assert.assertFalse(loaded.getComposite().getLazyStrMap() instanceof LazyMap);
            Assert.assertEquals(loaded.getComposite().getStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getComposite().getLazyStrList(), Arrays.asList("one", "two", "three"));
            Assert.assertEquals(loaded.getComposite().getStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getComposite().getLazyStrSet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getComposite().getStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            Assert.assertEquals(loaded.getComposite().getLazyStrMap().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
        } finally {session.close();}

    }

    @Test
    public void lazy_tpc() throws Exception {

        PersistenceSession session = persistenceManager.createSession();
        try {
            TpcInheritA instA = new TpcInheritA();
            instA.setId(1);
            instA.setInhA(Arrays.asList("one", "two", "three"));
            session.insert(instA);

            TpcInheritB instB = new TpcInheritB();
            instB.setId(2);
            instB.setInhB(new HashSet<String>(Arrays.asList("one", "two", "three")));
            session.insert(instB);

            TpcInheritB2 instB2 = new TpcInheritB2();
            instB2.setId(3);
            instB2.setInhB2(Arrays.asList("one", "two", "three"));
            session.insert(instB2);

            TpcInheritC instC = new TpcInheritC();
            instC.setId(4);
            instC.setInhC(new HashMap<String, String>());
            instC.getInhC().put("one", "1");
            instC.getInhC().put("two", "2");
            instC.getInhC().put("three", "3");
            session.insert(instC);

            TpcInheritA loadedA = (TpcInheritA) session.loadOne(TpcBaseEntity.class, 1);
            Assert.assertTrue(loadedA.getInhA() instanceof LazyList);
            Assert.assertEquals(loadedA.getInhA(), Arrays.asList("one", "two", "three"));
            loadedA = session.loadOne(TpcInheritA.class, 1);
            Assert.assertTrue(loadedA.getInhA() instanceof LazyList);
            Assert.assertEquals(loadedA.getInhA(), Arrays.asList("one", "two", "three"));

            TpcInheritB loadedB = (TpcInheritB) session.loadOne(TpcBaseEntity.class, 2);
            Assert.assertTrue(loadedB.getInhB() instanceof LazySet);
            Assert.assertEquals(loadedB.getInhB(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            loadedB = session.loadOne(TpcInheritB.class, 2);
            Assert.assertTrue(loadedB.getInhB() instanceof LazySet);
            Assert.assertEquals(loadedB.getInhB(), new HashSet<String>(Arrays.asList("one", "two", "three")));

            TpcInheritB2 loadedB2 = (TpcInheritB2) session.loadOne(TpcBaseEntity.class, 3);
            Assert.assertTrue(loadedB2.getInhB2() instanceof LazyList);
            Assert.assertEquals(loadedB2.getInhB2(), Arrays.asList("one", "two", "three"));
            loadedB2 = session.loadOne(TpcInheritB2.class, 3);
            Assert.assertTrue(loadedB2.getInhB2() instanceof LazyList);
            Assert.assertEquals(loadedB2.getInhB2(), Arrays.asList("one", "two", "three"));

            TpcInheritC loadedC = (TpcInheritC) session.loadOne(TpcBaseEntity.class, 4);
            Assert.assertTrue(loadedC.getInhC() instanceof LazyMap);
            Assert.assertEquals(loadedC.getInhC().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            loadedC = session.loadOne(TpcInheritC.class, 4);
            Assert.assertTrue(loadedC.getInhC() instanceof LazyMap);
            Assert.assertEquals(loadedC.getInhC().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));

        }finally {session.close();}
    }

    @Test
    public void lazy_st() throws Exception {

        PersistenceSession session = persistenceManager.createSession();
        try {
            StInheritA instA = new StInheritA();
            instA.setId(1);
            instA.setInhA(Arrays.asList("one", "two", "three"));
            session.insert(instA);

            StInheritB instB = new StInheritB();
            instB.setId(2);
            instB.setInhB(new HashSet<String>(Arrays.asList("one", "two", "three")));
            session.insert(instB);

            StInheritB2 instB2 = new StInheritB2();
            instB2.setId(3);
            instB2.setInhB2(Arrays.asList("one", "two", "three"));
            session.insert(instB2);

            StInheritC instC = new StInheritC();
            instC.setId(4);
            instC.setInhC(new HashMap<String, String>());
            instC.getInhC().put("one", "1");
            instC.getInhC().put("two", "2");
            instC.getInhC().put("three", "3");
            session.insert(instC);

            StInheritA loadedA = (StInheritA) session.loadOne(StBaseEntity.class, 1);
            Assert.assertTrue(loadedA.getInhA() instanceof LazyList);
            Assert.assertEquals(loadedA.getInhA(), Arrays.asList("one", "two", "three"));
            loadedA = session.loadOne(StInheritA.class, 1);
            Assert.assertTrue(loadedA.getInhA() instanceof LazyList);
            Assert.assertEquals(loadedA.getInhA(), Arrays.asList("one", "two", "three"));

            StInheritB loadedB = (StInheritB) session.loadOne(StBaseEntity.class, 2);
            Assert.assertTrue(loadedB.getInhB() instanceof LazySet);
            Assert.assertEquals(loadedB.getInhB(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            loadedB = session.loadOne(StInheritB.class, 2);
            Assert.assertTrue(loadedB.getInhB() instanceof LazySet);
            Assert.assertEquals(loadedB.getInhB(), new HashSet<String>(Arrays.asList("one", "two", "three")));

            StInheritB2 loadedB2 = (StInheritB2) session.loadOne(StBaseEntity.class, 3);
            Assert.assertTrue(loadedB2.getInhB2() instanceof LazyList);
            Assert.assertEquals(loadedB2.getInhB2(), Arrays.asList("one", "two", "three"));
            loadedB2 = session.loadOne(StInheritB2.class, 3);
            Assert.assertTrue(loadedB2.getInhB2() instanceof LazyList);
            Assert.assertEquals(loadedB2.getInhB2(), Arrays.asList("one", "two", "three"));

            StInheritC loadedC = (StInheritC) session.loadOne(StBaseEntity.class, 4);
            Assert.assertTrue(loadedC.getInhC() instanceof LazyMap);
            Assert.assertEquals(loadedC.getInhC().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));
            loadedC = session.loadOne(StInheritC.class, 4);
            Assert.assertTrue(loadedC.getInhC() instanceof LazyMap);
            Assert.assertEquals(loadedC.getInhC().keySet(), new HashSet<String>(Arrays.asList("one", "two", "three")));

        }finally {session.close();}
    }
}
