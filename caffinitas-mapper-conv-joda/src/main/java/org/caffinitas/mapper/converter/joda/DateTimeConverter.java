/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.converter.joda;

import org.caffinitas.mapper.core.api.MultiColumnConverter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Date;

/**
 * Multi-column converter for Joda {@link DateTime}.
 * <p>
 *     Types are: {@link Date}, {@link String}<br>
 *     Prop names are: {@code dt}, {@code tz}
 * </p>
 */
public class DateTimeConverter implements MultiColumnConverter<DateTime> {
    private static final Class<?>[] TYPES = {Date.class, String.class};
    private static final String[] NAMES = {"dt", "tz"};

    @Override public Class<DateTime> javaType() {
        return DateTime.class;
    }

    @Override public Class<?>[] cassandraTypes() {
        return TYPES;
    }

    @Override public String[] propertyNames() {
        return NAMES;
    }

    @Override public DateTime toJava(Object... value) {
        Date dt = (Date) value[0];
        if (dt == null) {
            return null;
        }
        DateTimeZone zone = DateTimeZone.forID((String) value[1]);
        return new DateTime(dt, zone);
    }

    @Override public Object[] fromJava(DateTime value) {
        if (value == null)
            return null;
        return new Object[]{
            value.toDate(),
            value.getChronology().getZone().getID()
        };
    }
}
