/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.converter.joda;

import org.caffinitas.mapper.core.PersistenceManagerBuilder;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

/**
 * Utility class to register all Joda converters against a {@link org.caffinitas.mapper.core.PersistenceManagerBuilder}.
 */
public final class JodaConverters {
    private JodaConverters() {
    }

    /**
     * Register all Joda converters against a {@link org.caffinitas.mapper.core.PersistenceManagerBuilder}.
     *
     * @param builder builder
     * @return builder
     */
    public static PersistenceManagerBuilder register(PersistenceManagerBuilder builder) {
        return builder.withClassConverter(DateTime.class, DateTimeConverter.class)
            .withClassConverter(LocalDate.class, LocalDateConverter.class)
            .withClassConverter(LocalDateTime.class, LocalDateTimeConverter.class);
    }
}
