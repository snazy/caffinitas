/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.converter.joda;

import org.caffinitas.mapper.annotations.DataTypeName;
import org.caffinitas.mapper.core.api.SingleColumnConverter;
import org.joda.time.LocalDateTime;

import java.util.Date;

/**
 * Single-column converter for Joda {@link org.joda.time.LocalDateTime}.
 */
public class LocalDateTimeConverter implements SingleColumnConverter<Date, LocalDateTime> {
    @Override public DataTypeName dataTypeName() {
        return DataTypeName.TIMESTAMP;
    }

    @Override public Class<LocalDateTime> javaType() {
        return LocalDateTime.class;
    }

    @Override public LocalDateTime toJava(Date value) {
        return value==null ? null : new LocalDateTime(value);
    }

    @Override public Date fromJava(LocalDateTime value) {
        return value==null ? null : value.toDate();
    }
}
