/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.converter.joda.test;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.scan.DataModelScanner;
import org.caffinitas.mapper.test.CassandraTestBase;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class JodaConvertTest extends CassandraTestBase {
    private static DataModel model;

    @BeforeClass
    public static void setup() {
        recreateKeyspace("joda");

        DataModelScanner modelScanner = new DataModelScanner();
        modelScanner.withCaffinitasPersistenceXml();
        modelScanner.getFilterBuilder().includePackage(JodaConvertTest.class.getPackage().getName());
        model = modelScanner.scan();
    }

    @BeforeMethod
    public void setupPersistenceManager() {
        Assert.assertTrue(!model.getEntityClasses().isEmpty(), "found no entities for model");
        persistenceManager = withPersistenceManager(model);
    }

    @AfterMethod
    public void closePersistenceManager() {
        persistenceManager.close();
    }

    @Test
    public void createSchema() throws Exception {
        persistenceManager.createSchemaGenerator().generateLiveAlterDDL().execute(persistenceManager.driverSession());
    }

    @Test(dependsOnMethods = "createSchema")
    public void columns() {
        MappedSchemaObject<?> eso = persistenceManager.getEntity(JodaConvertEntity.class);

        List<CqlColumn> cols;

        cols = eso.getColumnsByAttributePath("dateTime");
        Assert.assertEquals(cols.size(), 2);
        cols = eso.getColumnsByAttributePath("localDate");
        Assert.assertEquals(cols.size(), 1);
        cols = eso.getColumnsByAttributePath("localDateTime");
        Assert.assertEquals(cols.size(), 1);
    }

    @Test(dependsOnMethods = "createSchema")
    public void all_null() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            JodaConvertEntity inst = new JodaConvertEntity();
            inst.setId(42);
            session.insert(inst);

            JodaConvertEntity loaded = session.loadOne(JodaConvertEntity.class, 42);
            Assert.assertNotNull(loaded);
            Assert.assertNull(loaded.getDateTime());
            Assert.assertNull(loaded.getLocalDateTime());
            Assert.assertNull(loaded.getLocalDate());
        } finally {session.close();}
    }

    @Test(dependsOnMethods = "createSchema")
    public void all_not_null() throws Exception {
        PersistenceSession session = persistenceManager.createSession();
        try {
            JodaConvertEntity inst = new JodaConvertEntity();
            inst.setId(99);
            DateTime dt = new DateTime(System.currentTimeMillis(), DateTimeZone.forID("CET"));
            LocalDate ld = new LocalDate(System.currentTimeMillis(), DateTimeZone.forID("CET"));
            LocalDateTime ldt = new LocalDateTime(System.currentTimeMillis(), DateTimeZone.forID("CET"));
            inst.setDateTime(dt);
            inst.setLocalDate(ld);
            inst.setLocalDateTime(ldt);
            session.insert(inst);

            JodaConvertEntity loaded = session.loadOne(JodaConvertEntity.class, 99);
            Assert.assertNotNull(loaded);
            Assert.assertNotNull(loaded.getDateTime());
            Assert.assertNotNull(loaded.getLocalDateTime());
            Assert.assertNotNull(loaded.getLocalDate());
            Assert.assertEquals(loaded.getDateTime(), dt);
            Assert.assertEquals(loaded.getLocalDate(), ld);
            Assert.assertEquals(loaded.getLocalDateTime(), ldt);
        } finally {session.close();}
    }

}
