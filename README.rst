Caffinitas - Object mapper for Apache Cassandra
===============================================

Caffinitas is an advanced object mapper for Apache Cassandra which has been especially designed to work with
Datastax Java Driver 2.1+ against Apache Cassandra 2.1, 2.0, 1.2.

+--------------+------------------------------------------------------------------------+
| Website      | http://caffinitas.org/                                                 |
+--------------+------------------------------------------------------------------------+
| SCM          | https://bitbucket.org/snazy/caffinitas/                                |
+--------------+------------------------------------------------------------------------+
| Issues       | https://caffinitas.atlassian.net/                                      |
+--------------+------------------------------------------------------------------------+
| Mailing List | https://groups.google.com/d/forum/caffinitas-mapper                    |
+--------------+------------------------------------------------------------------------+
| API-Docs     | http://caffinitas.org/mapper/apidocs/                                  |
+--------------+------------------------------------------------------------------------+

Features
--------

Caffinitas Mapper features are:

- declarative model using Java annotations
- C* 2.1 user types mapped as Java composites
- support for C* 2.1 tuple types as composites
- Java composites mapped to "flat" columns
- "table per class" and "single table" (with discriminator column) inheritance
- runtime schema changes ("code more tables and columns than currently present in C*" - when support is added to DataStax Java Driver via JAVA-151 and JAVA-361)
- support for ``org.apache.cassandra.db.marshal.CompositeType`` and ``org.apache.cassandra.db.marshal.DynamicCompositeType``
- map-entities - access to CFs using partition key only and build a map of composite key to value column(s)
- Java ``List``/``Set``/``Map`` mapped to C* primitives and user types
- optional lazy loading of Java ``List``/``Set``/``Map`` mapped to C* primitives and user types
- denormalizing entities (but without automatic "reverse updating" - changing the "source" entity has no effect on the denormalized one)
- asynchronous execution of loads (``SELECT``) and modifications (``INSERT``/``UPDATE``/``DELETE``)
- execution of callbacks pre- and post-load/persist/delete with optional arguments
- allows generation of entity and user type dependency graph flattened to a list as a basis to code schema setup scripts
- allows generation of entity and user type CREATE / DROP / ALTER+CREATE DDL statement lists
- conversion API to convert Apache Cassandra data types to and from any other Java type using custom converter implementations
- writes with serial consistency (CAS - compare and swap)
- per request statement options (IF EXISTS / IF NOT EXISTS / IF / TTL / WRITETIME/ LIMIT...)
- update batches (DML statements - ``INSERT``/``UPDATE``/``DELETE``)
- use arbitrary ``WHERE`` clauses with prepared statement replacers for queries - either programmatically or as declaration via ``@CNamedQuery`` annotation
- very high throughput UUID generator - nearly lock free - uses C* CAS to generate unique host/clock combinations (in artifact ``caffinitas-mapper-uuidgen``)
- optionally ``INSERT``/``UPDATE`` only those columns that have been changed
- columns using ``DataTypeName.CUSTOM``
- execution tracing via flexible interface (per thread, filtering, own implementations possible)

Features which are "anti patterns" in Apache Cassandra:

- To-One relations (mapping of "foreign" primary key) using eager loading

Planned features are:

- composite inheritance - similar to single-table-inheritance for entities - includes discriminator field
- exclusive and read-write locks based on serial consistency
- To-Many references by mapping primary keys as a C* composite in C* ``list``/``set``/``map`` types using lazy/eager loading
- generic serialization of Java objects (with pluggable serializers)
- programmatic declaration of indexes and API that support "query using index"
- validation support
- generation of schema specific byte code generated during runtime to execute mapping as fast as possible
- allow optional limitation to a per-request set of attributes for an entity (for wide entities) to read/insert/update
- SPI to integrate other/additional backend storages like Elasticsearch or SolR
- More Converters
 - Converter for multi-column types (e.g. Joda DateRange)
 - Predefined converters for Joda
 - Predefined converters for JDK8 javax.time
 - Converter registration by class name
- Extend map entities with static (value) column support

Unclear:

- lazy loading
- instrumented entity object instances
- support for counter columns
- CQL table and user type options (e.g. bloom filter FPC) in annotation code - with indirection to allow configuration per target environment

Getting started
---------------

Open `Getting Started <http://caffinitas.org/mapper/getting-started.html>`_ on the Caffinitas Mapper website

Documentation
-------------

Look into the `Documentation <http://caffinitas.org/mapper/doc.html>`_ on the Caffinitas Mapper website and get familiar
with the `API docs <http://caffinitas.org/mapper/apidocs/>`_
and take a look into the class ``org.caffinitas.mapper.demo.Demo`` in the ``caffinitas-demo`` artifact.

Maven
-----

The last release of Caffinitas is available on Maven Central. You can install
it in your application using the following Maven dependency::

    <dependency>
      <groupId>org.caffinitas.mapper</groupId>
      <artifactId>caffinitas-mapper-core</artifactId>
      <version>0.1</version>
    </dependency>

Compatibility
-------------

Datastax Java driver 2.1.0-beta or newer is a required runtime and compile dependency.
Supports Apache Cassandra clusters running versions at least 1.2. Tested versions are 1.2.16, 2.0.8 and 2.1.0-rc3.

All unit tests run against these Apache Cassandra configurations (if the features are supported by the target C* version):

- Apache Cassandra latest version (2.1.0) single node (test configuration activated by default)
- Apache Cassandra latest version (2.1.0) 3 nodes (needs to be activated via system property ``allTestConfigs`` or profile ``v21-3nodes``)
- Apache Cassandra 2.0.8 single node (needs to be activated via system property ``allTestConfigs`` or profile ``v20-single``)
- Apache Cassandra 2.0.8 3 nodes (needs to be activated via system property ``allTestConfigs`` or profile ``v20-3nodes``)
- Apache Cassandra 1.2.16 single node (needs to be activated via system property ``allTestConfigs`` or profile ``v12-single``)
- Apache Cassandra 1.2.16 3 nodes (needs to be activated via system property ``allTestConfigs`` or profile ``v12-3nodes``)

See description on https://github.com/pcmanus/ccm especially for running multi-node tests on OSX.

The name "Caffinitas"
---------------------

... has no special meaining. It is an artificial word.

"C*" is used as an abbreviation for *Apache Cassandra*. For details about *Apache Cassandra* see http://cassandra.apache.org/ and http://datastax.com/docs

License
-------

Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
