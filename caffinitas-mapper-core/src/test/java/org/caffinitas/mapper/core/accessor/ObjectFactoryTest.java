/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.accessor;

import org.caffinitas.mapper.core.accessors.DefaultObjectFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ObjectFactoryTest {

  @Test
  public void objectFactory_publicConstructor() {
    Object inst = DefaultObjectFactory.INSTANCE.newInstance(Object.class);
    Assert.assertNotNull(inst);
    Assert.assertTrue(inst.getClass() == Object.class);
  }

  @Test
  public void objectFactory_outer() {
    Outer inst = DefaultObjectFactory.INSTANCE.newInstance(Outer.class);
    Assert.assertNotNull(inst);
    Assert.assertTrue(inst.getClass() == Outer.class);
  }

  @Test
  public void objectFactory_inner() {
    Outer.Inner inst = DefaultObjectFactory.INSTANCE.newInstance(Outer.Inner.class);
    Assert.assertNotNull(inst);
    Assert.assertTrue(inst.getClass() == Outer.Inner.class);
  }
}

class Outer {
  private Outer() {
    //
  }

  static class Inner {
    private Inner() {
      //
    }
  }
}
