/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.util;

import org.caffinitas.mapper.core.NameMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NameMapperTest {
  @Test
  public void none() {
    Assert.assertEquals(NameMapper.NONE.mapAttribute("aBcDeF"), "aBcDeF");
  }

  @Test
  public void lower() {
    Assert.assertEquals(NameMapper.LOWER.mapAttribute("aBcDeF"), "abcdef");
  }

  @Test
  public void capitalsToDashes() {
    Assert.assertEquals(NameMapper.CAPITALS_TO_UNDERSCORES_NAME_MAPPER.mapAttribute("aBcDeF"), "a_bc_de_f");
    Assert.assertEquals(NameMapper.CAPITALS_TO_UNDERSCORES_NAME_MAPPER.mapAttribute("ABcDeF"), "abc_de_f");
  }
}
