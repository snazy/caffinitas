/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AttrOverrideMapTest {
  @Test
  public void attrOverrideMap() {
    AttrOverrideMap empty = AttrOverrideMap.EMPTY;
    AttrOverrideMap single = new AttrOverrideMap("hello.my.world", "value");
    AttrOverrideMap merged = empty.merge(single);
    Assert.assertNotSame(empty, single);
    Assert.assertNotSame(empty, merged);
    Assert.assertNotSame(single, merged);

    Assert.assertNull(empty.get("hello.my.world"));
    Assert.assertEquals(single.get("hello.my.world"), "value");
    Assert.assertEquals(merged.get("hello.my.world"), "value");

    AttrOverrideMap prefixed1 = merged.forPrefix("hello");
    Assert.assertEquals(merged.get("hello.my.world"), "value");
    Assert.assertEquals(prefixed1.get("my.world"), "value");
    Assert.assertNull(prefixed1.get("hello.my.world"));

    AttrOverrideMap prefixed2 = prefixed1.forPrefix("my");
    Assert.assertNull(prefixed2.get("hello.my.world"));
    Assert.assertNull(prefixed2.get("my.world"));
    Assert.assertEquals(merged.get("hello.my.world"), "value");
    Assert.assertEquals(prefixed1.get("my.world"), "value");
    Assert.assertEquals(prefixed2.get("world"), "value");
  }
}
