/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.codec;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.SettableData;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.AbstractLazy;
import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.util.ArrayUtil;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Internal API.
 */
public final class Binder {
    private static final ByteBuffer[] NO_BYTE_BUFFERS = {};
    private final CqlColumn[] columns;
    private final CqlColumn[] columnsPk;
    private final ByteBuffer[] data;
    private final ByteBuffer[] dataPk;
    private final boolean[] used;
    private final boolean serializeDefault;

    private SettableData<?> bindSpecial;
    private CqlColumn bindSpecialCol;
    private int bindSpecialIdx;
    private final PersistMode mode;

    //
    // Used to bind values to columns (for using this information later in a BoundStatement)
    //
    // Usable for    SELECT columns WHERE columnsPk
    //               INSERT columns WHERE columnsPk
    //               UPDATE columns WHERE columnsPk
    //               DELETE columns WHERE columnsPk
    // Binding values to columns AND columnsPk is necessary for INSERT and UPDATE.
    // Binding values for columnsPK is neccessary for SELECT and DELETE - but columns are NOT bound.
    // Parameter bindColumns indicates whether columns should be bound.
    //

    public Binder(CqlColumn[] columns, boolean bindColumns, CqlColumn[] columnsPk, PersistMode mode, boolean serializeDefault) {
        this.mode = mode;
        this.columns = columns == null ? CqlColumn.NO_COLUMNS : columns;
        this.columnsPk = columnsPk == null ? CqlColumn.NO_COLUMNS : columnsPk;
        this.used = bindColumns ? new boolean[this.columns.length] : null;
        this.serializeDefault = serializeDefault;
        data = columns == null ? NO_BYTE_BUFFERS : new ByteBuffer[columns.length];
        dataPk = columnsPk == null ? NO_BYTE_BUFFERS : new ByteBuffer[columnsPk.length];
    }

    public void reset() {
        Arrays.fill(data, null);
        Arrays.fill(dataPk, null);
        Arrays.fill(used, false);
        bindSpecialIdx = 0;
        bindSpecialCol = null;
        bindSpecial = null;
    }

    public <E> void setList(CqlColumn col, List<E> v, TypeCodec<List<E>> codec) {
        set(col, v == null ? null : codec.serialize(v));
    }

    public <E> void setSet(CqlColumn col, Set<E> v, TypeCodec<Set<E>> codec) {
        set(col, v == null ? null : codec.serialize(v));
    }

    public <K, V> void setMap(CqlColumn col, Map<K, V> v, TypeCodec<Map<K, V>> codec) {
        set(col, v == null ? null : codec.serialize(v));
    }

    public void setUDTValue(CqlColumn col, CUDTValue v, TypeCodec<CUDTValue> codec) {
        set(col, v == null ? null : codec.serialize(v));
    }

    public void setUUID(CqlColumn col, UUID v) {
        set(col, v == null ? null : TypeCodec.uuidInstance.serialize(v));
    }

    public void setTimeUUID(CqlColumn col, UUID v) {
        set(col, v == null ? null : TypeCodec.timeUUIDInstance.serialize(v));
    }

    public void setInet(CqlColumn col, InetAddress v) {
        set(col, v == null ? null : TypeCodec.inetInstance.serialize(v));
    }

    public void setVarint(CqlColumn col, BigInteger v) {
        set(col, v == null ? null : TypeCodec.bigIntegerInstance.serialize(v));
    }

    public void setDecimal(CqlColumn col, BigDecimal v) {
        set(col, v == null ? null : TypeCodec.decimalInstance.serialize(v));
    }

    public void setDate(CqlColumn col, Date v) {
        set(col, v == null ? null : TypeCodec.dateInstance.serialize(v));
    }

    public void setSimpleDate(CqlColumn col, java.sql.Date v) {
        set(col, v == null ? null : TypeCodec.simpleDateInstance.serialize(v));
    }

    public void setTime(CqlColumn col, Time v) {
        set(col, v == null ? null : TypeCodec.timeInstance.serialize(v));
    }

    public void setAsciiString(CqlColumn col, String v) {
        set(col, v == null ? null : TypeCodec.StringCodec.asciiInstance.serialize(v));
    }

    public void setUTF8String(CqlColumn col, String v) {
        set(col, v == null ? null : TypeCodec.StringCodec.utf8Instance.serialize(v));
    }

    public void setBytes(CqlColumn col, ByteBuffer v) {
        set(col, v == null ? null : TypeCodec.bytesInstance.serialize(v));
    }

    public void setBytesUnsafe(CqlColumn col, ByteBuffer v) {
        set(col, v);
    }

    // primitives

    public void setBool(CqlColumn col, boolean v) {
        if (!serializeDefault && !v)
            return;
        set(col, TypeCodec.booleanInstance.serializeNoBoxing(v));
    }

    public void setInt(CqlColumn col, int v) {
        if (!serializeDefault && v == 0)
            return;
        set(col, TypeCodec.intInstance.serializeNoBoxing(v));
    }

    public void setLong(CqlColumn col, long v) {
        if (!serializeDefault && v == 0L)
            return;
        set(col, TypeCodec.longInstance.serializeNoBoxing(v));
    }

    public void setFloat(CqlColumn col, float v) {
        if (!serializeDefault && v == 0f)
            return;
        set(col, TypeCodec.floatInstance.serializeNoBoxing(v));
    }

    public void setDouble(CqlColumn col, double v) {
        if (!serializeDefault && v == 0d)
            return;
        set(col, TypeCodec.doubleInstance.serializeNoBoxing(v));
    }

    //

    private void set(CqlColumn col, ByteBuffer d) {
        if (col == bindSpecialCol) {
            bindSpecial.setBytesUnsafe(bindSpecialIdx, d);
            return;
        }

        int idx = used != null ? ArrayUtil.indexOfSame(columns, col) : -1;
        if (idx != -1 && serializeDefault) {
            data[idx] = d;
            used[idx] = true;
        }

        int idxPk = ArrayUtil.indexOfSame(columnsPk, col);
        if (idxPk != -1) {
            dataPk[idxPk] = idx == -1 ? d : d.duplicate();
        }
    }

    public boolean contains(CqlColumn col) {
        return col.isExists() && ((used != null && ArrayUtil.containsSame(columns, col)) || ArrayUtil.containsSame(columnsPk, col));
    }

    @SuppressWarnings("ManualArrayToCollectionCopy") public CqlColumn[] getColumnsForStatement() {
        if (used == null) {
            return columns;
        }
        List<CqlColumn> r = null;
        for (int i = 0; i < used.length; i++) {
            if (used[i]) {
                if (r != null) {
                    // if "used columns" list r!=null, add it
                    // r==null means all columns (until current index) are used
                    r.add(columns[i]);
                }
            } else {
                if (r == null) {
                    r = new ArrayList<CqlColumn>(columns.length - 1);
                    // all previous visited columns are used - so add them
                    for (int j = 0; j < i; j++) {
                        r.add(columns[j]);
                    }
                }
            }
        }
        return r == null ? columns : r.toArray(new CqlColumn[r.size()]);
    }

    public CqlColumn[] getColumns() {
        return columns;
    }

    public CqlColumn[] getColumnsPk() {
        return columnsPk;
    }

    public int bindColumns(int idx, SettableData<?> settableData) {
        if (used != null) {
            CqlColumn[] cols = columns;
            for (int i = 0; i < cols.length; i++) {
                if (!used[i] || !cols[i].isExists()) {
                    continue;
                }
                ByteBuffer value = data[i];
                settableData.setBytesUnsafe(idx++, value);
            }
        }
        for (int i = 0; i < columnsPk.length; i++) {
            if (!columnsPk[i].isExists()) {
                continue;
            }
            ByteBuffer value = dataPk[i];
            settableData.setBytesUnsafe(idx++, value);
        }
        return idx;
    }

    public int bindUDT(int idx, CUDTValue udt) {
        for (int i = 0; i < columns.length; i++) {
            if (!columns[i].isExists()) {
                continue;
            }
            ByteBuffer value = data[i];
            udt.setBytesUnsafe(idx++, value);
        }
        return idx;
    }

    public void bindSpecial(BoundStatement bStmt, CqlColumn col, int idx) {
        bindSpecial = bStmt;
        bindSpecialCol = col;
        bindSpecialIdx = idx;
    }

    public void clearSpecial() {
        bindSpecial = null;
        bindSpecialCol = null;
    }

    public ByteBuffer getBound(CqlColumn col) {
        int i = ArrayUtil.indexOfSame(columns, col);
        return i >= 0 ? data[i] : null;
    }

    public ByteBuffer[] getDataPk() {
        return dataPk;
    }

    public void unuse(int i) {
        used[i] = false;
    }

    public boolean noChanges() {
        for (boolean b : used) {
            if (b) {
                return false;
            }
        }
        return true;
    }

    public boolean skipValue(Object instance, Object val) {
        // skip lazy values if we perform an update and the lazy value has not been loaded yet
        return mode == PersistMode.UPDATE && val instanceof AbstractLazy && ((AbstractLazy) val).containedIn(instance);
    }

    public ByteBuffer bindComposite(boolean isStaticColumn) {
        // see Javadoc of org.apache.cassandra.db.marshal.CompositeType for protocol description
        int l = 0;
        if (isStaticColumn) {
            l += 2;
        }
        for (ByteBuffer arg : data) {
            if (arg == null) {
                l += 3;
            } else {
                int sz = arg.remaining();
                if (sz > 65535)
                    throw new IllegalArgumentException("Component at in CompositeType too long ("+sz+") - max 65535 bytes in serialized form");
                l += 3 + sz;
            }
        }
        ByteBuffer ser = ByteBuffer.allocate(l);
        if (isStaticColumn) {
            ser.putShort((short) 0xffff);
        }
        for (ByteBuffer arg : data) {
            bindCompositeArg(ser, arg);
        }
        ser.flip();
        return ser;
    }

    public ByteBuffer bindTuple() {
        // see Javadoc of org.apache.cassandra.db.marshal.CompositeType for protocol description
        int l = 0;
        for (ByteBuffer arg : data) {
            if (arg == null) {
                l += 4;
            } else {
                l += 4 + arg.remaining();
            }
        }
        ByteBuffer ser = ByteBuffer.allocate(l);
        for (ByteBuffer arg : data) {
            if (arg != null) {
                int clen = arg.remaining();
                ser.putInt(clen);
                ser.put(arg.duplicate());
            } else {
                ser.putInt(0);
            }
        }
        ser.flip();
        return ser;
    }

    public static void bindCompositeArg(ByteBuffer ser, ByteBuffer arg) {
        if (arg != null) {
            int clen = arg.remaining();
            if (clen > 65534) {
                throw new IllegalArgumentException("component in CompositeType exceeds maximum length of 65534 bytes");
            }
            ser.putShort((short) clen);
            ser.put(arg.duplicate());
        } else {
            ser.putShort((short) 0);
        }
        ser.put((byte) 0);
    }

    public boolean isSerializeDefault() {
        return serializeDefault;
    }
}
