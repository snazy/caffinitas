/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TableMetadata;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.CDT;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.caffinitas.mapper.core.util.ArrayUtil;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

abstract class MappedEntityBase extends MappedSchemaClassObject<TableMetadata> {

    final Interceptors prePersist;
    final Interceptors preDelete;
    final Interceptors postDelete;
    final Interceptors postLoad;
    final Interceptors postPersist;

    final boolean writeNullOnInsert;

    private final PersistOption.TTLOption defaultTTL;

    ConsistencyLevel readConsistencyLevel;
    ConsistencyLevel writeConsistencyLevel;
    ConsistencyLevel serialConsistencyLevel;

    final Map<String, String> namedQueries = new HashMap<String, String>();

    PreparedStatements preparedStatements;

    CqlColumn[] partitionKeyColumns;
    CqlColumn[] clusteringKeyColumns;
    CqlColumn[] primaryKeyColumns;
    CqlColumn[][] primaryKeyColumnsVariable;
    /**
     * See {@link #allColumns} which columns are include in this array.
     */
    CqlColumn[] allDataColumns;
    CqlColumn[] readDataColumnsAll;
    CqlColumn[] readColumnsLazy;
    CqlColumn[] readColumnsAll;
    CqlColumn[] writeColumns;
    CqlColumn[] writeDataColumns;

    protected MappedEntityBase(PersistenceManagerImpl persistenceManager, ParseEntityBase parseEntityBase, CqlTable cqlTable,
                               PersistOption.TTLOption defaultTTL, boolean writeNullOnInsert) {
        super(parseEntityBase.type, cqlTable);

        readConsistencyLevel = parseEntityBase.readConsistencyLevel;
        writeConsistencyLevel = parseEntityBase.writeConsistencyLevel;
        serialConsistencyLevel = parseEntityBase.serialConsistencyLevel;

        this.defaultTTL = defaultTTL;
        this.writeNullOnInsert = writeNullOnInsert;

        preparedStatements = new PreparedStatements(persistenceManager.preparedStatementCache, parseEntityBase.type, cqlTable);

        prePersist = parseEntityBase.prePersist.isEmpty() ? null : parseEntityBase.prePersist;
        preDelete = parseEntityBase.preDelete.isEmpty() ? null : parseEntityBase.preDelete;
        postDelete = parseEntityBase.postDelete.isEmpty() ? null : parseEntityBase.postDelete;
        postLoad = parseEntityBase.postLoad.isEmpty() ? null : parseEntityBase.postLoad;
        postPersist = parseEntityBase.postPersist.isEmpty() ? null : parseEntityBase.postPersist;
    }

    protected static CqlTable createCqlTable(PersistenceManagerImpl persistenceManager, ParseEntityBase parseEntity) {
        NameMapper nameMapper = persistenceManager.singletons.getInstanceOf(parseEntity.nameMapper);

        String keyspaceName = parseEntity.keyspace;
        keyspaceName = persistenceManager.mapKeyspaceName(keyspaceName);
        if (keyspaceName == null || keyspaceName.isEmpty()) {
            throw new ModelUseException("No keyspace defined for entity " + parseEntity.type);
        }

        String tableName = parseEntity.table;
        if (tableName.isEmpty()) {
            tableName = nameMapper.mapClass(parseEntity.type.getSimpleName());
        }

        boolean allowNotExists = parseEntity.ignore != null && parseEntity.ignore.notExists();
        return new CqlTable(keyspaceName, tableName, allowNotExists);
    }

    //
    // columns stuff
    //

    void setupEntityColumns(ParseEntityBase parseEntity) {
        List<CqlColumn> partitionKey = new ArrayList<CqlColumn>();
        List<CqlColumn> clusteringKey = new ArrayList<CqlColumn>();

        List<CqlColumn> data = setupEntityColumnsInner(parseEntity, partitionKey, clusteringKey);

        TableMetadata.Order[] clusteringKeyOrder = parseEntity.clusteringKeyOrder;
        for (int i = 0; i < clusteringKey.size() && i < clusteringKeyOrder.length; i++) {
            clusteringKey.get(i).orderDescending = clusteringKeyOrder[i] == TableMetadata.Order.DESC;
        }

        for (CqlColumn cqlColumn : clusteringKey) {
            if (partitionKey.contains(cqlColumn)) {
                throw new ModelUseException("The same column is used in both partition key and clustering key in " + type);
            }
        }

        Collections.addAll(data, allColumns);
        data.removeAll(partitionKey);
        data.removeAll(clusteringKey);

        Collections.sort(data);

        partitionKeyColumns = partitionKey.toArray(new CqlColumn[partitionKey.size()]);
        clusteringKeyColumns = clusteringKey.toArray(new CqlColumn[clusteringKey.size()]);
        primaryKeyColumns = ArrayUtil.concat(partitionKeyColumns, clusteringKeyColumns);
        primaryKeyColumnsVariable = new CqlColumn[clusteringKeyColumns.length + 1][];
        primaryKeyColumnsVariable[0] = partitionKeyColumns;
        for (int i = 1; i <= clusteringKeyColumns.length; i++) {
            primaryKeyColumnsVariable[i] = partitionKeyColumns.length + i == primaryKeyColumns.length ? primaryKeyColumns :
                Arrays.copyOf(primaryKeyColumns, partitionKeyColumns.length + i);
        }

        for (CqlColumn primaryKeyColumn : primaryKeyColumns) {
            primaryKeyColumn.setPrimaryKey(true);
        }

        updateAllColumns(data);

        setupEntityColumnsOuter();
    }

    protected abstract List<CqlColumn> setupEntityColumnsInner(ParseEntityBase parseEntity, List<CqlColumn> partitionKey,
                                                               List<CqlColumn> clusteringKey);

    protected abstract void setupEntityColumnsOuter();

    void updateAllColumns(List<CqlColumn> data) {
        if (data != null) {
            allDataColumns = data.toArray(new CqlColumn[data.size()]);
        }
        allColumns = ArrayUtil.concat(primaryKeyColumns, allDataColumns);
        readDataColumnsAll = filterReadColumns(allDataColumns, false);
        readColumnsAll = filterReadColumns(allColumns, false);
        readColumnsLazy = filterReadColumns(readColumnsAll, true);
        writeColumns = filterWriteColumns(ArrayUtil.concat(allDataColumns, primaryKeyColumns));
        writeDataColumns = filterWriteColumns(allDataColumns);
    }

    private static CqlColumn[] filterReadColumns(CqlColumn[] cols, boolean lazy) {
        List<CqlColumn> r = new ArrayList<CqlColumn>(cols.length);
        for (CqlColumn col : cols) {
            if (col.isForSelect() && (!lazy || !col.isLazy())) {
                r.add(col);
            }
        }
        return cols.length == r.size() ? cols : r.toArray(new CqlColumn[r.size()]);
    }

    private static CqlColumn[] filterWriteColumns(CqlColumn[] cols) {
        List<CqlColumn> r = new ArrayList<CqlColumn>(cols.length);
        for (CqlColumn col : cols) {
            if (col.isForUpdate()) {
                r.add(col);
            }
        }
        return cols.length == r.size() ? cols : r.toArray(new CqlColumn[r.size()]);
    }

    CqlColumn[] readColumns(PersistOption[] options) {
        return PersistOption.ColumnRestrictionOption.filter(options, ArrayUtil.contains(options, PersistOption.LoadEagerOption.INSTANCE) ? readColumnsAll : readColumnsLazy);
    }

    //
    // DDL stuff
    //

    @Override public UpdateSchemaStatus updateSchema(PersistenceManager persistenceManager, TableMetadata metadata) {
        UpdateSchemaStatus updateStatus = new UpdateSchemaStatus();
        try {

            PersistenceManagerImpl persistenceManagerImpl = (PersistenceManagerImpl) persistenceManager;

            if (metadata == null) {
                schemaObjectExists(persistenceManagerImpl, updateStatus, false);
                return updateStatus;
            }

            // verify partition key
            checkPrimaryKeyColumns(persistenceManagerImpl.protocolVersion, updateStatus, metadata.getPartitionKey(), partitionKeyColumns, "partition");

            // verify clustering key
            checkPrimaryKeyColumns(persistenceManagerImpl.protocolVersion, updateStatus, metadata.getClusteringColumns(), clusteringKeyColumns, "clustering");

            for (CqlColumn column : allColumns) {
                if (!column.isPhysical()) {
                    continue;
                }
                ColumnMetadata colMeta = metadata.getColumn(column.getName());
                DataType fieldType = colMeta != null ? CDT.copy(persistenceManagerImpl.protocolVersion, colMeta.getType()) : null;
                fieldExists(updateStatus, column, fieldType, true);
                if (!column.isExists() && !column.isAllowNotExists()) {
                    schemaObjectInvalid(updateStatus, "table column %s.%s for %s does not exist - exists:%s compatible:%s (%s vs %s)", cqlTable,
                        column.getEscapedName(), type, column.isExists(), column.compatible(fieldType), column.getDataType(), fieldType);
                }
            }

            schemaObjectExists(persistenceManagerImpl, updateStatus, true);

            updateStatus.toTable(cqlTable);

            if (updateStatus.change) {
                preparedStatements.invalidate();
            }

            boolean any = false;
            for (CqlColumn writeDataColumn : allDataColumns) {
                if (writeDataColumn.isExists()) {
                    any = true;
                    break;
                }
            }
            preparedStatements.enableUpdate(any);

            return updateSchemaUpdateConditions(updateStatus);
        } finally {
            initialUpdate = true;
            updateStatus.toTable(cqlTable);
        }
    }

    private void checkPrimaryKeyColumns(int protocolVersion, UpdateSchemaStatus updateStatus, List<ColumnMetadata> meta, CqlColumn[] columns, String type) {
        Iterator<ColumnMetadata> colIter = meta.iterator();
        for (CqlColumn column : columns) {
            if (!colIter.hasNext()) {
                schemaObjectInvalid(updateStatus, "too few %s key columns in table %s - expected %s", type, cqlTable, Arrays.toString(columns));
                break;
            }
            ColumnMetadata colMeta = colIter.next();
            if (!column.getName().equals(colMeta.getName())) {
                schemaObjectInvalid(updateStatus, "%s key column %s not defined in table %s - expected %s", type, column.getName(), cqlTable,
                    Arrays.toString(columns));
            }
            DataType fieldType = CDT.copy(protocolVersion, colMeta.getType());
            if (!column.compatible(fieldType)) {
                schemaObjectInvalid(updateStatus, "incompatible types for %s key column %s in table %s - expected %s but got %s", type,
                    column.getName(),
                    cqlTable, column.getDataType(), colMeta.getType());
            }
        }
        if (colIter.hasNext()) {
            schemaObjectInvalid(updateStatus, "too few %s key columns in table %s - expected %s", type, cqlTable, Arrays.toString(columns));
        }
    }

    void verifyForAccess() {
        if (!cqlTable.isExists()) {
            if (cqlTable.allowNotExists) {
                return;
            }
            throw new PersistenceRuntimeException("table " + cqlTable + " does not exist for type " + type);
        }
        if (!cqlTable.isValid()) {
            throw new PersistenceRuntimeException("table " + cqlTable + " is incompatible with coded data model for type " + type +
                " - check for missing columns, missing table, incompatible primary key or incompatible types: " + cqlTable.getErrors());
        }
    }

    @Override public CqlTable getCqlTable() {
        return cqlTable;
    }

    public CqlColumn[] getPrimaryKeyColumns() {
        return primaryKeyColumns.clone();
    }

    public CqlColumn[] getPartitionKeyColumns() {
        return partitionKeyColumns.clone();
    }

    public CqlColumn[] getClusteringKeyColumns() {
        return clusteringKeyColumns.clone();
    }

    @Override public CqlColumn[] getReadDataColumns() {
        return readDataColumnsAll.clone();
    }

    @Override public CqlColumn[] getReadColumns() {
        return readColumnsAll.clone();
    }

    @Override public CqlColumn[] getWriteColumns() {
        return writeColumns.clone();
    }

    @Override String ddlType() {
        return "TABLE";
    }

    @Override CqlStatementList getAlterDDL(KeyspaceMetadata keyspaceMeta) {
        TableMetadata tableMeta = keyspaceMeta.getTable(cqlTable.getTable());
        if (tableMeta == null) {
            CqlStatementList ddl = new CqlStatementList();
            ddl.addStatement(getCreateDDL());
            return ddl;
        }
        return getAlterDDL(tableMeta);
    }

    @Override public CqlStatementList getAlterDDL(TableMetadata tableMeta) {
        CqlStatementList result = new CqlStatementList();

        Set<String> validated = new HashSet<String>();

        int i = 0;
        for (ColumnMetadata colMeta : tableMeta.getPartitionKey()) {
            i = alterDDLColumns(result, validated, primaryKeyColumns, i, colMeta);
        }

        i = 0;
        Iterator<TableMetadata.Order> clusterOrderIter = tableMeta.getClusteringOrder().iterator();
        for (ColumnMetadata colMeta : tableMeta.getClusteringColumns()) {
            TableMetadata.Order order = clusterOrderIter.next();
            // TODO do we need to check cluster key sort order ??
            i = alterDDLColumns(result, validated, clusteringKeyColumns, i, colMeta);
        }

        for (ColumnMetadata colMeta : tableMeta.getColumns()) {
            String colName = colMeta.getName();
            validated.add(colName);
            CqlColumn col = getColumnByColumnName(colName);
            if (col == null) {
                result.addWarning("-- " + cqlTable + '.' + colName + " not mapped");
            } else {
                if (!col.getDataType().toString().equals(colMeta.getType().toString())) {
                    result.addError(
                        "-- ***** " + cqlTable + '.' + colName + " has different data types in " + cqlTable + " :\n" +
                            "--       metadata : " + colMeta.getType() + '\n' +
                            "--       entity   : " + col.getDataType()
                    );
                }
            }
        }

        for (CqlColumn col : allColumns) {
            if (!col.isPhysical()) {
                continue;
            }
            if (validated.contains(col.getName())) {
                continue;
            }
            result.addStatement("ALTER TABLE " + cqlTable + " ADD " + col.getEscapedName() + ' ' +
                col.getDataType() + ';');
        }

        return result;
    }

    private int alterDDLColumns(CqlStatementList result, Set<String> validated, CqlColumn[] columns, int i, ColumnMetadata colMeta) {
        String colName = colMeta.getName();
        validated.add(colName);
        if (i < columns.length) {
            CqlColumn col = columns[i];
            if (!col.getName().equals(colName) || !col.getDataType().toString().equals(colMeta.getType().toString())) {
                result.addError("-- ***** name and/or type mismatch in partition key in " + cqlTable);
            }
        }
        return ++i;
    }

    @Override public String getCreateDDL() {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        pw.println("-- " + type);
        pw.println("CREATE TABLE " + cqlTable + " (");
        for (CqlColumn col : allColumns) {
            if (!col.isPhysical()) {
                continue;
            }
            pw.print("  " + col.getEscapedName() + ' ' + col.getDataType());
            if (col.isStatic()) {
                pw.print(" STATIC");
            }
            pw.println(" ,");
        }
        pw.print("  PRIMARY KEY ( (");
        boolean first = true;
        for (CqlColumn col : partitionKeyColumns) {
            pw.print((first ? " " : ", ") + col.getEscapedName());
            first = false;
        }
        pw.print(" )");
        for (CqlColumn col : clusteringKeyColumns) {
            pw.print(", " + col.getEscapedName());
            if (col.orderDescending) {
                pw.print(" DESCENDING");
            }
        }
        pw.println(" )");
        pw.println(");");
        sw.flush();
        return sw.toString();
    }

    //
    //
    //

    protected final PersistOption[] withTTL(PersistOption[] persistOptions) {
        if (defaultTTL == null) {
            return persistOptions;
        }
        for (PersistOption persistOption : persistOptions) {
            if (persistOption instanceof PersistOption.TTLOption) {
                return persistOptions;
            }
        }
        persistOptions = Arrays.copyOf(persistOptions, persistOptions.length + 1);
        persistOptions[persistOptions.length - 1] = defaultTTL;
        return persistOptions;
    }

    //
    // modify and load
    //

    abstract <T> void setupQueryBinder(QueryBinderImpl<T> queryBinder, PersistOption[] persistOptions, String queryName,
                                       StatementOptions statementOptions);

    abstract <T> T fromRow(PersistenceSessionImpl session, Retriever retriever, CqlColumn[] columns, T last);

    abstract <T> void executeQuery(ExecutionTracer tracer, QueryBinderImpl<T> queryBinder, ReadFutureImpl<T> futures);

    abstract <T> boolean executeLoadLazy(PersistenceSessionImpl persistenceSession, StatementOptions statementOptions, T container,
                                         MappedAttribute[] attrs,
                                         PersistOption[] persistOptions);

    abstract void executeLoadBoundStatements(ExecutionTracer tracer, Session session, Object[] primaryKey, AbstractDelegatesFutureExt<?> futures,
                                             StatementOptions statementOptions, PersistOption... persistOptions);

    abstract void buildModifyBindColumns(Object instance, Binder binder);

    void buildModifyBindColumns(Object instance, Binder binder, MappedAttribute[] attributes) {
        for (MappedAttribute attr : attributes) {
            attr.bindToStatement(instance, binder);
        }
    }

    BinderAndStatement buildModifyBoundStatement(PersistMode type, PersistenceSessionImpl persistenceSession, StatementOptions statementOptions,
                                                 Object instance,
                                                 PersistOption... persistOptions) {
        switch (type) {
            case INSERT:
                return buildInsertStatement(statementOptions, persistenceSession, instance, persistOptions);
            case UPDATE:
                return buildUpdateStatement(statementOptions, persistenceSession, instance, persistOptions);
            case DELETE:
                return buildDeleteStatement(statementOptions, persistenceSession, instance, persistOptions);
        }
        throw new IllegalArgumentException("type " + type);
    }

    protected abstract BinderAndStatement buildInsertStatement(StatementOptions statementOptions, PersistenceSessionImpl persistenceSession,
                                                               Object instance,
                                                               PersistOption[] persistOptions);

    protected abstract BinderAndStatement buildUpdateStatement(StatementOptions statementOptions, PersistenceSessionImpl persistenceSession,
                                                               Object instance,
                                                               PersistOption[] persistOptions);

    protected abstract BinderAndStatement buildDeleteStatement(StatementOptions statementOptions, PersistenceSessionImpl persistenceSession,
                                                               Object instance,
                                                               PersistOption[] persistOptions);

    abstract BoundStatement updateBoundStatement(StatementOptions statementOptions, Session session, Object instance, Binder binder,
                                                 PersistOption... persistOptions);
}
