/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TableMetadata;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.caffinitas.mapper.core.util.ArrayUtil;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

final class MappedEntityST extends MappedEntityInherited<MappedEntityST> {
    final MappedEntitySTRoot root;
    final ByteBuffer discriminatorValue;

    MappedEntityST(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity, MappedEntitySTRoot root) {
        super(persistenceManager, parseEntity, MappedEntityST.class, root.cqlTable);

        this.discriminatorValue = root.initGetDiscriminatorValue(persistenceManager, parseEntity);

        this.root = root;

        if (root.entityByDiscriminator.put(discriminatorValue, this) != null)
            throw new ModelUseException("discriminator value '"+parseEntity.discriminatorValue.value()+"' not unique for entity "+parseEntity.type);
    }

    @Override public <T> T fromRow(PersistenceSessionImpl session, Retriever retriever, CqlColumn[] columns, T r) {
        MappedEntityContainer entity = root.entityTypeFromRow(retriever);

        // do not return superclass instances
        if (!type.isAssignableFrom(entity.type)) {
            return null;
        }

        if (entity == this) {
            return super.fromRow(session, retriever, columns, r);
        }
        return entity.fromRow(session, retriever, columns, r);
    }

    @Override BoundStatement buildModifyInitial(Session session, PersistOption[] persistOptions, PreparedStatements.StatementType statementType,
                                                Binder binder) {
        binder.setBytesUnsafe(root.discriminatorColumn, discriminatorValue);
        return super.buildModifyInitial(session, persistOptions, statementType, binder);
    }

    @Override void verifyForAccess() {
        root.verifyForAccess();
    }

    @Override public UpdateSchemaStatus updateSchema(PersistenceManager persistenceManager, TableMetadata metadata) {
        // nop - handled by related MappedEntitySTRoot instance
        return new UpdateSchemaStatus();
    }

    void pushColumnsToParent(List<CqlColumn> parent) {
        List<CqlColumn> r = new ArrayList<CqlColumn>(allColumns.length);
        for (CqlColumn column : allColumns) {
            int existing = ArrayUtil.indexOf(this.allColumns, column);
            if (existing != -1 && !this.allColumns[existing].compatible(column)) {
                throw new ModelUseException("Duplicate column " + column.getEscapedName() + " with incompatible data types in table " + cqlTable +
                    " for single-table inheritance of " + type);
            }
            r.add(column);
        }

        for (MappedEntityST child : directChildren) {
            child.pushColumnsToParent(r);
        }

        // test for duplicate columns with same name but different data types
        for (CqlColumn c : r) {
            for (CqlColumn tst : parent) {
                if (tst.getName().equals(c.getName())) {
                    if (!c.compatible(tst)) {
                        throw new ModelUseException("Duplicate column " + c.getEscapedName() + " with incompatible data types in table " + cqlTable +
                            " for single-table inheritance of " + type);
                    }
                }
            }
        }

        parent.addAll(r);
    }

    void updateColumnLists(MappedEntitySTRoot root) {
        pullPrimaryKey(root);

        allDataColumns = root.allDataColumns;
        updateAllColumns(null);

        for (MappedEntityST child : directChildren) {
            child.updateColumnLists(root);
        }
    }

    @Override public Set<MappedSchemaObject<?>> collectSchemaDependencies() {
        return Collections.emptySet();
    }

    @Override <T> void setupQueryBinder(QueryBinderImpl<T> queryBinder, PersistOption[] persistOptions, String queryName,
                                        StatementOptions statementOptions) {
        this.root.setupQueryBinder(queryBinder, persistOptions, queryName, statementOptions);
    }

    @Override <T> void executeQuery(ExecutionTracer tracer, QueryBinderImpl<T> queryBinder, ReadFutureImpl<T> futures) {
        BoundStatement bStmt = queryBinder.statementFor(root.type);
        if (bStmt != null) {
            futures.addResultSetFuture(this, queryBinder.session.executeAsync(bStmt), queryBinder.readColumnsFor(root.type));
        }
    }

    @Override public String getCreateDDL() {
        return "";
    }

    @Override public String getDropDDL() {
        return "";
    }

    @Override public CqlStatementList getAlterDDL(Cluster cluster) {
        return new CqlStatementList();
    }

    @Override public CqlStatementList getAlterDDL(TableMetadata tableMeta) {
        return new CqlStatementList();
    }
}
