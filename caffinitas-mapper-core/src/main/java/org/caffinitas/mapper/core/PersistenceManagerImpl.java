/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TableMetadata;
import com.datastax.driver.core.UserType;
import org.caffinitas.mapper.annotations.CInheritance;
import org.caffinitas.mapper.annotations.DataTypeName;
import org.caffinitas.mapper.core.accessors.AccessorFactory;
import org.caffinitas.mapper.core.accessors.DefaultObjectFactory;
import org.caffinitas.mapper.core.accessors.ObjectFactory;
import org.caffinitas.mapper.core.accessors.Singletons;
import org.caffinitas.mapper.core.api.Converter;
import org.caffinitas.mapper.core.api.converter.NoopConverter;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.caffinitas.mapper.core.tracing.ExecutionTracerFactory;
import org.caffinitas.mapper.core.util.ArrayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import javax.validation.ValidatorFactory;

final class PersistenceManagerImpl implements PersistenceManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceManagerImpl.class);

    private final Lsnr lsnr;
    final AccessorFactory accessorFactory;
    final ObjectFactory objectFactory;
    final Singletons singletons;

    final Map<Class<?>, MappedClassObject> mappedClasses = new HashMap<Class<?>, MappedClassObject>(64);
    final Map<CqlTable, MappedSchemaObject[]> schemaObjects = new HashMap<CqlTable, MappedSchemaObject[]>(64);

    final Map<String, PreparedStatement> preparedStatementCache = new ConcurrentHashMap<String, PreparedStatement>(256);

    final Map<String, String> keyspaceNameLookup;

    private Cluster cluster;
    private Session session;
    private final boolean mySession;

    final String defaultDiscriminatorColumnName = "discr";
    final DataTypeName defaultDiscriminatorColumnType = DataTypeName.TEXT;
    final StatementOptions statementOptions;

    int protocolVersion;
    private MappedSchemaObject<?>[] dependencyGraphList;

    private final ExecutionTracerFactory executionTracerFactory;

    private Map<Class<?>, Class<? extends Converter>> staticConverters;

    final ValidatorFactory validatorFactory;

    PersistenceManagerImpl(Class<? extends AccessorFactory> accessorFactory,
                           Map<String, String> keyspaceNameLookup, StatementOptions statementOptions, boolean mySession,
                           ExecutionTracerFactory executionTracerFactory, Map<Class<?>, Class<? extends Converter>> staticConverters,
                           ValidatorFactory validatorFactory) {
        this.lsnr = new Lsnr();
        this.singletons = new Singletons(DefaultObjectFactory.INSTANCE);
        this.accessorFactory = singletons.getInstanceOf(accessorFactory);
        this.objectFactory = DefaultObjectFactory.INSTANCE;
        this.keyspaceNameLookup = ArrayUtil.simplifyMap(keyspaceNameLookup);
        this.statementOptions = statementOptions;
        this.mySession = mySession;
        this.executionTracerFactory = executionTracerFactory;
        this.staticConverters = staticConverters;
        this.validatorFactory = validatorFactory;
    }

    //

    ExecutionTracerFactory executionTracerFactory() {
        return executionTracerFactory;
    }

    //

    void loadModel(DataModel model) {

        // this method is called only once from Builder.build()

        // just create the MappedEntity* instances and connect the entity inheritance structure
        for (ParseMapEntity parseMapEntity : model.mapEntities.values()) {
            loadModelMapEntity(parseMapEntity);
        }

        // just create the MappedEntity* instances and connect the entity inheritance structure
        for (ParseEntity parseEntity : model.entities.values()) {
            loadModelEntity(parseEntity);
        }

        // just create the MappedComposite* instances and connect the entity inheritance structure
        for (ParseComposite parseComposite : model.composites.values()) {
            loadModelComposite(parseComposite);
        }

        // setup entity and composite attributes (own and inherited - e.g. embedded entities or composites)
        Set<Class<?>> parseSet = new HashSet<Class<?>>();
        for (MappedClassObject classObject : new ArrayList<MappedClassObject>(mappedClasses.values())) {
            ParseClass parseClass = parseClass(model, classObject);
            classObject.setupAttributes(this, model, classObject, "", parseSet, parseClass);
        }

        // create direct column objects
        for (MappedClassObject classObject : new ArrayList<MappedClassObject>(mappedClasses.values())) {
            ParseClass parseClass = parseClass(model, classObject);
            classObject.setupSingleColumns(this, model, parseClass);
        }

        // TODO push dataType of MappedCompositeC to all MappedAttributeCompositeC
        // TODO push dataType of MappedCompositeUDT to all MappedAttributeCompositeUDT

        // collect all columns for entities and composites (including referenced objects)
        for (MappedClassObject classObject : new ArrayList<MappedClassObject>(mappedClasses.values())) {
            ParseClass parseClass = parseClass(model, classObject);
            classObject.setupClassColumns(this, model, parseClass);
        }

        // setup partition key, clustering key, primary key, data columns for entities
        for (MappedClassObject classObject : new ArrayList<MappedClassObject>(mappedClasses.values())) {
            if (classObject instanceof MappedEntityBase) {
                MappedEntityBase mappedEntity = (MappedEntityBase) classObject;
                ParseEntityBase parseEntity = model.entities.get(mappedEntity.type);
                if (parseEntity == null) {
                    parseEntity = model.mapEntities.get(mappedEntity.type);
                }
                mappedEntity.setupEntityColumns(parseEntity);
            }
        }

        for (MappedClassObject classObject : new ArrayList<MappedClassObject>(mappedClasses.values())) {
            classObject.postInitialize(this);
        }

        List<MappedSchemaObject<?>> l = generateDependencyGraphList();
        dependencyGraphList = l.toArray(new MappedSchemaObject[l.size()]);

        this.staticConverters = null;

        updateSchema();
    }

    @SuppressWarnings("unchecked") <CT extends Converter> CT converterFor(Class<CT> expectedType, Class<? extends Converter> explicitConverterClass, Class<?> javaType) {
        if (!(expectedType.isAssignableFrom(explicitConverterClass)))
            explicitConverterClass = null;

        if (explicitConverterClass == null || explicitConverterClass == NoopConverter.class)
            explicitConverterClass = staticConverters.get(javaType);
        if (explicitConverterClass != null && !(expectedType.isAssignableFrom(explicitConverterClass)))
            explicitConverterClass = null;

        if (explicitConverterClass == null || explicitConverterClass == NoopConverter.class)
            return null;

        return (CT) singletons.getInstanceOf(explicitConverterClass);
    }

    private ParseClass parseClass(DataModel model, MappedClassObject classObject) {
        ParseClass parseClass = model.entities.get(classObject.type);
        if (parseClass == null) {
            parseClass = model.mapEntities.get(classObject.type);
        }
        if (parseClass == null) {
            parseClass = model.composites.get(classObject.type);
        }
        return parseClass;
    }

    private void loadModelComposite(ParseComposite parseComposite) {
        MappedClassObject composite = getComposite(parseComposite.type, MappedClassObject.class);

        if (composite != null) {
            return;
        }

        try {
            switch (parseComposite.composite.compositeType()) {
                case COLUMNS:
                    composite = new MappedClassObject(parseComposite.type);
                    break;
                case COMPOSITE:
                    composite = new MappedCompositeC(parseComposite);
                    break;
                case TUPLE:
                    composite = new MappedCompositeTuple(parseComposite);
                    break;
                case USER_TYPE:
                    composite = new MappedCompositeUDT(this, parseComposite);
                    addSchemaObject((MappedSchemaObject) composite);
                    break;
            }
        } catch (Throwable t) {
            throw new ModelUseException("Failure preparing composite " + parseComposite.type + " for persistence manager", t);
        }

        mappedClasses.put(parseComposite.type, composite);
    }

    private MappedEntity loadModelEntity(ParseEntity parseEntity) {
        MappedEntity entity = getEntity(parseEntity.type);

        if (entity != null) {
            return entity;
        }

        try {
            if (parseEntity.superEntity == null) {
                // no super entity
                if (parseEntity.subEntities.length == 0) {
                    // no sub entities --> simple one
                    entity = new MappedEntity(this, parseEntity);
                } else {
                    CInheritance inheritance = parseEntity.inheritance;
                    if (inheritance == null) {
                        throw new ModelUseException(
                            "inherited entity root is not annotated with @" + CInheritance.class.getSimpleName());
                    }
                    // base entity of inheritance structure
                    switch (inheritance.type()) {
                        case SINGLE_TABLE:
                            entity = new MappedEntitySTRoot(this, parseEntity);
                            break;
                        case TABLE_PER_CLASS:
                            entity = new MappedEntityTPCRoot(this, parseEntity);
                            break;
                    }
                }
            } else {
                // has super entity
                MappedEntityInheritedRoot rootEntity;
                @SuppressWarnings("unchecked") MappedEntityContainer<MappedEntityInherited> superEntity =
                    (MappedEntityContainer) loadModelEntity(parseEntity.superEntity);
                for (ParseEntity e = parseEntity.superEntity; ; e = e.superEntity) {
                    if (e.superEntity == null) {
                        rootEntity = (MappedEntityInheritedRoot) loadModelEntity(e);
                        break;
                    }
                }

                // non-base entity of an inheritance structure
                entity = rootEntity.newMappedChildEntity(this, parseEntity);
                superEntity.addDirectChildEntity((MappedEntityInherited) entity);
            }
        } catch (Throwable t) {
            throw new ModelUseException("Failure preparing entity " + parseEntity.type + " for persistence manager: " + t, t);
        }

        entity.verifyType();

        mappedClasses.put(parseEntity.type, entity);

        addSchemaObject(entity);

        return entity;
    }

    private MappedMapEntity loadModelMapEntity(ParseMapEntity parseMapEntity) {
        MappedMapEntity entity = getEntity(parseMapEntity.type);

        if (entity != null) {
            return entity;
        }

        try {
            entity = new MappedMapEntity(this, parseMapEntity);
        } catch (Throwable t) {
            throw new ModelUseException("Failure preparing entity " + parseMapEntity.type + " for persistence manager: " + t, t);
        }

        entity.verifyType();

        mappedClasses.put(parseMapEntity.type, entity);

        addSchemaObject(entity);

        return entity;
    }

    String mapKeyspaceName(String keyspaceName) {
        if (keyspaceName != null && keyspaceName.startsWith("@")) {
            keyspaceName = keyspaceNameLookup.get(keyspaceName.substring(1));
        }
        if (keyspaceName == null || keyspaceName.isEmpty()) {
            return keyspaceNameLookup.get("");
        }
        return keyspaceName;
    }

    @SuppressWarnings({"CastConflictsWithInstanceof", "unchecked"}) @Override public <E extends MappedSchemaObject> E getEntity(Class<?> type) {
        MappedClassObject mappedClassObject = mappedClasses.get(type);
        return mappedClassObject instanceof MappedEntityBase ? (E) mappedClassObject : null;
    }

    @SuppressWarnings({"CastConflictsWithInstanceof", "unchecked"}) @Override public <E extends MappedSchemaObject> E getEntityForObject(
        Object instance) {
        for (Class<?> type = instance.getClass(); type != Object.class; type = type.getSuperclass()) {
            MappedClassObject mappedClassObject = mappedClasses.get(type);
            if (mappedClassObject instanceof MappedEntityBase) {
                return (E) mappedClassObject;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked") @Override public <E extends MappedSchemaObject> E getComposite(Class<?> type) {
        MappedClassObject mappedClassObject = mappedClasses.get(type);
        if (mappedClassObject instanceof MappedEntity) {
            return null;
        }
        return (E) mappedClassObject;
    }

    @SuppressWarnings("unchecked") <E> E getComposite(Class<?> type, Class<E> compositeType) {
        MappedClassObject mappedClassObject = mappedClasses.get(type);
        if (mappedClassObject instanceof MappedEntity) {
            return null;
        }
        return mappedClassObject != null && !(mappedClassObject instanceof MappedEntityBase) &&
            compositeType.isAssignableFrom(mappedClassObject.getClass()) ? (E) mappedClassObject : null;
    }

    @SuppressWarnings({"CastConflictsWithInstanceof", "unchecked"}) @Override public <E extends MappedSchemaObject> E getCompositeForObject(Object instance) {
        for (Class<?> type = instance.getClass(); type != Object.class; type = type.getSuperclass()) {
            MappedClassObject mappedClassObject = mappedClasses.get(type);
            if (mappedClassObject instanceof MappedCompositeUDT) {
                return (E) mappedClassObject;
            }
        }
        return null;
    }

    @Override public List<MappedSchemaObject<?>> generateDependencyGraphList() {

        // collect all schema objects

        Map<MappedSchemaObject<?>, Set<MappedSchemaObject<?>>> dependencyMap = new HashMap<MappedSchemaObject<?>, Set<MappedSchemaObject<?>>>();

        for (MappedClassObject classObject : mappedClasses.values()) {
            if (classObject instanceof MappedSchemaObject) {
                dependencyMap.put((MappedSchemaObject<?>) classObject, classObject.collectSchemaDependencies());
            }
        }

        // calculate list

        List<MappedSchemaObject<?>> result = new ArrayList<MappedSchemaObject<?>>();

        while (!dependencyMap.isEmpty()) {
            for (Map.Entry<MappedSchemaObject<?>, Set<MappedSchemaObject<?>>> entry : dependencyMap.entrySet()) {
                boolean unknownDep = false;
                for (MappedSchemaObject<?> dep : entry.getValue()) {
                    if (dep != entry.getKey() && !result.contains(dep)) {
                        unknownDep = true;
                        break;
                    }
                }
                if (!unknownDep) {
                    result.add(entry.getKey());
                    dependencyMap.remove(entry.getKey());
                    break; // loop over (prevent ConcModExept)
                }
            }
        }

        return result;
    }

    @Override public SchemaGenerator createSchemaGenerator() {
        return new SchemaGeneratorImpl(dependencyGraphList, cluster);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // java-driver Cluster and Session handling
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void useSession(Session session) {
        this.session = session;

        this.cluster = session.getCluster();

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Connected to Cassandra cluster {} with {} hosts", cluster.getClusterName(), cluster.getMetadata().getAllHosts().size());
        }

        try {
            Metadata meta = cluster.getMetadata();
            Field fClusterManager = Metadata.class.getDeclaredField("cluster");
            fClusterManager.setAccessible(true);
            Object clusterManager = fClusterManager.get(meta);
            Field fConnectionFactory = clusterManager.getClass().getDeclaredField("connectionFactory");
            fConnectionFactory.setAccessible(true);
            Object connectionFactory = fConnectionFactory.get(clusterManager);
            Field fProtocolVersion = connectionFactory.getClass().getDeclaredField("protocolVersion");
            fProtocolVersion.setAccessible(true);
            this.protocolVersion = fProtocolVersion.getInt(connectionFactory);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Cannot inquire C* native protocol version from Java Driver - initialization failed", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Cannot inquire C* native protocol version from Java Driver - initialization failed", e);
        }
        /*
        TODO https://datastax-oss.atlassian.net/browse/JAVA-151 + 361 required
        this.protocolVersion = cluster.getMetadata().getProtocolVersion();
        */

        if (protocolVersion <= 0) {
            throw new PersistenceRuntimeException("could not determine native protocol version");
        }
        if (protocolVersion > 3) {
            throw new PersistenceRuntimeException("unsupported native protocol version " + protocolVersion);
        }
    }

    void setupListeners() {
        /*
        TODO https://datastax-oss.atlassian.net/browse/JAVA-151 + 361 required
        cluster.register((SchemaChangeTracker) lsnr);
         */
        cluster.register((Host.StateListener) lsnr);
    }

    @Override public boolean dynamicSchemaAgreementSupported() {
        return false;
    }

    @Override public void refreshSchema() {
        updateSchema();
    }

    @Override public void close() {
        LOGGER.info("Closing persistence manager for Cassandra cluster {}", (cluster != null) ? cluster.getClusterName() : "<no-cluster>");

        if (mySession) {
            try {
                session.close();
            } catch (Throwable ignore) {
            } finally {
                session = null;
            }
        }

        // TODO maybe track open PersistenceSession instances and close them...
        if (cluster != null) {
            /*
        TODO https://datastax-oss.atlassian.net/browse/JAVA-151 + 361 required
            cluster.unregister((SchemaChangeTracker) lsnr);
            */
            cluster.unregister((Host.StateListener) lsnr);
        }
    }

    @SuppressWarnings("unchecked") void addSchemaObject(MappedSchemaObject schemaObject) {
        CqlTable tab = schemaObject.getCqlTable();
        MappedSchemaObject[] l = schemaObjects.get(tab);
        if (l == null) {
            schemaObjects.put(tab, new MappedSchemaObject[]{schemaObject});
        } else {
            if (l[0].getType().isAssignableFrom(schemaObject.getType()) || schemaObject.getType().isAssignableFrom(l[0].getType()))
                schemaObjects.put(tab, ArrayUtil.add2(l, schemaObject));
            else
                throw new ModelUseException("table " + tab + " used by incompatible classes " + l[0].getType().getName() + " and " + schemaObject.getType().getName());
        }
    }

    private class Lsnr
        /*
        TODO https://datastax-oss.atlassian.net/browse/JAVA-151 + 361 required
        extends AbstractSchemaChangeTracker
        */
        implements Host.StateListener {
        private final AtomicInteger upLevel = new AtomicInteger();

        @Override public void onAdd(Host host) {
            if (upLevel.incrementAndGet() == 1) {
                updateSchema();
            }
        }

        @Override public void onUp(Host host) {
            if (upLevel.incrementAndGet() == 1) {
                updateSchema();
            }
        }

        @Override public void onDown(Host host) {
            upLevel.decrementAndGet();
        }

        @Override public void onRemove(Host host) {
            upLevel.decrementAndGet();
        }

        @Override public void onSuspected(Host host) {
            // ignore
        }

        /*
        TODO https://datastax-oss.atlassian.net/browse/JAVA-151 + 361 required
        @Override public void onTableAdded(KeyspaceMetadata keyspace, TableMetadata table) {
            updateSchema(keyspace);
        }

        @Override public void onTableRemoved(KeyspaceMetadata keyspace, TableMetadata table) {
            updateSchema(keyspace);
        }

        @Override public void onTableChanged(KeyspaceMetadata keyspace, TableMetadata table, TableMetadata tablePrevious) {
            updateSchema(keyspace);
        }

        @Override public void onTypeAdded(KeyspaceMetadata keyspace, UserType type) {
            updateSchema(keyspace);
        }

        @Override public void onTypeRemoved(KeyspaceMetadata keyspace, UserType type) {
            updateSchema(keyspace);
        }

        @Override public void onTypeChanged(KeyspaceMetadata keyspace, UserType type, UserType typePrevious) {
            updateSchema(keyspace);
        }
        */
    }

    private final ReentrantLock updateSchemaLock = new ReentrantLock();

    void updateSchema() {
        for (KeyspaceMetadata keyspaceMetadata : cluster.getMetadata().getKeyspaces()) {
            updateSchema(keyspaceMetadata);
        }
    }

    void updateSchema(KeyspaceMetadata keyspaceMetadata) {
        if (session == null) {
            LOGGER.info("Ignoring updateSchema - not connected to Cassandra cluster {}",
                (cluster != null) ? cluster.getClusterName() : "<no cluster>");
            return;
        }
        if (keyspaceMetadata == null) {
            updateSchema();
            return;
        }

        LOGGER.debug("Received update schema for keyspace {}", keyspaceMetadata.getName());

        ExecutionTracer tracer = executionTracerFactory != null ? executionTracerFactory.getExecutionTracer(null, null) : null;
        if (tracer != null) {
            tracer.onUpdateSchema(this, keyspaceMetadata);
        }

        updateSchemaLock.lock();
        try {

            boolean change = false;

            for (MappedSchemaObject<?> schemaObject : dependencyGraphList) {
                if (!schemaObject.getCqlTable().getKeyspace().equals(keyspaceMetadata.getName()))
                    continue;

                if (schemaObject instanceof MappedCompositeUDT) {
                    @SuppressWarnings("unchecked") UpdateSchemaStatus status =
                        ((MappedSchemaObject<UserType>) schemaObject)
                            .updateSchema(this, keyspaceMetadata.getUserType(schemaObject.getCqlTable().getTable()));
                    if (tracer != null) {
                        tracer.onUpdateSchemaUserType(this, keyspaceMetadata, schemaObject, status);
                    }
                    change |= status.change;
                    if (status.change) {
                        CqlTable t = schemaObject.getCqlTable();
                        LOGGER.info("Changes in Cassandra user type {} successfully applied - exists:{} valid:{}", t, t.isExists(), t.isValid());
                    }
                }
                if (schemaObject instanceof MappedEntityBase) {
                    @SuppressWarnings("unchecked") UpdateSchemaStatus status =
                        ((MappedSchemaObject<TableMetadata>) schemaObject)
                            .updateSchema(this, keyspaceMetadata.getTable(schemaObject.getCqlTable().getTable()));
                    if (tracer != null) {
                        tracer.onUpdateSchemaEntity(this, keyspaceMetadata, schemaObject, status);
                    }
                    change |= status.change;
                    if (status.change) {
                        CqlTable t = schemaObject.getCqlTable();
                        LOGGER.info("Changes in Cassandra user type {} successfully applied - exists:{} valid:{}", t, t.isExists(), t.isValid());
                    }
                }
            }

            if (change) {
                LOGGER.info("Changes in Cassandra schema successfully applied");
            }

        } catch (Throwable t) {
            LOGGER.error(String.format("Failed to update schema for keyspace %s", keyspaceMetadata.getName()), t);
            throw new RuntimeException(t);
        } finally {
            updateSchemaLock.unlock();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // DDL execution
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override public void createEntitySchemaObject(MappedSchemaObject mappedSchemaObject) {
        String ddl = mappedSchemaObject.getCreateDDL();
        if (ddl == null) {
            return;
        }
        session.execute(ddl);
    }

    @Override public void alterEntitySchemaObject(MappedSchemaObject mappedSchemaObject) {
        CqlStatementList ddl = mappedSchemaObject.getAlterDDL(cluster);
        if (ddl == null) {
            return;
        }
        if (ddl.hasErrors()) {
            throw new ModelRuntimeException(
                "cannot apply DDL to " + mappedSchemaObject.getCqlTable() + " for " + mappedSchemaObject.getType() + ": " + ddl);
        }
        for (String stmt : ddl.getStatements()) {
            session.execute(stmt);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // PersistenceSession handling
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override public Session driverSession() {
        Session s = session;
        if (s == null) {
            throw new PersistenceRuntimeException("not connected");
        }
        return s;
    }

    @Override public PersistenceSession createSession() {
        return new PersistenceSessionImpl(this);
    }

    @Override public PersistenceSession createTrackingSession() {
        return new PersistenceSessionTrackingImpl(this);
    }
}
