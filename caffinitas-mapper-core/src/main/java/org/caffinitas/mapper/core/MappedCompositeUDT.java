/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.UserType;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.CUDTDefinition;
import org.caffinitas.mapper.core.codec.CUDTRetriever;
import org.caffinitas.mapper.core.codec.CUDTValue;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.codec.TypeCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a Cassandra user type mapped to a Java class.
 */
final class MappedCompositeUDT extends MappedSchemaClassObject<UserType> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MappedCompositeUDT.class);

    CUDTDefinition udtDefinition;
    DataType dataType;
    TypeCodec<CUDTValue> codec;

    MappedCompositeUDT(PersistenceManagerImpl persistenceManager, ParseComposite parseComposite) {
        super(parseComposite.type, genCqlTable(persistenceManager, parseComposite));

        dataType = DataType.userType(new CUDTDefinition(persistenceManager.protocolVersion, cqlTable));
    }

    @Override Logger logger() {
        return LOGGER;
    }

    private static CqlTable genCqlTable(PersistenceManagerImpl persistenceManager, ParseComposite parseComposite) {
        NameMapper nameMapper = persistenceManager.singletons.getInstanceOf(parseComposite.composite.nameMapper());

        String keyspaceName = parseComposite.composite.keyspace();
        if (keyspaceName.isEmpty() && parseComposite.keyspace != null) {
            keyspaceName = parseComposite.keyspace.keyspace().trim();
        }
        keyspaceName = persistenceManager.mapKeyspaceName(keyspaceName);
        if (keyspaceName == null || keyspaceName.isEmpty()) {
            throw new ModelUseException("No keyspace defined for entity " + parseComposite.type);
        }

        String typeName = parseComposite.composite.type();
        if (typeName.isEmpty()) {
            typeName = nameMapper.mapClass(parseComposite.type.getSimpleName());
        }

        boolean allowNotExists = parseComposite.ignore != null && parseComposite.ignore.notExists();

        return new CqlTable(keyspaceName, typeName, allowNotExists);
    }

    @Override public UpdateSchemaStatus updateSchema(PersistenceManager persistenceManager, UserType metadata) {
        UpdateSchemaStatus updateStatus = new UpdateSchemaStatus();
        try {

            if (metadata == null) {
                schemaObjectExists((PersistenceManagerImpl) persistenceManager, updateStatus, false);
                return updateStatus;
            }

            this.udtDefinition = new CUDTDefinition(((PersistenceManagerImpl)persistenceManager).protocolVersion, metadata);
            this.dataType = DataType.userType(udtDefinition);
            this.codec = TypeCodec.udtOf(udtDefinition);

            for (CqlColumn column : allColumns) {
                DataType fieldType = safeGetFieldType(udtDefinition, column);
                fieldExists(updateStatus, column, fieldType, column.compatible(fieldType));
                if (!column.isExists() && !column.isAllowNotExists()) {
                    schemaObjectInvalid(updateStatus, "type field %s.%s for %s does not exist - exists:%s compatible:%s (%s vs %s)", cqlTable,
                        column.getEscapedName(), this.type, column.isExists(), column.compatible(fieldType), column.getDataType(), fieldType);
                }
            }

            schemaObjectExists((PersistenceManagerImpl) persistenceManager, updateStatus, true);

            updateStatus.toTable(cqlTable);

            definitionChanged(persistenceManager);

            return updateSchemaUpdateConditions(updateStatus);
        } finally {
            initialUpdate = true;
            updateStatus.toTable(cqlTable);
        }
    }

    private static DataType safeGetFieldType(CUDTDefinition metadata, CqlColumn column) {
        try {
            return metadata.getFieldType(column.getName());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Override public CqlColumn[] getReadColumns() {
        throw new UnsupportedOperationException();
    }

    @Override public CqlColumn[] getWriteColumns() {
        throw new UnsupportedOperationException();
    }

    @Override public CqlColumn[] getReadDataColumns() {
        throw new UnsupportedOperationException();
    }

    @Override CqlStatementList getAlterDDL(KeyspaceMetadata keyspaceMeta) {
        UserType userTypeMeta = keyspaceMeta.getUserType(cqlTable.getTable());
        if (userTypeMeta == null) {
            CqlStatementList ddl = new CqlStatementList();
            ddl.addStatement(getCreateDDL());
            return ddl;
        }
        return getAlterDDL(userTypeMeta);
    }

    @Override public CqlStatementList getAlterDDL(UserType userTypeMeta) {
        CqlStatementList result = new CqlStatementList();

        Set<String> validated = new HashSet<String>();

        for (String colName : userTypeMeta.getFieldNames()) {
            validated.add(colName);
            CqlColumn col = getColumnByColumnName(colName);
            if (col == null) {
                result.addWarning("-- " + cqlTable + '.' + colName + " not mapped");
            } else {
                if (!col.getDataType().toString().equals(userTypeMeta.getFieldType(colName).toString())) {
                    result.addError(
                        "-- ***** " + cqlTable + '.' + colName + " has different data types in " + cqlTable + " :\n" +
                            "--       metadata : " + userTypeMeta.getFieldType(colName) + '\n' +
                            "--       entity   : " + col.getDataType()
                    );
                }
            }
        }

        for (CqlColumn col : allColumns) {
            if (validated.contains(col.getName())) {
                continue;
            }
            result.addStatement("ALTER TYPE " + cqlTable + " ADD " + col.getEscapedName() + ' ' +
                col.getDataType() + ';');
        }

        return result;
    }

    @Override public String getCreateDDL() {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            pw.println("-- " + type);
            pw.println("CREATE TYPE " + cqlTable + " (");
            for (int i = 0; i < allColumns.length; ) {
                CqlColumn col = allColumns[i++];
                pw.print("  " + col.getEscapedName() + ' ' + col.getDataType());
                if (i == allColumns.length) {
                    break;
                }
                pw.println(" ,");
            }
            pw.println(" );");
        }
        finally {
            pw.close();
        }
        sw.flush();
        return sw.toString();
    }

    @Override String ddlType() {
        return "TYPE";
    }

    Object deserializeAny(Object o, Object rootInstance, PersistenceSessionImpl session, Class<?> javaType) {
        if (o == null) {
            return null;
        }

        CUDTValue udtValue = (CUDTValue) o;

        Retriever udtRetriever = new CUDTRetriever(udtValue, allColumns);

        Object inst = session.persistenceManager.objectFactory.newInstance(javaType);
        for (MappedAttribute attribute : allAttributes) {
            attribute.fromRow(session, rootInstance, inst, udtRetriever);
        }

        return inst;
    }

    Object serializeAny(Object o) {
        if (o == null) {
            return null;
        }

        Binder udtBinder = new Binder(allColumns, true, null, PersistMode.INSERT, true);

        for (MappedAttribute attribute : allAttributes) {
            attribute.bindToStatement(o, udtBinder);
        }

        CUDTValue udtValue = udtDefinition.newValue();
        udtBinder.bindUDT(0, udtValue);

        return udtValue;
    }
}
