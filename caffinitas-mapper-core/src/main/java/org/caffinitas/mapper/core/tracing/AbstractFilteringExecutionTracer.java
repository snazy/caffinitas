/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.tracing;

import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Statement;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceManager;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.UpdateSchemaStatus;

public abstract class AbstractFilteringExecutionTracer extends AbstractDelegateExecutionTracer {

    private final ExecutionTracer delegate;

    public AbstractFilteringExecutionTracer(ExecutionTracer delegate) {
        this.delegate = delegate;
    }

    @Override protected ExecutionTracer getDelegate() {
        return delegate;
    }

    protected abstract boolean matches(PersistenceSession persistenceSession, MappedSchemaObject<?> entity);

    @Override public void onReadResultSetBegin(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet,
                                               CqlColumn[] columns) {
        if (matches(persistenceSession, entity))
            super.onReadResultSetBegin(persistenceSession, entity, resultSet, columns);
    }

    @Override public void onReadResultSetRow(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet,
                                             CqlColumn[] columns, Row row) {
        if (matches(persistenceSession, entity))
            super.onReadResultSetRow(persistenceSession, entity, resultSet, columns, row);
    }

    @Override public void onReadResultSetEnd(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet,
                                             CqlColumn[] columns) {
        if (matches(persistenceSession, entity))
            super.onReadResultSetEnd(persistenceSession, entity, resultSet, columns);
    }

    @Override public void onModifyWrapResultSet(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, PersistMode mode,
                                                ResultSet resultSet) {
        if (matches(persistenceSession, entity))
            super.onModifyWrapResultSet(persistenceSession, entity, mode, resultSet);
    }

    @Override public void onBeginModify(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, PersistMode mode, Statement statement) {
        if (matches(persistenceSession, entity))
            super.onBeginModify(persistenceSession, entity, mode, statement);
    }

    @Override public void onBeginQuery(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, Statement statement) {
        if (matches(persistenceSession, entity))
            super.onBeginQuery(persistenceSession, entity, statement);
    }

    @Override public void onUpdateSchema(PersistenceManager persistenceManager, KeyspaceMetadata keyspaceMetadata) {
        super.onUpdateSchema(persistenceManager, keyspaceMetadata);
    }

    @Override public void onUpdateSchemaUserType(PersistenceManager persistenceManager, KeyspaceMetadata keyspaceMetadata,
                                                 MappedSchemaObject<?> schemaObject, UpdateSchemaStatus status) {
        super.onUpdateSchemaUserType(persistenceManager, keyspaceMetadata, schemaObject, status);
    }

    @Override public void onUpdateSchemaEntity(PersistenceManager persistenceManager, KeyspaceMetadata keyspaceMetadata,
                                               MappedSchemaObject<?> schemaObject, UpdateSchemaStatus status) {
        super.onUpdateSchemaEntity(persistenceManager, keyspaceMetadata, schemaObject, status);
    }
}
