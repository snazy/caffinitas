/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.tracing;

import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceSession;

import java.util.HashSet;
import java.util.Set;

/**
 * Default filtering execution tracer that allows filtering per entity or per package.
 * Note that the sets that configure the filterings are not thread safe - do not modify the sets when the execution tracer is
 * in use.
 */
public class DefaultFilteringExecutionTracer extends AbstractFilteringExecutionTracer {
    private final Set<String> packages = new HashSet<String>();
    private final Set<Class<?>> entities = new HashSet<Class<?>>();

    public DefaultFilteringExecutionTracer(ExecutionTracer delegate) {
        super(delegate);
    }

    public Set<String> getPackages() {
        return packages;
    }

    public Set<Class<?>> getEntities() {
        return entities;
    }

    @Override protected boolean matches(PersistenceSession persistenceSession, MappedSchemaObject<?> entity) {
        Class<?> type = entity.getType();
        if (entities.contains(type)) {
            return true;
        }
        String pkg = type.getPackage().getName();
        return packages.contains(pkg);
    }
}
