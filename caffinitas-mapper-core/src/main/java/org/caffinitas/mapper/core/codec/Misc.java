/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.codec;

import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.exceptions.DriverInternalError;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.util.CharsetUtil;

import java.nio.charset.CharacterCodingException;
import java.util.regex.Pattern;

final class Misc {

    private static final Pattern cqlId = Pattern.compile("\\w+");
    private static final Pattern lowercaseId = Pattern.compile("[a-z][a-z0-9_]*");

    // Deal with case sensitivity for a given keyspace or table id
    static String handleId(String id) {
        // Shouldn't really happen for this method, but no reason to fail here
        if (id == null) {
            return null;
        }

        if (cqlId.matcher(id).matches()) {
            return id.toLowerCase();
        }

        // Check if it's enclosed in quotes. If it is, remove them
        if (id.charAt(0) == '"' && id.charAt(id.length() - 1) == '"') {
            return id.substring(1, id.length() - 1);
        }

        // otherwise, just return the id.
        return id;
    }

    // Escape a CQL3 identifier based on its value as read from the schema
    // tables. Because it comes from Cassandra, we could just always quote it,
    // but to get a nicer output we don't do it if it's not necessary.
    static String escapeId(String ident) {
        // we don't need to escape if it's lowercase and match non-quoted CQL3 ids.
        return lowercaseId.matcher(ident).matches() ? ident : Metadata.quote(ident);
    }

    private static String readString(ChannelBuffer cb, int length) {
        try {
            String str = cb.toString(cb.readerIndex(), length, CharsetUtil.UTF_8);
            cb.readerIndex(cb.readerIndex() + length);
            return str;
        } catch (IllegalStateException e) {
            // That's the way netty encapsulate a CCE
            if (e.getCause() instanceof CharacterCodingException) {
                throw new DriverInternalError("Cannot decode string as UTF8");
            } else {
                throw e;
            }
        }
    }

    public static String readString(ChannelBuffer cb) {
        try {
            int length = cb.readUnsignedShort();
            return readString(cb, length);
        } catch (IndexOutOfBoundsException e) {
            throw new DriverInternalError("Not enough bytes to read an UTF8 serialized string preceded by it's 2 bytes length");
        }
    }
}
