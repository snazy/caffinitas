/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.Row;
import org.caffinitas.mapper.annotations.PersistMode;

/**
 * Indicates that a Cassandra CAS operation (for example {@code INSERT IF NOT EXISTS}) did not succeed.
 */
@SuppressWarnings("serial") public class CasNotAppliedException extends CaffinitasException {

    private final MappedEntityBase entity;
    private final Object instance;
    private final PersistMode mode;
    private final Row row;

    CasNotAppliedException(MappedEntityBase entity, Object instance, PersistMode mode, Row row) {
        super(entity != null ? "CAS operation not applied on entity " + entity.type.getName() : "CAS operation not applied");
        this.entity = entity;
        this.instance = instance;
        this.mode = mode;
        this.row = row;
    }

    public MappedEntityBase getEntity() {
        return entity;
    }

    public Object getInstance() {
        return instance;
    }

    public PersistMode getMode() {
        return mode;
    }

    public Row getRow() {
        return row;
    }
}
