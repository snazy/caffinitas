/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.Cluster;

import java.util.List;
import java.util.Set;

/**
 * Declares functions that are available on mapped Java classes represented by Cassandra tables or user types.
 *
 * @param <META_TYPE> the type of the object that describes the schema object in the Java driver - e.g. {@link com.datastax.driver.core.TableMetadata}
 */
public interface MappedSchemaObject<META_TYPE> {
    /**
     * Generates the ALTER DDL statement based on the metadata of the existing schema object in Cassandra.
     * This means, that the object must exist in Cassandra to let this this method execute successfully.
     */
    CqlStatementList getAlterDDL(Cluster cluster);

    /**
     * Generates the ALTER DDL statement based on the metadata of the existing schema object in Cassandra.
     * This means, that the object must exist in Cassandra to let this this method execute successfully.
     */
    CqlStatementList getAlterDDL(META_TYPE tableMeta);

    /**
     * Generates the DROP DDL statement.
     */
    String getDropDDL();

    /**
     * Generates the CREATE DDL statement.
     */
    String getCreateDDL();

    CqlTable getCqlTable();

    Class<?> getType();

    CqlColumn[] getAllColumns();

    CqlColumn[] getReadColumns();

    CqlColumn[] getWriteColumns();

    CqlColumn[] getReadDataColumns();

    List<CqlColumn> getColumnsByAttributePath(String attrPath);

    Set<String> getAttributeNames();

    MappedAttribute getAttributeByPath(String attrPath);

    /**
     * Force re-reading the meta data and update of the internal status like ignored tables, user types or columns.
     *
     * @param persistenceManager
     * @param metadata           type or table metadata from Java driver
     * @return status update status
     */
    UpdateSchemaStatus updateSchema(PersistenceManager persistenceManager, META_TYPE metadata);

    /**
     * Retrieve a awaiter for the condition that the underlying table is known to exist and valid against the defined data model.
     */
    ConditionAwait forSchemaObjectExistsAndValid();

    /**
     * Retrieve a awaiter for the condition that the underlying table is known to exist.
     */
    ConditionAwait forSchemaObjectExists();
}
