/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.mapper;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.DataTypeMapper;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;

abstract class AbstractDataTypeMapperObject extends DataTypeMapper {

    protected AbstractDataTypeMapperObject() {
    }

    @Override public boolean toBoolean(Retriever retriever, CqlColumn col) {
        throw new UnsupportedOperationException();
    }

    @Override public byte toByte(Retriever retriever, CqlColumn col) {
        throw new UnsupportedOperationException();
    }

    @Override public short toShort(Retriever retriever, CqlColumn col) {
        throw new UnsupportedOperationException();
    }

    @Override public int toInt(Retriever retriever, CqlColumn col) {
        throw new UnsupportedOperationException();
    }

    @Override public long toLong(Retriever retriever, CqlColumn col) {
        throw new UnsupportedOperationException();
    }

    @Override public char toChar(Retriever retriever, CqlColumn col) {
        throw new UnsupportedOperationException();
    }

    @Override public float toFloat(Retriever retriever, CqlColumn col) {
        throw new UnsupportedOperationException();
    }

    @Override public double toDouble(Retriever retriever, CqlColumn col) {
        throw new UnsupportedOperationException();
    }

    @Override public void fromBoolean(Binder binder, CqlColumn col, boolean value) {
        throw new UnsupportedOperationException();
    }

    @Override public void fromByte(Binder binder, CqlColumn col, byte value) {
        throw new UnsupportedOperationException();
    }

    @Override public void fromShort(Binder binder, CqlColumn col, short value) {
        throw new UnsupportedOperationException();
    }

    @Override public void fromInt(Binder binder, CqlColumn col, int value) {
        throw new UnsupportedOperationException();
    }

    @Override public void fromLong(Binder binder, CqlColumn col, long value) {
        throw new UnsupportedOperationException();
    }

    @Override public void fromChar(Binder binder, CqlColumn col, char value) {
        throw new UnsupportedOperationException();
    }

    @Override public void fromFloat(Binder binder, CqlColumn col, float value) {
        throw new UnsupportedOperationException();
    }

    @Override public void fromDouble(Binder binder, CqlColumn col, double value) {
        throw new UnsupportedOperationException();
    }
}
