/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.api.converter;

import org.caffinitas.mapper.annotations.DataTypeName;
import org.caffinitas.mapper.core.api.SingleColumnConverter;

/**
 * Convert between Java {@code char[]} to Cassandra {@code String}.
 */
public class CharArrayStringConverter implements SingleColumnConverter<String, char[]> {
    @Override public DataTypeName dataTypeName() {
        return DataTypeName.TEXT;
    }

    @Override public Class<char[]> javaType() {
        return char[].class;
    }

    @Override public char[] toJava(String value) {
        return value != null ? value.toCharArray() : null;
    }

    @Override public String fromJava(char[] value) {
        return value != null ? new String(value) : null;
    }
}
