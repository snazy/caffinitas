/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.MetaType;
import org.caffinitas.mapper.core.codec.DataType;

final class CqlColumnFunction extends CqlColumn {
    private final CqlColumn baseColumn;
    private final MetaType metaType;

    public CqlColumnFunction(CqlColumn baseColumn, DataType type, MetaType metaType) {
        super(baseColumn.getName(), type);
        this.baseColumn = baseColumn;
        this.metaType = metaType;
    }

    @Override void setPrimaryKey(boolean primaryKey) {
        throw new UnsupportedOperationException();
    }

    @Override public String getEscapedName() {
        switch (metaType) {
            case WRITETIME:
                return "WRITETIME(" + baseColumn.getEscapedName() + ')';
            case TTL:
                return "TTL(" + baseColumn.getEscapedName() + ')';
        }
        throw new IllegalStateException("metaType==" + metaType);
    }

    @Override public boolean isPrimaryKey() {
        return false;
    }

    @Override public boolean isLazy() {
        return false;
    }

    @Override public boolean isForSelect() {
        return true;
    }

    @Override public boolean isForUpdate() {
        return false;
    }

    @Override public boolean isExists() {
        return baseColumn.isExists();
    }

    @Override void setExists(boolean exists) {
        // ignore
    }

    @Override public boolean isAllowNotExists() {
        return baseColumn.isAllowNotExists();
    }

    @Override public boolean isIgnoreTypeMismatch() {
        return baseColumn.isIgnoreTypeMismatch();
    }

    @Override public boolean isStatic() {
        return baseColumn.isStatic();
    }

    @Override public boolean isPhysical() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        CqlColumnFunction that = (CqlColumnFunction) o;

        return baseColumn.equals(that.baseColumn) && metaType == that.metaType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + baseColumn.hashCode();
        result = 31 * result + metaType.hashCode();
        return result;
    }

    @Override public String toString() {
        return "CqlColumnFunction{" +
            "escapedName='" + getEscapedName() + '\'' +
            ", dataType=" + getDataType() +
            ", baseColumn=" + baseColumn +
            '}';
    }

}
