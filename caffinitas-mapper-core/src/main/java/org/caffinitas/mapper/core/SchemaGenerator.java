/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import java.util.List;

/**
 * Provides schema DDL gerneration support for all schema objects declared in a {@link PersistenceManager}.
 */
public interface SchemaGenerator {
    /**
     * Create {@code CREATE} statements based on the coded model.
     */
    CqlStatementList generateOfflineCreateDDL();

    /**
     * Create {@code DROP} statements based on the coded model.
     */
    CqlStatementList generateOfflineDropDDL();

    /**
     * Create {@code ALTER} and {@code CREATE} statements based on the coded model and the current schema the Cassandra.
     * Unlike {@link MappedSchemaObject#getAlterDDL(com.datastax.driver.core.Cluster) MappedSchemaObject.getAlterDDL()} methods
     * this method checks if a schema object already exists. It generates {@code CREATE} statements for non-existing objects and
     * {@code ALTER} statements (if necessary) for existing objects.
     */
    CqlStatementList generateLiveAlterDDL();

    /**
     * Retrieves the schema objects used by this schema generator. This is usually the same as {@link PersistenceManager#generateDependencyGraphList()}.
     *
     * @see PersistenceManager#generateDependencyGraphList()
     */
    List<MappedSchemaObject<?>> getSchemaObjects();

    ConditionAwait forSchemaObjectsAvailable();
}
