/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CComposite;
import org.caffinitas.mapper.annotations.CIgnore;
import org.caffinitas.mapper.annotations.CKeyspace;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

final class ParseComposite extends ParseClass {
    final CComposite composite;
    final CIgnore ignore;

    ParseComposite(CKeyspace keyspace, Class<?> type) {
        super(keyspace, type);

        if (Modifier.isAbstract(type.getModifiers())) {
            throw new ModelParseException("type " + type.getName() + " is abstract");
        }

        composite = type.getAnnotation(CComposite.class);
        if (composite == null) {
            throw new ModelParseException("composite " + type + " is missing " + CComposite.class.getSimpleName() + " annotation");
        }

        nameMapper = composite.nameMapper();

        ignore = type.getAnnotation(CIgnore.class);
    }

    void processAccessibles(DataModel model, Class<?> clazz) {
        for (Field field : clazz.getDeclaredFields()) {
            processFieldAttribute(model, field, parseAttributeMap);
        }
        for (Method method : clazz.getDeclaredMethods()) {
            processMethodAttribute(model, method, parseAttributeMap);
        }
    }
}
