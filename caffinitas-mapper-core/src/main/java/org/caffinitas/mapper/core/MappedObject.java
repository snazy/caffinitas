/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CompositeType;
import org.caffinitas.mapper.annotations.DataTypeName;
import org.caffinitas.mapper.core.util.ArrayUtil;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Base class for all mapped Java classes and attributes.
 */
abstract class MappedObject {
    final Class<?> type;
    final Map<String, MappedAttribute> attributes;
    MappedAttribute[] allAttributes = MappedAttribute.NONE;

    /**
     * Contains the columns for this object.
     * <p/>
     * In case of a MappedAttributeSimple this is always one CqlColumn.
     * In case of a MappedClassObject this array contains all directly reachable columns.
     * In case of a MappedEntitySTRoot this array contains all columns of the root entity and all inherited entities.
     * In case of a MappedEntityST this array contains all columns relevant for the current entity.
     * In case of a MappedEntityTPC/MappedEntityTPCRoot this array contains all columns relevant for the current entity.
     */
    CqlColumn[] allColumns = CqlColumn.NO_COLUMNS;

    MappedObject(Class<?> type, boolean hasAttributes) {
        this.type = type;
        attributes = hasAttributes ? new HashMap<String, MappedAttribute>() : Collections.<String, MappedAttribute>emptyMap();
    }

    void addColumn(CqlColumn cqlColumn) {
        allColumns = ArrayUtil.add2(allColumns, cqlColumn);
    }

    void setupAttributes(PersistenceManagerImpl persistenceManager, DataModel model, MappedClassObject rootClassObject, String attrPrefix,
                         Set<Class<?>> parseSet, ParseClass parseClass) {
        setupAttributes(persistenceManager, model, rootClassObject, attrPrefix, parseSet, parseClass.parseAttributeMap);
    }

    final void setupAttributes(PersistenceManagerImpl persistenceManager, DataModel model, MappedClassObject rootClassObject, String attrPrefix,
                               Set<Class<?>> parseSet, Map<String, ParseAttribute> sourceAttributeMap) {
        if (!parseSet.add(type)) {
            throw new ModelUseException("cyclic class reference for " + type + " and " + parseSet);
        }

        for (ParseAttribute parseAttribute : sourceAttributeMap.values()) {
            String attrPath = attrPrefix + parseAttribute.attrName;
            MappedAttribute attr = setupAttribute(persistenceManager, model, rootClassObject, attrPath + '.', parseSet, parseAttribute);

            // add attribute to this attributes map
            attributes.put(parseAttribute.attrName, attr);
            addAttribute(attr);

            // add attribute to container attributePaths map
            rootClassObject.attributePaths.put(attrPath, attr);
        }

        parseSet.remove(type);
    }

    void addAttribute(MappedAttribute attr) {
        allAttributes = ArrayUtil.add2(allAttributes, attr);
    }

    private MappedAttribute setupAttribute(PersistenceManagerImpl persistenceManager, DataModel model, MappedClassObject rootClassObject,
                                           String attrPrefix, Set<Class<?>> parseSet, ParseAttribute parseAttribute) {
        MappedAttribute attr;
        ParseClass parseClass = null;
        switch (parseAttribute.dataTypeName) {
            case LIST:
                attr = new MappedAttributeList(persistenceManager, model, parseAttribute);
                break;
            case SET:
                attr = new MappedAttributeSet(persistenceManager, model, parseAttribute);
                break;
            case MAP:
                attr = new MappedAttributeMap(persistenceManager, model, parseAttribute);
                break;
            case CUSTOM:
                attr = new MappedAttributeCustom(persistenceManager, model, parseAttribute, parseSet);
                break;
            case COMPOSITE:
                ParseComposite composite = model.composites.get(parseAttribute.attrType);
                if (composite == null) {
                    throw new ModelUseException("no compoite of " + parseAttribute.attrType + " defined for " + parseAttribute.accessible);
                }
                switch (parseAttribute.column != null ? composite.composite.compositeType() : CompositeType.COLUMNS) {
                    case COLUMNS:
                        attr = new MappedAttributeEntityBased(persistenceManager, parseAttribute);
                        break;
                    case USER_TYPE:
                        attr = new MappedAttributeCompositeUDT(persistenceManager, parseAttribute);
                        break;
                    case COMPOSITE:
                        attr = new MappedAttributeCompositeC(persistenceManager, parseAttribute);
                        break;
                    case TUPLE:
                        attr = new MappedAttributeCompositeTuple(persistenceManager, parseAttribute);
                        break;
                    default:
                        throw new IllegalStateException();
                }
                parseClass = composite;
                break;
            case DYNAMIC:
                attr = new MappedAttributeDynamic(persistenceManager, parseAttribute);
                break;
            case REFERENCE:
                attr = new MappedAttributeEntityReference(persistenceManager, parseAttribute);
                parseClass = model.entities.get(attr.type);
                break;
            case DENORMALIZED:
                attr = new MappedAttributeEntityBased(persistenceManager, parseAttribute);
                parseClass = model.entities.get(attr.type);
                break;
            case META:
                attr = new MappedAttributeMeta(persistenceManager, parseAttribute, this);
                parseClass = model.entities.get(attr.type);
                break;
            case GUESS:
                throw new ModelUseException("Must not pass " + DataTypeName.class.getSimpleName() + '.' +
                    DataTypeName.GUESS.name() + " as attribute data type");
            case MULTI_COLUMN:
                attr = new MappedAttributeMulti(persistenceManager, parseAttribute);
                break;
            default:
                attr = new MappedAttributeSimple(persistenceManager, parseAttribute);
                break;
        }

        if (parseClass != null) {
            attr.setupAttributes(persistenceManager, model, rootClassObject, attrPrefix, parseSet, parseClass);
        }

        return attr;
    }

    public Class<?> getType() {
        return type;
    }

    public MappedObject getAttribute(String name) {
        return attributes.get(name);
    }

    public CqlColumn[] getAllColumns() {
        return allColumns.clone();
    }

    void collectColumns(List<CqlColumn> columnList) {
        if (allColumns.length == 0) {
            for (MappedObject mappedObject : allAttributes) {
                mappedObject.collectColumns(columnList);
            }
        } else {
            Collections.addAll(columnList, allColumns);
        }
    }

    public CqlColumn getColumnByColumnName(String name) {
        for (CqlColumn column : allColumns) {
            if (column.getName().equals(name)) {
                return column;
            }
        }
        return null;
    }

    @Override public String toString() {
        return getClass().getSimpleName() + '{' +
            "type=" + type +
            '}';
    }

    public MappedAttribute resolveAttributePath(String attributePath) {
        MappedObject other = this;
        while (true) {
            int i = attributePath.indexOf('.');
            if (i == -1) {
                return other.attributes.get(attributePath);
            }

            MappedAttribute sub = other.attributes.get(attributePath.substring(0, i));
            attributePath = attributePath.substring(i + 1);
            other = sub;
        }
    }

    /**
     * Collect all Cassandra schema schema object this object directly depends on
     * and includes itself if {@code this} is a table or user type.
     */
    public Set<MappedSchemaObject<?>> collectSchemaDependencies() {
        Set<MappedSchemaObject<?>> deps = null;
        if (this instanceof MappedSchemaObject) {
            deps = new HashSet<MappedSchemaObject<?>>();
            deps.add((MappedSchemaObject<?>) this);
        }
        for (MappedAttribute attr : allAttributes) {
            Set<MappedSchemaObject<?>> r = attr.collectSchemaDependencies();
            if (!r.isEmpty()) {
                if (deps == null) {
                    deps = new HashSet<MappedSchemaObject<?>>();
                }
                deps.addAll(r);
            }
        }
        return deps == null ? Collections.<MappedSchemaObject<?>>emptySet() : deps;
    }
}
