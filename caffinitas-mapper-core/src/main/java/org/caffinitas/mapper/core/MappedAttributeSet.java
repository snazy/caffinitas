/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.lazy.LazySet;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

final class MappedAttributeSet extends MappedAttributeCollection<Set> {
    MappedAttributeSet(PersistenceManagerImpl persistenceManager, DataModel model, ParseAttribute parseAttribute) {
        super(persistenceManager, model, parseAttribute);
    }

    @Override boolean fromRow(PersistenceSessionImpl session, Object rootInstance, Object instance, Retriever retriever) {
        CqlColumn col = singleColumn();
        boolean missing = !retriever.contains(col);
        if (isLazy() && missing) {
            accessor.setAny(instance, new LazySet(session, rootInstance, instance, this));
            return false;
        }
        return super.fromRow(session, rootInstance, instance, retriever);
    }

    @Override protected Set newCollectionInstance(int srcSize) {
        return new HashSet(srcSize * 3 / 4);
    }

    @Override protected Set emptyCollection() {
        return Collections.emptySet();
    }
}
