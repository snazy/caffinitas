/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.util.ArrayUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a mapped Java class.
 */
class MappedClassObject extends MappedObject {

    final Map<String, MappedAttribute> attributePaths = new HashMap<String, MappedAttribute>();

    private DefinitionChangedCallback[] definitionChangedCallbacks = DefinitionChangedCallback.NONE;

    MappedClassObject(Class<?> type) {
        super(type, true);
    }

    void addDefinitionChangedCallback(DefinitionChangedCallback definitionChangedCallback) {
        if (!ArrayUtil.containsSame(definitionChangedCallbacks, definitionChangedCallback)) {
            definitionChangedCallbacks = ArrayUtil.add2(definitionChangedCallbacks, definitionChangedCallback);
        }
    }

    void definitionChanged(PersistenceManager persistenceManager) {
        if (definitionChangedCallbacks != null) {
            for (DefinitionChangedCallback cb : definitionChangedCallbacks) {
                cb.definitionChanged(persistenceManager, this);
            }
        }
    }

    final void setupSingleColumns(PersistenceManagerImpl persistenceManager, DataModel model, ParseClass parseClass) {
        NameMapper nameMapper = persistenceManager.singletons.getInstanceOf(parseClass.nameMapper);
        for (MappedAttribute mappedAttribute : allAttributes) {
            ParseAttribute parseAttribute = parseClass.attributeByName(mappedAttribute.name);
            mappedAttribute.setupSingleColumns(persistenceManager, model, parseAttribute,
                AttrOverrideMap.EMPTY, nameMapper, "");
        }
    }

    final void setupClassColumns(PersistenceManagerImpl persistenceManager, DataModel model, ParseClass parseClass) {
        NameMapper nameMapper = persistenceManager.singletons.getInstanceOf(parseClass.nameMapper);
        List<CqlColumn> columnList = new ArrayList<CqlColumn>(allAttributes.length * 2);
        for (MappedAttribute mappedAttribute : allAttributes) {
            ParseAttribute parseAttribute = parseClass.attributeByName(mappedAttribute.name);
            mappedAttribute.setupClassColumns(persistenceManager, this, columnList, model,
                parseAttribute, AttrOverrideMap.EMPTY, nameMapper, "");
        }
        allColumns = columnList.toArray(new CqlColumn[columnList.size()]);
    }

    void postInitialize(PersistenceManagerImpl persistenceManager) {
        for (Map.Entry<String, MappedAttribute> entry : attributePaths.entrySet()) {
            entry.getValue().attrPath = entry.getKey();
        }
    }

    public MappedAttribute getAttributeByPath(String attrPath) {
        return attributePaths.get(attrPath);
    }

    public <T> Object resolveParentInstance(T container, MappedAttribute attr) {
        String attrPath = attr.attrPath;
        MappedClassObject classObject = this;
        Object instance = container;
        while (true) {
            int dot = attrPath.indexOf('.');
            if (dot == -1) {
                break;
            }
            String attrName = attrPath.substring(0, dot);
            MappedAttribute a = (MappedAttribute) classObject.getAttribute(attrName);
            instance = a.accessor.getObject(instance);
            attrPath = attrPath.substring(dot + 1);
        }
        return instance;
    }
}
