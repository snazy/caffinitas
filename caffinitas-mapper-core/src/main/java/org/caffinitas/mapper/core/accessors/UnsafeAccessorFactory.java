/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.accessors;

import org.caffinitas.mapper.core.AccessorType;
import sun.misc.Unsafe;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;

/**
 * Internal API.
 */
public final class UnsafeAccessorFactory implements AccessorFactory {

    static final Unsafe UNSAFE;

    static {

        UNSAFE = AccessController.doPrivileged(new PrivilegedAction<Unsafe>() {
            @Override public Unsafe run() {
                try {
                    Field field = Unsafe.class.getDeclaredField("theUnsafe");
                    boolean accessible = field.isAccessible();
                    field.setAccessible(true);
                    try {
                        return (Unsafe) field.get(null);
                    } finally {
                        if (!accessible) {
                            field.setAccessible(false);
                        }
                    }
                } catch (Throwable t) {
                    throw new RuntimeException(t);
                }
            }
        });

    }

    @Override public AttributeAccessor accessorFor(AccessorType accessorType, AccessibleObject accessible,
                                                   Method setter) {
        switch (accessorType) {
            case FIELD:
                return new UnsafeFieldAccessor((Field) accessible);
            case GETTER:
                return new ReflectionMethodAccessor((Method) accessible, setter);
        }
        throw new IllegalArgumentException(accessorType.toString());
    }
}
