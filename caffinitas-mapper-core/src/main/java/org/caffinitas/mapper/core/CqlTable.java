/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.util.Names;

import java.util.Collections;
import java.util.List;

/**
 * Represents the keyspace, name and status of a Cassandra schema object like a table or user type.
 */
public final class CqlTable {
    private final String keyspace;
    private final String table;
    final boolean allowNotExists;

    private boolean exists;
    private boolean valid;
    private List<String> errors;

    CqlTable(String keyspace, String table, boolean allowNotExists) {
        keyspace = keyspace.trim();
        table = table.trim();
        if (table.isEmpty()) {
            throw new ModelUseException("keyspace=" + keyspace + " table=" + table);
        }
        this.keyspace = keyspace;
        this.table = table;
        this.allowNotExists = allowNotExists;
    }

    public String getKeyspace() {
        return keyspace;
    }

    public String getTable() {
        return table;
    }

    public String getKeyspaceEscaped() {
        return Names.maybeEscapeCassandraName(keyspace);
    }

    public String getTableEscaped() {
        return Names.maybeEscapeCassandraName(table);
    }

    public String getFullName() {
        return toString();
    }

    public boolean isExists() {
        return exists;
    }

    void setExists(boolean exists) {
        this.exists = exists;
    }

    public boolean isValid() { return valid; }

    void setValid(boolean valid) {
        this.valid = valid;
    }

    public List<String> getErrors() {
        return errors != null ? errors : Collections.<String>emptyList();
    }

    void setErrors(List<String> errors) {
        this.errors = errors;
    }

    @Override public String toString() {
        return getKeyspaceEscaped() + '.' + getTableEscaped();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CqlTable cqlTable = (CqlTable) o;

        return keyspace.equals(cqlTable.keyspace) && table.equals(cqlTable.table);

    }

    @Override
    public int hashCode() {
        int result = keyspace.hashCode();
        result = 31 * result + table.hashCode();
        return result;
    }

    public boolean isExistsAndValid() {
        return exists && valid;
    }
}
