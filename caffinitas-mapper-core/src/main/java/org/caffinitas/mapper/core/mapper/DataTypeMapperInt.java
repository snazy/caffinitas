/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.mapper;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.DataTypeMapper;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;

/**
 * Internal API.
 */
public final class DataTypeMapperInt extends DataTypeMapper {
    public static final DataTypeMapperInt INSTANCE = new DataTypeMapperInt();

    private DataTypeMapperInt() {
    }

    private static int get(Retriever retriever, CqlColumn col) {
        return retriever.getInt(col);
    }

    private static void set(Binder binder, CqlColumn col, int value) {
        binder.setInt(col, value);
    }

    @Override public boolean toBoolean(Retriever retriever, CqlColumn col) {
        return !retriever.isNull(col) && get(retriever, col) != 0;
    }

    @Override public byte toByte(Retriever retriever, CqlColumn col) {
        return (byte) (retriever.isNull(col) ? 0 : get(retriever, col));
    }

    @Override public short toShort(Retriever retriever, CqlColumn col) {
        return (short) (retriever.isNull(col) ? 0 : get(retriever, col));
    }

    @Override public int toInt(Retriever retriever, CqlColumn col) {
        return retriever.isNull(col) ? 0 : get(retriever, col);
    }

    @Override public long toLong(Retriever retriever, CqlColumn col) {
        return retriever.isNull(col) ? 0l : get(retriever, col);
    }

    @Override public char toChar(Retriever retriever, CqlColumn col) {
        return retriever.isNull(col) ? 0 : (char) get(retriever, col);
    }

    @Override public float toFloat(Retriever retriever, CqlColumn col) {
        return retriever.isNull(col) ? 0f : get(retriever, col);
    }

    @Override public double toDouble(Retriever retriever, CqlColumn col) {
        return retriever.isNull(col) ? 0d : get(retriever, col);
    }

    @Override public Object toObject(Retriever retriever, CqlColumn col, Class<?> targetClass) {
        if (retriever.isNull(col)) {
            return null;
        }
        Object v = get(retriever, col);
        // TODO convert to targetClass
        if (v instanceof String) {
            v = Integer.parseInt((String) v);
        }
        if (targetClass != null && targetClass.isEnum()) {
            return targetClass.getEnumConstants()[((Number) v).intValue()];
        }
        return v;
    }

    @Override public void fromBoolean(Binder binder, CqlColumn col, boolean value) {
        set(binder, col, value ? 1 : 0);
    }

    @Override public void fromByte(Binder binder, CqlColumn col, byte value) {
        set(binder, col, value);
    }

    @Override public void fromShort(Binder binder, CqlColumn col, short value) {
        set(binder, col, value);
    }

    @Override public void fromInt(Binder binder, CqlColumn col, int value) {
        set(binder, col, value);
    }

    @Override public void fromLong(Binder binder, CqlColumn col, long value) {
        set(binder, col, (int) value);
    }

    @Override public void fromChar(Binder binder, CqlColumn col, char value) {
        set(binder, col, value);
    }

    @Override public void fromFloat(Binder binder, CqlColumn col, float value) {
        set(binder, col, (int) value);
    }

    @Override public void fromDouble(Binder binder, CqlColumn col, double value) {
        set(binder, col, (int) value);
    }

    @Override public void fromObject(Binder binder, CqlColumn col, Object value) {
        if (value == null) {
            return;
        }
        // TODO convert to long
        int i;
        if (value instanceof Enum) {
            i = ((Enum) value).ordinal();
        } else {
            i = ((Number) value).intValue();
        }
        set(binder, col, i);
    }
}
