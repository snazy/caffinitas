/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.codec;

import com.datastax.driver.core.utils.Bytes;
import org.caffinitas.mapper.core.CqlColumn;

import java.nio.ByteBuffer;

/**
 * Internal API.
 */
public final class TupleRetriever extends BufferedRetriever {

    // see Javadoc of org.apache.cassandra.db.marshal.CompositeType for protocol description

    public TupleRetriever(int protocolVersion, ByteBuffer src, CqlColumn[] columns) {
        super(protocolVersion, columns);
        ByteBuffer source = src.duplicate();
        for (int i = 0; i < columns.length; i++) {
            if (source.remaining() < 4) {
                throw new IllegalArgumentException("illegally serialized TupleType for component #" + i +
                    ": length field not present for " + Bytes.toHexString(src));
            }
            int l = source.getInt();
            if (source.remaining() < l) {
                throw new IllegalArgumentException(
                    "illegally serialized TupleType at component #" + i + ": indicated length " + l + "(0x" + Integer.toHexString(l) +
                        " does not match remaining buffer length " + source.remaining() + " for " + Bytes.toHexString(src));
            }
            ByteBuffer b = components[i] = source.duplicate();
            int e = b.position() + l;
            b.limit(e);
            source.position(e);
        }
    }
}
