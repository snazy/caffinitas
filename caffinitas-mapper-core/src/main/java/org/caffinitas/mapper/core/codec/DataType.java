/*
 *      Copyright (C) 2012 DataStax Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.caffinitas.mapper.core.codec;

import com.datastax.driver.core.exceptions.DriverInternalError;
import com.datastax.driver.core.exceptions.InvalidTypeException;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.sql.Time;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Data types supported by cassandra.
 */
public abstract class DataType {

    /**
     * The CQL type name.
     */
    public enum Name {

        ASCII(1, String.class, "AsciiType"),
        BIGINT(2, Long.class, "LongType"),
        BLOB(3, ByteBuffer.class, "BytesType"),
        BOOLEAN(4, Boolean.class, "BooleanType"),
        COUNTER(5, Long.class, "CounterColumnType"),
        DECIMAL(6, BigDecimal.class, "DecimalType"),
        DOUBLE(7, Double.class, "DoubleType"),
        FLOAT(8, Float.class, "FloatType"),
        INET(16, InetAddress.class, "InetAddressType"),
        INT(9, Integer.class, "Int32Type"),
        TEXT(10, String.class, "UTF8Type"),
        TIMESTAMP(11, Date.class, "TimestampType"),
        UUID(12, UUID.class, "UUIDType"),
        VARCHAR(13, String.class, "UTF8Type"),
        VARINT(14, BigInteger.class, "IntegerType"),
        TIMEUUID(15, UUID.class, "TimeUUIDType"),
        DATE(17, java.sql.Date.class, "DateType"),
        TIME(18, Time.class, "TimeType"),
        LIST(32, List.class, "ListType"),
        SET(34, Set.class, "SetType"),
        MAP(33, Map.class, "MapType"),
        UDT(48, CUDTValue.class, "UserType"),
        TUPLE(49, TupleValue.class, "TupleType"),
        CUSTOM(0, ByteBuffer.class, "");

        final int protocolId;
        final Class<?> javaType;
        final String cassandraType;

        private static final Name[] nameToIds;

        static {
            int maxCode = -1;
            for (Name name : Name.values()) {
                maxCode = Math.max(maxCode, name.protocolId);
            }
            nameToIds = new Name[maxCode + 1];
            for (Name name : Name.values()) {
                if (nameToIds[name.protocolId] != null) {
                    throw new IllegalStateException("Duplicate Id");
                }
                nameToIds[name.protocolId] = name;
            }
        }

        Name(int protocolId, Class<?> javaType, String cassandraType) {
            this.protocolId = protocolId;
            this.javaType = javaType;
            this.cassandraType = cassandraType;
        }

        static Name fromProtocolId(int id) {
            Name name = nameToIds[id];
            if (name == null) {
                throw new DriverInternalError("Unknown data type protocol id: " + id);
            }
            return name;
        }

        /**
         * Returns whether this data type name represent the name of a collection type
         * that is a list, set or map.
         *
         * @return whether this data type name represent the name of a collection type.
         */
        public boolean isCollection() {
            switch (this) {
                case LIST:
                case SET:
                case MAP:
                    return true;
                default:
                    return false;
            }
        }

        /**
         * Returns the Java Class corresponding to this CQL type name.
         * <p>
         * The correspondence between CQL types and java ones is as follow:
         * </p>
         * <table>
         * <caption>DataType to Java class correspondence</caption>
         * <tr><th>DataType (CQL)</th><th>Java Class</th></tr>
         * <tr><td>ASCII         </td><td>String</td></tr>
         * <tr><td>BIGINT        </td><td>Long</td></tr>
         * <tr><td>BLOB          </td><td>ByteBuffer</td></tr>
         * <tr><td>BOOLEAN       </td><td>Boolean</td></tr>
         * <tr><td>COUNTER       </td><td>Long</td></tr>
         * <tr><td>CUSTOM        </td><td>ByteBuffer</td></tr>
         * <tr><td>DECIMAL       </td><td>BigDecimal</td></tr>
         * <tr><td>DOUBLE        </td><td>Double</td></tr>
         * <tr><td>FLOAT         </td><td>Float</td></tr>
         * <tr><td>INET          </td><td>InetAddress</td></tr>
         * <tr><td>INT           </td><td>Integer</td></tr>
         * <tr><td>LIST          </td><td>List</td></tr>
         * <tr><td>MAP           </td><td>Map</td></tr>
         * <tr><td>SET           </td><td>Set</td></tr>
         * <tr><td>TEXT          </td><td>String</td></tr>
         * <tr><td>TIMESTAMP     </td><td>java.util.Date</td></tr>
         * <tr><td>DATE          </td><td>java.sql.Date</td></tr>
         * <tr><td>TIME          </td><td>java.sql.Time</td></tr>
         * <tr><td>UUID          </td><td>UUID</td></tr>
         * <tr><td>UDT           </td><td>UDTValue</td></tr>
         * <tr><td>VARCHAR       </td><td>String</td></tr>
         * <tr><td>VARINT        </td><td>BigInteger</td></tr>
         * <tr><td>TIMEUUID      </td><td>UUID</td></tr>
         * </table>
         *
         * @return the java Class corresponding to this CQL type name.
         */
        public Class<?> asJavaClass() {
            return javaType;
        }

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }

        public String cassandraType() {
            return cassandraType;
        }
    }

    protected final DataType.Name name;

    private static final Map<Name, DataType> primitiveTypeMap = new EnumMap<Name, DataType>(Name.class);

    static {
        for (Name name : Name.values()) {
            if (!name.isCollection() && name != Name.CUSTOM && name != Name.UDT && name != Name.TUPLE) {
                primitiveTypeMap.put(name, new DataType.Native(name));
            }
        }
    }

    protected DataType(DataType.Name name) {
        this.name = name;
    }

    abstract TypeCodec<Object> codec(int protocolVersion);

    /**
     * Returns the ASCII type.
     *
     * @return The ASCII type.
     */
    public static DataType ascii() {
        return primitiveTypeMap.get(Name.ASCII);
    }

    /**
     * Returns the BIGINT type.
     *
     * @return The BIGINT type.
     */
    public static DataType bigint() {
        return primitiveTypeMap.get(Name.BIGINT);
    }

    /**
     * Returns the BLOB type.
     *
     * @return The BLOB type.
     */
    public static DataType blob() {
        return primitiveTypeMap.get(Name.BLOB);
    }

    /**
     * Returns the BOOLEAN type.
     *
     * @return The BOOLEAN type.
     */
    public static DataType cboolean() {
        return primitiveTypeMap.get(Name.BOOLEAN);
    }

    /**
     * Returns the COUNTER type.
     *
     * @return The COUNTER type.
     */
    public static DataType counter() {
        return primitiveTypeMap.get(Name.COUNTER);
    }

    /**
     * Returns the DECIMAL type.
     *
     * @return The DECIMAL type.
     */
    public static DataType decimal() {
        return primitiveTypeMap.get(Name.DECIMAL);
    }

    /**
     * Returns the DOUBLE type.
     *
     * @return The DOUBLE type.
     */
    public static DataType cdouble() {
        return primitiveTypeMap.get(Name.DOUBLE);
    }

    /**
     * Returns the FLOAT type.
     *
     * @return The FLOAT type.
     */
    public static DataType cfloat() {
        return primitiveTypeMap.get(Name.FLOAT);
    }

    /**
     * Returns the INET type.
     *
     * @return The INET type.
     */
    public static DataType inet() {
        return primitiveTypeMap.get(Name.INET);
    }

    /**
     * Returns the INT type.
     *
     * @return The INT type.
     */
    public static DataType cint() {
        return primitiveTypeMap.get(Name.INT);
    }

    /**
     * Returns the TEXT type.
     *
     * @return The TEXT type.
     */
    public static DataType text() {
        return primitiveTypeMap.get(Name.TEXT);
    }

    /**
     * Returns the TIMESTAMP type.
     *
     * @return The TIMESTAMP type.
     */
    public static DataType timestamp() {
        return primitiveTypeMap.get(Name.TIMESTAMP);
    }

    /**
     * Returns the DATE type.
     *
     * @return The DATE type.
     */
    public static DataType date() {
        return primitiveTypeMap.get(Name.DATE);
    }

    /**
     * Returns the TIME type.
     *
     * @return The TIME type.
     */
    public static DataType time() {
        return primitiveTypeMap.get(Name.TIME);
    }

    /**
     * Returns the UUID type.
     *
     * @return The UUID type.
     */
    public static DataType uuid() {
        return primitiveTypeMap.get(Name.UUID);
    }

    /**
     * Returns the VARCHAR type.
     *
     * @return The VARCHAR type.
     */
    public static DataType varchar() {
        return primitiveTypeMap.get(Name.VARCHAR);
    }

    /**
     * Returns the VARINT type.
     *
     * @return The VARINT type.
     */
    public static DataType varint() {
        return primitiveTypeMap.get(Name.VARINT);
    }

    /**
     * Returns the TIMEUUID type.
     *
     * @return The TIMEUUID type.
     */
    public static DataType timeuuid() {
        return primitiveTypeMap.get(Name.TIMEUUID);
    }

    /**
     * Returns the type of lists of {@code elementType} elements.
     *
     * @param elementType the type of the list elements.
     * @return the type of lists of {@code elementType} elements.
     */
    public static DataType list(DataType elementType) {
        return new DataType.Collection(Name.LIST, ImmutableList.of(elementType));
    }

    /**
     * Returns the type of sets of {@code elementType} elements.
     *
     * @param elementType the type of the set elements.
     * @return the type of sets of {@code elementType} elements.
     */
    public static DataType set(DataType elementType) {
        return new DataType.Collection(Name.SET, ImmutableList.of(elementType));
    }

    /**
     * Returns the type of maps of {@code keyType} to {@code valueType} elements.
     *
     * @param keyType   the type of the map keys.
     * @param valueType the type of the map values.
     * @return the type of map of {@code keyType} to {@code valueType} elements.
     */
    public static DataType map(DataType keyType, DataType valueType) {
        return new DataType.Collection(Name.MAP, ImmutableList.of(keyType, valueType));
    }

    /**
     * Returns a Custom type.
     * <p>
     * A custom type is defined by the name of the class used on the Cassandra
     * side to implement it. Note that the support for custom type by the
     * driver is limited: values of a custom type won't be interpreted by the
     * driver in any way. They will thus have to be set (by {@link com.datastax.driver.core.BoundStatement#setBytesUnsafe}
     * and retrieved (by {@link com.datastax.driver.core.Row#getBytesUnsafe}) as raw ByteBuffer.
     * </p>
     * The use of custom types is rarely useful and is thus not encouraged.
     *
     * @param typeClassName the server-side fully qualified class name for the type.
     * @return the custom type for {@code typeClassName}.
     */
    public static DataType custom(String typeClassName) {
        if (typeClassName == null) {
            throw new NullPointerException();
        }
        return new DataType.Custom(Name.CUSTOM, typeClassName);
    }

    // TODO: do we want to make this public somehow?
    public static DataType userType(CUDTDefinition definition) {
        return new UserType(definition);
    }

    public static DataType tupleType(List<DataType> types) {
        return new TupleType(types);
    }

    public static DataType dynamic(Map<Byte, DataType> aliases) {
        return new Dynamic(aliases);
    }

    public static DataType composite(List<DataType> components) {
        return new Composite(components);
    }

    /**
     * Returns the name of that type.
     *
     * @return the name of that type.
     */
    public Name getName() {
        return name;
    }

    public abstract String cassandraType();

    /**
     * Parses a string value for the type this object represent, returning its
     * Cassandra binary representation.
     * <p>
     * Please note that currently, parsing collections is not supported and will
     * throw an {@code InvalidTypeException}.
     * </p>
     *
     * @param value the value to parse.
     * @return the binary representation of {@code value}.
     * @throws com.datastax.driver.core.exceptions.InvalidTypeException if {@code value} is not a valid string
     *                                                                  representation for this type. Please note that values for custom types
     *                                                                  can never be parsed and will always return this exception.
     */
    public abstract ByteBuffer parse(String value);

    /**
     * Returns whether this type is a collection one, i.e. a list, set or map type.
     *
     * @return whether this type is a collection one.
     */
    public boolean isCollection() {
        return name.isCollection();
    }

    /**
     * Returns the Java Class corresponding to this type.
     * <p>
     * This is a shortcut for {@code getName().asJavaClass()}.
     * </p>
     *
     * @return the java Class corresponding to this type.
     * @see Name#asJavaClass
     */
    public Class<?> asJavaClass() {
        return name.asJavaClass();
    }

    /**
     * Deserialize a value of this type from the provided bytes.
     * <p>
     * The format of {@code bytes} must correspond to the Cassandra
     * encoding for this type.
     * </p>
     *
     * @param bytes bytes holding the value to deserialize.
     * @return the deserialized value (of class {@code this.asJavaClass()}).
     * Will return {@code null} if either {@code bytes} is {@code null} or if
     * {@code bytes.remaining() == 0} and this type has no value corresponding
     * to an empty byte buffer (the latter somewhat strange behavior is due to
     * the fact that for historical/technical reason, Cassandra types always
     * accept empty byte buffer as valid value of those type, and so we avoid
     * throwing an exception in that case. It is however highly discouraged to
     * store empty byte buffers for types for which it doesn't make sense, so
     * this implementation can generally be ignored).
     * @throws com.datastax.driver.core.exceptions.InvalidTypeException if {@code bytes} is not a valid
     *                                                                  encoding of an object of this {@code DataType}.
     */
    public Object deserialize(ByteBuffer bytes, int protocolVersion) {
        return codec(protocolVersion).deserialize(bytes);
    }

    public ByteBuffer serialize(Object value, int protocolVersion) {
        return codec(protocolVersion).serialize(value);
    }

    private static class Native extends DataType {
        private Native(Name name) {
            super(name);
        }

        @Override TypeCodec<Object> codec(int protocolVersion) {
            return TypeCodec.createFor(name);
        }

        @Override
        public ByteBuffer parse(String value) {
            TypeCodec<Object> codec = codec(0);
            return codec.serialize(codec.parse(value));
        }

        @Override
        public final int hashCode() {
            return name.hashCode();
        }

        @Override public String cassandraType() {
            return name.cassandraType();
        }

        @Override
        public final boolean equals(Object o) {
            return o instanceof Native && name == ((DataType) o).name;
        }

        @Override
        public String toString() {
            return name.toString();
        }
    }

    private static class Collection extends DataType {

        private final List<DataType> typeArguments;

        private Collection(Name name, List<DataType> typeArguments) {
            super(name);
            this.typeArguments = typeArguments;
        }

        @SuppressWarnings("unchecked")
        @Override TypeCodec<Object> codec(int protocolVersion) {
            switch (name) {
                case LIST:
                    return (TypeCodec) TypeCodec
                        .listOf(typeArguments.get(0), protocolVersion);
                case SET:
                    return (TypeCodec) TypeCodec.setOf(typeArguments.get(0), protocolVersion);
                case MAP:
                    return (TypeCodec) TypeCodec
                        .mapOf(typeArguments.get(0), typeArguments.get(1), protocolVersion);
            }
            throw new AssertionError();
        }

        @Override
        public ByteBuffer parse(String value) {
            throw new InvalidTypeException(String.format("Cannot parse value as %s, parsing collections is not currently supported", name));
        }

        @Override
        public final int hashCode() {
            return Arrays.hashCode(new Object[]{name, typeArguments});
        }

        @Override
        public final boolean equals(Object o) {
            if (!(o instanceof DataType.Collection)) {
                return false;
            }

            DataType.Collection d = (DataType.Collection) o;
            return name == d.name && typeArguments.equals(d.typeArguments);
        }

        @Override public String cassandraType() {
            if (name == Name.MAP) {
                return String.format("%s<%s, %s>", name.cassandraType, typeArguments.get(0).cassandraType(), typeArguments.get(1).cassandraType());
            } else {
                return String.format("%s<%s>", name.cassandraType, typeArguments.get(0).cassandraType());
            }
        }

        @Override
        public String toString() {
            if (name == Name.MAP) {
                return String.format("%s<%s, %s>", name, typeArguments.get(0), typeArguments.get(1));
            } else {
                return String.format("%s<%s>", name, typeArguments.get(0));
            }
        }
    }

    private static class Composite extends Custom {

        final List<DataType> typeParameters;

        Composite(List<DataType> typeParameters) {
            super(Name.CUSTOM, makeCompositeClassName(typeParameters));
            this.typeParameters = typeParameters;
        }

        private static String makeCompositeClassName(List<DataType> typeParameters) {
            StringBuilder sb = new StringBuilder("CompositeType(");
            boolean first = true;
            for (DataType component : typeParameters) {
                if (first) {
                    first = false;
                } else {
                    sb.append(", ");
                }
                sb.append(component.getName().cassandraType());
            }
            return sb.append(')').toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            if (!super.equals(o)) {
                return false;
            }

            Composite composite = (Composite) o;

            return typeParameters.equals(composite.typeParameters);
        }

        @Override
        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + (typeParameters != null ? typeParameters.hashCode() : 0);
            return result;
        }
    }

    @SuppressWarnings("EqualsAndHashcode") private static class Dynamic extends Custom {

        final Map<Byte, DataType> aliases;

        private Dynamic(Map<Byte, DataType> aliases) {
            super(Name.CUSTOM, makeCompositeClassName(aliases));
            this.aliases = aliases;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            Dynamic dynamic = (Dynamic) o;

            return getName() == dynamic.getName() && aliases.equals(dynamic.aliases);
        }

        @Override
        public int hashCode() {
            int result = getName().hashCode();
            result = 31 * result + aliases.hashCode();
            return result;
        }

        private static String makeCompositeClassName(Map<Byte, DataType> typeParameters) {
            StringBuilder sb = new StringBuilder("DynamicCompositeType");
            boolean first = true;
            for (Map.Entry<Byte, DataType> entry : typeParameters.entrySet()) {
                sb.append((char) entry.getKey().byteValue());
                sb.append(" => ");
                if (first) {
                    first = false;
                    sb.append('(');
                } else {
                    sb.append(", ");
                }
                sb.append(entry.getValue().getName().cassandraType());
            }
            if (!first) {
                sb.append(')');
            }
            return sb.toString();
        }
    }

    private static class UserType extends DataType {

        private final CUDTDefinition definition;

        private UserType(CUDTDefinition definition) {
            super(Name.UDT);
            this.definition = definition;
        }

        @SuppressWarnings("unchecked")
        @Override TypeCodec<Object> codec(int protocolVersion) {
            return (TypeCodec) TypeCodec.udtOf(definition);
        }

        @Override
        public ByteBuffer parse(String value) {
            // TODO: we actuall need that, because UDT can be in the partition key and so BuiltStatement needs that
            throw new InvalidTypeException(String.format("Cannot parse value as %s, parsing user types is not currently supported", name));
        }

        @Override
        public final int hashCode() {
            return Arrays.hashCode(new Object[]{name, definition});
        }

        @Override
        public final boolean equals(Object o) {
            if (!(o instanceof DataType.UserType)) {
                return false;
            }

            DataType.UserType d = (DataType.UserType) o;
            return name == d.name && definition.equals(d.definition);
        }

        @Override public String cassandraType() {
            return "UserType<" + this + '>';
        }

        @Override
        public String toString() {
            return Misc.escapeId(definition.getKeyspace()) + '.' + Misc.escapeId(definition.getName());
        }
    }

    private static class TupleType extends DataType {
        private final DataType[] types;

        private TupleType(List<DataType> types) {
            super(Name.TUPLE);
            this.types = types.toArray(new DataType[types.size()]);
        }

        @SuppressWarnings("unchecked")
        @Override TypeCodec codec(int protocolVersion) {
            return TypeCodec.tupleOf(protocolVersion, types);
        }

        @Override public String cassandraType() {
            throw new UnsupportedOperationException("implemnt this");
        }

        @Override
        public ByteBuffer parse(String value) {
            // TODO: we actually need that, because UDT can be in the partition key and so BuiltStatement needs that
            throw new InvalidTypeException(String.format("Cannot parse value as %s, parsing user types is not currently supported", name));
        }

        @Override
        public final int hashCode() {
            return Arrays.hashCode(types);
        }

        @Override
        public final boolean equals(Object o) {
            if (!(o instanceof DataType.TupleType)) {
                return false;
            }

            DataType.TupleType d = (DataType.TupleType) o;
            return name == d.name && Arrays.equals(types, d.types);
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (DataType type : types) {
                sb.append(sb.length()==0 ? "tuple<" : ", ");
                sb.append(type);
            }
            return sb.append('>').toString();
        }
    }

    private static class Custom extends DataType {

        private final String customClassName;

        private Custom(Name name, String className) {
            super(name);
            this.customClassName = className;
        }

        @SuppressWarnings("unchecked")
        @Override TypeCodec<Object> codec(int protocolVersion) {
            return (TypeCodec) TypeCodec.bytesInstance;
        }

        @Override
        public ByteBuffer parse(String value) {
            throw new InvalidTypeException(String.format("Cannot parse '%s' as value of custom type of class '%s' "
                + "(values for custom type cannot be parse and must be inputted as bytes directly)", value, customClassName));
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(new Object[]{name, customClassName});
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof DataType.Custom)) {
                return false;
            }

            DataType.Custom d = (DataType.Custom) o;
            return name == d.name && Objects.equal(customClassName, d.customClassName);
        }

        @Override public String cassandraType() {
            return customClassName;
        }

        @Override
        public String toString() {
            return '\'' + customClassName + '\'';
        }
    }
}
