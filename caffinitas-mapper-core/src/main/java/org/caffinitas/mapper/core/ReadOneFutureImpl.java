/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import org.caffinitas.mapper.core.codec.GettableRetriever;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

final class ReadOneFutureImpl<T> extends AbstractDelegatesFutureExt<T> implements ReadOneFuture<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReadOneFutureImpl.class);

    protected ReadOneFutureImpl(PersistenceSessionImpl session, ExecutionTracer tracer) {
        super(session, tracer);
    }

    @Override public boolean cancel(boolean mayInterruptIfRunning) {
        boolean r = false;
        for (EntityResultSetFuture delegate : delegates) {
            r |= delegate.resultSetFuture.cancel(mayInterruptIfRunning);
        }
        return r;
    }

    @Override public boolean isCancelled() {
        for (EntityResultSetFuture delegate : delegates) {
            if (delegate.resultSetFuture.isCancelled()) {
                return true;
            }
        }
        return false;
    }

    @Override public boolean isDone() {
        for (EntityResultSetFuture delegate : delegates) {
            if (!delegate.resultSetFuture.isDone()) {
                return false;
            }
        }
        return true;
    }

    @Override public T getUninterruptibly() {
        T r = null;
        for (EntityResultSetFuture delegate : delegates) {
            T r2 = wrapResultSet(delegate.entity, delegate.resultSetFuture.getUninterruptibly(), delegate.columns, r);
            if (r2 != null) {
                r = r2;
            }
        }
        return r;
    }

    @Override public T getUninterruptibly(long timeout, TimeUnit unit) throws TimeoutException {
        T r = null;
        for (EntityResultSetFuture delegate : delegates) {
            // TODO handle timeout correctly !!!
            T r2 = wrapResultSet(delegate.entity, delegate.resultSetFuture.getUninterruptibly(timeout, unit), delegate.columns, r);
            if (r2 != null) {
                r = r2;
            }
        }
        return r;
    }

    @Override public T get() throws InterruptedException, ExecutionException {
        T r = null;
        for (EntityResultSetFuture delegate : delegates) {
            T r2 = wrapResultSet(delegate.entity, delegate.resultSetFuture.get(), delegate.columns, r);
            if (r2 != null) {
                r = r2;
            }
        }
        return r;
    }

    @Override public T get(long timeout, TimeUnit unit)
        throws InterruptedException, ExecutionException, TimeoutException {
        T r = null;
        for (EntityResultSetFuture delegate : delegates) {
            // TODO handle timeout correctly !!!
            T r2 = wrapResultSet(delegate.entity, delegate.resultSetFuture.get(timeout, unit), delegate.columns, r);
            if (r2 != null) {
                r = r2;
            }
        }
        return r;
    }

    @SuppressWarnings("unchecked") private T wrapResultSet(MappedEntityBase entity, ResultSet resultSet, CqlColumn[] columns, T r) {
        if (tracer != null) {
            tracer.onReadResultSetBegin(session, entity, resultSet, columns);
        }
        GettableRetriever retriever = new GettableRetriever(session.persistenceManager.protocolVersion, columns);
        while (!resultSet.isExhausted()) {
            Row row = resultSet.one();
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("got {} row for read of {}", row != null ? "a" : "no", entity.type);
            }
            retriever.setCurrentSource(row);
            if (tracer != null) {
                tracer.onReadResultSetRow(session, entity, resultSet, columns, row);
            }
            T r2 = entity.fromRow(session, retriever, columns, r);
            if (r2 != null) {
                r = r2;
            }
        }
        if (tracer != null) {
            tracer.onReadResultSetEnd(session, entity, resultSet, columns);
        }
        return r;
    }
}
