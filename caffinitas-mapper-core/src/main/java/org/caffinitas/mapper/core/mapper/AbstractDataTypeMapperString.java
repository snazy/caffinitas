/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.mapper;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.DataTypeMapper;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;

abstract class AbstractDataTypeMapperString extends DataTypeMapper {

    protected AbstractDataTypeMapperString() {
    }

    private static String get(Retriever retriever, CqlColumn col) {
        return retriever.getString(col);
    }

    protected abstract void set(Binder binder, CqlColumn col, String s);

    @Override public boolean toBoolean(Retriever retriever, CqlColumn col) {
        String s = get(retriever, col);
        return s != null && Boolean.parseBoolean(s);
    }

    @Override public byte toByte(Retriever retriever, CqlColumn col) {
        String s = get(retriever, col);
        return s != null ? Byte.parseByte(s) : (byte) 0;
    }

    @Override public short toShort(Retriever retriever, CqlColumn col) {
        String s = get(retriever, col);
        return s != null ? Short.parseShort(s) : (short) 0;
    }

    @Override public int toInt(Retriever retriever, CqlColumn col) {
        String s = get(retriever, col);
        return s != null ? Integer.parseInt(s) : 0;
    }

    @Override public long toLong(Retriever retriever, CqlColumn col) {
        String s = get(retriever, col);
        return s != null ? Long.parseLong(s) : 0L;
    }

    @Override public char toChar(Retriever retriever, CqlColumn col) {
        String s = get(retriever, col);
        return s != null ? s.charAt(0) : 0;
    }

    @Override public float toFloat(Retriever retriever, CqlColumn col) {
        String s = get(retriever, col);
        return s != null ? Float.parseFloat(s) : 0f;
    }

    @Override public double toDouble(Retriever retriever, CqlColumn col) {
        String s = get(retriever, col);
        return s != null ? Double.parseDouble(s) : 0d;
    }

    @SuppressWarnings("unchecked") @Override public Object toObject(Retriever retriever, CqlColumn col, Class<?> targetClass) {
        String s = get(retriever, col);
        if (s == null) {
            return null;
        }
        if (targetClass == String.class) {
            return s;
        }
        if (targetClass == char[].class) {
            return s.toCharArray();
        }
        if (targetClass != null && targetClass.isEnum()) {
            return Enum.valueOf((Class<Enum>) targetClass, s);
        }
        // TODO convert to targetClass
        return s;
    }

    @Override public void fromBoolean(Binder binder, CqlColumn col, boolean value) {
        String s = Boolean.toString(value);
        set(binder, col, s);
    }

    @Override public void fromByte(Binder binder, CqlColumn col, byte value) {
        String s = Byte.toString(value);
        set(binder, col, s);
    }

    @Override public void fromShort(Binder binder, CqlColumn col, short value) {
        String s = Short.toString(value);
        set(binder, col, s);
    }

    @Override public void fromInt(Binder binder, CqlColumn col, int value) {
        String s = Integer.toString(value);
        set(binder, col, s);
    }

    @Override public void fromLong(Binder binder, CqlColumn col, long value) {
        String s = Long.toString(value);
        set(binder, col, s);
    }

    @Override public void fromChar(Binder binder, CqlColumn col, char value) {
        String s = Character.toString(value);
        set(binder, col, s);
    }

    @Override public void fromFloat(Binder binder, CqlColumn col, float value) {
        String s = Float.toString(value);
        set(binder, col, s);
    }

    @Override public void fromDouble(Binder binder, CqlColumn col, double value) {
        String s = Double.toString(value);
        set(binder, col, s);
    }

    @Override public void fromObject(Binder binder, CqlColumn col, Object value) {
        if (value == null) {
            return;
        }
        String s;
        if (value instanceof char[]) {
            s = new String((char[]) value);
        }
        // TODO convert to string
        else {
            s = value.toString();
        }
        set(binder, col, s);
    }
}
