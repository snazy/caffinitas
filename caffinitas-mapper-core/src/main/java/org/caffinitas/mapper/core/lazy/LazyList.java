/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.lazy;

import org.caffinitas.mapper.core.AbstractLazy;
import org.caffinitas.mapper.core.PersistenceSession;

import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectStreamException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Internal API.
 */
public final class LazyList<E> extends AbstractLazy<List<E>> implements List<E> {
    private static final long serialVersionUID = -5041004165971514013L;

    public LazyList(PersistenceSession session, Object rootInstance, Object instance, Object attr) {
        super(session, rootInstance, instance, attr);
    }

    private Object writeReplace() throws ObjectStreamException {
        return new ArrayList<E>(this);
    }

    private Object readResolve() throws ObjectStreamException {
        throw new NotSerializableException("deserializing of " + getClass() + " not supported");
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        throw new NotSerializableException("deserializing of " + getClass() + " not supported");
    }

    @Override protected List<E> notFound() {
        return Collections.emptyList();
    }

    @Override public int size() {return delegate().size();}

    @Override public Object[] toArray() {return delegate().toArray();}

    @Override public boolean containsAll(Collection c) {return delegate().containsAll(c);}

    @Override public List<E> subList(int fromIndex, int toIndex) {return delegate().subList(fromIndex, toIndex);}

    @Override public void add(int index, E element) {delegate().add(index, element);}

    @Override public boolean removeAll(Collection c) {return delegate().removeAll(c);}

    @SuppressWarnings("SuspiciousToArrayCall") @Override public <T> T[] toArray(T[] a) {return delegate().toArray(a);}

    @Override public boolean add(E o) {return delegate().add(o);}

    @Override public ListIterator<E> listIterator() {return delegate().listIterator();}

    @Override public boolean contains(Object o) {return delegate().contains(o);}

    @Override public E set(int index, E element) {return delegate().set(index, element);}

    @Override public boolean isEmpty() {return delegate().isEmpty();}

    @Override public boolean addAll(Collection<? extends E> c) {return delegate().addAll(c);}

    @Override public boolean addAll(int index, Collection<? extends E> c) {return delegate().addAll(index, c);}

    @Override public int indexOf(Object o) {return delegate().indexOf(o);}

    @Override public void clear() {delegate().clear();}

    @Override public ListIterator<E> listIterator(int index) {return delegate().listIterator(index);}

    @Override public boolean retainAll(Collection c) {return delegate().retainAll(c);}

    @Override public int lastIndexOf(Object o) {return delegate().lastIndexOf(o);}

    @Override public Iterator<E> iterator() {return delegate().iterator();}

    @Override public boolean remove(Object o) {return delegate().remove(o);}

    @Override public E remove(int index) {return delegate().remove(index);}

    @Override public E get(int index) {return delegate().get(index);}

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass") @Override public boolean equals(Object o) {return delegate().equals(o);}

    @Override public int hashCode() {return delegate().hashCode();}

    @Override public String toString() {return isLoaded() ? delegate().toString() : "lazy list (not loaded yet)";}
}
