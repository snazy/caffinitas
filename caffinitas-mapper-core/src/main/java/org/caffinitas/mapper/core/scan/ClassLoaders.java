/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.scan;

import com.google.common.collect.Sets;
import org.reflections.util.ClasspathHelper;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Set;

/**
 * Utility class to perform some class loader and URL filtering - use if appropiate.
 */
final class ClassLoaders {

    private ClassLoaders() {
    }

    static Set<Resolved> forResource(String resourceName, ClassLoader... classLoaders) {
        Set<Resolved> result = Sets.newHashSet();

        @SuppressWarnings("CollectionContainsUrl") Collection<URL> allUrls = null;

        for (ClassLoader classLoader : ClasspathHelper.classLoaders(classLoaders)) {
            try {
                Enumeration<URL> urls = classLoader.getResources(resourceName);
                while (urls.hasMoreElements()) {
                    URL url = urls.nextElement();
                    int index = url.toExternalForm().lastIndexOf(resourceName);
                    if (index != -1) {
                        boolean jar = "jar".equals(url.getProtocol());
                        String baseUrl;
                        String urlExtForm = url.toExternalForm();
                        if (jar) {
                            String x = url.getPath();
                            baseUrl = x.substring(0, x.length() - resourceName.length() - 2);
                        } else {
                            baseUrl = urlExtForm.substring(0, urlExtForm.length() - resourceName.length());
                        }

                        if (allUrls == null) {
                            allUrls = ClasspathHelper.forClassLoader();
                            allUrls.addAll(ClasspathHelper.forJavaClassPath());

                            //Set<String> extForms = new TreeSet<String>();
                            //for (URL u : allUrls)
                            //    extForms.add(u.toExternalForm());
                            //for (String s : extForms)
                            //    System.out.println("x: "+s);
                        }

                        for (URL clUrl : allUrls) {
                            String clExtUrl = clUrl.toExternalForm();
                            if (baseUrl.equals(clExtUrl)) {
                                result.add(new Resolved(urlExtForm, clUrl, clExtUrl));
                                break;
                            }
                        }
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException("Failed to resolve resource for " + resourceName, e);
            }
        }
        return result;
    }

}
