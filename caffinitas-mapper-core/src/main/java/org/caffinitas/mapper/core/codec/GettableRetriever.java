/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.codec;

import com.datastax.driver.core.GettableData;
import com.datastax.driver.core.Row;
import org.caffinitas.mapper.core.CqlColumn;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.sql.Time;
import java.util.Date;
import java.util.UUID;

/**
 * Internal API.
 */
public final class GettableRetriever extends Retriever {

    private GettableData src;

    public GettableRetriever(int protocolVersion, CqlColumn[] columns) {
        super(protocolVersion, columns);
    }

    public GettableRetriever(int protocolVersion, GettableData src, CqlColumn[] columns) {
        super(protocolVersion, columns);
        this.src = src;
    }

    public void setCurrentSource(GettableData src) {
        this.src = src;
    }

    @Override public DataType getType(CqlColumn col) {
        if (src instanceof Row) {
            int i = indexOf(col);
            if (i == -1) {
                return null;
            }
            return CDT.copy(protocolVersion, ((Row) src).getColumnDefinitions().getType(i));
        }

        return super.getType(col);
    }

    @Override public double getDouble(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getDouble(i) : 0d;
    }

    @Override public float getFloat(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getFloat(i) : 0f;
    }

    @Override public Date getDate(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getDate(i) : null;
    }

    @Override public java.sql.Date getSimpleDate(CqlColumn col) {
        int i = indexOf(col);
        if (i==-1) return null;
        ByteBuffer bb = src.getBytesUnsafe(i);
        if (bb==null) return null;
        return TypeCodec.simpleDateInstance.deserialize(bb);
    }

    @Override public Time getTime(CqlColumn col) {
        int i = indexOf(col);
        if (i==-1) return null;
        ByteBuffer bb = src.getBytesUnsafe(i);
        if (bb==null) return null;
        return TypeCodec.timeInstance.deserialize(bb);
    }

    @Override public long getLong(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getLong(i) : 0L;
    }

    @Override public int getInt(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getInt(i) : 0;
    }

    @Override public boolean getBool(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 && src.getBool(i);
    }

    @Override public ByteBuffer getBytes(CqlColumn col) {
        int i = indexOf(col);
        if (i == -1 || src.isNull(i)) {
            return null;
        }
        return src.getBytes(i);
    }

    @Override public String getString(CqlColumn col) {
        int i = indexOf(col);
        if (i == -1 || src.isNull(i)) {
            return null;
        }
        return src.getString(i);
    }

    @Override public BigInteger getVarint(CqlColumn col) {
        int i = indexOf(col);
        if (i == -1 || src.isNull(i)) {
            return null;
        }
        return src.getVarint(i);
    }

    @Override public BigDecimal getDecimal(CqlColumn col) {
        int i = indexOf(col);
        if (i == -1 || src.isNull(i)) {
            return null;
        }
        return src.getDecimal(i);
    }

    @Override public UUID getUUID(CqlColumn col) {
        int i = indexOf(col);
        if (i == -1 || src.isNull(i)) {
            return null;
        }
        return src.getUUID(i);
    }

    @Override public InetAddress getInet(CqlColumn col) {
        int i = indexOf(col);
        if (i == -1 || src.isNull(i)) {
            return null;
        }
        return src.getInet(i);
    }

    @Override public boolean isNull(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 && src.isNull(i);
    }

    @Override public ByteBuffer getBytesUnsafe(CqlColumn col) {
        int i = indexOf(col);
        if (i == -1 || src.isNull(i)) {
            return null;
        }
        return getBytesUnsafe(i);
    }

    private ByteBuffer getBytesUnsafe(int i) {
        return src.getBytesUnsafe(i);
    }

    @Override public ByteBuffer[] primaryKeyValues(CqlColumn[] primaryKeyColumns) {
        ByteBuffer[] pk = new ByteBuffer[primaryKeyColumns.length];
        for (int i = 0; i < pk.length; i++) {
            pk[i] = getBytesUnsafe(primaryKeyColumns[i]);
        }
        return pk;
    }
}
