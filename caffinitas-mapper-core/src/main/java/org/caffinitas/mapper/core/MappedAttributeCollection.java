/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CompositeType;
import org.caffinitas.mapper.core.api.SingleColumnConverter;
import org.caffinitas.mapper.core.api.converter.NoopConverter;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.CUDTValue;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.codec.TypeCodec;
import org.caffinitas.mapper.core.mapper.DataTypeMapperList;
import org.caffinitas.mapper.core.mapper.DataTypeMapperSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

abstract class MappedAttributeCollection<COLL_TYPE extends Collection> extends MappedAttributeSingle implements DefinitionChangedCallback {
    final Class<?> elementTypeJava;
    final Class<?> elementTypeRow;
    final boolean needsRemap;
    MappedCompositeUDT elementComposite;
    final SingleColumnConverter elementConverter;
    private DataType elementDataType;

    MappedAttributeCollection(PersistenceManagerImpl persistenceManager, DataModel model, ParseAttribute parseAttribute) {
        super(persistenceManager, parseAttribute);

        elementTypeJava = parseAttribute.collectionElementTypeJava;
        ParseComposite comp = model.composites.get(elementTypeJava);
        elementTypeRow =
            comp != null && comp.composite.compositeType() == CompositeType.USER_TYPE ? CUDTValue.class :
                parseAttribute.collectionDataType.asJavaClass();
        elementConverter = persistenceManager.converterFor(SingleColumnConverter.class,
            parseAttribute.list != null ?
                parseAttribute.list.element().converter() :
                (parseAttribute.set != null ? parseAttribute.set.element().converter() : NoopConverter.class),
            elementTypeJava);
        needsRemap = elementTypeJava != elementTypeRow;
    }

    @Override void setupClassColumns(PersistenceManagerImpl persistenceManager, MappedObject container,
                                     List<CqlColumn> cqlColumns, DataModel model,
                                     ParseAttribute parseAttribute, AttrOverrideMap attributeOverrides,
                                     NameMapper nameMapper,
                                     String columnNamePrefix) {
        super.setupClassColumns(persistenceManager, container, cqlColumns, model, parseAttribute, attributeOverrides, nameMapper, columnNamePrefix);

        elementComposite = persistenceManager.getComposite(elementTypeJava, MappedCompositeUDT.class);
        if (elementComposite != null) {
            elementComposite.addDefinitionChangedCallback(this);
            elementDataType = elementComposite.dataType;
        } else {
            elementDataType = parseAttribute.collectionDataType;
        }

        updateSingleColumn(persistenceManager, parseAttribute.dataType.getName());
    }

    @Override public void definitionChanged(PersistenceManager persistenceManager, MappedClassObject classObject) {
        if (elementComposite != null) {
            elementDataType = elementComposite.dataType;
        }
        updateSingleColumn((PersistenceManagerImpl) persistenceManager, singleColumn().dataType.getName());
    }

    @SuppressWarnings("unchecked") private void updateSingleColumn(PersistenceManagerImpl persistenceManager, DataType.Name dataTypeName) {
        CqlColumn col = singleColumn();
        if (dataTypeName == DataType.Name.SET) {
            col.dataType = DataType.set(elementDataType);
            col.dataTypeMapper = new DataTypeMapperSet(TypeCodec.setOf(elementDataType, persistenceManager.protocolVersion));
        } else if (dataTypeName == DataType.Name.LIST) {
            col.dataType = DataType.list(elementDataType);
            col.dataTypeMapper = new DataTypeMapperList(TypeCodec.listOf(elementDataType, persistenceManager.protocolVersion));
        }
    }

    @SuppressWarnings("unchecked") @Override void bindSingleValueToStatement(Object instance, Binder binder, CqlColumn col) {
        if (needsRemap) {
            Object val = accessor.getAny(instance);
            if (binder.skipValue(instance, val)) {
                return;
            }
            if (val != null) {
                val = remapToRow((COLL_TYPE) val);
            }
            col.dataTypeMapper.fromObject(binder, col, val);
        } else {
            super.bindSingleValueToStatement(instance, binder, col);
        }
    }

    @Override boolean fromRow(PersistenceSessionImpl session, Object rootInstance, Object instance, Retriever retriever) {
        if (needsRemap) {
            CqlColumn col = singleColumn();
            if (!retriever.contains(col)) {
                return true;
            }
            Object data = col.dataTypeMapper.toObject(retriever, col, null);
            @SuppressWarnings("unchecked") Object conv = data != null ? remapFromRow((COLL_TYPE) data, rootInstance, session) : null;
            accessor.setAny(instance, conv);
            return conv == null;
        } else {
            return super.fromRow(session, rootInstance, instance, retriever);
        }
    }

    @SuppressWarnings("unchecked") protected COLL_TYPE remapToRow(COLL_TYPE src) {
        if (src.isEmpty()) {
            return emptyCollection();
        }
        COLL_TYPE conv = newCollectionInstance(src.size());
        for (Object o : src) {
            if (elementConverter != null) {
                o = elementConverter.fromJava(o);
                conv.add(o);
            } else if (elementComposite != null) {
                conv.add(elementComposite.serializeAny(o));
            } else if (elementTypeRow == String.class) {
                conv.add(anyToString(o));
            } else if (elementTypeRow == Integer.class) {
                conv.add(anyToInt(o));
            } else {
                throw new ModelUseException("unsupported collection element types : " + elementTypeRow + " vs. " + elementTypeJava);
            }
        }
        return conv;
    }

    @SuppressWarnings("unchecked") protected COLL_TYPE remapFromRow(COLL_TYPE src, Object rootInstance, PersistenceSessionImpl session) {
        // do not return a Collections.empty*() to make the returned object modifyable by the application
        COLL_TYPE conv = newCollectionInstance(src.size());
        for (Object o : src) {
            if (elementConverter != null) {
                o = elementConverter.toJava(o);
                conv.add(o);
            } else if (elementComposite != null) {
                conv.add(elementComposite.deserializeAny(o, rootInstance, session, elementTypeJava));
            } else if (elementTypeRow == String.class) {
                conv.add(stringToAny((String) o, elementTypeJava));
            } else if (elementTypeRow == Integer.class) {
                conv.add(intToAny((Integer) o, elementTypeJava));
            } else {
                throw new ModelUseException("unsupported collection element types : " + elementTypeRow + " vs. " + elementTypeJava);
            }
        }
        return conv;
    }

    protected abstract COLL_TYPE newCollectionInstance(int srcSize);

    protected abstract COLL_TYPE emptyCollection();

    @Override public Set<MappedSchemaObject<?>> collectSchemaDependencies() {
        return elementComposite != null ?
            Collections.<MappedSchemaObject<?>>singleton(elementComposite) :
            Collections.<MappedSchemaObject<?>>emptySet();
    }
}
