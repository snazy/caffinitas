/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.accessors;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.DataTypeMapper;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;

/**
 * Internal API.
 */
public abstract class AbstractAttributeAccessor implements AttributeAccessor {

    @Override public void bind(DataTypeMapper dataTypeMapper, Binder binder, CqlColumn col, Object instance) {
        Class<?> t = type();
        if (t == boolean.class) {
            dataTypeMapper.fromBoolean(binder, col, getBoolean(instance));
        } else if (t == byte.class) {
            dataTypeMapper.fromByte(binder, col, getByte(instance));
        } else if (t == short.class) {
            dataTypeMapper.fromShort(binder, col, getShort(instance));
        } else if (t == int.class) {
            dataTypeMapper.fromInt(binder, col, getInt(instance));
        } else if (t == long.class) {
            dataTypeMapper.fromLong(binder, col, getLong(instance));
        } else if (t == char.class) {
            dataTypeMapper.fromChar(binder, col, getChar(instance));
        } else if (t == float.class) {
            dataTypeMapper.fromFloat(binder, col, getFloat(instance));
        } else if (t == double.class) {
            dataTypeMapper.fromDouble(binder, col, getDouble(instance));
        } else {
            dataTypeMapper.fromObject(binder, col, getObject(instance));
        }
    }

    @Override public boolean fromRow(DataTypeMapper dataTypeMapper, Retriever row, CqlColumn col, Object instance) {
        Class<?> t = type();
        if (t == boolean.class) {
            setBoolean(instance, dataTypeMapper.toBoolean(row, col));
        } else if (t == byte.class) {
            setByte(instance, dataTypeMapper.toByte(row, col));
        } else if (t == short.class) {
            setShort(instance, dataTypeMapper.toShort(row, col));
        } else if (t == int.class) {
            setInt(instance, dataTypeMapper.toInt(row, col));
        } else if (t == long.class) {
            setLong(instance, dataTypeMapper.toLong(row, col));
        } else if (t == char.class) {
            setChar(instance, dataTypeMapper.toChar(row, col));
        } else if (t == float.class) {
            setFloat(instance, dataTypeMapper.toFloat(row, col));
        } else if (t == double.class) {
            setDouble(instance, dataTypeMapper.toDouble(row, col));
        } else {
            return setObject(instance, dataTypeMapper.toObject(row, col, t));
        }
        return false;
    }

    @Override public Object getAny(Object instance) {
        Class<?> t = type();
        if (t == boolean.class) {
            return getBoolean(instance);
        } else if (t == byte.class) {
            return getByte(instance);
        } else if (t == short.class) {
            return getShort(instance);
        } else if (t == int.class) {
            return getInt(instance);
        } else if (t == long.class) {
            return getLong(instance);
        } else if (t == char.class) {
            return getChar(instance);
        } else if (t == float.class) {
            return getFloat(instance);
        } else if (t == double.class) {
            return getDouble(instance);
        } else {
            return getObject(instance);
        }
    }

    @Override public boolean setAny(Object instance, Object value) {
        Class<?> t = type();
        if (t == boolean.class) {
            setBoolean(instance, value == null ? false : (Boolean) value);
        } else if (t == byte.class) {
            setByte(instance, value == null ? 0 : (Byte) value);
        } else if (t == short.class) {
            setShort(instance, value == null ? 0 : (Short) value);
        } else if (t == int.class) {
            setInt(instance, value == null ? 0 : (Integer) value);
        } else if (t == long.class) {
            setLong(instance, value == null ? 0L : (Long) value);
        } else if (t == char.class) {
            setChar(instance, value == null ? 0 : (Character) value);
        } else if (t == float.class) {
            setFloat(instance, value == null ? 0f : (Float) value);
        } else if (t == double.class) {
            setDouble(instance, value == null ? 0d : (Double) value);
        } else {
            return setObject(instance, value);
        }
        return false;
    }
}
