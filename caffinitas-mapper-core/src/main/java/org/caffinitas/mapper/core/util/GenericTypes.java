/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;

/**
 * Java generic types utility functions.
 */
public final class GenericTypes {
    private GenericTypes() {}

    /**
     * Extract the class type from a field or method parameter / argument type.
     */
    @SuppressWarnings("unchecked")
    public static <T> Class<T> getGenericType(Type type, int index, int expectedTypes) {
        if (type instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) type;
            Type[] typeArgs = pType.getActualTypeArguments();
            if (typeArgs == null || expectedTypes != typeArgs.length) {
                throw new IllegalArgumentException(
                    "type " + type + " has not the expected amount of actual type arguments (" + expectedTypes +
                        ')'
                );
            }
            Type typeArg = typeArgs[index];
            return getClassFromType(typeArg);
        } else {
            throw new IllegalArgumentException("unknown type class " + type);
        }
    }

    /**
     * Extract the class type from an argument of a {@link java.lang.reflect.ParameterizedType}.
     */
    @SuppressWarnings("unchecked")
    public static <T> Class<T> getClassFromType(Type typeArg) {
        if (typeArg instanceof Class<?>) {
            return (Class<T>) typeArg;
        }
        if (typeArg instanceof WildcardType) {
            WildcardType wType = (WildcardType) typeArg;
            Type[] lower = wType.getLowerBounds();
            if (lower.length < 1) {
                Type[] upper = wType.getUpperBounds();
                if (upper.length < 1) {
                    return (Class<T>) Object.class;
                }
                return (Class<T>) upper[0];
            }
            return (Class<T>) lower[0];
        }
        throw new IllegalArgumentException("unknown type class " + typeArg);
    }
}
