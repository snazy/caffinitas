/*
 *      Copyright (C) 2012 DataStax Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.caffinitas.mapper.core.codec;

import java.nio.ByteBuffer;

// We don't want to expose this one: it's less useful externally and it's a bit ugly to expose anyway (but it's convenient).
abstract class AbstractData<T> extends AbstractGettableData {

    final T wrapped;
    final ByteBuffer[] values;

    // Ugly, we coould probably clean that: it is currently needed however because we sometimes
    // want wrapped to be 'this' (UDTValue), and sometimes some other object (in BoundStatement).
    @SuppressWarnings("unchecked")
    protected AbstractData(int protocolVersion, int size) {
        super(protocolVersion);
        this.wrapped = (T) this;
        this.values = new ByteBuffer[size];
    }

    private T setValue(int i, ByteBuffer value) {
        values[i] = value;
        return wrapped;
    }

    @Override protected ByteBuffer getValue(int i) {
        return values[i];
    }

    void setBytesUnsafe(int i, ByteBuffer v) {
        setValue(i, v == null ? null : v.duplicate());
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof CUDTValue)) {
            return false;
        }

        AbstractData<?> that = (AbstractData<?>) o;
        if (values.length != that.values.length) {
            return false;
        }

        // Deserializing each value is slightly inefficient, but comparing
        // the bytes could in theory be wrong (for varint for instance, 2 values
        // can have different binary representation but be the same value due to
        // leading zeros). So we don't take any risk.
        for (int i = 0; i < values.length; i++) {
            DataType thisType = getType(i);
            DataType thatType = that.getType(i);
            if (!thisType.equals(thatType)) {
                return false;
            }

            if ((values[i] == null) != (that.values[i] == null)) {
                return false;
            }

            if (values[i] != null && !bbEquals(values[i], that.values[i])) {
                return false;
            }
        }
        return true;
    }

    static boolean bbEquals(ByteBuffer b1, ByteBuffer b2) {
        return b1 == null && b2 == null || !(b1 == null || b2 == null) && b1.equals(b2);
    }

    @Override
    public int hashCode() {
        // Same as equals
        int hash = 31;
        for (ByteBuffer value : values) {
            hash += value == null ? 1 : value.hashCode();
        }
        return hash;
    }
}
