/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.Uninterruptibles;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.caffinitas.mapper.core.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

final class ModifyFutureImpl<T> extends AbstractDelegateFutureExt<T> implements ModifyFuture<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModifyFutureImpl.class);

    private final PersistenceSessionImpl persistenceSession;
    private final ExecutionTracer tracer;
    private final MappedEntityBase entity;
    private final T instance;
    private final PersistMode mode;
    private final Binder binder;

    ModifyFutureImpl(PersistenceSessionImpl persistenceSession, ExecutionTracer tracer, Binder binder, ResultSetFuture delegate,
                     MappedEntityBase entity, T instance, PersistMode mode) {
        super(delegate);
        this.persistenceSession = persistenceSession;
        this.tracer = tracer;
        this.entity = entity;
        this.instance = instance;
        this.mode = mode;
        this.binder = binder;
    }

    @Override protected T wrapResultSet(ResultSet resultSet) {
        Row row = resultSet.one();
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("got {} row for {} modify {}", row != null ? "1" : "no", mode, entity.type);
        }

        if (tracer != null) {
            tracer.onModifyWrapResultSet(persistenceSession, entity, mode, resultSet);
        }

        if (row != null) {
            int idx = row.getColumnDefinitions().getIndexOf("[applied]");
            if (idx != -1 && !row.getBool(idx)) {
                throw new CasNotAppliedException(entity, instance, mode, row);
            }
        }
        persistenceSession.modified(binder, mode, entity, instance);

        Interceptors postModify = mode == PersistMode.DELETE ? entity.postDelete : entity.postPersist;
        if (postModify != null) {
            postModify.invoke(instance, entity, mode);
        }
        return null;
    }

    public static <T> ModifyFuture<T> immediate() {
        return new Immediate<T>();
    }

    static class Immediate<T> implements ModifyFuture<T> {
        private final ListenableFuture<T> delegate = Futures.immediateFuture(null);

        @Override public T getUninterruptibly() {
            try {
                return Uninterruptibles.getUninterruptibly(delegate);
            } catch (ExecutionException e) {
                Exceptions.throwImmediate(e);
                return null;
            }
        }

        @Override public T getUninterruptibly(long timeout, TimeUnit unit) throws TimeoutException {
            try {
                return Uninterruptibles.getUninterruptibly(delegate);
            } catch (ExecutionException e) {
                Exceptions.throwImmediate(e);
                return null;
            }
        }

        @Override public void addListener(Runnable listener, Executor executor) {
            delegate.addListener(listener, executor);
        }

        @Override public boolean cancel(boolean mayInterruptIfRunning) {
            return delegate.cancel(mayInterruptIfRunning);
        }

        @Override public boolean isCancelled() {
            return delegate.isCancelled();
        }

        @Override public boolean isDone() {
            return delegate.isDone();
        }

        @Override public T get() throws InterruptedException, ExecutionException {
            return delegate.get();
        }

        @Override public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return delegate.get(timeout, unit);
        }
    }
}
