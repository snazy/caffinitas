/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.policies.RetryPolicy;

final class StatementOptions implements Cloneable {
    ConsistencyLevel readConsistencyLevel;
    ConsistencyLevel writeConsistencyLevel;
    ConsistencyLevel serialConsistencyLevel;
    RetryPolicy retryPolicy;
    int fetchSize = -1;
    boolean tracing;

    void applyRead(Statement statement, ConsistencyLevel readConsistencyLevel, PersistOption... persistOptions) {
        if (fetchSize >= 0) {
            statement.setFetchSize(fetchSize);
        }
        if (retryPolicy != null) {
            statement.setRetryPolicy(retryPolicy);
        }
        if (readConsistencyLevel != null) {
            statement.setConsistencyLevel(readConsistencyLevel);
        } else if (!PersistOption.ConsistencyLevelOption.apply(statement, persistOptions) && this.readConsistencyLevel != null) {
            statement.setConsistencyLevel(this.readConsistencyLevel);
        }
        if (tracing) {
            statement.enableTracing();
        }
    }

    void applyWrite(Statement statement, ConsistencyLevel writeConsistencyLevel, ConsistencyLevel serialConsistencyLevel, PersistOption... persistOptions) {
        if (writeConsistencyLevel != null) {
            statement.setConsistencyLevel(writeConsistencyLevel);
        } else if (!PersistOption.ConsistencyLevelOption.apply(statement, persistOptions) && this.writeConsistencyLevel != null) {
            statement.setConsistencyLevel(this.writeConsistencyLevel);
        }
        if (serialConsistencyLevel != null) {
            statement.setConsistencyLevel(serialConsistencyLevel);
        } else if (!PersistOption.SerialConsistencyLevelOption.apply(statement, persistOptions) && this.serialConsistencyLevel != null) {
            statement.setSerialConsistencyLevel(this.serialConsistencyLevel);
        }
        if (tracing) {
            statement.enableTracing();
        }
    }

    public ConsistencyLevel getReadConsistencyLevel() {
        return readConsistencyLevel;
    }

    public void setReadConsistencyLevel(ConsistencyLevel readConsistencyLevel) {
        this.readConsistencyLevel = readConsistencyLevel;
    }

    public ConsistencyLevel getWriteConsistencyLevel() {
        return writeConsistencyLevel;
    }

    public void setWriteConsistencyLevel(ConsistencyLevel writeConsistencyLevel) {
        this.writeConsistencyLevel = writeConsistencyLevel;
    }

    public ConsistencyLevel getSerialConsistencyLevel() {
        return serialConsistencyLevel;
    }

    public void setSerialConsistencyLevel(ConsistencyLevel serialConsistencyLevel) {
        this.serialConsistencyLevel = serialConsistencyLevel;
    }

    public RetryPolicy getRetryPolicy() {
        return retryPolicy;
    }

    public void setRetryPolicy(RetryPolicy retryPolicy) {
        this.retryPolicy = retryPolicy;
    }

    public int getFetchSize() {
        return fetchSize;
    }

    public void setFetchSize(int fetchSize) {
        this.fetchSize = fetchSize;
    }

    public void setTracing(boolean tracing) {
        this.tracing = tracing;
    }

    public boolean isTracing() {
        return tracing;
    }

    @Override protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public StatementOptions copy() {
        try {
            return (StatementOptions) clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
