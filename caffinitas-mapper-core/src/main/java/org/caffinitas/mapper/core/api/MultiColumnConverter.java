/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.api;

/**
 * Support for conversion of <i>any</i> Java class to <i>any</i> Cassandra type using multiple columns.
 *
 * @param <J> Java type
 */
public interface MultiColumnConverter<J> extends Converter<J> {

    /**
     * Column types used in Cassandra.
     * Each return class must be a supported Cassandra type (must be compatible with data type configured using {@link org.caffinitas.mapper.annotations.CColumn})
     */
    Class<?>[] cassandraTypes();

    /**
     * Pseudo property names assumed for each column.
     * Useful when used in cunjunction with {@link org.caffinitas.mapper.annotations.CAttributeOverrides}.
     *
     * @return array with property names - safe to return {@code null} for single-column converters
     */
    String[] propertyNames();

    /**
     * Convert Cassandra types to Java type.
     */
    J toJava(Object... value);

    /**
     * Convert Java type to Cassandra types.
     */
    Object[] fromJava(J value);
}
