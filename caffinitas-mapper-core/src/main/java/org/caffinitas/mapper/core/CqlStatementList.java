/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains CQL CREATE/ALTER DDL statements including errors and warnings.
 */
public final class CqlStatementList {
    private final List<String> errors = new ArrayList<String>();
    private final List<String> warnings = new ArrayList<String>();
    private final List<String> statements = new ArrayList<String>();

    public void addError(String err) {
        errors.add(err);
    }

    public void addWarning(String warn) {
        warnings.add(warn);
    }

    public void addStatement(String stmt) {
        if (stmt != null && !stmt.isEmpty()) {
            statements.add(stmt);
        }
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    public List<String> getErrors() {
        return errors;
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public List<String> getStatements() {
        return statements;
    }

    @Override public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String error : errors) {
            sb.append(error).append('\n');
        }
        for (String warn : warnings) {
            sb.append(warn).append('\n');
        }
        for (String stmt : statements) {
            sb.append(stmt).append('\n');
        }
        return sb.toString();
    }

    /**
     * Add the content of the given container into this container.
     *
     * @param stmts source
     */
    public void merge(CqlStatementList stmts) {
        if (stmts == null) {
            return;
        }
        this.errors.addAll(stmts.errors);
        this.warnings.addAll(stmts.warnings);
        this.statements.addAll(stmts.statements);
    }

    /**
     * Execute the statements in this container against the provided session.
     *
     * @param session sesion to execute the statements against
     */
    public void execute(Session session) {
        if (hasErrors()) {
            throw new PersistenceRuntimeException("this statement list contains errrors: " + errors);
        }

        for (String stmt : statements) {
            session.execute(stmt);
        }
    }
}
