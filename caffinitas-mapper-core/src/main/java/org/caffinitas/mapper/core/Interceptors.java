/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.util.ArrayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

final class Interceptors {
    static final Logger LOGGER = LoggerFactory.getLogger(Interceptors.class);

    private static final Endpoint[] NO_METHODS = {};

    private Endpoint[] methods = NO_METHODS;

    boolean isEmpty() { return methods.length == 0; }

    void addHandler(Method method) {
        // check modifiers
        int modifiers = method.getModifiers();
        if ((modifiers & Modifier.PUBLIC) == 0) {
            throw new ModelParseException("method " + method + " must be public");
        }
        if ((modifiers & Modifier.STATIC) != 0) {
            throw new ModelParseException("method " + method + " must not be static");
        }
        if ((modifiers & Modifier.ABSTRACT) != 0) {
            throw new ModelParseException("method " + method + " must not be abstract");
        }

        methods = ArrayUtil.add2(methods, new Endpoint(method));
    }

    void invoke(Object instance, Object... arguments) {
        for (Endpoint method : methods) {
            method.invoke(instance, arguments);
        }
    }

    static final Object[] NO_ARGS = {};

    private class Endpoint {
        private final Method method;
        private final Class<?>[] params;

        private Endpoint(Method method) {
            this.method = method;
            this.params = method.getParameterTypes();
        }

        void invoke(Object instance, Object[] arguments) {
            if (params.length == 0) {
                doInvoke(instance, NO_ARGS);
                return;
            }

            Object[] args = new Object[params.length];
            for (Object argument : arguments) {
                for (int i = 0; i < params.length; i++) {
                    Class<?> param = params[i];
                    if (param.isInstance(argument)) {
                        args[i] = argument;
                        break;
                    }
                }
            }

            doInvoke(instance, args);
        }

        private void doInvoke(Object instance, Object[] args) {
            try {
                method.invoke(instance, args);
            } catch (Throwable e) {
                LOGGER.warn("Failed to invoke {}", instance, e);
            }
        }
    }
}
