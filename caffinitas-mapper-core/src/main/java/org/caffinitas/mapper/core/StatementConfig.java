/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.policies.RetryPolicy;

/**
 * Support for general statement configuration.
 *
 * @param <C> type of extending interface
 */
public interface StatementConfig<C> {

    /**
     * Configures fetch size for selects.
     */
    C setFetchSize(int fetchSize);

    /**
     * Configures fetch size for selects.
     */
    int getFetchSize();

    /**
     * Configures retry policy for statements.
     */
    C setRetryPolicy(RetryPolicy retryPolicy);

    /**
     * Configures retry policy for statements.
     */
    RetryPolicy getRetryPolicy();

    /**
     * Configures serial consistency for statements.
     */
    C setSerialConsistencyLevel(ConsistencyLevel serialConsistencyLevel);

    /**
     * Configures serial consistency for statements.
     */
    ConsistencyLevel getSerialConsistencyLevel();

    /**
     * Configures write consistency for statements.
     */
    C setWriteConsistencyLevel(ConsistencyLevel writeConsistencyLevel);

    /**
     * Configures write consistency for statements.
     */
    ConsistencyLevel getWriteConsistencyLevel();

    /**
     * Configures read consistency for statements.
     */
    C setReadConsistencyLevel(ConsistencyLevel readConsistencyLevel);

    /**
     * Configures read consistency for statements.
     */
    ConsistencyLevel getReadConsistencyLevel();

    C setTracing(boolean tracing);

    boolean isTracing();

}
