/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.accessors;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Internal API.
 */
final class ReflectionMethodAccessor extends AbstractAttributeAccessor {
    private final Method getter;
    private final Method setter;

    public ReflectionMethodAccessor(Method getter, Method setter) {
        if (!Modifier.isPublic(getter.getModifiers())) {
            getter.setAccessible(true);
        }
        if (!Modifier.isPublic(setter.getModifiers())) {
            setter.setAccessible(true);
        }
        this.getter = getter;
        this.setter = setter;
    }

    @Override public AccessibleObject accessible() {
        return getter;
    }

    @Override public Class<?> type() {
        return getter.getReturnType();
    }

    @Override public boolean getBoolean(Object instance) {
        return (Boolean)getObject(instance);
    }

    @Override public byte getByte(Object instance) {
        return (Byte)getObject(instance);
    }

    @Override public char getChar(Object instance) {
        return (Character)getObject(instance);
    }

    @Override public short getShort(Object instance) {
        return (Short)getObject(instance);
    }

    @Override public int getInt(Object instance) {
        return (Integer)getObject(instance);
    }

    @Override public long getLong(Object instance) {
        return (Long)getObject(instance);
    }

    @Override public float getFloat(Object instance) {
        return (Float)getObject(instance);
    }

    @Override public double getDouble(Object instance) {
        return (Double)getObject(instance);
    }

    @SuppressWarnings("unchecked") @Override public <T> T getObject(Object instance) {
        try {
            return (T) getter.invoke(instance);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void setBoolean(Object instance, boolean value) {
        setObject(instance, value);
    }

    @Override public void setByte(Object instance, byte value) {
        setObject(instance, value);
    }

    @Override public void setChar(Object instance, char value) {
        setObject(instance, value);
    }

    @Override public void setShort(Object instance, short value) {
        setObject(instance, value);
    }

    @Override public void setInt(Object instance, int value) {
        setObject(instance, value);
    }

    @Override public void setLong(Object instance, long value) {
        setObject(instance, value);
    }

    @Override public void setFloat(Object instance, float value) {
        setObject(instance, value);
    }

    @Override public void setDouble(Object instance, double value) {
        setObject(instance, value);
    }

    @Override public boolean setObject(Object instance, Object value) {
        try {
            setter.invoke(value);
            return value == null;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
