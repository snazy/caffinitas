/*
 *      Copyright (C) 2012 DataStax Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.caffinitas.mapper.core.codec;

import com.datastax.driver.core.exceptions.InvalidTypeException;
import com.datastax.driver.core.utils.Bytes;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.Time;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Internal API.
 */
public abstract class TypeCodec<T> {

    // Somehow those don't seem to get properly initialized if they're not here. The reason
    // escape me right now so let's just leave it here for now
    public static final StringCodec utf8Instance = new StringCodec(Charset.forName("UTF-8"));
    public static final StringCodec asciiInstance = new StringCodec(Charset.forName("US-ASCII"));
    public static final LongCodec longInstance = new LongCodec();
    public static final IntCodec intInstance = new IntCodec();
    public static final DateCodec dateInstance = new DateCodec();
    public static final SimpleDateCodec simpleDateInstance = new SimpleDateCodec();
    public static final TimeCodec timeInstance = new TimeCodec();
    public static final UUIDCodec uuidInstance = new UUIDCodec();
    public static final TimeUUIDCodec timeUUIDInstance = new TimeUUIDCodec();
    public static final BigIntegerCodec bigIntegerInstance = new BigIntegerCodec();
    public static final BytesCodec bytesInstance = new BytesCodec();
    public static final BooleanCodec booleanInstance = new BooleanCodec();
    public static final DecimalCodec decimalInstance = new DecimalCodec();
    public static final DoubleCodec doubleInstance = new DoubleCodec();
    public static final FloatCodec floatInstance = new FloatCodec();
    public static final InetCodec inetInstance = new InetCodec();

    private static final Map<DataType.Name, TypeCodec<?>> primitiveCodecs = new EnumMap<DataType.Name, TypeCodec<?>>(DataType.Name.class);

    static {
        primitiveCodecs.put(DataType.Name.ASCII, asciiInstance);
        primitiveCodecs.put(DataType.Name.BIGINT, longInstance);
        primitiveCodecs.put(DataType.Name.BLOB, bytesInstance);
        primitiveCodecs.put(DataType.Name.BOOLEAN, booleanInstance);
        primitiveCodecs.put(DataType.Name.COUNTER, longInstance);
        primitiveCodecs.put(DataType.Name.DECIMAL, decimalInstance);
        primitiveCodecs.put(DataType.Name.DOUBLE, doubleInstance);
        primitiveCodecs.put(DataType.Name.FLOAT, floatInstance);
        primitiveCodecs.put(DataType.Name.INET, inetInstance);
        primitiveCodecs.put(DataType.Name.INT, intInstance);
        primitiveCodecs.put(DataType.Name.TEXT, utf8Instance);
        primitiveCodecs.put(DataType.Name.TIMESTAMP, dateInstance);
        primitiveCodecs.put(DataType.Name.DATE, simpleDateInstance);
        primitiveCodecs.put(DataType.Name.TIME, timeInstance);
        primitiveCodecs.put(DataType.Name.UUID, uuidInstance);
        primitiveCodecs.put(DataType.Name.VARCHAR, utf8Instance);
        primitiveCodecs.put(DataType.Name.VARINT, bigIntegerInstance);
        primitiveCodecs.put(DataType.Name.TIMEUUID, timeUUIDInstance);
        primitiveCodecs.put(DataType.Name.CUSTOM, bytesInstance);
    }

    private static class PrimitiveCollectionCodecs {
        public final Map<DataType.Name, TypeCodec<List<?>>> primitiveListsCodecs = new EnumMap<DataType.Name, TypeCodec<List<?>>>(DataType.Name.class);
        public final Map<DataType.Name, TypeCodec<Set<?>>> primitiveSetsCodecs = new EnumMap<DataType.Name, TypeCodec<Set<?>>>(DataType.Name.class);
        public final Map<DataType.Name, Map<DataType.Name, TypeCodec<Map<?, ?>>>> primitiveMapsCodecs = new EnumMap<DataType.Name, Map<DataType.Name, TypeCodec<Map<?, ?>>>>(DataType.Name.class);

        @SuppressWarnings({"unchecked", "rawtypes"})
        public PrimitiveCollectionCodecs(int protocolVersion) {
            for (Map.Entry<DataType.Name, TypeCodec<?>> entry : primitiveCodecs.entrySet()) {
                DataType.Name type = entry.getKey();
                TypeCodec<?> codec = entry.getValue();
                if (codec == null) {
                    throw new ExceptionInInitializerError("codec==null for " + type);
                }
                primitiveListsCodecs.put(type, new ListCodec(codec, protocolVersion));
                primitiveSetsCodecs.put(type, new SetCodec(codec, protocolVersion));
                Map<DataType.Name, TypeCodec<Map<?, ?>>> valueMap = new EnumMap<DataType.Name, TypeCodec<Map<?, ?>>>(DataType.Name.class);
                for (Map.Entry<DataType.Name, TypeCodec<?>> valueEntry : primitiveCodecs.entrySet()) {
                    if (valueEntry.getValue() == null) {
                        throw new ExceptionInInitializerError("codec==null for " + valueEntry.getKey());
                    }
                    valueMap.put(valueEntry.getKey(), new MapCodec(codec, valueEntry.getValue(), protocolVersion));
                }
                primitiveMapsCodecs.put(type, valueMap);
            }
        }
    }

    private static final PrimitiveCollectionCodecs primitiveCollectionCodecsV2 = new PrimitiveCollectionCodecs(2);
    private static final PrimitiveCollectionCodecs primitiveCollectionCodecsV3 = new PrimitiveCollectionCodecs(3);

    private TypeCodec() {}

    public abstract T parse(String value);

    public abstract ByteBuffer serialize(T value);

    public abstract T deserialize(ByteBuffer bytes);

    @SuppressWarnings("unchecked")
    public static <T> TypeCodec<T> createFor(DataType.Name name) {
        assert !name.isCollection();
        return (TypeCodec<T>) primitiveCodecs.get(name);
    }

    @SuppressWarnings("unchecked")
    public static <T> TypeCodec<List<T>> listOf(DataType arg, int protocolVersion) {
        PrimitiveCollectionCodecs codecs = protocolVersion <= 2 ? primitiveCollectionCodecsV2 : primitiveCollectionCodecsV3;
        TypeCodec<List<?>> codec = codecs.primitiveListsCodecs.get(arg.getName());
        return codec != null ? (TypeCodec) codec : new ListCodec(arg.codec(protocolVersion), protocolVersion);
    }

    @SuppressWarnings("unchecked")
    public static <T> TypeCodec<Set<T>> setOf(DataType arg, int protocolVersion) {
        PrimitiveCollectionCodecs codecs = protocolVersion <= 2 ? primitiveCollectionCodecsV2 : primitiveCollectionCodecsV3;
        TypeCodec<Set<?>> codec = codecs.primitiveSetsCodecs.get(arg.getName());
        return codec != null ? (TypeCodec) codec : new SetCodec(arg.codec(protocolVersion), protocolVersion);
    }

    @SuppressWarnings("unchecked")
    public static <K, V> TypeCodec<Map<K, V>> mapOf(DataType keys, DataType values, int protocolVersion) {
        PrimitiveCollectionCodecs codecs = protocolVersion <= 2 ? primitiveCollectionCodecsV2 : primitiveCollectionCodecsV3;
        Map<DataType.Name, TypeCodec<Map<?, ?>>> valueCodecs = codecs.primitiveMapsCodecs.get(keys.getName());
        TypeCodec<Map<?, ?>> codec = valueCodecs == null ? null : valueCodecs.get(values.getName());
        return codec != null ? (TypeCodec) codec : new MapCodec(keys.codec(protocolVersion), values.codec(protocolVersion), protocolVersion);
    }

    public static UDTCodec udtOf(CUDTDefinition definition) {
        return new UDTCodec(definition);
    }

    static TupleCodec tupleOf(int protocolVersion, DataType[] types) {
        return new TupleCodec(protocolVersion, types);
    }

    private static ByteBuffer pack(List<ByteBuffer> buffers, int elements, int version) {
        int size = 0;
        for (ByteBuffer bb : buffers) {
            size += sizeOfValue(bb, version);
        }

        ByteBuffer result = ByteBuffer.allocate(sizeOfCollectionSize(version) + size);
        writeCollectionSize(result, elements, version);
        for (ByteBuffer bb : buffers) {
            writeCollectionValue(result, bb, version);
        }
        return (ByteBuffer) result.flip();
    }

    private static void writeCollectionSize(ByteBuffer output, int elements, int version) {
        if (version >= 3)
            output.putInt(elements);
        else if (elements > 65535)
            throw new IllegalArgumentException("Native protocol version 2 supports up to 65535 elements in any collection - but collection contains "+elements+" elements");
        else
            output.putShort((short) elements);
    }

    private static int getUnsignedShort(ByteBuffer bb) {
        int length = (bb.get() & 0xFF) << 8;
        return length | (bb.get() & 0xFF);
    }

    private static int readCollectionSize(ByteBuffer input, int version) {
        return version >= 3 ? input.getInt() : getUnsignedShort(input);
    }

    private static int sizeOfCollectionSize(int version) {
        return version >= 3 ? 4 : 2;
    }

    private static void writeCollectionValue(ByteBuffer output, ByteBuffer value, int version) {
        if (version >= 3) {
            if (value == null) {
                output.putInt(-1);
                return;
            }

            output.putInt(value.remaining());
            output.put(value.duplicate());
        } else {
            assert value != null;
            int remain = value.remaining();
            if (remain > Short.MAX_VALUE) {
                throw new IllegalArgumentException("too many remaining elements - native protocol v2 only supports 32767 - v3 much more");
            }
            output.putShort((short) remain);
            output.put(value.duplicate());
        }
    }

    private static ByteBuffer readBytes(ByteBuffer bb, int length) {
        ByteBuffer copy = bb.duplicate();
        copy.limit(copy.position() + length);
        bb.position(bb.position() + length);
        return copy;
    }

    private static ByteBuffer readCollectionValue(ByteBuffer input, int version) {
        int size = version >= 3 ? input.getInt() : getUnsignedShort(input);
        return size < 0 ? null : readBytes(input, size);
    }

    private static int sizeOfValue(ByteBuffer value, int version) {
        return version >= 3
            ? (value == null ? 4 : 4 + value.remaining())
            : 2 + value.remaining();
    }

    static class StringCodec extends TypeCodec<String> {

        private final Charset charset;

        private StringCodec(Charset charset) {
            this.charset = charset;
        }

        @Override
        public String parse(String value) {
            return value;
        }

        @Override
        public ByteBuffer serialize(String value) {
            return ByteBuffer.wrap(value.getBytes(charset));
        }

        @Override
        public String deserialize(ByteBuffer bytes) {
            return new String(Bytes.getArray(bytes), charset);
        }
    }

    static class LongCodec extends TypeCodec<Long> {

        private static final ByteBuffer[] CACHE = new ByteBuffer[256];

        private LongCodec() {}

        @Override
        public Long parse(String value) {
            try {
                return Long.parseLong(value);
            } catch (NumberFormatException e) {
                throw new InvalidTypeException(String.format("Cannot parse 64-bits long value from \"%s\"", value));
            }
        }

        @Override
        public ByteBuffer serialize(Long value) {
            return serializeNoBoxing(value);
        }

        public ByteBuffer serializeNoBoxing(long value) {
            ByteBuffer bb;
            if (value >= -128 && value <= 127) {
                bb = CACHE[((int) (value + 128))];
                if (bb == null) {
                    bb = ByteBuffer.allocate(8);
                    bb.putLong(0, value);
                    CACHE[((int) (value + 128))] = bb;
                }
                return bb.duplicate();
            }

            bb = ByteBuffer.allocate(8);
            bb.putLong(0, value);
            return bb;
        }

        @Override
        public Long deserialize(ByteBuffer bytes) {
            return deserializeNoBoxing(bytes);
        }

        public long deserializeNoBoxing(ByteBuffer bytes) {
            if (bytes.remaining() != 8) {
                throw new InvalidTypeException("Invalid 64-bits long value, expecting 8 bytes but got " + bytes.remaining());
            }

            return bytes.getLong(bytes.position());
        }
    }

    static class BytesCodec extends TypeCodec<ByteBuffer> {

        private BytesCodec() {}

        @Override
        public ByteBuffer parse(String value) {
            return Bytes.fromHexString(value);
        }

        @Override
        public ByteBuffer serialize(ByteBuffer value) {
            return value.duplicate();
        }

        @Override
        public ByteBuffer deserialize(ByteBuffer bytes) {
            return bytes.duplicate();
        }
    }

    static class BooleanCodec extends TypeCodec<Boolean> {
        private static final ByteBuffer TRUE = ByteBuffer.wrap(new byte[]{1});
        private static final ByteBuffer FALSE = ByteBuffer.wrap(new byte[]{0});

        private BooleanCodec() {}

        @Override
        public Boolean parse(String value) {
            if (value.equalsIgnoreCase(Boolean.FALSE.toString())) {
                return false;
            }
            if (value.equalsIgnoreCase(Boolean.TRUE.toString())) {
                return true;
            }

            throw new InvalidTypeException(String.format("Cannot parse boolean value from \"%s\"", value));
        }

        @Override
        public ByteBuffer serialize(Boolean value) {
            return serializeNoBoxing(value);
        }

        public ByteBuffer serializeNoBoxing(boolean value) {
            return value ? TRUE.duplicate() : FALSE.duplicate();
        }

        @Override
        public Boolean deserialize(ByteBuffer bytes) {
            return deserializeNoBoxing(bytes);
        }

        public boolean deserializeNoBoxing(ByteBuffer bytes) {
            if (bytes.remaining() != 1) {
                throw new InvalidTypeException("Invalid boolean value, expecting 1 byte but got " + bytes.remaining());
            }

            return bytes.get(bytes.position()) != 0;
        }
    }

    static class DecimalCodec extends TypeCodec<BigDecimal> {

        private DecimalCodec() {}

        @Override
        public BigDecimal parse(String value) {
            try {
                return new BigDecimal(value);
            } catch (NumberFormatException e) {
                throw new InvalidTypeException(String.format("Cannot parse decimal value from \"%s\"", value));
            }
        }

        @Override
        public ByteBuffer serialize(BigDecimal value) {
            BigInteger bi = value.unscaledValue();
            int scale = value.scale();
            byte[] bibytes = bi.toByteArray();

            ByteBuffer bytes = ByteBuffer.allocate(4 + bibytes.length);
            bytes.putInt(scale);
            bytes.put(bibytes);
            bytes.rewind();
            return bytes;
        }

        @Override
        public BigDecimal deserialize(ByteBuffer bytes) {
            if (bytes.remaining() < 4) {
                throw new InvalidTypeException("Invalid decimal value, expecting at least 4 bytes but got " + bytes.remaining());
            }

            bytes = bytes.duplicate();
            int scale = bytes.getInt();
            byte[] bibytes = new byte[bytes.remaining()];
            bytes.get(bibytes);

            BigInteger bi = new BigInteger(bibytes);
            return new BigDecimal(bi, scale);
        }
    }

    static class DoubleCodec extends TypeCodec<Double> {

        private DoubleCodec() {}

        @Override
        public Double parse(String value) {
            try {
                return Double.parseDouble(value);
            } catch (NumberFormatException e) {
                throw new InvalidTypeException(String.format("Cannot parse 64-bits double value from \"%s\"", value));
            }
        }

        @Override
        public ByteBuffer serialize(Double value) {
            return serializeNoBoxing(value);
        }

        public ByteBuffer serializeNoBoxing(double value) {
            ByteBuffer bb = ByteBuffer.allocate(8);
            bb.putDouble(0, value);
            return bb;
        }

        @Override
        public Double deserialize(ByteBuffer bytes) {
            return deserializeNoBoxing(bytes);
        }

        public double deserializeNoBoxing(ByteBuffer bytes) {
            if (bytes.remaining() != 8) {
                throw new InvalidTypeException("Invalid 64-bits double value, expecting 8 bytes but got " + bytes.remaining());
            }

            return bytes.getDouble(bytes.position());
        }
    }

    static class FloatCodec extends TypeCodec<Float> {

        private FloatCodec() {}

        @Override
        public Float parse(String value) {
            try {
                return Float.parseFloat(value);
            } catch (NumberFormatException e) {
                throw new InvalidTypeException(String.format("Cannot parse 32-bits float value from \"%s\"", value));
            }
        }

        @Override
        public ByteBuffer serialize(Float value) {
            return serializeNoBoxing(value);
        }

        public ByteBuffer serializeNoBoxing(float value) {
            ByteBuffer bb = ByteBuffer.allocate(4);
            bb.putFloat(0, value);
            return bb;
        }

        @Override
        public Float deserialize(ByteBuffer bytes) {
            return deserializeNoBoxing(bytes);
        }

        public float deserializeNoBoxing(ByteBuffer bytes) {
            if (bytes.remaining() != 4) {
                throw new InvalidTypeException("Invalid 32-bits float value, expecting 4 bytes but got " + bytes.remaining());
            }

            return bytes.getFloat(bytes.position());
        }
    }

    static class InetCodec extends TypeCodec<InetAddress> {

        private InetCodec() {}

        @Override
        public InetAddress parse(String value) {
            try {
                return InetAddress.getByName(value);
            } catch (Exception e) {
                throw new InvalidTypeException(String.format("Cannot parse inet value from \"%s\"", value));
            }
        }

        @Override
        public ByteBuffer serialize(InetAddress value) {
            return ByteBuffer.wrap(value.getAddress());
        }

        @Override
        public InetAddress deserialize(ByteBuffer bytes) {
            try {
                return InetAddress.getByAddress(Bytes.getArray(bytes));
            } catch (UnknownHostException e) {
                throw new InvalidTypeException("Invalid bytes for inet value, got " + bytes.remaining() + " bytes");
            }
        }
    }

    static class IntCodec extends TypeCodec<Integer> {

        private IntCodec() {}

        private static final ByteBuffer[] CACHE = new ByteBuffer[256];

        @Override
        public Integer parse(String value) {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException e) {
                throw new InvalidTypeException(String.format("Cannot parse 32-bits int value from \"%s\"", value));
            }
        }

        @Override
        public ByteBuffer serialize(Integer value) {
            return serializeNoBoxing(value);
        }

        public ByteBuffer serializeNoBoxing(int value) {
            ByteBuffer bb;
            if (value >= -128 && value <= 127) {
                bb = CACHE[value + 128];
                if (bb == null) {
                    bb = ByteBuffer.allocate(4);
                    bb.putInt(0, value);
                    CACHE[value + 128] = bb;
                }
                return bb.duplicate();
            }

            bb = ByteBuffer.allocate(4);
            bb.putInt(0, value);
            return bb;
        }

        @Override
        public Integer deserialize(ByteBuffer bytes) {
            return deserializeNoBoxing(bytes);
        }

        public int deserializeNoBoxing(ByteBuffer bytes) {
            if (bytes.remaining() != 4) {
                throw new InvalidTypeException("Invalid 32-bits integer value, expecting 4 bytes but got " + bytes.remaining());
            }

            return bytes.getInt(bytes.position());
        }
    }

    static class DateCodec extends TypeCodec<Date> {

        private static final String[] iso8601Patterns = {
            "yyyy-MM-dd HH:mm",
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd HH:mmZ",
            "yyyy-MM-dd HH:mm:ssZ",
            "yyyy-MM-dd HH:mm:ss.SSS",
            "yyyy-MM-dd HH:mm:ss.SSSZ",
            "yyyy-MM-dd'T'HH:mm",
            "yyyy-MM-dd'T'HH:mmZ",
            "yyyy-MM-dd'T'HH:mm:ss",
            "yyyy-MM-dd'T'HH:mm:ssZ",
            "yyyy-MM-dd'T'HH:mm:ss.SSS",
            "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
            "yyyy-MM-dd",
            "yyyy-MM-ddZ"
        };

        private static final Pattern IS_LONG_PATTERN = Pattern.compile("^-?\\d+$");

        private DateCodec() {}

        /*
         * Copied and adapted from apache commons DateUtils.parseStrictly method (that is used Cassandra side
         * to parse date strings). It is copied here so as to not create a dependency on apache commons "just
         * for this".
         */
        private static Date parseDate(String str, String[] parsePatterns) throws ParseException {
            SimpleDateFormat parser = new SimpleDateFormat();
            parser.setLenient(false);

            ParsePosition pos = new ParsePosition(0);
            for (String parsePattern : parsePatterns) {
                String pattern = parsePattern;

                parser.applyPattern(pattern);
                pos.setIndex(0);

                String str2 = str;
                Date date = parser.parse(str2, pos);
                if (date != null && pos.getIndex() == str2.length()) {
                    return date;
                }
            }
            throw new ParseException("Unable to parse the date: " + str, -1);
        }

        @Override
        public Date parse(String value) {
            if (IS_LONG_PATTERN.matcher(value).matches()) {
                try {
                    return new Date(Long.parseLong(value));
                } catch (NumberFormatException e) {
                    throw new InvalidTypeException(String.format("Cannot parse timestamp value from \"%s\"", value));
                }
            }

            try {
                return parseDate(value, iso8601Patterns);
            } catch (ParseException e) {
                throw new InvalidTypeException(String.format("Cannot parse date value from \"%s\"", value));
            }
        }

        @Override
        public ByteBuffer serialize(Date value) {
            return longInstance.serializeNoBoxing(value.getTime());
        }

        @Override
        public Date deserialize(ByteBuffer bytes) {
            return new Date(longInstance.deserializeNoBoxing(bytes));
        }
    }

    static class SimpleDateCodec extends TypeCodec<java.sql.Date> {

        private static final String[] iso8601Patterns = {
            "yyyy-MM-dd",
            "yyyy-MM-ddZ"
        };

        private static final Pattern IS_LONG_PATTERN = Pattern.compile("^-?\\d+$");

        private SimpleDateCodec() {}

        /*
         * Copied and adapted from apache commons DateUtils.parseStrictly method (that is used Cassandra side
         * to parse date strings). It is copied here so as to not create a dependency on apache commons "just
         * for this".
         */
        private static java.sql.Date parseDate(String str, String[] parsePatterns) throws ParseException {
            SimpleDateFormat parser = new SimpleDateFormat();
            parser.setLenient(false);

            ParsePosition pos = new ParsePosition(0);
            for (String parsePattern : parsePatterns) {
                String pattern = parsePattern;

                parser.applyPattern(pattern);
                pos.setIndex(0);

                String str2 = str;
                Date date = parser.parse(str2, pos);
                if (date != null && pos.getIndex() == str2.length()) {
                    return new java.sql.Date(date.getTime());
                }
            }
            throw new ParseException("Unable to parse the date: " + str, -1);
        }

        @Override
        public java.sql.Date parse(String value) {
            if (IS_LONG_PATTERN.matcher(value).matches()) {
                try {
                    return new java.sql.Date(Long.parseLong(value));
                } catch (NumberFormatException e) {
                    throw new InvalidTypeException(String.format("Cannot parse timestamp value from \"%s\"", value));
                }
            }

            try {
                return parseDate(value, iso8601Patterns);
            } catch (ParseException e) {
                throw new InvalidTypeException(String.format("Cannot parse date value from \"%s\"", value));
            }
        }

        @Override
        public ByteBuffer serialize(java.sql.Date value) {
            return longInstance.serializeNoBoxing(value.getTime());
        }

        @Override
        public java.sql.Date deserialize(ByteBuffer bytes) {
            return new java.sql.Date(longInstance.deserializeNoBoxing(bytes));
        }
    }

    static class TimeCodec extends TypeCodec<Time> {

        private static final String[] iso8601Patterns = {
            "HH:mm",
            "HH:mm:ss",
            "HH:mmX",
            "HH:mmXX",
            "HH:mmXXX",
            "HH:mm:ssX",
            "HH:mm:ssXX",
            "HH:mm:ssXXX",
            "HH:mm:ss.SSS",
            "HH:mm:ss.SSSX",
            "HH:mm:ss.SSSXX",
            "HH:mm:ss.SSSXXX",
        };

        private static final Pattern IS_LONG_PATTERN = Pattern.compile("^-?\\d+$");

        private TimeCodec() {}

        /*
         * Copied and adapted from apache commons DateUtils.parseStrictly method (that is used Cassandra side
         * to parse date strings). It is copied here so as to not create a dependency on apache commons "just
         * for this".
         */
        private static Time parseDate(String str, String[] parsePatterns) throws ParseException {
            SimpleDateFormat parser = new SimpleDateFormat();
            parser.setLenient(false);

            ParsePosition pos = new ParsePosition(0);
            for (String parsePattern : parsePatterns) {
                String pattern = parsePattern;

                parser.applyPattern(pattern);
                pos.setIndex(0);

                String str2 = str;
                Date date = parser.parse(str2, pos);
                if (date != null && pos.getIndex() == str2.length()) {
                    return new Time(date.getTime());
                }
            }
            throw new ParseException("Unable to parse the date: " + str, -1);
        }

        @Override
        public Time parse(String value) {
            if (IS_LONG_PATTERN.matcher(value).matches()) {
                try {
                    return new Time(Long.parseLong(value));
                } catch (NumberFormatException e) {
                    throw new InvalidTypeException(String.format("Cannot parse timestamp value from \"%s\"", value));
                }
            }

            try {
                return parseDate(value, iso8601Patterns);
            } catch (ParseException e) {
                throw new InvalidTypeException(String.format("Cannot parse date value from \"%s\"", value));
            }
        }

        @Override
        public ByteBuffer serialize(Time value) {
            return longInstance.serializeNoBoxing(value.getTime());
        }

        @Override
        public Time deserialize(ByteBuffer bytes) {
            return new Time(longInstance.deserializeNoBoxing(bytes));
        }
    }

    static class UUIDCodec extends TypeCodec<UUID> {

        protected UUIDCodec() {}

        @Override
        public UUID parse(String value) {
            try {
                return UUID.fromString(value);
            } catch (IllegalArgumentException e) {
                throw new InvalidTypeException(String.format("Cannot parse UUID value from \"%s\"", value));
            }
        }

        @Override
        public ByteBuffer serialize(UUID value) {
            ByteBuffer bb = ByteBuffer.allocate(16);
            bb.putLong(0, value.getMostSignificantBits());
            bb.putLong(8, value.getLeastSignificantBits());
            return bb;
        }

        @Override
        public UUID deserialize(ByteBuffer bytes) {
            return new UUID(bytes.getLong(bytes.position()), bytes.getLong(bytes.position() + 8));
        }
    }

    static class TimeUUIDCodec extends UUIDCodec {

        private TimeUUIDCodec() {}

        @Override
        public UUID parse(String value) {
            UUID id = super.parse(value);
            if (id.version() != 1) {
                throw new InvalidTypeException(
                    String.format("Cannot parse type 1 UUID value from \"%s\": represents a type %d UUID", value, id.version()));
            }
            return id;
        }

        @Override
        public UUID deserialize(ByteBuffer bytes) {
            UUID id = super.deserialize(bytes);
            if (id.version() != 1) {
                throw new InvalidTypeException(
                    String.format("Error deserializing type 1 UUID: deserialized value %s represents a type %d UUID", id, id.version()));
            }
            return id;
        }
    }

    static class BigIntegerCodec extends TypeCodec<BigInteger> {

        private BigIntegerCodec() {}

        @Override
        public BigInteger parse(String value) {
            try {
                return new BigInteger(value);
            } catch (NumberFormatException e) {
                throw new InvalidTypeException(String.format("Cannot parse varint value from \"%s\"", value));
            }
        }

        @Override
        public ByteBuffer serialize(BigInteger value) {
            return ByteBuffer.wrap(value.toByteArray());
        }

        @Override
        public BigInteger deserialize(ByteBuffer bytes) {
            return new BigInteger(Bytes.getArray(bytes));
        }
    }

    static class ListCodec<T> extends TypeCodec<List<T>> {

        private final TypeCodec<T> eltCodec;
        private final int protocolVersion;

        public ListCodec(TypeCodec<T> eltCodec, int protocolVersion) {
            this.eltCodec = eltCodec;
            this.protocolVersion = protocolVersion;
        }

        @Override
        public List<T> parse(String value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ByteBuffer serialize(List<T> value) {
            List<ByteBuffer> bbs = new ArrayList<ByteBuffer>(value.size());
            for (T elt : value) {
                bbs.add(eltCodec.serialize(elt));
            }

            return pack(bbs, value.size(), protocolVersion);
        }

        @Override
        public List<T> deserialize(ByteBuffer bytes) {
            try {
                ByteBuffer input = bytes.duplicate();
                int n = readCollectionSize(input, protocolVersion);
                List<T> l = new ArrayList<T>(n);
                for (int i = 0; i < n; i++) {
                    ByteBuffer databb = readCollectionValue(input, protocolVersion);
                    l.add(eltCodec.deserialize(databb));
                }
                return l;
            } catch (BufferUnderflowException e) {
                throw new InvalidTypeException("Not enough bytes to deserialize list");
            }
        }
    }

    static class SetCodec<T> extends TypeCodec<Set<T>> {

        private final TypeCodec<T> eltCodec;
        private final int protocolVersion;

        public SetCodec(TypeCodec<T> eltCodec, int protocolVersion) {
            this.eltCodec = eltCodec;
            this.protocolVersion = protocolVersion;
        }

        @Override
        public Set<T> parse(String value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ByteBuffer serialize(Set<T> value) {
            List<ByteBuffer> bbs = new ArrayList<ByteBuffer>(value.size());
            for (T elt : value) {
                bbs.add(eltCodec.serialize(elt));
            }

            return pack(bbs, value.size(), protocolVersion);
        }

        @Override
        public Set<T> deserialize(ByteBuffer bytes) {
            try {
                ByteBuffer input = bytes.duplicate();
                int n = readCollectionSize(input, protocolVersion);
                Set<T> l = new HashSet<T>(n * 4 / 3);
                for (int i = 0; i < n; i++) {
                    ByteBuffer databb = readCollectionValue(input, protocolVersion);
                    l.add(eltCodec.deserialize(databb));
                }
                return l;
            } catch (BufferUnderflowException e) {
                throw new InvalidTypeException("Not enough bytes to deserialize a set");
            }
        }
    }

    static class MapCodec<K, V> extends TypeCodec<Map<K, V>> {

        private final TypeCodec<K> keyCodec;
        private final TypeCodec<V> valueCodec;
        private final int protocolVersion;

        public MapCodec(TypeCodec<K> keyCodec, TypeCodec<V> valueCodec, int protocolVersion) {
            this.keyCodec = keyCodec;
            this.valueCodec = valueCodec;
            this.protocolVersion = protocolVersion;
        }

        @Override
        public Map<K, V> parse(String value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ByteBuffer serialize(Map<K, V> value) {
            List<ByteBuffer> bbs = new ArrayList<ByteBuffer>(2 * value.size());
            for (Map.Entry<K, V> entry : value.entrySet()) {
                bbs.add(keyCodec.serialize(entry.getKey()));
                bbs.add(valueCodec.serialize(entry.getValue()));
            }
            return pack(bbs, value.size(), protocolVersion);
        }

        @Override
        public Map<K, V> deserialize(ByteBuffer bytes) {
            try {
                ByteBuffer input = bytes.duplicate();
                int n = readCollectionSize(input, protocolVersion);
                Map<K, V> m = new HashMap<K, V>(n * 4 / 3);
                for (int i = 0; i < n; i++) {
                    ByteBuffer kbb = readCollectionValue(input, protocolVersion);
                    ByteBuffer vbb = readCollectionValue(input, protocolVersion);
                    m.put(keyCodec.deserialize(kbb), valueCodec.deserialize(vbb));
                }
                return m;
            } catch (BufferUnderflowException e) {
                throw new InvalidTypeException("Not enough bytes to deserialize a map");
            }
        }
    }

    static class UDTCodec extends TypeCodec<CUDTValue> {

        private final CUDTDefinition definition;

        public UDTCodec(CUDTDefinition definition) {
            this.definition = definition;
        }

        @Override public CUDTValue parse(String value) {
            throw new UnsupportedOperationException();
        }

        @Override public ByteBuffer serialize(CUDTValue value) {
            int size = 0;
            for (ByteBuffer v : value.values) {
                size += 4 + (v == null ? 0 : v.remaining());
            }

            ByteBuffer result = ByteBuffer.allocate(size);
            for (ByteBuffer bb : value.values) {
                if (bb == null) {
                    result.putInt(-1);
                } else {
                    result.putInt(bb.remaining());
                    result.put(bb.duplicate());
                }
            }
            return (ByteBuffer) result.flip();
        }

        @Override public CUDTValue deserialize(ByteBuffer bytes) {
            ByteBuffer input = bytes.duplicate();
            CUDTValue value = definition.newValue();

            int i = 0;
            while (input.hasRemaining() && i < definition.size()) {
                int n = input.getInt();
                value.values[i++] = n < 0 ? null : readBytes(input, n);
            }
            return value;
        }
    }

    static class TupleCodec extends TypeCodec<TupleValue> {

        private final int protocolVersion;
        private final DataType[] types;

        public TupleCodec(int protocolVersion, DataType[] types) {
            this.protocolVersion = protocolVersion;
            this.types = types;
        }

        @Override public TupleValue parse(String value) {
            throw new UnsupportedOperationException();
        }

        @Override public ByteBuffer serialize(TupleValue value) {
            int size = 0;
            for (ByteBuffer v : value.values) {
                size += 4 + (v == null ? 0 : v.remaining());
            }

            ByteBuffer result = ByteBuffer.allocate(size);
            for (ByteBuffer bb : value.values) {
                if (bb == null) {
                    result.putInt(-1);
                } else {
                    result.putInt(bb.remaining());
                    result.put(bb.duplicate());
                }
            }
            return (ByteBuffer) result.flip();
        }

        @Override public TupleValue deserialize(ByteBuffer bytes) {
            ByteBuffer input = bytes.duplicate();
            TupleValue value = new TupleValue(protocolVersion, types);

            int i = 0;
            while (input.hasRemaining() && i < types.length) {
                int n = input.getInt();
                value.setBytesUnsafe(i++, n < 0 ? null : readBytes(input, n));
            }
            return value;
        }
    }
}
