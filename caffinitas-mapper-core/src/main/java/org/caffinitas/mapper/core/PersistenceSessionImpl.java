/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.RetryPolicy;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.Uninterruptibles;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.caffinitas.mapper.core.tracing.ExecutionTracerFactory;
import org.caffinitas.mapper.core.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

/**
 * Represents a session to access data in Cassandra.
 */
class PersistenceSessionImpl implements PersistenceSession {
    final PersistenceManagerImpl persistenceManager;
    final StatementOptions statementOptions;

    private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceSessionImpl.class);

    PersistenceSessionImpl(PersistenceManagerImpl persistenceManager) {
        this.persistenceManager = persistenceManager;
        this.statementOptions = persistenceManager.statementOptions.copy();
    }

    ExecutionTracer executionTracer(PersistenceSessionImpl persistenceSession, MappedSchemaObject<?> mappedSchemaObject) {
        ExecutionTracerFactory f = persistenceManager.executionTracerFactory();
        return f != null ? f.getExecutionTracer(persistenceSession, mappedSchemaObject) : null;
    }

    @Override public void close() {
        try {
            Uninterruptibles.getUninterruptibly(flush());
        } catch (ExecutionException e) {
            Exceptions.throwImmediate(e);
        }
    }

    //
    // session configuration
    //

    @Override public PersistenceSession setFetchSize(int fetchSize) {
        statementOptions.setFetchSize(fetchSize);
        return this;
    }

    @Override public int getFetchSize() {return statementOptions.getFetchSize();}

    @Override public PersistenceSession setRetryPolicy(RetryPolicy retryPolicy) {
        statementOptions.setRetryPolicy(retryPolicy);
        return this;
    }

    @Override public RetryPolicy getRetryPolicy() {return statementOptions.getRetryPolicy();}

    @Override public PersistenceSession setSerialConsistencyLevel(ConsistencyLevel serialConsistencyLevel) {
        statementOptions.setSerialConsistencyLevel(serialConsistencyLevel);
        return this;
    }

    @Override public ConsistencyLevel getSerialConsistencyLevel() {return statementOptions.getSerialConsistencyLevel();}

    @Override public PersistenceSession setWriteConsistencyLevel(ConsistencyLevel writeConsistencyLevel) {
        statementOptions.setWriteConsistencyLevel(writeConsistencyLevel);
        return this;
    }

    @Override public ConsistencyLevel getWriteConsistencyLevel() {return statementOptions.getWriteConsistencyLevel();}

    @Override public PersistenceSession setReadConsistencyLevel(ConsistencyLevel readConsistencyLevel) {
        statementOptions.setReadConsistencyLevel(readConsistencyLevel);
        return this;
    }

    @Override public ConsistencyLevel getReadConsistencyLevel() {return statementOptions.getReadConsistencyLevel();}

    @Override public PersistenceManager getPersistenceManager() {
        return persistenceManager;
    }

    @Override public PersistenceSession setTracing(boolean tracing) {
        statementOptions.setTracing(tracing);
        return this;
    }

    @Override public boolean isTracing() {
        return statementOptions.isTracing();
    }

    Session driverSession() {
        return persistenceManager.driverSession();
    }

    //
    // DML operations
    //

    @Override public <T> void insert(T instance, PersistOption... persistOptions) {
        insertAsync(instance, persistOptions).getUninterruptibly();
    }

    @Override public <T> void update(T instance, PersistOption... persistOptions) {
        updateAsync(instance, persistOptions).getUninterruptibly();
    }

    @Override public <T> void delete(T instance, PersistOption... persistOptions) {
        deleteAsync(instance, persistOptions).getUninterruptibly();
    }

    @Override public <T> ModifyFuture<T> insertAsync(T instance, PersistOption... persistOptions) {
        return modifyAsync(instance, PersistMode.INSERT, persistOptions);
    }

    @Override public <T> ModifyFuture<T> updateAsync(T instance, PersistOption... persistOptions) {
        return modifyAsync(instance, PersistMode.UPDATE, persistOptions);
    }

    @Override public <T> ModifyFuture<T> deleteAsync(T instance, PersistOption... persistOptions) {
        return modifyAsync(instance, PersistMode.DELETE, persistOptions);
    }

    private <T> void validate(T instance) {
        if (persistenceManager.validatorFactory == null)
            return;
        Validator v = persistenceManager.validatorFactory.getValidator();
        Set<ConstraintViolation<T>> violations = v.validate(instance);
        if (violations != null && !violations.isEmpty())
            throw new ConstraintViolationException(violations);
    }

    private <T> ModifyFuture<T> modifyAsync(T instance, PersistMode type, PersistOption[] persistOptions) {
        MappedEntityBase entity = forAccessWithInstance(instance);
        BinderAndStatement binderAndStatement =
            entity.buildModifyBoundStatement(type, this, statementOptions, instance, persistOptions);
        if (binderAndStatement == null) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("No change in instance - not adding batch bound statement for {}", type);
            }
            return ModifyFutureImpl.immediate();
        }
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Submitting bound statement for {} : {}", type, binderAndStatement.statement);
        }
        validate(instance);
        ExecutionTracer tracer = executionTracer(this, entity);
        if (tracer != null) {
            tracer.onBeginModify(this, entity, type, binderAndStatement.statement);
        }
        return new ModifyFutureImpl<T>(this, tracer, binderAndStatement.binder, driverSession().executeAsync(binderAndStatement.statement),
            entity, instance, type);
    }

    MappedEntityBase modifyBatch(BatchImpl batch, Object instance, PersistMode type, PersistOption[] persistOptions) {
        MappedEntityBase entity = forAccessWithInstance(instance);
        BinderAndStatement binderAndStatement =
            entity.buildModifyBoundStatement(type, this, statementOptions, instance, persistOptions);
        if (binderAndStatement == null) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("No change in instance - not adding batch bound statement for {}", type);
            }
            return entity;
        }
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Adding batch bound statement for {} : {}", type, binderAndStatement.statement);
        }
        batch.addBatch(binderAndStatement.statement);
        return entity;
    }

    //
    // entity loading by primary key
    //

    @Override public <T> T loadOne(Class<T> type, Object... primaryKey) {
        return loadOneAsync(type, primaryKey).getUninterruptibly();
    }

    @Override public <T> List<T> loadMultiple(Class<T> type, Object... primaryKey) {
        return loadMultipleAsync(type, primaryKey).getUninterruptibly();
    }

    @Override public <T> T loadOneWithOptions(Class<T> type, PersistOption[] options, Object... primaryKey) {
        return loadOneWithOptionsAsync(type, options, primaryKey).getUninterruptibly();
    }

    @Override public <T> List<T> loadMultipleWithOptions(Class<T> type, PersistOption[] options, Object... primaryKey) {
        return loadMultipleWithOptionsAsync(type, options, primaryKey).getUninterruptibly();
    }

    @Override public <T> ReadOneFuture<T> loadOneAsync(Class<T> type, Object... primaryKey) {
        return loadOneWithOptionsAsync(type, PersistOption.NO_OPTIONS, primaryKey);
    }

    @Override public <T> ReadFuture<List<T>> loadMultipleAsync(Class<T> type, Object... primaryKey) {
        return loadMultipleWithOptionsAsync(type, PersistOption.NO_OPTIONS, primaryKey);
    }

    @SuppressWarnings("unchecked") @Override public <T> ReadOneFuture<T> loadOneWithOptionsAsync(Class<T> type, PersistOption[] options,
                                                                                                 Object... primaryKey) {
        MappedEntityBase entity = loadXPrecheck(type, primaryKey);

        ExecutionTracer tracer = executionTracer(this, entity);
        ReadOneFutureImpl<T> future = new ReadOneFutureImpl<T>(this, tracer);
        entity.executeLoadBoundStatements(tracer, driverSession(), primaryKey, future, statementOptions, options);
        return future;
    }

    @SuppressWarnings("unchecked") @Override public <T> ReadFuture<List<T>> loadMultipleWithOptionsAsync(Class<T> type, PersistOption[] options,
                                                                                                         Object... primaryKey) {
        MappedEntityBase entity = loadXPrecheck(type, primaryKey);

        ExecutionTracer tracer = executionTracer(this, entity);
        ReadFutureImpl<T> future = new ReadFutureImpl<T>(this, tracer);
        entity.executeLoadBoundStatements(tracer, driverSession(), primaryKey, future, statementOptions, options);
        return future;
    }

    private <T> MappedEntityBase loadXPrecheck(Class<T> type, Object[] primaryKey) {
        MappedEntityBase entity = entityForAccess(type);

        if (primaryKey.length < entity.partitionKeyColumns.length) {
            throw new PersistenceRuntimeException("too few values for partition key of entity " + entity.type + " (" +
                Arrays.toString(entity.partitionKeyColumns) + ") : " + Arrays.toString(primaryKey));
        }
        return entity;
    }

    //
    // queries
    //

    @Override public <T> QueryBinder<T> createNamedQueryBinder(Class<T> type, PersistOption[] options, String queryName) {
        MappedEntityBase entity = entityForAccess(type);

        QueryBinderImpl<T> queryBinder = new QueryBinderImpl<T>(driverSession(), entity);

        entity.setupQueryBinder(queryBinder, options, queryName, statementOptions);

        return queryBinder;

    }

    @SuppressWarnings("unchecked") @Override public <T> QueryBinder<T> createQueryBinder(Class<T> type, PersistOption[] options, String condition,
                                                                                         Map<Class<? extends T>, String> additionalConditions) {
        MappedEntityBase entity = entityForAccess(type);

        Class<? extends T> entityType = (Class<? extends T>) entity.type;
        if (entity instanceof MappedEntityST) {
            entityType = (Class<? extends T>) ((MappedEntityST) entity).root.type;
        }

        QueryBinderImpl<T> queryBinder = new QueryBinderImpl<T>(driverSession(), entity);

        if (condition != null) {
            queryBinder.addCondition(entityType, condition);
        }
        if (additionalConditions != null) {
            queryBinder.addConditions(additionalConditions);
        }

        entity.setupQueryBinder(queryBinder, options, null, statementOptions);

        return queryBinder;
    }

    @Override public <T> List<T> executeQuery(QueryBinder<T> queryBinder) {
        return executeQueryAsync(queryBinder).getUninterruptibly();
    }

    @Override public <T> ReadFuture<List<T>> executeQueryAsync(QueryBinder<T> queryBinder) {
        QueryBinderImpl<T> binderImpl = (QueryBinderImpl<T>) queryBinder;

        ExecutionTracer tracer = executionTracer(this, binderImpl.entity);
        ReadFutureImpl<T> future = new ReadFutureImpl<T>(this, tracer);
        binderImpl.entity.executeQuery(tracer, binderImpl, future);
        return future;
    }

    //
    // lazy loading
    //

    @Override public <T> boolean loadLazy(T container, PersistOption... persistOptions) {
        return loadLazyInternal(container, null);
    }

    <T> boolean loadLazyInternal(T container, MappedAttribute[] attrs, PersistOption... persistOptions) {
        MappedEntityBase entity = forAccessWithInstance(container);

        return entity.executeLoadLazy(this, statementOptions, container, attrs, persistOptions);
    }

    //
    // batch statements
    //

    @Override public Batch startBatch() {
        return startBatch(BatchStatement.Type.LOGGED);
    }

    @Override public Batch startBatch(BatchStatement.Type type) {
        return new BatchImpl(this, type);
    }

    //
    // internal helpers
    //

    <T> MappedEntityBase entityForAccess(Class<T> type) {
        MappedEntityBase entity = persistenceManager.getEntity(type);
        if (entity == null) {
            throw new PersistenceRuntimeException("no entity registered for type " + type);
        }
        entity.verifyForAccess();
        return entity;
    }

    private <T> MappedEntityBase forAccessWithInstance(T instance) {
        MappedEntityBase entity = persistenceManager.getEntityForObject(instance);
        if (entity == null) {
            throw new PersistenceRuntimeException("no entity registered for type " + instance.getClass());
        }
        entity.verifyForAccess();
        return entity;
    }

    //
    // tracking session dummy impl
    //

    @Override public <T> T evict(T instance) {
        return null;
    }

    @Override public Future flush() {
        return Futures.immediateFuture(null);
    }

    @Override public void clear() {}

    <T> void loaded(MappedEntityBase entity, Retriever retriever, T instance) {}

    <T> void modified(Binder binder, PersistMode mode, MappedEntityBase entity, T instance) {}

    <T> T cachedEntity(MappedEntityBase entity, Retriever retriever) {
        return null;
    }

    <T> boolean filterUnmodifiedForUpdate(MappedEntityBase entity, T instance, Binder binder) {
        return false;
    }
}
