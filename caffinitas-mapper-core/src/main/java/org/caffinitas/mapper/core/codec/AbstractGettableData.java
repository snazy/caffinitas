/*
 *      Copyright (C) 2012 DataStax Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.caffinitas.mapper.core.codec;

import com.datastax.driver.core.ProtocolOptions;
import com.datastax.driver.core.exceptions.InvalidTypeException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.sql.Time;
import java.util.Date;
import java.util.UUID;

abstract class AbstractGettableData {

    protected final int version;

    /**
     * Creates a new AbstractGettableData object.
     *
     * @param protocolVersion the protocol version in which values returned
     *                        by {@link #getValue} will be returned. This must be a protocol version
     *                        supported by this driver. In general, the correct value will be the
     *                        value returned by {@link com.datastax.driver.core.ProtocolOptions#getProtocolVersion}.
     * @throws IllegalArgumentException if {@code protocolVersion} is not a valid protocol version.
     */
    protected AbstractGettableData(int protocolVersion) {
        if (protocolVersion == 0 || protocolVersion > ProtocolOptions.NEWEST_SUPPORTED_PROTOCOL_VERSION)
            throw new IllegalArgumentException(String.format("Unsupported protocol version %d; valid values must be between 1 and %d or negative (for auto-detect).",
                                                             protocolVersion,
                                                             ProtocolOptions.NEWEST_SUPPORTED_PROTOCOL_VERSION));
        this.version = protocolVersion;
    }

    /**
     * Returns the type for the value at index {@code i}.
     *
     * @param i the index of the type to fetch.
     * @return the type of the value at index {@code i}.
     * @throws IndexOutOfBoundsException if {@code i} is not a valid index.
     */
    protected abstract DataType getType(int i);

    /**
     * Returns the name corresponding to the value at index {@code i}.
     *
     * @param i the index of the name to fetch.
     * @return the name corresponding to the value at index {@code i}.
     * @throws IndexOutOfBoundsException if {@code i} is not a valid index.
     */
    protected abstract String getName(int i);

    /**
     * Returns the value at index {@code i}.
     *
     * @param i the index to fetch.
     * @return the value at index {@code i}.
     * @throws IndexOutOfBoundsException if {@code i} is not a valid index.
     */
    protected abstract ByteBuffer getValue(int i);

    // Note: we avoid having a vararg method to avoid the array allocation that comes with it.
    void checkType(int i, DataType.Name name) {
        DataType defined = getType(i);
        if (name != defined.getName()) {
            throw new InvalidTypeException(String.format("Value %s is of type %s", getName(i), defined));
        }
    }

    DataType.Name checkType(int i, DataType.Name name1, DataType.Name name2) {
        DataType defined = getType(i);
        if (name1 != defined.getName() && name2 != defined.getName()) {
            throw new InvalidTypeException(String.format("Value %s is of type %s", getName(i), defined));
        }

        return defined.getName();
    }

    DataType.Name checkType(int i, DataType.Name name1, DataType.Name name2, DataType.Name name3) {
        DataType defined = getType(i);
        if (name1 != defined.getName() && name2 != defined.getName() && name3 != defined.getName()) {
            throw new InvalidTypeException(String.format("Value %s is of type %s", getName(i), defined));
        }

        return defined.getName();
    }

    boolean isNull(int i) {
        return getValue(i) == null;
    }

    boolean getBool(int i) {
        checkType(i, DataType.Name.BOOLEAN);

        ByteBuffer value = getValue(i);
        return !(value == null || value.remaining() == 0) && TypeCodec.booleanInstance.deserializeNoBoxing(value);
    }

    int getInt(int i) {
        checkType(i, DataType.Name.INT);

        ByteBuffer value = getValue(i);
        if (value == null || value.remaining() == 0) {
            return 0;
        }

        return TypeCodec.intInstance.deserializeNoBoxing(value);
    }

    long getLong(int i) {
        checkType(i, DataType.Name.BIGINT, DataType.Name.COUNTER);

        ByteBuffer value = getValue(i);
        if (value == null || value.remaining() == 0) {
            return 0L;
        }

        return TypeCodec.longInstance.deserializeNoBoxing(value);
    }

    Date getDate(int i) {
        checkType(i, DataType.Name.TIMESTAMP);

        ByteBuffer value = getValue(i);
        if (value == null || value.remaining() == 0) {
            return null;
        }

        return TypeCodec.dateInstance.deserialize(value);
    }

    java.sql.Date getSimpleDate(int i) {
        checkType(i, DataType.Name.DATE);

        ByteBuffer value = getValue(i);
        if (value == null || value.remaining() == 0) {
            return null;
        }

        return TypeCodec.simpleDateInstance.deserialize(value);
    }

    Time getTime(int i) {
        checkType(i, DataType.Name.TIME);

        ByteBuffer value = getValue(i);
        if (value == null || value.remaining() == 0) {
            return null;
        }

        return TypeCodec.timeInstance.deserialize(value);
    }

    float getFloat(int i) {
        checkType(i, DataType.Name.FLOAT);

        ByteBuffer value = getValue(i);
        if (value == null || value.remaining() == 0) {
            return 0.0f;
        }

        return TypeCodec.floatInstance.deserializeNoBoxing(value);
    }

    double getDouble(int i) {
        checkType(i, DataType.Name.DOUBLE);

        ByteBuffer value = getValue(i);
        if (value == null || value.remaining() == 0) {
            return 0.0;
        }

        return TypeCodec.doubleInstance.deserializeNoBoxing(value);
    }

    ByteBuffer getBytesUnsafe(int i) {
        ByteBuffer value = getValue(i);
        if (value == null) {
            return null;
        }

        return value.duplicate();
    }

    ByteBuffer getBytes(int i) {
        checkType(i, DataType.Name.BLOB);
        return getBytesUnsafe(i);
    }

    String getString(int i) {
        DataType.Name type = checkType(i, DataType.Name.VARCHAR,
            DataType.Name.TEXT,
            DataType.Name.ASCII);

        ByteBuffer value = getValue(i);
        if (value == null) {
            return null;
        }

        return type == DataType.Name.ASCII
            ? TypeCodec.StringCodec.asciiInstance.deserialize(value)
            : TypeCodec.StringCodec.utf8Instance.deserialize(value);
    }

    BigInteger getVarint(int i) {
        checkType(i, DataType.Name.VARINT);

        ByteBuffer value = getValue(i);
        if (value == null || value.remaining() == 0) {
            return null;
        }

        return TypeCodec.bigIntegerInstance.deserialize(value);
    }

    BigDecimal getDecimal(int i) {
        checkType(i, DataType.Name.DECIMAL);

        ByteBuffer value = getValue(i);
        if (value == null || value.remaining() == 0) {
            return null;
        }

        return TypeCodec.decimalInstance.deserialize(value);
    }

    UUID getUUID(int i) {
        DataType.Name type = checkType(i, DataType.Name.UUID, DataType.Name.TIMEUUID);

        ByteBuffer value = getValue(i);
        if (value == null || value.remaining() == 0) {
            return null;
        }

        return type == DataType.Name.UUID
            ? TypeCodec.uuidInstance.deserialize(value)
            : TypeCodec.timeUUIDInstance.deserialize(value);
    }

    InetAddress getInet(int i) {
        checkType(i, DataType.Name.INET);

        ByteBuffer value = getValue(i);
        if (value == null || value.remaining() == 0) {
            return null;
        }

        return TypeCodec.inetInstance.deserialize(value);
    }
}
