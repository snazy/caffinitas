/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.accessors;

import sun.reflect.ReflectionFactory;

import java.lang.reflect.Constructor;
import java.security.AccessController;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Internal API.
 */
public class DefaultObjectFactory implements ObjectFactory {
    public static final ObjectFactory INSTANCE = new DefaultObjectFactory();

    private static final ReflectionFactory reflFactory =
        (ReflectionFactory)AccessController.doPrivileged(
            new ReflectionFactory.GetReflectionFactoryAction());

    private final Map<Class<?>, Constructor<?>> constructorMap = new WeakHashMap<Class<?>, Constructor<?>>();

    private DefaultObjectFactory() {}

    @SuppressWarnings("unchecked") @Override public <T> T newInstance(Class<T> clazz) {
        Constructor ctor = constructorForClass(clazz);
        try {
            return (T) ctor.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private <T> Constructor constructorForClass(Class<T> clazz) {
        synchronized (this) {
            Constructor<T> ctor = (Constructor<T>) constructorMap.get(clazz);
            if (ctor == null) {
                ctor = fetchConstructor(clazz);
                constructorMap.put(clazz, ctor);
            }
            return ctor;
        }
    }

    private static Constructor fetchConstructor(Class<?> cl) {
        Constructor cons = findApplicableConstructor(cl);
        cons = reflFactory.newConstructorForSerialization(cl, cons);
        cons.setAccessible(true);
        return cons;
    }

    private static Constructor<?> findApplicableConstructor(Class<?> cl) {
        for (; ; cl = cl.getSuperclass()) {
            try {
                return cl.getDeclaredConstructor((Class<?>[]) null);
            } catch (NoSuchMethodException e) {
                // just continue
            }
        }
    }
}
