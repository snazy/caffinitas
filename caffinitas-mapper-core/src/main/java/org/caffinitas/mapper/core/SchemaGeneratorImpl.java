/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.Cluster;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

final class SchemaGeneratorImpl implements SchemaGenerator {
    private final MappedSchemaObject<?>[] schemaObjects;
    private final Cluster cluster;

    SchemaGeneratorImpl(MappedSchemaObject<?>[] schemaObjects, Cluster cluster) {
        this.schemaObjects = schemaObjects;
        this.cluster = cluster;
    }

    @Override public CqlStatementList generateOfflineCreateDDL() {
        CqlStatementList stmtList = new CqlStatementList();
        for (MappedSchemaObject<?> schemaObject : schemaObjects) {
            stmtList.addStatement(schemaObject.getCreateDDL());
        }
        return stmtList;
    }

    @Override public CqlStatementList generateOfflineDropDDL() {
        CqlStatementList stmtList = new CqlStatementList();
        // reverse order for DROP
        for (int i = schemaObjects.length - 1; i >= 0; i--) {
            MappedSchemaObject<?> schemaObject = schemaObjects[i];
            stmtList.addStatement(schemaObject.getDropDDL());
        }
        return stmtList;
    }

    @Override public CqlStatementList generateLiveAlterDDL() {
        CqlStatementList stmtList = new CqlStatementList();
        for (MappedSchemaObject<?> schemaObject : schemaObjects) {
            if (schemaObject.getCqlTable().isExists()) {
                CqlStatementList stmts = schemaObject.getAlterDDL(cluster);
                stmtList.merge(stmts);
            } else {
                stmtList.addStatement(schemaObject.getCreateDDL());
            }
        }
        return stmtList;
    }

    @Override public List<MappedSchemaObject<?>> getSchemaObjects() {
        return Arrays.asList(schemaObjects);
    }

    @Override public ConditionAwait forSchemaObjectsAvailable() {
        return new ConditionAwait() {
            @Override public boolean isSignalled() {
                throw new UnsupportedOperationException();
            }

            @Override public void await() throws InterruptedException {
                for (MappedSchemaObject<?> schemaObject : schemaObjects) {
                    schemaObject.forSchemaObjectExistsAndValid().await();
                }
            }

            @Override public boolean awaitUntil(Date deadline) throws InterruptedException {
                for (MappedSchemaObject<?> schemaObject : schemaObjects) {
                    schemaObject.forSchemaObjectExistsAndValid().awaitUntil(deadline);
                }
                return true; // TODO return value correct ???
            }

            @Override public boolean await(long time, TimeUnit unit) throws InterruptedException {
                for (MappedSchemaObject<?> schemaObject : schemaObjects) {
                    schemaObject.forSchemaObjectExistsAndValid().await(time, unit);
                }
                return true; // TODO return value correct ???
            }

            @Override public long awaitNanos(long nanosTimeout) throws InterruptedException {
                for (MappedSchemaObject<?> schemaObject : schemaObjects) {
                    nanosTimeout = schemaObject.forSchemaObjectExistsAndValid().awaitNanos(nanosTimeout);
                }
                return nanosTimeout;
            }

            @Override public void awaitUninterruptibly() {
                for (MappedSchemaObject<?> schemaObject : schemaObjects) {
                    schemaObject.forSchemaObjectExistsAndValid().awaitUninterruptibly();
                }
            }
        };
    }
}
