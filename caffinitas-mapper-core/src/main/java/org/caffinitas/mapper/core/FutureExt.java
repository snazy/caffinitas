/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Extend functionality of {@link java.util.concurrent.Future}.
 *
 * @param <T> future type param
 */
public interface FutureExt<T> extends ListenableFuture<T> {
    /**
     * See {@link com.google.common.util.concurrent.Uninterruptibles#getUninterruptibly(java.util.concurrent.Future)}.
     */
    T getUninterruptibly();

    /**
     * See {@link com.google.common.util.concurrent.Uninterruptibles#getUninterruptibly(java.util.concurrent.Future)}.
     */
    T getUninterruptibly(long timeout, TimeUnit unit) throws TimeoutException;
}
