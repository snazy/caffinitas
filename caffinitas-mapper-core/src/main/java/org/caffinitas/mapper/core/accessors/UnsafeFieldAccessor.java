/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.accessors;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;

/**
 * Internal API.
 */
public class UnsafeFieldAccessor extends AbstractAttributeAccessor {
    private final long offset;
    private final Field field;

    public UnsafeFieldAccessor(Field field) {
        offset = UnsafeAccessorFactory.UNSAFE.objectFieldOffset(field);
        this.field = field;
    }

    @Override public AccessibleObject accessible() {
        return field;
    }

    @Override public Class<?> type() {
        return field.getType();
    }

    @Override public boolean getBoolean(Object instance) {
        if (field.getType() != boolean.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        return UnsafeAccessorFactory.UNSAFE.getBoolean(instance, offset);
    }

    @Override public byte getByte(Object instance) {
        if (field.getType() != byte.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        return UnsafeAccessorFactory.UNSAFE.getByte(instance, offset);
    }

    @Override public char getChar(Object instance) {
        if (field.getType() != char.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        return UnsafeAccessorFactory.UNSAFE.getChar(instance, offset);
    }

    @Override public short getShort(Object instance) {
        if (field.getType() != short.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        return UnsafeAccessorFactory.UNSAFE.getShort(instance, offset);
    }

    @Override public int getInt(Object instance) {
        if (field.getType() != int.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        return UnsafeAccessorFactory.UNSAFE.getInt(instance, offset);
    }

    @Override public long getLong(Object instance) {
        if (field.getType() != long.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        return UnsafeAccessorFactory.UNSAFE.getLong(instance, offset);
    }

    @Override public float getFloat(Object instance) {
        if (field.getType() != float.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        return UnsafeAccessorFactory.UNSAFE.getFloat(instance, offset);
    }

    @Override public double getDouble(Object instance) {
        if (field.getType() != double.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        return UnsafeAccessorFactory.UNSAFE.getDouble(instance, offset);
    }

    @SuppressWarnings("unchecked") @Override public <T> T getObject(Object instance) {
        if (field.getType().isPrimitive()) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        return (T) UnsafeAccessorFactory.UNSAFE.getObject(instance, offset);
    }

    @Override public void setBoolean(Object instance, boolean value) {
        if (field.getType() != boolean.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        UnsafeAccessorFactory.UNSAFE.putBoolean(instance, offset, value);
    }

    @Override public void setByte(Object instance, byte value) {
        if (field.getType() != byte.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        UnsafeAccessorFactory.UNSAFE.putByte(instance, offset, value);
    }

    @Override public void setChar(Object instance, char value) {
        if (field.getType() != char.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        UnsafeAccessorFactory.UNSAFE.putChar(instance, offset, value);
    }

    @Override public void setShort(Object instance, short value) {
        if (field.getType() != short.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        UnsafeAccessorFactory.UNSAFE.putShort(instance, offset, value);
    }

    @Override public void setInt(Object instance, int value) {
        if (field.getType() != int.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        UnsafeAccessorFactory.UNSAFE.putInt(instance, offset, value);
    }

    @Override public void setLong(Object instance, long value) {
        if (field.getType() != long.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        UnsafeAccessorFactory.UNSAFE.putLong(instance, offset, value);
    }

    @Override public void setFloat(Object instance, float value) {
        if (field.getType() != float.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        UnsafeAccessorFactory.UNSAFE.putFloat(instance, offset, value);
    }

    @Override public void setDouble(Object instance, double value) {
        if (field.getType() != double.class) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        UnsafeAccessorFactory.UNSAFE.putDouble(instance, offset, value);
    }

    @Override public boolean setObject(Object instance, Object value) {
        if (field.getType().isPrimitive()) {
            throw new IllegalStateException();
        }
        if (instance == null) {
            throw new NullPointerException();
        }
        if (value != null && !field.getType().isInstance(value)) {
            throw new ClassCastException("value type " + value.getClass().getName() + " cannot be cast to " + field.getType().getName());
        }
        UnsafeAccessorFactory.UNSAFE.putObject(instance, offset, value);
        return value == null;
    }
}
