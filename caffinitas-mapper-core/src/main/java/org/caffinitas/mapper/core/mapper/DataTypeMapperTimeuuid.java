/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.mapper;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.codec.Binder;

import java.util.UUID;

/**
 * Internal API.
 */
public final class DataTypeMapperTimeuuid extends AbstractDataTypeMapperUuid {
    public static final DataTypeMapperTimeuuid INSTANCE = new DataTypeMapperTimeuuid();

    protected DataTypeMapperTimeuuid() {
    }

    @Override public void fromObject(Binder binder, CqlColumn col, Object value) {
        if (value == null) {
            return;
        }
        if (value instanceof String) {
            value = UUID.fromString((String) value);
        }
        // TODO convert to UUID
        binder.setTimeUUID(col, (UUID) value);
    }
}
