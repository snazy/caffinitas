/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.util.Names;

import java.util.Comparator;

/**
 * Represents a column or metadata in CQL DML statements.
 */
public abstract class CqlColumn implements Cloneable, Comparable<CqlColumn> {
    public static final CqlColumn[] NO_COLUMNS = {};
    public static final Comparator<CqlColumn> NAME_NUMERIC_COMPARATOR = new Comparator<CqlColumn>() {
        @Override public int compare(CqlColumn o1, CqlColumn o2) {
            int i1 = Integer.parseInt(o1.getName().trim());
            int i2 = Integer.parseInt(o2.getName().trim());
            if (i1 == i2) {
                throw new IllegalArgumentException("duplicate names " + i1 + " + " + i2);
            }
            return i1 - i2;
        }
    };

    private String name;
    DataType dataType;
    DataTypeMapper dataTypeMapper;
    boolean orderDescending;

    protected CqlColumn(String name, DataType dataType) {
        name = name.trim();
        if (name.isEmpty()) {
            throw new ModelUseException("name=" + name + " dataType=" + dataType);
        }
        this.name = name;
        this.dataType = dataType;
    }

    @Override public int compareTo(CqlColumn o) {
        int r = name.compareTo(o.name);
        if (r != 0) {
            return r;
        }
        return getEscapedName().compareTo(o.getEscapedName());
    }

    public String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    public String getEscapedName() {
        return Names.maybeEscapeCassandraName(name);
    }

    public DataType getDataType() {
        return dataType;
    }

    public abstract boolean isPrimaryKey();

    public abstract boolean isForSelect();

    public abstract boolean isLazy();

    public abstract boolean isForUpdate();

    public abstract boolean isExists();

    abstract void setExists(boolean exists);

    public abstract boolean isAllowNotExists();

    public abstract boolean isIgnoreTypeMismatch();

    public abstract boolean isStatic();

    public abstract boolean isPhysical();

    @Override protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public CqlColumn copy() {
        try {
            return (CqlColumn) clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CqlColumn cqlColumn = (CqlColumn) o;

        return name != null ? name.equals(cqlColumn.name) : cqlColumn.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    boolean compatible(CqlColumn other) {
        // CqlColumn.dataType field is null for UDT or traditional composites because the exact data type cannot be determined
        // at specific parse/initialization stages (even though we need it before we get any schema information)
        return dataType == null || dataType.equals(other.dataType);
    }

    boolean compatible(DataType other) {
        // CqlColumn.dataType field is null for UDT or traditional composites because the exact data type cannot be determined
        // at specific parse/initialization stages (even though we need it before we get any schema information)
        return dataType == null || dataType.equals(other);
    }

    abstract void setPrimaryKey(boolean primaryKey);

    public int indexForPstmt(CqlColumn[] columns) {
        int idx = 0;
        for (CqlColumn column : columns) {
            if (column.equals(this)) {
                return column.isExists() ? idx : -1;
            }
            if (column.isExists()) {
                idx++;
            }
        }
        return -1;
    }
}
