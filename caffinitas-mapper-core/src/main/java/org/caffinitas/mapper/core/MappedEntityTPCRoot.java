/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.Session;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;

import java.util.List;

final class MappedEntityTPCRoot extends MappedEntityInheritedRoot<MappedEntityTPC> {
    MappedEntityTPCRoot(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity) {
        super(persistenceManager, parseEntity, MappedEntityTPC.class);
    }

    @Override MappedEntityTPC newMappedChildEntity(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity) {
        return new MappedEntityTPC(persistenceManager, parseEntity);
    }

    @Override protected List<CqlColumn> setupEntityColumnsInner(ParseEntityBase parseEntity, List<CqlColumn> partitionKey,
                                                                List<CqlColumn> clusteringKey) {

        List<CqlColumn> r = super.setupEntityColumnsInner(parseEntity, partitionKey, clusteringKey);

        pushAttributesToChildren();

        // note: do not add own 'data' columns

        for (MappedEntityTPC child : directChildren) {
            child.pullParentColumns();
        }

        return r;
    }

    @Override protected void setupEntityColumnsOuter() {
        for (MappedEntityTPC child : directChildren) {
            child.updatePrimaryKey(this);
        }
    }

    @Override void executeLoadBoundStatements(ExecutionTracer tracer, Session session, Object[] primaryKey,
                                              AbstractDelegatesFutureExt<?> futures, StatementOptions statementOptions,
                                              PersistOption... persistOptions) {
        super.executeLoadBoundStatements(tracer, session, primaryKey, futures, statementOptions, persistOptions);

        for (MappedEntityTPC child : directChildren) {
            child.executeLoadBoundStatements(tracer, session, primaryKey, futures, statementOptions, persistOptions);
        }
    }

    @Override <T> void setupQueryBinder(QueryBinderImpl<T> queryBinder, PersistOption[] options, String queryName,
                                        StatementOptions statementOptions) {
        super.setupQueryBinder(queryBinder, options, queryName, statementOptions);

        for (MappedEntityTPC child : directChildren) {
            child.setupQueryBinder(queryBinder, options, queryName, statementOptions);
        }
    }

    @Override <T> void executeQuery(ExecutionTracer tracer, QueryBinderImpl<T> queryBinder, ReadFutureImpl<T> futures) {
        super.executeQuery(tracer, queryBinder, futures);

        for (MappedEntityTPC child : directChildren) {
            child.executeQuery(tracer, queryBinder, futures);
        }
    }

    @Override void verifyType() {
        // nop
    }
}
