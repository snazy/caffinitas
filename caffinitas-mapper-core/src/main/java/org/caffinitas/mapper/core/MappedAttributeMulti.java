/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CIgnore;
import org.caffinitas.mapper.annotations.CMultiColumn;
import org.caffinitas.mapper.core.api.MultiColumnConverter;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.codec.Retriever;

final class MappedAttributeMulti extends MappedAttributeSingle {
    final MultiColumnConverter converter;

    MappedAttributeMulti(PersistenceManagerImpl persistenceManager, ParseAttribute parseAttribute) {
        super(persistenceManager, parseAttribute);

        this.converter = persistenceManager.converterFor(MultiColumnConverter.class,
            parseAttribute.multiColumn.converter(), type);
    }

    @Override void setupSingleColumns(PersistenceManagerImpl persistenceManager, DataModel model,
                                      ParseAttribute parseAttribute, AttrOverrideMap attributeOverrides, NameMapper nameMapper,
                                      String columnNamePrefix) {
        attributeOverrides = attributeOverrides.forPrefix(parseAttribute.attrName);
        attributeOverrides = attributeOverrides.merge(parseAttribute.attributeOverrides);
        columnNamePrefix = makeColumnName(attributeOverrides, nameMapper, columnNamePrefix, null);

        Class[] cTypes = converter.cassandraTypes();
        String[] pNames = converter.propertyNames();
        CColumn[] cols = parseAttribute.multiColumn.columns();
        CIgnore[] ignores = parseAttribute.multiColumn.ignore();

        int l = cTypes.length;
        if (pNames.length != l || l == 0) {
            throw new ModelUseException(
                "Converter " + converter + " returns inconsistent number of column types (" + l + ") and property names (" + pNames.length + ')');
        }
        if (cols.length != 0 && cols.length != l) {
            throw new ModelUseException(
                '@' + CMultiColumn.class.getSimpleName() + ".columns[] returns inconsistent number of columns (" + cols.length + "), expected " + l);
        }
        if (ignores.length != 0 && ignores.length != l) {
            throw new ModelUseException(
                '@' + CMultiColumn.class.getSimpleName() + ".ignores[] returns inconsistent number of ignores (" + ignores.length + "), expected " +
                    l);
        }

        for (int i = 0; i < l; i++) {

            CColumn column = cols.length > 0 ? cols[i] : null;
            CIgnore ignore = ignores.length > 0 ? ignores[i] : null;

            String columnName = makeColumnName(attributeOverrides, nameMapper, columnNamePrefix, column, pNames[i]);

            boolean allowNotExists = ignore != null && ignore.notExists();
            boolean ignoreTypeMismatch = ignore != null && ignore.typeMismatch();
            boolean staticCol = column != null && column.isStatic();

            DataType dataType = column != null ? column.type().getDataType() : null;
            if (dataType == null) {
                dataType = ParseAttribute.guessDataTypeName(cTypes[i], null, accessor.accessible()).getDataType();
            }

            CqlColumn cqlColumn =
                allowNotExists || ignoreTypeMismatch || staticCol
                    ? new CqlColumnSimple(columnName, dataType, allowNotExists, ignoreTypeMismatch, staticCol, false)
                    : new CqlColumnSimpleStd(columnName, dataType);
            cqlColumn.dataTypeMapper = dataTypeMapperFor(dataType);
            addColumn(cqlColumn);
        }
    }

    @Override void bindToStatement(Object instance, Binder binder) {
        for (CqlColumn column : allColumns) {
            if (!binder.contains(column)) {
                return;
            }
        }

        Object javaInst = accessor.getObject(instance);
        @SuppressWarnings("unchecked") Object[] values = converter.fromJava(javaInst);
        CqlColumn[] cols = allColumns;
        int l = cols.length;

        if (values == null) {
            // null value
            for (CqlColumn col : cols) {
                binder.setBytesUnsafe(col, null);
            }
            return;
        }

        if (values.length != l) {
            throw new IllegalStateException("Converter " + converter.getClass() + " returns illegal number of parts as result of fromJava() call");
        }

        for (int i = 0; i < l; i++) {
            CqlColumn col = cols[i];
            col.dataTypeMapper.fromObject(binder, col, values[i]);
        }
    }

    @Override boolean fromRow(PersistenceSessionImpl session, Object rootInstance, Object instance, Retriever retriever) {
        CqlColumn[] cols = allColumns;
        for (CqlColumn column : cols) {
            if (!retriever.contains(column)) {
                return true;
            }
        }

        int l = cols.length;
        Object[] values = null;
        Class[] pTypes = converter.cassandraTypes();
        for (int i = 0; i < l; i++) {
            CqlColumn col = cols[i];
            Object part = col.dataTypeMapper.toObject(retriever, col, pTypes[i]);
            if (part != null) {
                if (values == null) {
                    values = new Object[l];
                }
                values[i] = part;
            }
        }

        Object denormInstance = values == null ? null : converter.toJava(values);

        accessor.setObject(instance, denormInstance);

        return denormInstance == null;
    }
}
