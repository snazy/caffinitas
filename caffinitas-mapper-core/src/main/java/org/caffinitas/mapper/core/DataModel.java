/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.annotations.CKeyspace;

import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Parse entity and composite classes and build a parsed model which is used as the input for {@link PersistenceManagerBuilder}.
 */
public class DataModel {

    final Map<Class<?>, ParseEntity> entities = new HashMap<Class<?>, ParseEntity>();
    final Map<Class<?>, ParseMapEntity> mapEntities = new HashMap<Class<?>, ParseMapEntity>();
    final Map<Class<?>, ParseComposite> composites = new HashMap<Class<?>, ParseComposite>();

    /**
     * Add the contents of the {@code other} data model into this datamodel.
     *
     * @param other model to merge
     * @return this model
     */
    public DataModel merge(DataModel other) {
        entities.putAll(other.entities);
        mapEntities.putAll(other.mapEntities);
        composites.putAll(other.composites);
        return this;
    }

    public Set<Class<?>> getEntityClasses() {
        return Collections.unmodifiableSet(entities.keySet());
    }

    public Set<Class<?>> getMapEntityClasses() {
        return Collections.unmodifiableSet(mapEntities.keySet());
    }

    public Set<Class<?>> getCompositeClasses() {
        return Collections.unmodifiableSet(composites.keySet());
    }

    public void addEntity(Class<?> type) {
        addEntityInternal(type);
    }

    ParseEntity addEntityInternal(Class<?> type) {

        ParseEntity parseEntity = entities.get(type);
        if (parseEntity != null) {
            return parseEntity;
        }

        CKeyspace keyspace = findCKeyspace(type);

        parseEntity = new ParseEntity(keyspace, type);
        for (Class<?> c = type; c != Object.class; ) {

            parseEntity.processAccessibles(this, c, parseEntity.parseAttributeMap);

            c = c.getSuperclass();
            if (c == Object.class) {
                break;
            }

            CEntity superEntity = c.getAnnotation(CEntity.class);
            if (superEntity != null) {
                ParseEntity parseEntitySuper = addEntityInternal(c);
                parseEntity.mapSuperEntity(parseEntitySuper);
                break;
            }
        }
        entities.put(type, parseEntity);

        return parseEntity;
    }

    public void addMapEntity(Class<? extends MapEntity> type) {
        addMapEntityInternal(type);
    }

    ParseMapEntity addMapEntityInternal(Class<? extends MapEntity> type) {

        ParseMapEntity parseMapEntity = mapEntities.get(type);
        if (parseMapEntity != null) {
            return parseMapEntity;
        }

        CKeyspace keyspace = findCKeyspace(type);

        parseMapEntity = new ParseMapEntity(keyspace, type);
        for (Class<?> c = type; c != Object.class; ) {

            parseMapEntity.processAccessibles(this, c, parseMapEntity.parseAttributeMap);

            c = c.getSuperclass();
            if (c == Object.class) {
                break;
            }
        }
        for (Class<?> c = parseMapEntity.entity.clusteringType(); c != Object.class; ) {

            parseMapEntity.processAccessibles(this, c, parseMapEntity.clusteringAttributeMap);

            c = c.getSuperclass();
            if (c == Object.class) {
                break;
            }
        }
        for (Class<?> c = parseMapEntity.entity.valueType(); c != Object.class; ) {

            parseMapEntity.processAccessibles(this, c, parseMapEntity.valueAttributeMap);

            c = c.getSuperclass();
            if (c == Object.class) {
                break;
            }
        }
        mapEntities.put(type, parseMapEntity);

        return parseMapEntity;
    }

    public void addComposite(Class<?> type) {
        addCompositeInternal(type);
    }

    void addCompositeInternal(Class<?> type) {

        ParseComposite parseComposite = composites.get(type);
        if (parseComposite != null) {
            return;
        }

        CKeyspace keyspace = findCKeyspace(type);

        parseComposite = new ParseComposite(keyspace, type);
        for (Class<?> c = type; c != Object.class; ) {

            parseComposite.processAccessibles(this, c);

            c = c.getSuperclass();
            if (c == Object.class) {
                break;
            }
        }

        composites.put(type, parseComposite);
    }

    private CKeyspace findCKeyspace(Class<?> type) {
        Package pkg = type.getPackage();
        CKeyspace ks = pkg.getAnnotation(CKeyspace.class);
        if (ks != null) {
            return ks;
        }
        ClassLoader cl = type.getClassLoader();
        String pkgName = pkg.getName();
        while (true) {
            int i = pkgName.lastIndexOf('.');
            if (i != -1) {
                pkgName = pkgName.substring(0, i);
            }
            pkg = getPackage(cl, pkgName);
            if (pkg != null) {
                ks = pkg.getAnnotation(CKeyspace.class);
                if (ks != null) {
                    return ks;
                }
            }
            if (i == -1) {
                return null;
            }
        }
    }

    private static Package getPackage(final ClassLoader cl, final String pkg) {
        return AccessController.doPrivileged(new PrivilegedAction<Package>() {
            @Override public Package run() {
                try {
                    Method method = ClassLoader.class.getDeclaredMethod("getPackage", String.class);
                    boolean previousState = method.isAccessible();
                    try {
                        if (!previousState) {
                            method.setAccessible(true);
                        }
                        return (Package) method.invoke(cl, pkg);
                    } finally {
                        if (!previousState) {
                            method.setAccessible(false);
                        }
                    }
                } catch (Throwable t) {
                    throw new RuntimeException(t);
                }
            }
        });
    }
}
