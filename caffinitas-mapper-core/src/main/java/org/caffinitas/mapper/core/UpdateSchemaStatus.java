/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Internal API.
 */
public final class UpdateSchemaStatus {
    boolean exists;
    private boolean valid = true;
    boolean change;
    private List<String> errors;

    void addError(String err) {
        if (errors == null) {
            errors = new ArrayList<String>();
        }
        errors.add(err);
        valid = false;
    }

    /**
     * Check whether the target object exists in the Cassandra schema
     *
     * @return {@code true} if it exists
     */
    public boolean isExists() {
        return exists;
    }

    /**
     * Check whether the target object in the Cassandra schema is compatible with the declared code.
     *
     * @return {@code true} if it is considered valid
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * Check whether a change has been detected
     *
     * @return {@code true} if a change was detected
     */
    public boolean isChange() {
        return change;
    }

    /**
     * Retrieve list of errors.
     *
     * @return list of errors
     */
    public List<String> getErrors() {
        return errors == null ? Collections.<String>emptyList() : new ArrayList<String>(errors);
    }

    void toTable(CqlTable cqlTable) {
        cqlTable.setExists(exists);
        cqlTable.setValid(valid);
        cqlTable.setErrors(errors);
    }
}
