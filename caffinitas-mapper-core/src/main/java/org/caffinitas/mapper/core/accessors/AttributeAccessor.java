/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.accessors;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.DataTypeMapper;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;

import java.lang.reflect.AccessibleObject;

/**
 * Internal API.
 */
public interface AttributeAccessor {
    AccessibleObject accessible();

    Class<?> type();

    boolean getBoolean(Object instance);

    byte getByte(Object instance);

    char getChar(Object instance);

    short getShort(Object instance);

    int getInt(Object instance);

    long getLong(Object instance);

    float getFloat(Object instance);

    double getDouble(Object instance);

    <T> T getObject(Object instance);

    void setBoolean(Object instance, boolean value);

    void setByte(Object instance, byte value);

    void setChar(Object instance, char value);

    void setShort(Object instance, short value);

    void setInt(Object instance, int value);

    void setLong(Object instance, long value);

    void setFloat(Object instance, float value);

    void setDouble(Object instance, double value);

    boolean setObject(Object instance, Object value);

    Object getAny(Object instance);

    boolean setAny(Object instance, Object value);

    void bind(DataTypeMapper dataTypeMapper, Binder binder, CqlColumn col, Object instance);

    boolean fromRow(DataTypeMapper dataTypeMapper, Retriever row, CqlColumn col, Object instance);
}
