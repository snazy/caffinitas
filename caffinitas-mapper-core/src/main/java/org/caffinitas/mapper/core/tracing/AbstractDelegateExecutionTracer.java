/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.tracing;

import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Statement;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceManager;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.UpdateSchemaStatus;

/**
 * Dynamic execution tracer delegate.
 */
public abstract class AbstractDelegateExecutionTracer implements ExecutionTracer {

    protected abstract ExecutionTracer getDelegate();

    @Override public void onReadResultSetBegin(PersistenceSession persistenceSession,
                                               MappedSchemaObject<?> entity, ResultSet resultSet,
                                               CqlColumn[] columns) {
        ExecutionTracer tracer = getDelegate();
        if (tracer!=null)
            tracer.onReadResultSetBegin(persistenceSession, entity, resultSet, columns);
    }

    @Override public void onReadResultSetRow(PersistenceSession persistenceSession,
                                             MappedSchemaObject<?> entity, ResultSet resultSet,
                                             CqlColumn[] columns, Row row) {
        ExecutionTracer tracer = getDelegate();
        if (tracer!=null)
            tracer.onReadResultSetRow(persistenceSession, entity, resultSet, columns, row);
    }

    @Override public void onReadResultSetEnd(PersistenceSession persistenceSession,
                                             MappedSchemaObject<?> entity, ResultSet resultSet,
                                             CqlColumn[] columns) {
        ExecutionTracer tracer = getDelegate();
        if (tracer!=null)
            tracer.onReadResultSetEnd(persistenceSession, entity, resultSet, columns);
    }

    @Override public void onModifyWrapResultSet(PersistenceSession persistenceSession,
                                                MappedSchemaObject<?> entity, PersistMode mode,
                                                ResultSet resultSet) {
        ExecutionTracer tracer = getDelegate();
        if (tracer!=null)
            tracer.onModifyWrapResultSet(persistenceSession, entity, mode, resultSet);
    }

    @Override public void onBeginModify(PersistenceSession persistenceSession,
                                        MappedSchemaObject<?> entity, PersistMode mode,
                                        Statement statement) {
        ExecutionTracer tracer = getDelegate();
        if (tracer!=null)
            tracer.onBeginModify(persistenceSession, entity, mode, statement);
    }

    @Override public void onBeginQuery(PersistenceSession persistenceSession,
                                       MappedSchemaObject<?> entity, Statement statement) {
        ExecutionTracer tracer = getDelegate();
        if (tracer!=null)
            tracer.onBeginQuery(persistenceSession, entity, statement);
    }

    @Override public void onUpdateSchema(PersistenceManager persistenceManager,
                                         KeyspaceMetadata keyspaceMetadata) {
        ExecutionTracer tracer = getDelegate();
        if (tracer!=null)
            tracer.onUpdateSchema(persistenceManager, keyspaceMetadata);
    }

    @Override public void onUpdateSchemaUserType(PersistenceManager persistenceManager,
                                                 KeyspaceMetadata keyspaceMetadata,
                                                 MappedSchemaObject<?> schemaObject,
                                                 UpdateSchemaStatus status) {
        ExecutionTracer tracer = getDelegate();
        if (tracer!=null)
            tracer.onUpdateSchemaUserType(persistenceManager, keyspaceMetadata, schemaObject, status);
    }

    @Override public void onUpdateSchemaEntity(PersistenceManager persistenceManager,
                                               KeyspaceMetadata keyspaceMetadata,
                                               MappedSchemaObject<?> schemaObject,
                                               UpdateSchemaStatus status) {
        ExecutionTracer tracer = getDelegate();
        if (tracer!=null)
            tracer.onUpdateSchemaEntity(persistenceManager, keyspaceMetadata, schemaObject, status);
    }
}
