/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Session;
import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CDiscriminatorColumn;
import org.caffinitas.mapper.annotations.DataTypeName;
import org.caffinitas.mapper.annotations.InheritanceType;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.codec.Retriever;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class MappedEntitySTRoot extends MappedEntityInheritedRoot<MappedEntityST> {
    final CqlColumn discriminatorColumn;
    final DataType discriminatorDataType;
    private final ByteBuffer discriminatorValue;

    final Map<ByteBuffer, MappedEntityContainer> entityByDiscriminator = new HashMap<ByteBuffer, MappedEntityContainer>();

    MappedEntitySTRoot(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity) {
        super(persistenceManager, parseEntity, MappedEntityST.class);
        if (parseEntity.discriminatorColumn == null) {
            throw new ModelUseException("Use of @" + CDiscriminatorColumn.class.getSimpleName() + " mandatory for root entity " +
                InheritanceType.class.getSimpleName() + '.' +
                InheritanceType.SINGLE_TABLE);
        }

        CColumn discCol = parseEntity.discriminatorColumn.column();
        if (discCol.isStatic()) {
            throw new ModelUseException("static columns not allowed for discriminator columns for " + type);
        }
        String colName = discCol.columnName().trim();
        if (colName.isEmpty()) {
            colName = persistenceManager.defaultDiscriminatorColumnName;
        }
        DataTypeName colType = discCol.type();
        if (colType == DataTypeName.GUESS) {
            colType = persistenceManager.defaultDiscriminatorColumnType;
        }

        discriminatorDataType = colType.getDataType();

        if (discriminatorDataType == null) {
            throw new ModelUseException(
                "data type " + colType + " not a valid data type for discriminator columns for " + type);
        }

        discriminatorColumn = new CqlColumnSimpleStd(colName, discriminatorDataType);
        discriminatorValue = initGetDiscriminatorValue(persistenceManager, parseEntity);

        if (entityByDiscriminator.put(discriminatorValue, this) != null)
            throw new ModelUseException("discriminator value '"+parseEntity.discriminatorValue.value()+"' not unique for entity "+parseEntity.type);
    }

    ByteBuffer initGetDiscriminatorValue(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity) {
        String discriminatorValue = parseEntity.discriminatorValue != null ? parseEntity.discriminatorValue.value() : "";
        if (discriminatorValue.isEmpty()) {
            NameMapper nameMapper = persistenceManager.singletons.getInstanceOf(parseEntity.entity.nameMapper());
            discriminatorValue = nameMapper.mapClass(parseEntity.type.getSimpleName());
        }
        return discriminatorDataType.parse(discriminatorValue);
    }

    @Override public <T> T fromRow(PersistenceSessionImpl session, Retriever retriever, CqlColumn[] columns, T r) {
        MappedEntityContainer entity = entityTypeFromRow(retriever);
        if (entity == this) {
            return super.fromRow(session, retriever, columns, r);
        }
        return entity.fromRow(session, retriever, columns, r);
    }

    @Override BoundStatement buildModifyInitial(Session session, PersistOption[] persistOptions, PreparedStatements.StatementType statementType,
                                                Binder binder) {
        binder.setBytesUnsafe(discriminatorColumn, discriminatorValue);
        return super.buildModifyInitial(session, persistOptions, statementType, binder);
    }

    MappedEntityContainer entityTypeFromRow(Retriever row) {
        int idx = discriminatorColumn.indexForPstmt(allColumns);
        if (idx == -1) {
            throw new PersistenceRuntimeException(
                "discriminator column " + discriminatorColumn + " does not exist in table " + cqlTable + " for " + type);
        }
        ByteBuffer discVal = row.getBytesUnsafe(discriminatorColumn);
        MappedEntityContainer e = entityByDiscriminator.get(discVal);
        if (e == null) {
            throw new PersistenceRuntimeException("cannot determine type for row " + row + " in table " + cqlTable + " for " + type);
        }
        return e;
    }

    @Override MappedEntityST newMappedChildEntity(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity) {
        MappedEntityST child = new MappedEntityST(persistenceManager, parseEntity, this);
        child.preparedStatements = preparedStatements;
        return child;
    }

    @Override protected List<CqlColumn> setupEntityColumnsInner(ParseEntityBase parseEntity, List<CqlColumn> partitionKey,
                                                                List<CqlColumn> clusteringKey) {
        boolean discColUsed = false;

        for (String attrPath : parseEntity.partitionKey) {
            if ("@".equals(attrPath)) {
                partitionKey.add(discriminatorColumn);
                discColUsed = true;
            } else {
                partitionKey.add(validAttributeColumnByPath(attrPath, "partition key"));
            }
        }
        if (partitionKey.isEmpty()) {
            throw new ModelUseException("partition key of " + type + " must not be empty");
        }

        for (String attrPath : parseEntity.clusteringKey) {
            if ("@".equals(attrPath)) {
                clusteringKey.add(discriminatorColumn);
                discColUsed = true;
            } else {
                clusteringKey.add(validAttributeColumnByPath(attrPath, "clustering key"));
            }
        }

        pushAttributesToChildren();

        List<CqlColumn> r = new ArrayList<CqlColumn>();
        if (!discColUsed) {
            r.add(discriminatorColumn);
        }
        // note: do not add own 'data' columns

        for (MappedEntityST child : directChildren) {
            child.pushColumnsToParent(r);
        }

        return r;
    }

    @Override protected void setupEntityColumnsOuter() {
        for (MappedEntityST child : directChildren) {
            child.updateColumnLists(this);
        }
    }

    @Override void verifyForAccess() {
        super.verifyForAccess();
        if (!discriminatorColumn.isExists()) {
            throw new PersistenceRuntimeException(
                "discriminator column " + cqlTable + '.' + discriminatorColumn + " does not exist for type " + type);
        }
    }
}
