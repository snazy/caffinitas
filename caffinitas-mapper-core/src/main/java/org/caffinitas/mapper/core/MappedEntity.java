/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SettableData;
import com.datastax.driver.core.TableMetadata;
import org.caffinitas.mapper.annotations.CNamedQuery;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.GettableRetriever;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a Java class mapped to a Cassandra table.
 */
class MappedEntity extends MappedEntityBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(MappedEntity.class);

    private final boolean isAbstract;

    MappedEntity(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity) {
        this(persistenceManager, parseEntity, createCqlTable(persistenceManager, parseEntity));
    }

    MappedEntity(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity, CqlTable cqlTable) {
        super(persistenceManager, parseEntity, cqlTable,
            parseEntity.entity.defaultTTL() > 0 ? PersistOption.ttl(parseEntity.entity.defaultTTL()) : null,
            parseEntity.entity.writeNullOnInsert());

        isAbstract = Modifier.isAbstract(type.getModifiers());

        for (CNamedQuery namedQuery : parseEntity.entity.namedQueries()) {
            namedQueries.put(namedQuery.name(), namedQuery.condition());
        }

        if (isAbstract) {
            lock.lock();
            try {
                existsCondition.signalAll();
                existsAndValidCondition.signalAll();
            } finally {
                lock.unlock();
            }
        }
    }

    @Override Logger logger() {
        return LOGGER;
    }

    void pullPrimaryKey(MappedEntity source) {
        partitionKeyColumns = source.partitionKeyColumns;
        clusteringKeyColumns = source.clusteringKeyColumns;
        primaryKeyColumns = source.primaryKeyColumns;
        primaryKeyColumnsVariable = source.primaryKeyColumnsVariable;
    }

    @Override protected List<CqlColumn> setupEntityColumnsInner(ParseEntityBase parseEntity, List<CqlColumn> partitionKey,
                                                                List<CqlColumn> clusteringKey) {
        for (String attrPath : parseEntity.partitionKey) {
            partitionKey.add(validAttributeColumnByPath(attrPath, "partition key"));
        }
        if (partitionKey.isEmpty()) {
            throw new ModelUseException("partition key of " + type + " must not be empty");
        }

        for (String attrPath : parseEntity.clusteringKey) {
            clusteringKey.add(validAttributeColumnByPath(attrPath, "clustering key"));
        }

        return new ArrayList<CqlColumn>();
    }

    CqlColumn validAttributeColumnByPath(String attrPath, String attrType) {
        MappedAttribute attr = attributePaths.get(attrPath);
        if (attr instanceof MappedAttributeSingle) {
            return ((MappedAttributeSingle) attr).singleColumn();
        } else if (attr == null) {
            throw new ModelUseException("attribute path '" + attrPath + "' of " + type + ' ' + attrType + " does not map to a an attribute");
        } else {
            throw new ModelUseException("last part in attribute path '" + attrPath + "' of " + type + ' ' + attrType + " must not be a composite");
        }
    }

    @Override protected void setupEntityColumnsOuter() {

    }

    @Override void verifyForAccess() {
        if (isAbstract) {
            return;
        }
        super.verifyForAccess();
    }

    @Override public CqlStatementList getAlterDDL(TableMetadata tableMeta) {
        if (isAbstract) {
            return null;
        }
        return super.getAlterDDL(tableMeta);
    }

    @Override public String getDropDDL() {
        if (isAbstract) {
            return null;
        }
        return super.getDropDDL();
    }

    @Override public String getCreateDDL() {
        if (isAbstract) {
            return null;
        }
        return super.getCreateDDL();
    }

    @Override @SuppressWarnings("unchecked") <T> T fromRow(PersistenceSessionImpl session, Retriever retriever, CqlColumn[] columns, T last) {
        if (isAbstract) {
            throw new ModelUseException("type " + type.getName() + " is abstract");
        }

        T instance = session.cachedEntity(this, retriever);
        if (instance == null) {
            instance = (T) session.persistenceManager.objectFactory.newInstance(type);
        }

        session.loaded(this, retriever, instance);

        for (MappedAttribute attr : allAttributes) {
            attr.fromRow(session, instance, instance, retriever);
        }

        if (postLoad != null) {
            postLoad.invoke(instance, this, retriever);
        }

        return instance;
    }

    @Override protected BinderAndStatement buildInsertStatement(StatementOptions statementOptions, PersistenceSessionImpl persistenceSession,
                                                                Object instance, PersistOption... persistOptions) {
        persistOptions = withTTL(persistOptions);

        // INSERT INTO keyspace_name.table_name
        // ( column_name, column_name...)
        // VALUES ( value, value ... )
        // IF NOT EXISTS
        // USING option AND option
        boolean serializeDefault = PersistOption.WriteDefaultValueOption.get(persistOptions, writeNullOnInsert);
        CqlColumn[] columns = PersistOption.ColumnRestrictionOption.filter(persistOptions, writeDataColumns);
        Binder binder = new Binder(columns, true, primaryKeyColumns, PersistMode.INSERT, serializeDefault);

        buildModifyBindColumns(instance, binder);

        BoundStatement bStmt =
            buildModifyInitial(persistenceSession.driverSession(), persistOptions, PreparedStatements.StatementType.INSERT, binder);

        int idx = binder.bindColumns(0, bStmt);

        bindUsingOptions(bStmt, persistOptions, idx);

        statementOptions.applyWrite(bStmt, writeConsistencyLevel, serialConsistencyLevel, persistOptions);

        if (prePersist != null) {
            prePersist.invoke(instance, this, type, bStmt);
        }

        return new BinderAndStatement(bStmt, binder);
    }

    @Override protected BinderAndStatement buildUpdateStatement(StatementOptions statementOptions, PersistenceSessionImpl persistenceSession,
                                                                Object instance, PersistOption... persistOptions) {
        if (preparedStatements.noUpdates()) {
            // entities without any data columns (just primary key columns) have no updateable columns - so just do an insert
            return buildInsertStatement(statementOptions, persistenceSession, instance, persistOptions);
        }

        persistOptions = withTTL(persistOptions);

        // UPDATE keyspace_name.table_name
        // USING option AND option
        // SET assignment , assignment, ...
        // WHERE row_specification
        // IF column_name = literal AND column_name = literal . . .
        CqlColumn[] columns = PersistOption.ColumnRestrictionOption.filter(persistOptions, writeDataColumns);
        Binder binder = new Binder(columns, true, primaryKeyColumns, PersistMode.UPDATE, true);

        buildModifyBindColumns(instance, binder);

        if (persistenceSession.filterUnmodifiedForUpdate(this, instance, binder)) {
            // return null to indicate that the update will not be executed
            return null;
        }

        BoundStatement bStmt = updateBoundStatement(statementOptions, persistenceSession.driverSession(), instance, binder, persistOptions);

        return new BinderAndStatement(bStmt, binder);
    }

    // used by buildUpdateStatement and PersistenceSesss
    @Override BoundStatement updateBoundStatement(StatementOptions statementOptions, Session session, Object instance, Binder binder,
                                                  PersistOption... persistOptions) {
        persistOptions = withTTL(persistOptions);

        BoundStatement bStmt = buildModifyInitial(session, persistOptions, PreparedStatements.StatementType.UPDATE, binder);

        int idx = bindUsingOptions(bStmt, persistOptions, 0);

        idx = binder.bindColumns(idx, bStmt);

        bindIfOptions(binder, bStmt, persistOptions, idx);

        statementOptions.applyWrite(bStmt, writeConsistencyLevel, serialConsistencyLevel, persistOptions);

        if (prePersist != null) {
            prePersist.invoke(instance, this, type, bStmt);
        }

        return bStmt;
    }

    @Override protected BinderAndStatement buildDeleteStatement(StatementOptions statementOptions, PersistenceSessionImpl persistenceSession,
                                                                Object instance, PersistOption... persistOptions) {

        // DELETE column_name, ... | ( column_name term )
        // FROM keyspace_name. table_name
        // USING TIMESTAMP integer
        // WHERE row_specification
        // ( IF ( EXISTS | (column_name = literal ) ) AND ( column_name = literal ) . . . )

        Binder binder = new Binder(null, false, primaryKeyColumns, PersistMode.DELETE, true);

        buildModifyBindColumns(instance, binder);

        BoundStatement bStmt =
            buildModifyInitial(persistenceSession.driverSession(), persistOptions, PreparedStatements.StatementType.DELETE, binder);

        int idx = bindUsingOptions(bStmt, persistOptions, 0);

        binder.bindColumns(idx, bStmt);

        bindIfOptions(binder, bStmt, persistOptions, idx);

        statementOptions.applyWrite(bStmt, writeConsistencyLevel, serialConsistencyLevel, persistOptions);

        if (preDelete != null) {
            preDelete.invoke(instance, this, type, bStmt);
        }

        return new BinderAndStatement(bStmt, binder);
    }

    private int bindUsingOptions(SettableData<?> settableData, PersistOption[] persistOptions, int idx) {
        for (PersistOption option : persistOptions) {
            if (option instanceof PersistOption.UsingOption) {
                ((PersistOption.UsingOption) option).bind(settableData, idx++);
            }
        }
        return idx;
    }

    private void bindIfOptions(Binder binder, BoundStatement bStmt, PersistOption[] persistOptions, int idx) {
        for (PersistOption option : persistOptions) {
            if (option instanceof PersistOption.IfOption) {
                PersistOption.IfOption ifOption = (PersistOption.IfOption) option;
                MappedAttribute attr = getAttributeByPath(ifOption.getAttrPath());
                if (!(attr instanceof MappedAttributeSingle)) {
                    throw new PersistenceRuntimeException(
                        "attribute path '" + ifOption + "' specified in IfOption does not resolve to a single-column attribute in type " +
                            type.getName());
                }
                MappedAttributeSingle singleAttr = (MappedAttributeSingle) attr;
                CqlColumn col = singleAttr.singleColumn();
                DataTypeMapper dataTypeMapper = col.dataTypeMapper;
                binder.bindSpecial(bStmt, col, idx);
                dataTypeMapper.fromObject(binder, col, ifOption.getValue());
            }
        }
        binder.clearSpecial();
    }

    BoundStatement buildModifyInitial(Session session, PersistOption[] persistOptions, PreparedStatements.StatementType statementType,
                                      Binder binder) {
        if (isAbstract) {
            throw new ModelUseException("type " + type.getName() + " is abstract");
        }

        PreparedStatement pStmt = preparedStatements.statementFor(session, statementType, binder, persistOptions);
        BoundStatement bStmt = pStmt.bind();

        binder.bindColumns(0, bStmt);

        PersistOption.forBoundStatement(persistOptions, bStmt);
        return bStmt;
    }

    @Override void buildModifyBindColumns(Object instance, Binder binder) {
        // TODO optimize to iterate only over either primaryKey attributes or all attributes
        buildModifyBindColumns(instance, binder, allAttributes);
    }

    @Override void executeLoadBoundStatements(ExecutionTracer tracer, Session session, Object[] primaryKey, AbstractDelegatesFutureExt<?> futures,
                                              StatementOptions statementOptions, PersistOption... persistOptions) {
        if (isAbstract) {
            return;
        }

        if (primaryKey.length < partitionKeyColumns.length) {
            throw new ModelRuntimeException("too few values for partition key for " + type + " - expected:" +
                Arrays.toString(partitionKeyColumns) + " - got " + Arrays.toString(primaryKey));
        }
        if (primaryKey.length > primaryKeyColumns.length) {
            throw new ModelRuntimeException("too many values for primary key for " + type + " - expected:" +
                Arrays.toString(primaryKeyColumns) + " - got " + Arrays.toString(primaryKey));
        }

        CqlColumn[] readColumns = readColumns(persistOptions);
        CqlColumn[] pkColumns = primaryKeyColumnsVariable[primaryKey.length - partitionKeyColumns.length];

        Binder binder = new Binder(readColumns, false, pkColumns, null, true);

        int idx = 0;
        try {
            for (CqlColumn col : pkColumns) {
                col.dataTypeMapper.fromObject(binder, col, primaryKey[idx++]);
            }
        } catch (Throwable t) {
            throw new RuntimeException("Invalid primary key element "+Arrays.toString(primaryKey), t);
        }

        PreparedStatement pstmt = preparedStatements.statementFor(session, PreparedStatements.StatementType.SELECT, binder, persistOptions);
        BoundStatement bStmt = pstmt.bind();
        statementOptions.applyRead(bStmt, readConsistencyLevel, persistOptions);
        PersistOption.forBoundStatement(persistOptions, bStmt);

        idx = binder.bindColumns(0, bStmt);

        PersistOption.LimitOption.apply(bStmt, persistOptions, idx);

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Submitting load for {} : {}", type, bStmt.preparedStatement().getQueryString());
        }
        if (tracer != null) {
            tracer.onBeginQuery(futures.session, this, bStmt);
        }
        futures.addResultSetFuture(this, session.executeAsync(bStmt), readColumns);
    }

    @Override <T> boolean executeLoadLazy(PersistenceSessionImpl persistenceSession, StatementOptions statementOptions, T container,
                                          MappedAttribute[] attrs,
                                          PersistOption[] persistOptions) {
        if (attrs == null || attrs.length == 0) {
            // load all lazy attributes

            List<MappedAttribute> lazyAttr = null;
            for (MappedAttribute attr : attributePaths.values()) {
                if (attr.isLazy()) {
                    if (lazyAttr == null)
                        lazyAttr = new ArrayList<MappedAttribute>();
                    lazyAttr.add(attr);
                }
            }
            attrs = lazyAttr == null ? MappedAttribute.NONE : lazyAttr.toArray(new MappedAttribute[lazyAttr.size()]);
        }

        if (attrs.length == 0) {
            throw new PersistenceRuntimeException("no lazy attributes to load");
        }

        // concat all attribute columns in a single array
        int colCount = 0;
        for (MappedAttribute attr : attrs) {
            colCount += attr.allColumns.length;
        }
        CqlColumn[] loadColumns = new CqlColumn[colCount];
        int iCol = 0;
        for (MappedAttribute attr : attrs) {
            CqlColumn[] aCols = attr.allColumns;
            int aColCnt = aCols.length;
            if (aColCnt == 1)
                loadColumns[iCol++] = aCols[0];
            else if (aColCnt > 0) {
                System.arraycopy(aCols, 0, loadColumns, iCol, aColCnt);
                iCol += aColCnt;
            }
        }

        Binder binder = new Binder(loadColumns, false, primaryKeyColumns, null, true);

        buildModifyBindColumns(container, binder);

        Session session = persistenceSession.driverSession();

        PreparedStatement pstmt = preparedStatements
            .statementFor(session, PreparedStatements.StatementType.SELECT, binder, persistOptions);
        BoundStatement bStmt = pstmt.bind();
        statementOptions.applyRead(bStmt, readConsistencyLevel, persistOptions);
        PersistOption.forBoundStatement(persistOptions, bStmt);

        int idx = binder.bindColumns(0, bStmt);

        PersistOption.LimitOption.apply(bStmt, persistOptions, idx);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Lazy load of {} attributes for {}", attrs.length, type);
        }

        ResultSet resultSet = session.execute(bStmt);
        Row row = resultSet.one();
        if (row != null) {
            Retriever retriever = new GettableRetriever(persistenceSession.persistenceManager.protocolVersion, row, loadColumns);
            for (MappedAttribute attr : attrs) {
                Object instance = resolveParentInstance(container, attr);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Applying attribute {} to {} for {}", attr.name, instance.getClass(), type);
                }
                attr.fromRow(persistenceSession, container, instance, retriever);
            }
            return true;
        }

        LOGGER.warn("Lazy load of {} attributes returned no results for {}", attrs.length, type);
        return false;
    }

    @SuppressWarnings("unchecked") @Override <T> void setupQueryBinder(QueryBinderImpl<T> queryBinder, PersistOption[] persistOptions,
                                                                       String queryName,
                                                                       StatementOptions statementOptions) {
        if (queryName != null) {
            String namedQuery = namedQueries.get(queryName);
            if (namedQuery == null) {
                throw new PersistenceRuntimeException("no named query '" + queryName + "' for entity type " + type);
            }
            queryBinder.addCondition((Class<? extends T>) type, namedQuery);
        }

        if (queryBinder.statementFor(type) != null) {
            return;
        }

        CqlColumn[] readColumns = readColumns(persistOptions);

        PreparedStatement pstmt = preparedStatements.statementFor(queryBinder.session, PreparedStatements.StatementType.SELECT,
            readColumns, queryBinder.conditionFor((Class<? extends T>) type), persistOptions);
        BoundStatement bStmt = pstmt.bind();
        statementOptions.applyRead(bStmt, readConsistencyLevel, persistOptions);
        PersistOption.forBoundStatement(persistOptions, bStmt);
        queryBinder.addStatement((Class<? extends T>) type, bStmt, readColumns);
    }

    @Override <T> void executeQuery(ExecutionTracer tracer, QueryBinderImpl<T> queryBinder, ReadFutureImpl<T> futures) {
        if (isAbstract) {
            return;
        }

        BoundStatement bStmt = queryBinder.statementFor(type);
        if (bStmt != null) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("Submitting query for {} : {}", type, bStmt.preparedStatement().getQueryString());
            }
            futures.addResultSetFuture(this, queryBinder.session.executeAsync(bStmt), queryBinder.readColumnsFor(type));
        }
    }

    void verifyType() {
        if (Modifier.isAbstract(type.getModifiers())) {
            throw new ModelUseException("type " + type.getName() + " is abstract");
        }
    }
}
