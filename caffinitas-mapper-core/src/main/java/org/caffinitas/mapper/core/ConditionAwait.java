/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Contains a specific status flag (for example CQL table exists flag) and provides methods to await that flag to be {@code true}.
 */
public interface ConditionAwait {

    /**
     * Test if the condition has been signalled.
     */
    boolean isSignalled();

    /**
     * Wait for the condition to get signalled (or has already been signalled).
     *
     * @see java.util.concurrent.locks.Condition#await
     */
    void await() throws InterruptedException;

    /**
     * Wait for the condition to get signalled (or has already been signalled).
     *
     * @param deadline the absolute time to wait until
     * @return {@code false} if the deadline has elapsed upon return, else
     * {@code true}
     * @see java.util.concurrent.locks.Condition#awaitUntil(java.util.Date)
     */
    boolean awaitUntil(Date deadline) throws InterruptedException;

    /**
     * Wait for the condition to get signalled (or has already been signalled).
     *
     * @param time the maximum time to wait
     * @param unit the time unit of the {@code time} argument
     * @return {@code false} if the waiting time detectably elapsed
     * before return from the method, else {@code true}
     * @see java.util.concurrent.locks.Condition#await(long, java.util.concurrent.TimeUnit)
     */
    boolean await(long time, TimeUnit unit) throws InterruptedException;

    /**
     * @param nanosTimeout the maximum time to wait, in nanoseconds
     * @return an estimate of the {@code nanosTimeout} value minus
     * the time spent waiting upon return from this method.
     * A positive value may be used as the argument to a
     * subsequent call to this method to finish waiting out
     * the desired time.  A value less than or equal to zero
     * indicates that no time remains.
     * @see java.util.concurrent.locks.Condition#awaitNanos(long)
     */
    long awaitNanos(long nanosTimeout) throws InterruptedException;

    /**
     * Similar to {@link #await()}.
     *
     * @see java.util.concurrent.locks.Condition#awaitUninterruptibly()
     */
    void awaitUninterruptibly();
}
