/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Cassandra identifier name utility methods.
 */
public final class Names {
    static final Set<String> KEYWORDS = new HashSet<String>(Arrays.asList(
        "ADD", "ALLOW", "ALTER", "AND", "ANY", "APPLY", "ASC", "AUTHORIZE",
        "BATCH", "BEGIN", "BY",
        "COLUMNFAMILY", "CREATE", "DELETE", "DESC", "DROP",
        "EACH_QUORUM", "FROM", "GRANT",
        "IN", "INDEX", "INET", "INFINITY", "INSERT", "INTO",
        "KEYSPACE", "KEYSPACES",
        "LIMIT", "LOCAL_ONE", "LOCAL_QUORUM",
        "MODIFY",
        "NAN", "NORECURSIVE",
        "OF", "ON", "ONE", "ORDER",
        "PASSWORD", "PRIMARY", "QUORUM",
        "RENAME", "REVOKE", "SCHEMA", "SELECT", "SET",
        "TABLE", "THREE", "TO", "TOKEN", "TRUNCATE", "TUPLE", "TWO",
        "UNLOGGED", "UPDATE", "USE", "USING",
        "WHERE", "WITH"));

    private Names() {}

    /**
     * Checks whether the given name must be escaped and returns the escaped form.
     *
     * @param name identifier to test
     * @return either the escaped form (if {@code name} must be escaped) or just the input value if no escaping is necessary
     */
    public static String maybeEscapeCassandraName(String name) {
        int l = name.length();
        for (int i = 0; i < l; i++) {
            char c = name.charAt(i);
            if (c == '_' || (c >= 'a' && c <= 'z') || (i > 0 && c >= '0' && c <= '9')) {
                continue;
            }
            return '\"' + name + '\"';
        }
        if (KEYWORDS.contains(name.toUpperCase())) {
            return '\"' + name + '\"';
        }
        return name;
    }
}
