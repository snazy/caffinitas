/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.BatchStatement;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * Represents a session to access data in Cassandra.
 * <p>
 * Note: unlike relational databases Cassandra does not (really) distinguish between {@code INSERT} and {@code UPDATE}.
 * It is perfectly legal to {@code INSERT} the same primary key twice - this results in an update.
 * Similar for {@code UPDATE} - an {@code UPDATE} to a non-existing primary key results in an persist.
 * This is called an <i>{@code UPSERT}</i>.
 * </p>
 */
public interface PersistenceSession extends StatementConfig<PersistenceSession>, Closeable {
    PersistenceManager getPersistenceManager();

    @Override void close() throws IOException;

    /**
     * Evict a specific cached entity object instance.
     * Only for sessions created with {@code useInstanceTracking==true}.
     *
     * @param instance entity object instance to evict
     * @see #clear()
     */
    <T> T evict(T instance);

    /**
     * Check all cached entity object instances and update them in C* if the objects changed.
     * Only for sessions created with {@code useInstanceTracking==true}.
     * Note that this method returns after all DML statements have been submitted. To wait for the
     * modifications to complete, wait for the returned future to complete.
     *
     * @return future to wait for
     */
    Future flush();

    /**
     * Unconditionally clear the cached entity object instances.
     * Only for sessions created with {@code useInstanceTracking==true}.
     *
     * @see #evict(Object)
     */
    void clear();

    /**
     * Shortcut of {@link #insertAsync(Object, PersistOption...)}{@code .getUninterruptibly}.
     */
    <T> void insert(T instance, PersistOption... persistOptions);

    /**
     * Shortcut of {@link #updateAsync(Object, PersistOption...)}{@code .getUninterruptibly}.
     * Note: if the entity only has primary key columns, this method delegates to {@link #insert(Object, PersistOption...)}.
     */
    <T> void update(T instance, PersistOption... persistOptions);

    /**
     * Shortcut of {@link #deleteAsync(Object, PersistOption...)}{@code .getUninterruptibly}.
     */
    <T> void delete(T instance, PersistOption... persistOptions);

    /**
     * Shortcut of {@link #loadOneAsync(Class, Object...)}{@code .getUninterruptibly}.
     */
    <T> T loadOne(Class<T> type, Object... primaryKey);

    /**
     * Shortcut of {@link #loadMultipleAsync(Class, Object...)}{@code .getUninterruptibly}.
     */
    <T> List<T> loadMultiple(Class<T> type, Object... primaryKey);

    /**
     * Shortcut of {@link #loadOneWithOptionsAsync(Class, PersistOption[], Object...)}{@code .getUninterruptibly}.
     */
    <T> T loadOneWithOptions(Class<T> type, PersistOption[] options, Object... primaryKey);

    /**
     * Shortcut of {@link #loadMultipleWithOptionsAsync(Class, PersistOption[], Object...)}{@code .getUninterruptibly}.
     */
    <T> List<T> loadMultipleWithOptions(Class<T> type, PersistOption[] options, Object... primaryKey);

    /**
     * Load one entity object instance for the given (partial) primary key.
     *
     * @param type       entity type
     * @param primaryKey primary key - must contain at least the partition key columns and may include clustering key columns
     * @param <T>        entity type
     * @return future to retrieve the first row from the result. The future returns {@code null} if no row could be found.
     */
    <T> ReadOneFuture<T> loadOneAsync(Class<T> type, Object... primaryKey);

    /**
     * Load multiple entity object instance for the given (partial) primary key.
     *
     * @param type       entity type
     * @param primaryKey primary key - must contain at least the partition key columns and may include clustering key columns
     * @param <T>        entity type
     * @return future to retrieve the list of results.
     */
    <T> ReadFuture<List<T>> loadMultipleAsync(Class<T> type, Object... primaryKey);

    /**
     * Load one entity object instance for the given (partial) primary key.
     *
     * @param type       entity type
     * @param options    options to use with {@code SELECT}
     * @param primaryKey primary key - must contain at least the partition key columns and may include clustering key columns
     * @param <T>        entity type
     * @return future to retrieve the first row from the result. The future returns {@code null} if no row could be found.
     */
    <T> ReadOneFuture<T> loadOneWithOptionsAsync(Class<T> type, PersistOption[] options, Object... primaryKey);

    /**
     * Load multiple entity object instance for the given (partial) primary key.
     *
     * @param type       entity type
     * @param options    options to use with {@code SELECT}
     * @param primaryKey primary key - must contain at least the partition key columns and may include clustering key columns
     * @param <T>        entity type
     * @return future to retrieve the list of results.
     */
    <T> ReadFuture<List<T>> loadMultipleWithOptionsAsync(Class<T> type, PersistOption[] options, Object... primaryKey);

    /**
     * Create a new query binder for the given entity type.
     *
     * @param type                 (base) entity type
     * @param options              persist options used during query execution. Pass options for {@code ORDER BY}, {@code LIMIT} an {@code ORDER BY} if applicable using options.
     * @param condition            CQL condition string (without {@code WHERE} keyword)
     * @param additionalConditions map with entity type class to conditions (without {@code WHERE} keyword. Only meaningful if used for subclasses
     *                             of {@code type} in a {@link org.caffinitas.mapper.annotations.InheritanceType#TABLE_PER_CLASS} tree
     * @param <T>                  entity (base) type
     * @return new query binder
     */
    <T> QueryBinder<T> createQueryBinder(Class<T> type, PersistOption[] options, String condition,
                                         Map<Class<? extends T>, String> additionalConditions);

    /**
     * Create a new query binder for the given entity type.
     *
     * @param type      (base) entity type
     * @param options   persist options used during query execution. Pass options for {@code ORDER BY}, {@code LIMIT} an {@code ORDER BY} if applicable using options.
     * @param queryName name of the named query specified using {@link org.caffinitas.mapper.annotations.CEntity#namedQueries() CEntity.namedQueries}
     * @param <T>       entity (base) type
     * @return new query binder
     */
    <T> QueryBinder<T> createNamedQueryBinder(Class<T> type, PersistOption[] options, String queryName);

    /**
     * Execute the given parameterized query.
     * Convinience for {@link #executeQueryAsync(QueryBinder)}.{@link ReadFuture#getUninterruptibly()}.
     *
     * @param queryBinder parameterized query binder
     * @param <T>         entity (base) type
     * @return result list
     */
    <T> List<T> executeQuery(QueryBinder<T> queryBinder);

    /**
     * Execute the given parameterized query.
     *
     * @param queryBinder parameterized query binder
     * @param <T>         entity (base) type
     * @return read future
     */
    <T> ReadFuture<List<T>> executeQueryAsync(QueryBinder<T> queryBinder);

    /**
     * Persists the given entity object instance. Executes a CQL {@code INSERT}.
     *
     * @param instance       entity object instance to persist
     * @param persistOptions persist options
     * @param <T>            entity type
     * @return modification future
     */
    <T> ModifyFuture<T> insertAsync(T instance, PersistOption... persistOptions);

    /**
     * Update the given entity object instance. Executes a CQL {@code UPDATE}.
     * Note: if the entity only has primary key columns, this method delegates to {@link #insertAsync(Object, PersistOption...)}.
     *
     * @param instance       entity object instance to persist
     * @param persistOptions persist options
     * @param <T>            entity type
     * @return modification future
     */
    <T> ModifyFuture<T> updateAsync(T instance, PersistOption... persistOptions);

    /**
     * Delete the given entity object instance.
     *
     * @param instance       entity object instance to persist
     * @param persistOptions persist options
     * @param <T>            entity type
     * @return modification future
     */
    <T> ModifyFuture<T> deleteAsync(T instance, PersistOption... persistOptions);

    /**
     * Used to call lazyly loaded attributes (those annotated with {@link org.caffinitas.mapper.annotations.CLazy}).
     *
     * @param container      containing entity instance
     * @param persistOptions persist options
     */
    <T> boolean loadLazy(T container, PersistOption... persistOptions);

    /**
     * Equivalent to calling {@link #startBatch(com.datastax.driver.core.BatchStatement.Type) startBatch(BatchStatement.Type.LOGGED)}.
     */
    Batch startBatch();

    /**
     * Starts DML batch.
     */
    Batch startBatch(BatchStatement.Type type);
}
