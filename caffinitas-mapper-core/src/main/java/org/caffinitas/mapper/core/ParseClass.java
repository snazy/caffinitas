/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CKeyspace;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

abstract class ParseClass {
    final CKeyspace keyspace;
    final Class<?> type;
    Class<? extends NameMapper> nameMapper;
    final Map<String, ParseAttribute> parseAttributeMap = new HashMap<String, ParseAttribute>();

    protected ParseClass(CKeyspace keyspace, Class<?> type) {

        if (Modifier.isInterface(type.getModifiers())) {
            throw new ModelParseException("type " + type.getName() + " is an interface");
        }

        this.keyspace = keyspace;
        this.type = type;
    }

    void processFieldAttribute(DataModel model, Field field, Map<String, ParseAttribute> targetAttributeMap) {
        String attrName = field.getName();
        Class<?> attrType = field.getType();
        Type attrGenericType = field.getGenericType();

        processAccessible(model, new ParseAttribute(field, attrName, attrType, attrGenericType, AccessorType.FIELD), targetAttributeMap);
    }

    boolean processMethodAttribute(DataModel model, Method method, Map<String, ParseAttribute> targetAttributeMap) {
        String methodName = method.getName();
        Class<?> methodReturnType = method.getReturnType();
        Class<?>[] methodParameterTypes = method.getParameterTypes();

        String attrName;
        String methodNamePart;
        if (methodReturnType != void.class && methodParameterTypes.length == 0) {
            if (methodName.length() > 3 && methodName.startsWith("get") && Character.isUpperCase(methodName.charAt(3))) {
                methodNamePart = methodName.substring(3);
                attrName = methodToProperty(methodNamePart);
                return processMethodAttribute(model, method, attrName, methodReturnType, method.getGenericReturnType(), methodNamePart,
                    targetAttributeMap);
            } else if (methodName.length() > 2 && methodName.startsWith("is") &&
                Character.isUpperCase(methodName.charAt(2))) {
                methodNamePart = methodName.substring(2);
                attrName = methodToProperty(methodNamePart);
                return processMethodAttribute(model, method, attrName, methodReturnType, method.getGenericReturnType(), methodNamePart,
                    targetAttributeMap);
            }
        }

        return false;
    }

    private boolean processMethodAttribute(DataModel model, Method method, String attrName,
                                           Class<?> attrType, Type attrGenericType, String methodNamePart,
                                           Map<String, ParseAttribute> targetAttributeMap) {

        ParseAttribute parseAttr = new ParseAttribute(method, attrName, attrType, attrGenericType, AccessorType.GETTER);
        if (!processAccessible(model, parseAttr, targetAttributeMap)) {
            return false;
        }

        for (Class<?> c = type; c != Object.class; c = c.getSuperclass()) {
            try {
                parseAttr.withSetter(c.getDeclaredMethod("set" + methodNamePart, attrType));
                return true;
            } catch (NoSuchMethodException e) {
                // loop
            }
        }
        throw new ModelParseException("Declared getter has no corresponding setter : " + method);
    }

    boolean processAccessible(DataModel model, ParseAttribute parseAttribute, Map<String, ParseAttribute> targetAttributeMap) {

        if (!parseAttribute.handle(model)) {
            return false;
        }

        ParseAttribute existing = targetAttributeMap.get(parseAttribute.attrName);
        if (existing != null) {
            throw new ModelParseException(
                "Duplicate definition for same attribute '" + existing.attrName + "' : " + parseAttribute.accessible + " , " +
                    existing.accessible
            );
        }
        targetAttributeMap.put(parseAttribute.attrName, parseAttribute);

        return true;
    }

    ParseAttribute attributeByName(String name) {
        return parseAttributeMap.get(name);
    }

    /**
     * Does method part name conversion (just the part after 'get'/'set'/'is)
     * to property name.
     * <p/>
     * Rules are:<br/>
     * bLA --> isbLA/getbLA/setbLA<br/>
     * BLA --> isBLA/getBLA/setBLA<br/>
     * bla --> isBla/getBla/setBla<br/>
     */
    private static String methodToProperty(String s) {

        char[] arr = s.toCharArray();
        char first = arr[0];
        if (!Character.isJavaIdentifierStart(first)) {
            return null;
        }

        if (Character.isLowerCase(first) //
            || arr.length > 1 && Character.isUpperCase(arr[1])) {
            return s;
        }

        arr[0] = Character.toLowerCase(first);

        return new String(arr);
    }
}
