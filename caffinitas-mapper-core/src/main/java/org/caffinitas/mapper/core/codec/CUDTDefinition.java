/*
 *      Copyright (C) 2012 DataStax Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.caffinitas.mapper.core.codec;

import com.datastax.driver.core.UserType;
import com.google.common.collect.ImmutableMap;
import org.caffinitas.mapper.core.CqlTable;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * The concrete definition of a user defined type (UDT).
 * <p>
 * A UDT is a essentially a named collection of fields (with a name and a type).
 * </p>
 */
public class CUDTDefinition {

    private final String keyspace;
    private final String name;

    final int protocolVersion;

    // Note that we don't expose the order of fields, from an API perspective this is a map
    // of String->Field, but internally we care about the order because the serialization format
    // of UDT expects a particular order.
    final Field[] byIdx;
    // For a given name, we can only have one field with that name, so we don't need a int[] in
    // practice. However, storing one element arrays save allocations in UDTValue.getAllIndexesOf
    // implementation.
    final Map<String, int[]> byName;

    public CUDTDefinition(int protocolVersion, CqlTable table) {
        this.protocolVersion = protocolVersion;
        this.keyspace = table.getKeyspace();
        this.name = table.getTable();
        this.byIdx = null;
        this.byName = null;
    }

    public CUDTDefinition(int protocolVersion, UserType driver) {
        this.protocolVersion = protocolVersion;
        this.keyspace = driver.getKeyspace();
        this.name = driver.getTypeName();
        int sz = driver.size();
        byIdx = new Field[sz];
        int i = 0;
        byName = new HashMap<String, int[]>();
        for (String fieldName : driver.getFieldNames()) {
            com.datastax.driver.core.DataType ft = driver.getFieldType(fieldName);
            Field f = new Field(fieldName, CDT.copy(protocolVersion, ft));
            byName.put(fieldName, new int[]{i});
            byIdx[i++] = f;
        }
    }

    CUDTDefinition(int protocolVersion, String keyspace, String name, Collection<Field> fields) {
        this.protocolVersion = protocolVersion;
        this.keyspace = keyspace;
        this.name = name;
        this.byIdx = fields.toArray(new Field[fields.size()]);

        ImmutableMap.Builder<String, int[]> builder = new ImmutableMap.Builder<String, int[]>();
        for (int i = 0; i < byIdx.length; i++) {
            builder.put(byIdx[i].getName(), new int[]{i});
        }
        this.byName = builder.build();
    }

    /**
     * Returns a new empty value for this user type definition.
     *
     * @return an empty value for this user type definition.
     */
    public CUDTValue newValue() {
        return new CUDTValue(this);
    }

    /**
     * The name of the keyspace this UDT is part of.
     *
     * @return the name of the keyspace this UDT is part of.
     */
    public String getKeyspace() {
        return keyspace;
    }

    /**
     * The name of this UDT.
     *
     * @return the name of this UDT.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the number of fields in this UDT.
     *
     * @return the number of fields in this UDT.
     */
    public int size() {
        if (byIdx == null) {
            throw new IllegalStateException("abstract UDT definition - implementation bug");
        }
        return byIdx.length;
    }

    /**
     * Returns the type of a given field.
     *
     * @param name the name of the field. Note that {@code name} obey the usual
     *             CQL identifier rules: it should be quoted if it denotes a case sensitive
     *             identifier (you can use {@link com.datastax.driver.core.Metadata#quote} for the quoting).
     * @return the type of field {@code name} if this UDT has a field of this
     * name, {@code null} otherwise.
     * @throws IllegalArgumentException if {@code name} is not a field of this
     *                                  UDT definition.
     */
    public DataType getFieldType(String name) {
        if (byName == null) {
            throw new IllegalStateException("abstract UDT definition - implementation bug");
        }
        int[] idx = byName.get(Misc.handleId(name));
        if (idx == null) {
            throw new IllegalArgumentException(name + " is not a field defined in this definition");
        }

        return byIdx[idx[0]].getType();
    }

    @Override
    public String toString() {
        return Misc.escapeId(keyspace) + '.' + Misc.escapeId(name);
    }

    @Override
    public final int hashCode() {
        return Arrays.hashCode(new Object[]{keyspace, name, byIdx});
    }

    @Override
    public final boolean equals(Object o) {
        if (!(o instanceof CUDTDefinition)) {
            return false;
        }

        CUDTDefinition other = (CUDTDefinition) o;

        // Note: we don't test byName because it's redundant with byIdx in practice,
        // but also because the map holds 'int[]' which don't have proper equal.
        return keyspace.equals(other.keyspace)
            && name.equals(other.name)
            && Arrays.equals(byIdx, other.byIdx);
    }

    /**
     * A UDT field.
     */
    public static class Field {
        private final String name;
        private final DataType type;

        Field(String name, DataType type) {
            this.name = name;
            this.type = type;
        }

        /**
         * Returns the name of the field.
         *
         * @return the name of the field.
         */
        public String getName() {
            return name;
        }

        /**
         * Returns the type of the field.
         *
         * @return the type of the field.
         */
        public DataType getType() {
            return type;
        }

        @Override
        public final int hashCode() {
            return Arrays.hashCode(new Object[]{name, type});
        }

        @Override
        public final boolean equals(Object o) {
            if (!(o instanceof Field)) {
                return false;
            }

            Field other = (Field) o;
            return name.equals(other.name)
                && type.equals(other.type);
        }

        @Override
        public String toString() {
            return Misc.escapeId(name) + ' ' + type;
        }
    }
}
