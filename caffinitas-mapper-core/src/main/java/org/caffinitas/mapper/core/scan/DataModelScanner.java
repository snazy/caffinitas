/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.scan;

import org.caffinitas.mapper.annotations.CComposite;
import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.annotations.CMapEntity;
import org.caffinitas.mapper.core.DataModel;
import org.caffinitas.mapper.core.MapEntity;
import org.caffinitas.mapper.core.ModelParseException;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.util.Set;

/**
 * Use this class to scan the class path for entities and composites.
 * See documentation for details how to use the <a href="https://github.com/ronmamo/reflections" target="_new">Java runtime metadata analysis</a>.
 */
public class DataModelScanner {

    private final ConfigurationBuilder configurationBuilder;
    private FilterBuilder filterBuilder;

    /**
     * This is a convinience for {@code new DataModelScanner().withCaffinitasPersistenceXml()}.
     * The scanner returned by this class should be usually fine.
     */
    public static DataModelScanner createDefault() {
        return new DataModelScanner().withCaffinitasPersistenceXml();
    }

    public DataModelScanner() {
        configurationBuilder = new ConfigurationBuilder();
    }

    public FilterBuilder getFilterBuilder() {
        if (filterBuilder == null) {
            filterBuilder = new FilterBuilder();
        }
        return filterBuilder;
    }

    public DataModelScanner withExcludePackage(String prefix) {
        filterBuilder = getFilterBuilder().excludePackage(prefix);
        return this;
    }

    public DataModelScanner withExcludePackage(Class<?> aClass) {
        filterBuilder = getFilterBuilder().excludePackage(aClass);
        return this;
    }

    public DataModelScanner withIncludePackage(String prefix) {
        filterBuilder = getFilterBuilder().includePackage(prefix);
        return this;
    }

    public DataModelScanner withIncludePackage(Class<?> aClass) {
        filterBuilder = getFilterBuilder().includePackage(aClass);
        return this;
    }

    public DataModelScanner withCaffinitasPersistenceXml(ClassLoader... classLoaders) {
        Set<Resolved> resources = ClassLoaders.forResource("META-INF/caffinitas-persistence.xml", classLoaders);
        if (resources == null || resources.isEmpty()) {
            return this;
        }

        for (Resolved resolved : resources) {
            configurationBuilder.addUrls(resolved.getResource());
        }

        return this;
    }

    public DataModelScanner withClassLoaders(ClassLoader... classLoaders) {
        configurationBuilder.addUrls(ClasspathHelper.forClassLoader(classLoaders));
        return this;
    }

    /**
     * Use this method to add the scan results into an existing data model.
     *
     * @param model existing data model
     * @return parameter value of {@code model}.
     */
    @SuppressWarnings("unchecked") public DataModel scan(DataModel model) {

        if (filterBuilder != null) {
            configurationBuilder.filterInputsBy(filterBuilder);
        }

        Reflections reflections = new Reflections(configurationBuilder);

        Set<Class<?>> entityClasses = reflections.getTypesAnnotatedWith(CEntity.class, true);
        Set<Class<?>> mapEntityClasses = reflections.getTypesAnnotatedWith(CMapEntity.class, true);
        Set<Class<?>> compositeClasses = reflections.getTypesAnnotatedWith(CComposite.class, true);

        for (Class<?> compositeClass : compositeClasses) {
            model.addComposite(compositeClass);
        }
        for (Class<?> entityClass : entityClasses) {
            model.addEntity(entityClass);
        }
        for (Class<?> mapEntityClass : mapEntityClasses) {
            if (!MapEntity.class.isAssignableFrom(mapEntityClass)) {
                throw new ModelParseException(mapEntityClass + " must extend " + MapEntity.class);
            }
            model.addMapEntity((Class<? extends MapEntity>) mapEntityClass);
        }

        return model;
    }

    /**
     * Similar as calling {@link #scan(org.caffinitas.mapper.core.DataModel)} with {@code new DataModel()}.
     *
     * @return new data model
     */
    public DataModel scan() {
        return scan(new DataModel());
    }
}
