/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.TableMetadata;
import org.caffinitas.mapper.annotations.CIgnore;
import org.caffinitas.mapper.annotations.CKeyspace;
import org.caffinitas.mapper.annotations.CPostDelete;
import org.caffinitas.mapper.annotations.CPostLoad;
import org.caffinitas.mapper.annotations.CPostPersist;
import org.caffinitas.mapper.annotations.CPreDelete;
import org.caffinitas.mapper.annotations.CPrePersist;
import org.caffinitas.mapper.annotations.CReadConsistencyLevel;
import org.caffinitas.mapper.annotations.CSerialConsistencyLevel;
import org.caffinitas.mapper.annotations.CWriteConsistencyLevel;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

abstract class ParseEntityBase extends ParseClass {

    final Interceptors prePersist = new Interceptors();
    final Interceptors preDelete = new Interceptors();
    final Interceptors postDelete = new Interceptors();
    final Interceptors postLoad = new Interceptors();
    final Interceptors postPersist = new Interceptors();

    String keyspace;
    String table;

    final CIgnore ignore;

    TableMetadata.Order[] clusteringKeyOrder;
    String[] partitionKey;
    String[] clusteringKey;

    ConsistencyLevel readConsistencyLevel;
    ConsistencyLevel writeConsistencyLevel;
    ConsistencyLevel serialConsistencyLevel;

    ParseEntityBase(CKeyspace keyspace, Class<?> type) {
        super(keyspace, type);

        ignore = type.getAnnotation(CIgnore.class);

        if (type.getAnnotation(CReadConsistencyLevel.class) != null)
            readConsistencyLevel = type.getAnnotation(CReadConsistencyLevel.class).consistencyLevel();
        if (type.getAnnotation(CWriteConsistencyLevel.class) != null)
            writeConsistencyLevel = type.getAnnotation(CWriteConsistencyLevel.class).consistencyLevel();
        if (type.getAnnotation(CSerialConsistencyLevel.class) != null)
            serialConsistencyLevel = type.getAnnotation(CSerialConsistencyLevel.class).consistencyLevel();
    }

    void processAccessibles(DataModel model, Class<?> clazz, Map<String, ParseAttribute> targetAttributeMap) {
        for (Field field : clazz.getDeclaredFields()) {
            processFieldAttribute(model, field, targetAttributeMap);
        }
        for (Method method : clazz.getDeclaredMethods()) {
            processMethod(model, method, targetAttributeMap);
        }
    }

    void processMethod(DataModel model, Method method, Map<String, ParseAttribute> targetAttributeMap) {
        boolean interceptor = processMethodInterceptor(method);

        boolean attribute = processMethodAttribute(model, method, targetAttributeMap);

        if (interceptor && attribute) {
            throw new ModelParseException("method " + method + " must not be both an interceptor and an attribute");
        }
    }

    private boolean processMethodInterceptor(Method method) {
        CPrePersist prePersist = method.getAnnotation(CPrePersist.class);
        CPreDelete preRemove = method.getAnnotation(CPreDelete.class);
        CPostDelete postDelete = method.getAnnotation(CPostDelete.class);
        CPostLoad postLoad = method.getAnnotation(CPostLoad.class);
        CPostPersist postPersist = method.getAnnotation(CPostPersist.class);

        boolean interceptor = false;

        if (prePersist != null) {
            this.prePersist.addHandler(method);
            interceptor = true;
        }
        if (preRemove != null) {
            this.preDelete.addHandler(method);
            interceptor = true;
        }
        if (postDelete != null) {
            this.postDelete.addHandler(method);
            interceptor = true;
        }
        if (postLoad != null) {
            this.postLoad.addHandler(method);
            interceptor = true;
        }
        if (postPersist != null) {
            this.postPersist.addHandler(method);
            interceptor = true;
        }

        return interceptor;
    }
}
