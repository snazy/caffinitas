/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

/**
 * Array utility functions.
 */
public final class ArrayUtil {
    private ArrayUtil() {}

    /**
     * Return a new array that contains the contents of the provided arrays.
     *
     * @param arr1 first array - must not be {@code null}
     * @param arr2 second array - must not be {@code null}
     * @param <T>  array element type
     * @return new array of length {@code arr1.length + arr2.length} that contains the contents of both arguments
     */
    public static <T> T[] concat(T[] arr1, T[] arr2) {
        T[] newArr = Arrays.copyOf(arr1, arr1.length + arr2.length);
        System.arraycopy(arr2, 0, newArr, arr1.length, arr2.length);
        return newArr;
    }

    /**
     * Add a single element to an existing array.
     *
     * @param source source array - must not be {@code null}
     * @param add    element to add
     * @param <T>    array element type
     * @return new array of length {@code source.length+1}
     */
    public static <T> T[] add2(T[] source, T add) {
        T[] newArr = Arrays.copyOf(source, source.length + 1);
        newArr[source.length] = add;
        return newArr;
    }

    /**
     * Checks if the given array contains {@code elem}.
     * Uses {@link Object#equals(Object)} comparison - not same reference equality.
     *
     * @param arr  array to check  - must not be {@code null}
     * @param elem element to find
     * @param <T>  array element type
     * @return {@code true} if {@code arr} contains an object equal to {@code elem}
     */
    public static <T> boolean contains(T[] arr, T elem) {
        if (arr != null) {
            for (T t : arr) {
                if (t != null ? t.equals(elem) : elem == null) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if the given array contains {@code elem}.
     * Uses reference equality - not object comparison.
     *
     * @param arr  array to check  - must not be {@code null}
     * @param elem element to find
     * @param <T>  array element type
     * @return {@code true} if {@code arr} contains an object equal to {@code elem}
     */
    public static <T> boolean containsSame(T[] arr, T elem) {
        if (arr != null) {
            for (T t : arr) {
                if (t == elem) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if the given array contains {@code elem} and returns the index.
     * Uses {@link Object#equals(Object)} comparison - not same reference equality.
     *
     * @param arr  array to check  - must not be {@code null}
     * @param elem element to find
     * @param <T>  array element type
     * @return {@code -1} if {@code arr} does not contain an object equal to {@code elem} or the index in the source array
     */
    public static <T> int indexOf(T[] arr, T elem) {
        if (arr != null) {
            int i = 0;
            for (T t : arr) {
                if (t != null ? t.equals(elem) : elem == null) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    /**
     * Checks if the given array contains {@code elem} and returns the index.
     * Uses reference equality - not object comparison.
     *
     * @param arr  array to check  - must not be {@code null}
     * @param elem element to find
     * @param <T>  array element type
     * @return {@code -1} if {@code arr} does not contain an object equal to {@code elem} or the index in the source array
     */
    public static <T> int indexOfSame(T[] arr, T elem) {
        if (arr != null) {
            int i = 0;
            for (T t : arr) {
                if (t == elem) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    public static <K, V> Map<K, V> simplifyMap(Map<K, V> c) {
        if (c == null || c.isEmpty())
            return Collections.emptyMap();
        if (c.size() == 1) {
            Map.Entry<K, V> e = c.entrySet().iterator().next();
            return Collections.singletonMap(e.getKey(), e.getValue());
        }
        return c;
    }
}
