/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.RetryPolicy;
import org.caffinitas.mapper.core.accessors.AccessorFactory;
import org.caffinitas.mapper.core.accessors.ReflectionAccessorFactory;
import org.caffinitas.mapper.core.api.Converter;
import org.caffinitas.mapper.core.tracing.ExecutionTracerFactory;

import java.util.HashMap;
import java.util.Map;
import javax.validation.ValidatorFactory;

/**
 * Builder to create configured instances of {@code PersistenceManager}.
 */
public final class PersistenceManagerBuilder implements StatementConfig<PersistenceManagerBuilder> {

    private Cluster cluster;
    private Session session;
    private DataModel model;
    private Class<? extends AccessorFactory> accessorFactory;
    private final Map<String, String> keyspaceNameLookup = new HashMap<String, String>();
    private final StatementOptions statementOptions = new StatementOptions();
    private ExecutionTracerFactory executionTracerFactory;
    private final Map<Class<?>, Class<? extends Converter>> staticConverters = new HashMap<Class<?>, Class<? extends Converter>>();
    private ValidatorFactory validatorFactory;

    /**
     * A {@code Builder} is used to create the configuration for a {@code PersistenceManager}.
     *
     * @return new {@code Builder}
     */
    public static PersistenceManagerBuilder newBuilder() {
        return new PersistenceManagerBuilder();
    }

    @SuppressWarnings("unchecked") private PersistenceManagerBuilder() {
        try {
            accessorFactory = (Class<? extends AccessorFactory>) Class.forName("ReflectionAccessorFactory");
        } catch (Throwable ignored) {
            accessorFactory = ReflectionAccessorFactory.class;
        }
    }

    @Override public PersistenceManagerBuilder setFetchSize(int fetchSize) {
        statementOptions.setFetchSize(fetchSize);
        return this;
    }

    public PersistenceManagerBuilder withFetchSize(int fetchSize) {
        statementOptions.setFetchSize(fetchSize);
        return this;
    }

    @Override public int getFetchSize() {return statementOptions.getFetchSize();}

    @Override public PersistenceManagerBuilder setRetryPolicy(RetryPolicy retryPolicy) {
        statementOptions.setRetryPolicy(retryPolicy);
        return this;
    }

    public PersistenceManagerBuilder withRetryPolicy(RetryPolicy retryPolicy) {
        statementOptions.setRetryPolicy(retryPolicy);
        return this;
    }

    @Override public RetryPolicy getRetryPolicy() {return statementOptions.getRetryPolicy();}

    @Override public PersistenceManagerBuilder setSerialConsistencyLevel(ConsistencyLevel serialConsistencyLevel) {
        statementOptions.setSerialConsistencyLevel(serialConsistencyLevel);
        return this;
    }

    public PersistenceManagerBuilder withSerialConsistencyLevel(ConsistencyLevel serialConsistencyLevel) {
        statementOptions.setSerialConsistencyLevel(serialConsistencyLevel);
        return this;
    }

    @Override public ConsistencyLevel getSerialConsistencyLevel() {return statementOptions.getSerialConsistencyLevel();}

    @Override public PersistenceManagerBuilder setWriteConsistencyLevel(ConsistencyLevel writeConsistencyLevel) {
        statementOptions.setWriteConsistencyLevel(writeConsistencyLevel);
        return this;
    }

    public PersistenceManagerBuilder withWriteConsistencyLevel(ConsistencyLevel writeConsistencyLevel) {
        statementOptions.setWriteConsistencyLevel(writeConsistencyLevel);
        return this;
    }

    @Override public ConsistencyLevel getWriteConsistencyLevel() {return statementOptions.getWriteConsistencyLevel();}

    @Override public PersistenceManagerBuilder setReadConsistencyLevel(ConsistencyLevel readConsistencyLevel) {
        statementOptions.setReadConsistencyLevel(readConsistencyLevel);
        return this;
    }

    public PersistenceManagerBuilder withReadConsistencyLevel(ConsistencyLevel readConsistencyLevel) {
        statementOptions.setReadConsistencyLevel(readConsistencyLevel);
        return this;
    }

    @Override public ConsistencyLevel getReadConsistencyLevel() {return statementOptions.getReadConsistencyLevel();}

    @Override public PersistenceManagerBuilder setTracing(boolean tracing) {
        statementOptions.setTracing(tracing);
        return this;
    }

    @Override public boolean isTracing() {
        return statementOptions.isTracing();
    }

    public PersistenceManagerBuilder withAccessorFactory(Class<? extends AccessorFactory> accessorFactory) {
        this.accessorFactory = accessorFactory;
        return this;
    }

    public Class<? extends AccessorFactory> getAccessorFactory() {
        return accessorFactory;
    }

    public void setAccessorFactory(Class<? extends AccessorFactory> accessorFactory) {
        this.accessorFactory = accessorFactory;
    }

    public PersistenceManagerBuilder withDefaultKeyspace(String defaultKeyspace) {
        keyspaceNameLookup.put("", defaultKeyspace);
        return this;
    }

    public String getDefaultKeyspace() {
        return keyspaceNameLookup.get("");
    }

    public void setDefaultKeyspace(String defaultKeyspace) {
        keyspaceNameLookup.put("", defaultKeyspace);
    }

    public PersistenceManagerBuilder withModel(DataModel model) {
        this.model = model;
        return this;
    }

    public DataModel getModel() {
        return model;
    }

    public void setModel(DataModel model) {
        this.model = model;
    }

    /**
     * Configure the persistence manager to use this Java driver cluster instance.
     * The persistence manager is able to handle connect failures and start the session asynchonously.
     *
     * @param cluster cluster to use
     * @return this builder
     */
    public PersistenceManagerBuilder withCluster(Cluster cluster) {
        this.cluster = cluster;
        return this;
    }

    public Cluster getCluster() {
        return cluster;
    }

    /**
     * @see #withCluster(com.datastax.driver.core.Cluster)
     */
    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    /**
     * Configure the persistence manager to use this Java driver session instance.
     *
     * @param session session to use
     * @return this builder
     */
    public PersistenceManagerBuilder withSession(Session session) {
        this.session = session;
        return this;
    }

    public Session getSession() {
        return session;
    }

    /**
     * @see #withSession(com.datastax.driver.core.Session)
     */
    public void setSession(Session session) {
        this.session = session;
    }

    public PersistenceManagerBuilder withMappedKeyspace(String codedName, String realName) {
        keyspaceNameLookup.put(codedName, realName);
        return this;
    }

    /**
     * @see #withExecutionTracerFactory(org.caffinitas.mapper.core.tracing.ExecutionTracerFactory)
     */
    public void setExecutionTracerFactory(ExecutionTracerFactory executionTracerFactory) {
        this.executionTracerFactory = executionTracerFactory;
    }

    /**
     * Set execution tracer instance.
     * @param executionTracerFactory execution tracer instance to use
     * @return this builder
     */
    public PersistenceManagerBuilder withExecutionTracerFactory(ExecutionTracerFactory executionTracerFactory) {
        this.executionTracerFactory = executionTracerFactory;
        return this;
    }

    /**
     * Register a default converter for a Java type.
     * @param javaClass Java type
     * @param converterClass converter class
     * @return this builder
     */
    public <J> PersistenceManagerBuilder withClassConverter(Class<J> javaClass, Class<? extends Converter<J>> converterClass) {
        if (staticConverters.put(javaClass, converterClass) != null)
            throw new IllegalArgumentException("duplicate converter for Java "+javaClass);
        return this;
    }

    public ValidatorFactory getValidatorFactory() {
        return validatorFactory;
    }

    /**
     * @see #withValidatorFactory(javax.validation.ValidatorFactory)
     */
    public void setValidatorFactory(ValidatorFactory validatorFactory) {
        this.validatorFactory = validatorFactory;
    }

    /**
     * Set the JSR-303 validator factory to use
     * @param validatorFactory validator factory
     * @return this builder
     */
    public PersistenceManagerBuilder withValidatorFactory(ValidatorFactory validatorFactory) {
        this.validatorFactory = validatorFactory;
        return this;
    }

    public PersistenceManager build() {
        if (model == null) {
            throw new NullPointerException();
        }

        Session s = session;
        boolean mySession = s == null;
        if (s == null && cluster != null) {
            s = cluster.connect();
        }

        if (s == null) {
            throw new IllegalStateException("no Session");
        }

        PersistenceManagerImpl persistenceManager =
            new PersistenceManagerImpl(accessorFactory, keyspaceNameLookup, statementOptions, mySession, executionTracerFactory, staticConverters, validatorFactory);
        try {
            persistenceManager.useSession(s);
            persistenceManager.loadModel(model);
            persistenceManager.setupListeners();
            return persistenceManager;
        } catch (RuntimeException t) {
            try {
                persistenceManager.close();
            } catch (Throwable ignore) {
            }
            throw t;
        } catch (Throwable t) {
            try {
                persistenceManager.close();
            } catch (Throwable ignore) {
            }
            throw new RuntimeException("Failed to build persistence manager", t);
        }
    }
}
