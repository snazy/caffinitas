/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.api.converter;

import org.caffinitas.mapper.annotations.DataTypeName;
import org.caffinitas.mapper.core.api.SingleColumnConverter;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Convert between Java {@code URL} to Cassandra {@code String}.
 *
 * @deprecated Do not use URL objects because their usage may result into unexpected DNS lookups during conversion.
 */
@Deprecated
public class URLStringConverter implements SingleColumnConverter<String, URL> {
    @Override public DataTypeName dataTypeName() {
        return DataTypeName.TEXT;
    }

    @Override public Class<URL> javaType() {
        return URL.class;
    }

    @Override public URL toJava(String value) {
        try {
            return value != null ? new URL(value) : null;
        } catch (MalformedURLException e) {
            return null;
        }
    }

    @Override public String fromJava(URL value) {
        return value != null ? value.toExternalForm() : null;
    }
}
