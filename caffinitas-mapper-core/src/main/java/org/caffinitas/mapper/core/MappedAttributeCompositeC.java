/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.mapper.DataTypeMapperBytesUnsafe;

import java.nio.ByteBuffer;

/**
 * Represents an attribute that references a {@link org.caffinitas.mapper.annotations.CComposite} of type
 * {@link org.caffinitas.mapper.annotations.CompositeType#COMPOSITE}.
 */
final class MappedAttributeCompositeC extends MappedAttributeSingle implements DefinitionChangedCallback {

    final MappedCompositeC composite;

    MappedAttributeCompositeC(PersistenceManagerImpl persistenceManager, ParseAttribute parseAttribute) {
        super(persistenceManager, parseAttribute, true);

        composite = persistenceManager.getComposite(parseAttribute.attrType, MappedCompositeC.class);
        composite.addDefinitionChangedCallback(this);
    }

    @Override void setupSingleColumns(PersistenceManagerImpl persistenceManager, DataModel model,
                                      ParseAttribute parseAttribute,
                                      AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix) {
        attributeOverrides = attributeOverrides.forPrefix(parseAttribute.attrName);
        attributeOverrides = attributeOverrides.merge(parseAttribute.attributeOverrides);
        ParseComposite parseComposite = model.composites.get(parseAttribute.attrType);

        for (MappedAttribute mappedAttribute : allAttributes) {
            ParseAttribute childParseAttribute = parseComposite.attributeByName(mappedAttribute.name);
            mappedAttribute.setupSingleColumns(persistenceManager, model, childParseAttribute, attributeOverrides, nameMapper, "");
        }

        //

        setupSingleColumn(this, parseAttribute, attributeOverrides, nameMapper, columnNamePrefix);

        singleColumn().dataTypeMapper = new DataTypeMapperBytesUnsafe();
    }

    @Override public void definitionChanged(PersistenceManager persistenceManager, MappedClassObject classObject) {
        singleColumn().dataType = composite.dataType;
    }

    @SuppressWarnings("unchecked") @Override void bindToStatement(Object instance, Binder binder) {
        CqlColumn col = singleColumn();
        if (!binder.contains(col)) {
            return;
        }

        Object val = accessor.getObject(instance);
        if (converter != null) {
            val = converter.fromJava(val);
        }
        if (val != null) {
            ByteBuffer compValue = (ByteBuffer) composite.serializeAny(val, col.isStatic(), binder.isSerializeDefault());
            binder.setBytesUnsafe(col, compValue);
        } else {
            binder.setBytesUnsafe(col, null);
        }
    }

    @SuppressWarnings("unchecked") @Override boolean fromRow(PersistenceSessionImpl session, Object rootInstance, Object instance,
                                                             Retriever retriever) {
        CqlColumn col = singleColumn();
        if (!retriever.contains(col)) {
            return true;
        }

        ByteBuffer compValue = retriever.getBytesUnsafe(col);
        if (compValue == null || compValue.remaining() == 0) {
            return true;
        }
        Object val = composite.deserializeAny(compValue, rootInstance, session, type);
        if (converter != null) {
            val = converter.toJava(val);
        }
        accessor.setObject(instance, val);

        return false;
    }
}
