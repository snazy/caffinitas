/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.codec;

import com.datastax.driver.core.TupleType;
import com.datastax.driver.core.UserType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper to convert from Java driver {@code DataType} class to Caffinitas {@code DataType} class.
 */
public final class CDT {
    static final Map<com.datastax.driver.core.DataType, DataType> convFromDriver = new HashMap<com.datastax.driver.core.DataType, DataType>();

    static {
        convFromDriver.put(com.datastax.driver.core.DataType.ascii(), DataType.ascii());
        convFromDriver.put(com.datastax.driver.core.DataType.bigint(), DataType.bigint());
        convFromDriver.put(com.datastax.driver.core.DataType.blob(), DataType.blob());
        convFromDriver.put(com.datastax.driver.core.DataType.cboolean(), DataType.cboolean());
        convFromDriver.put(com.datastax.driver.core.DataType.cdouble(), DataType.cdouble());
        convFromDriver.put(com.datastax.driver.core.DataType.cfloat(), DataType.cfloat());
        convFromDriver.put(com.datastax.driver.core.DataType.cint(), DataType.cint());
        convFromDriver.put(com.datastax.driver.core.DataType.counter(), DataType.counter());
        convFromDriver.put(com.datastax.driver.core.DataType.decimal(), DataType.decimal());
        convFromDriver.put(com.datastax.driver.core.DataType.inet(), DataType.inet());
        convFromDriver.put(com.datastax.driver.core.DataType.text(), DataType.text());
        convFromDriver.put(com.datastax.driver.core.DataType.uuid(), DataType.uuid());
        convFromDriver.put(com.datastax.driver.core.DataType.varchar(), DataType.varchar());
        convFromDriver.put(com.datastax.driver.core.DataType.varint(), DataType.varint());
        convFromDriver.put(com.datastax.driver.core.DataType.timeuuid(), DataType.timeuuid());
        convFromDriver.put(com.datastax.driver.core.DataType.timestamp(), DataType.timestamp());
        // TODO CM-32 convFromDriver.put(com.datastax.driver.core.DataType.date(), DataType.date());
        // TODO CM-32 convFromDriver.put(com.datastax.driver.core.DataType.time(), DataType.time());
    }

    private CDT() {}

    public static DataType copy(int protocolVersion, com.datastax.driver.core.DataType src) {
        DataType conv = convFromDriver.get(src);
        if (conv != null) {
            return conv;
        }

        if (src.getName() == com.datastax.driver.core.DataType.Name.LIST) {
            return DataType.list(copy(protocolVersion, src.getTypeArguments().get(0)));
        }
        if (src.getName() == com.datastax.driver.core.DataType.Name.SET) {
            return DataType.set(copy(protocolVersion, src.getTypeArguments().get(0)));
        }
        if (src.getName() == com.datastax.driver.core.DataType.Name.MAP) {
            return DataType.map(copy(protocolVersion, src.getTypeArguments().get(0)), copy(protocolVersion, src.getTypeArguments().get(1)));
        }
        if (src.getName() == com.datastax.driver.core.DataType.Name.UDT) {
            UserType udt = (UserType) src;
            return DataType.userType(new CUDTDefinition(protocolVersion, udt));
        }
        if (src.getName() == com.datastax.driver.core.DataType.Name.TUPLE) {
            TupleType tupleType = (TupleType) src;
            List<com.datastax.driver.core.DataType> srcTypes = tupleType.getComponentTypes();
            List<DataType> types = new ArrayList<DataType>(srcTypes.size());
            for (com.datastax.driver.core.DataType srcType : srcTypes) {
                types.add(copy(protocolVersion, srcType));
            }
            return DataType.tupleType(types);
        }

        return CassandraTypeParser.parseOne(protocolVersion, src.toString());
    }
}
