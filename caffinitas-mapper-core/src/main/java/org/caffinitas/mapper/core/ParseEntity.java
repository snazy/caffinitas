/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CDiscriminatorColumn;
import org.caffinitas.mapper.annotations.CDiscriminatorValue;
import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.annotations.CInheritance;
import org.caffinitas.mapper.annotations.CKeyspace;
import org.caffinitas.mapper.core.util.ArrayUtil;

final class ParseEntity extends ParseEntityBase {
    private static final ParseEntity[] NO_SUB_ENTITIES = {};

    final CEntity entity;

    final CInheritance inheritance;
    final CDiscriminatorColumn discriminatorColumn;
    final CDiscriminatorValue discriminatorValue;

    ParseEntity superEntity;
    ParseEntity[] subEntities = NO_SUB_ENTITIES;

    ParseEntity(CKeyspace keyspace, Class<?> type) {
        super(keyspace, type);

        // parse

        entity = type.getAnnotation(CEntity.class);
        if (entity == null) {
            throw new ModelParseException("type " + type.getName() + " has no @" + CEntity.class.getSimpleName() + " annotation");
        }

        partitionKey = entity.partitionKey();
        clusteringKey = entity.clusteringKey();
        clusteringKeyOrder = entity.clusteringKeyOrder();

        this.keyspace = entity.keyspace();
        if (this.keyspace.isEmpty() && keyspace != null) {
            this.keyspace = keyspace.keyspace().trim();
        }
        this.table = entity.table();

        nameMapper = entity.nameMapper();

        discriminatorColumn = type.getAnnotation(CDiscriminatorColumn.class);
        discriminatorValue = type.getAnnotation(CDiscriminatorValue.class);
        inheritance = type.getAnnotation(CInheritance.class);
    }

    void mapSuperEntity(ParseEntity superEntity) {
        this.superEntity = superEntity;
        superEntity.subEntities = ArrayUtil.add2(superEntity.subEntities, this);
    }

}
