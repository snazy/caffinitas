/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.codec.DataType;

final class CqlColumnSimple extends CqlColumn {
    private final boolean allowNotExists;
    private final boolean ignoreTypeMismatch;
    private final boolean staticCol;
    private final boolean lazy;
    private boolean primaryKey;
    private boolean exists;

    CqlColumnSimple(String name, DataType type, boolean allowNotExists, boolean ignoreTypeMismatch, boolean staticCol, boolean lazy) {
        super(name, type);
        this.allowNotExists = allowNotExists;
        this.ignoreTypeMismatch = ignoreTypeMismatch;
        this.staticCol = staticCol;
        this.lazy = lazy;
    }

    @Override public boolean isPrimaryKey() {
        return primaryKey;
    }

    @Override public boolean isLazy() {
        return lazy;
    }

    @Override public boolean isForSelect() {
        return true;
    }

    @Override public boolean isForUpdate() {
        return true;
    }

    @Override public boolean isExists() {
        return exists;
    }

    @Override public boolean isAllowNotExists() {
        return allowNotExists;
    }

    @Override public boolean isIgnoreTypeMismatch() {
        return ignoreTypeMismatch;
    }

    @Override public boolean isStatic() {
        return staticCol;
    }

    @Override void setExists(boolean exists) {
        this.exists = exists;
    }

    @Override void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    @Override public boolean isPhysical() {
        return true;
    }

    @Override public String toString() {
        return "CqlColumnSimple{" +
            "escapedName='" + getEscapedName() + '\'' +
            ", dataType=" + getDataType() +
            ", exists=" + exists +
            ", staticCol=" + staticCol +
            '}';
    }

}
