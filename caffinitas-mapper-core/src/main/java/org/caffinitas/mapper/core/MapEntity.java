/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Base class for all <i>map entities</i> declared with {@link org.caffinitas.mapper.annotations.CMapEntity}.
 *
 * @param <CK> type of the <i>clustering key</i>
 * @param <CE> type of the <i>clustering entity</i>
 */
public abstract class MapEntity<CK, CE> {
    private final Map<CK, CE> cluster = new HashMap<CK, CE>();
    private Set<CK> removed;

    Map<CK, CE> getCluster() {
        return cluster;
    }

    Set<CK> getRemoved() {
        return removed != null ? removed : Collections.<CK>emptySet();
    }

    void reset() {
        removed = null;
    }

    public boolean containsKey(CK key) {
        return cluster.containsKey(key);
    }

    public CE get(CK key) {
        return cluster.get(key);
    }

    public CE put(CK key, CE value) {
        return cluster.put(key, value);
    }

    public CE remove(CK key) {
        CE prev = cluster.remove(key);
        if (prev != null) {
            if (removed == null) {
                removed = new HashSet<CK>();
            }
            removed.add(key);
        }
        return prev;
    }

    public int size() {
        return cluster.size();
    }

    public boolean isEmpty() {
        return cluster.isEmpty();
    }
}
