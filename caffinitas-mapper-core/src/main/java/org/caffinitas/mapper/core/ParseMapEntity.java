/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CDiscriminatorColumn;
import org.caffinitas.mapper.annotations.CDiscriminatorValue;
import org.caffinitas.mapper.annotations.CInheritance;
import org.caffinitas.mapper.annotations.CKeyspace;
import org.caffinitas.mapper.annotations.CMapEntity;
import org.caffinitas.mapper.annotations.DataTypeName;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

final class ParseMapEntity extends ParseEntityBase {

    final CMapEntity entity;
    final Map<String, ParseAttribute> clusteringAttributeMap = new HashMap<String, ParseAttribute>();
    final Map<String, ParseAttribute> valueAttributeMap = new HashMap<String, ParseAttribute>();

    ParseMapEntity(CKeyspace keyspace, Class<?> type) {
        super(keyspace, type);

        // parse

        entity = type.getAnnotation(CMapEntity.class);
        if (entity == null) {
            throw new ModelParseException("type " + type.getName() + " has no @" + CMapEntity.class.getSimpleName() + " annotation");
        }

        partitionKey = entity.partitionKey();
        clusteringKey = entity.clusteringKey();
        clusteringKeyOrder = entity.clusteringKeyOrder();

        if (entity.clusteringDataType() != DataTypeName.GUESS) {
            if (!entity.clusteringDataType().isPrimitive()) {
                throw new ModelParseException("Illegal clusteringDataType for " + type.getName());
            }
        }
        if (entity.valueDataType() != DataTypeName.GUESS) {
            if (!entity.valueDataType().isPrimitive()) {
                throw new ModelParseException("Illegal clusteringDataType for " + type.getName());
            }
        }

        this.keyspace = entity.keyspace();
        if (this.keyspace.isEmpty() && keyspace != null) {
            this.keyspace = keyspace.keyspace().trim();
        }
        this.table = entity.table();

        if (Modifier.isAbstract(type.getModifiers())) {
            throw new ModelParseException("type " + type.getName() + " must not be abstract");
        }

        if (!MapEntity.class.isAssignableFrom(type)) {
            throw new ModelParseException("type " + type.getName() + " must extend " + MapEntity.class);
        }

        nameMapper = entity.nameMapper();

        if (type.getAnnotation(CInheritance.class) != null || type.getAnnotation(CDiscriminatorColumn.class) != null ||
            type.getAnnotation(CDiscriminatorValue.class) != null) {
            throw new ModelParseException("type " + type.getName() + " has no not allowed @" + CInheritance.class.getSimpleName() + "/@" +
                CDiscriminatorColumn.class.getSimpleName() + "/@" + CDiscriminatorValue.class.getSimpleName() + " annotation");
        }
    }

    @Override ParseAttribute attributeByName(String name) {
        ParseAttribute a = parseAttributeMap.get(name);
        ParseAttribute c = clusteringAttributeMap.get(name);
        ParseAttribute v = valueAttributeMap.get(name);
        if (a != null && c == null && v == null) {
            return a;
        }
        if (a == null && c != null && v == null) {
            return c;
        }
        if (a == null && c == null && v != null) {
            return v;
        }
        throw new ModelUseException("duplicate attribute " + name + " uses in any classes of map entity " + type.getName());
    }

}
