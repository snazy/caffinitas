/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * Contains a specific status flag (for example CQL table exists flag) and provides methods to await that flag to be {@code true}.
 */
final class ConditionAwaitImpl implements ConditionAwait {
    private final Lock lock;
    private final Condition condition;
    private volatile boolean signalled;

    ConditionAwaitImpl(Lock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }

    @Override public boolean isSignalled() {
        return signalled;
    }

    @Override public void await() throws InterruptedException {
        lock.lock();
        try {
            if (signalled) {
                return;
            }
            condition.await();
        } finally {
            lock.unlock();
        }
    }

    @Override public boolean awaitUntil(Date deadline) throws InterruptedException {
        lock.lock();
        try {
            return signalled || condition.awaitUntil(deadline);
        } finally {
            lock.unlock();
        }
    }

    @Override public boolean await(long time, TimeUnit unit) throws InterruptedException {
        lock.lock();
        try {
            return signalled || condition.await(time, unit);
        } finally {
            lock.unlock();
        }
    }

    @Override public long awaitNanos(long nanosTimeout) throws InterruptedException {
        lock.lock();
        try {
            return signalled ? nanosTimeout : condition.awaitNanos(nanosTimeout);
        } finally {
            lock.unlock();
        }
    }

    @Override public void awaitUninterruptibly() {
        lock.lock();
        try {
            if (signalled) {
                return;
            }
            condition.awaitUninterruptibly();
        } finally {
            lock.unlock();
        }
    }

    void signalAll() {
        // must be called with lock held
        signalled = true;
        condition.signalAll();
    }

    void clearSignalled() {
        signalled = false;
    }
}
