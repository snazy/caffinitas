/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CAttributeOverride;

import java.util.HashMap;
import java.util.Map;

final class AttrOverrideMap {

    static final AttrOverrideMap EMPTY = new AttrOverrideMap("");

    final Map<String, String> fieldToColumn;
    final String prefix;

    private AttrOverrideMap(String prefix, Map<String, String> fieldToColumn) {
        this.prefix = prefix;
        this.fieldToColumn = fieldToColumn;
    }

    private AttrOverrideMap(String prefix) {
        this(prefix, new HashMap<String, String>());
    }

    AttrOverrideMap(CAttributeOverride[] value) {
        this("");
        for (CAttributeOverride ao : value) {
            fieldToColumn.put(ao.attributePath(), ao.column());
        }
    }

    AttrOverrideMap(String field, String column) {
        this("");
        fieldToColumn.put(field, column);
    }

    AttrOverrideMap merge(AttrOverrideMap other) {
        if (other == null || other.fieldToColumn.isEmpty()) {
            return this;
        }

        AttrOverrideMap map = new AttrOverrideMap("");
        for (Map.Entry<String, String> e : this.fieldToColumn.entrySet()) {
            if (e.getKey().startsWith(prefix)) {
                map.fieldToColumn.put(e.getKey().substring(prefix.length()), e.getValue());
            }
        }
        map.fieldToColumn.putAll(other.fieldToColumn);
        return map;
    }

    AttrOverrideMap forPrefix(String prefix) {
        if (this == EMPTY)
            return new AttrOverrideMap("", fieldToColumn);
        String np = this.prefix.isEmpty() ? prefix : (this.prefix + '.' + prefix);
        return new AttrOverrideMap(np, fieldToColumn);
    }

    String get(String attr) {
        return fieldToColumn.get(prefix.isEmpty() ? attr : (prefix + '.' + attr));
    }
}
