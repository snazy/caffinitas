/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.UDTValue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

final class QueryBinderImpl<ENTITY> implements QueryBinder<ENTITY> {

    final Session session;
    final MappedEntityBase entity;
    private Map<Class<? extends ENTITY>, BoundStatement> statements = Collections.emptyMap();
    private Map<Class<? extends ENTITY>, CqlColumn[]> readColumns = Collections.emptyMap();
    private Map<Class<? extends ENTITY>, String> conditions = Collections.emptyMap();

    @SuppressWarnings("unchecked") QueryBinderImpl(Session session, MappedEntityBase entity) {
        this.session = session;
        this.entity = entity;
    }

    void addStatement(Class<? extends ENTITY> type, BoundStatement statement, CqlColumn[] columns) {
        if (statements.isEmpty()) {
            statements = Collections.<Class<? extends ENTITY>, BoundStatement>singletonMap(type, statement);
            readColumns = Collections.<Class<? extends ENTITY>, CqlColumn[]>singletonMap(type, columns);
            return;
        }

        if (statements.size() == 1) {
            // revert singletonMap
            statements = new HashMap<Class<? extends ENTITY>, BoundStatement>(statements);
            readColumns = new HashMap<Class<? extends ENTITY>, CqlColumn[]>(readColumns);
        }
        statements.put(type, statement);
        readColumns.put(type, columns);
    }


    void addCondition(Class<? extends ENTITY> type, String namedQuery) {
        if (conditions.isEmpty()) {
            conditions = Collections.<Class<? extends ENTITY>, String>singletonMap(type, namedQuery);
            return;
        }

        if (conditions.size() == 1) {
            // revert singletonMap
            conditions = new HashMap<Class<? extends ENTITY>, String>(conditions);
        }
        conditions.put(type, namedQuery);
    }

    void addConditions(Map<Class<? extends ENTITY>, String> additionalConditions) {
        if (conditions.isEmpty() || conditions.size() == 1)
            conditions = new HashMap<Class<? extends ENTITY>, String>(conditions);
        conditions.putAll(additionalConditions);
    }

    @SuppressWarnings("SuspiciousMethodCalls") BoundStatement statementFor(Class<?> type) {
        return statements.get(type);
    }

    @SuppressWarnings("SuspiciousMethodCalls") CqlColumn[] readColumnsFor(Class<?> type) {
        return readColumns.get(type);
    }

    @SuppressWarnings({"unchecked", "SuspiciousMethodCalls"}) String conditionFor(Class<? extends ENTITY> type) {
        Class<?> t = type;
        while (true) {
            String condition = conditions.get(t);
            if (condition != null) {
                return condition;
            }
            if (t == entity.type) {
                return null;
            }
            t = t.getSuperclass();
            if (t == Object.class) {
                return null;
            }
        }
    }

    @Override public QueryBinder<ENTITY> setBool(String name, boolean v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setBool(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setInt(String name, int v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setInt(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setLong(String name, long v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setLong(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setDate(String name, Date v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setDate(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setFloat(String name, float v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setFloat(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setDouble(String name, double v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setDouble(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setString(String name, String v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setString(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setBytes(String name, ByteBuffer v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setBytes(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setBytesUnsafe(String name, ByteBuffer v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setBytesUnsafe(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setVarint(String name, BigInteger v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setVarint(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setDecimal(String name, BigDecimal v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setDecimal(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setUUID(String name, UUID v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setUUID(name, v);
            }
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setInet(String name, InetAddress v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setInet(name, v);
            }
        }
        return this;
    }

    @Override public <T> QueryBinder<ENTITY> setList(String name, List<T> v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setList(name, v);
            }
        }
        return this;
    }

    @Override public <K, V> QueryBinder<ENTITY> setMap(String name, Map<K, V> v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setMap(name, v);
            }
        }
        return this;
    }

    @Override public <T> QueryBinder<ENTITY> setSet(String name, Set<T> v) {
        for (BoundStatement statement : statements.values()) {
            if (statement.preparedStatement().getVariables().contains(name)) {
                statement.setSet(name, v);
            }
        }
        return this;
    }

    @Override public <T> QueryBinder<ENTITY> setUDTValue(String name, UDTValue v) {
        for (BoundStatement statement : statements.values()) {
            statement.setUDTValue(name, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setBool(int i, boolean v) {
        for (BoundStatement statement : statements.values()) {
            statement.setBool(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setInt(int i, int v) {
        for (BoundStatement statement : statements.values()) {
            statement.setInt(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setLong(int i, long v) {
        for (BoundStatement statement : statements.values()) {
            statement.setLong(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setDate(int i, Date v) {
        for (BoundStatement statement : statements.values()) {
            statement.setDate(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setFloat(int i, float v) {
        for (BoundStatement statement : statements.values()) {
            statement.setFloat(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setDouble(int i, double v) {
        for (BoundStatement statement : statements.values()) {
            statement.setDouble(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setString(int i, String v) {
        for (BoundStatement statement : statements.values()) {
            statement.setString(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setBytes(int i, ByteBuffer v) {
        for (BoundStatement statement : statements.values()) {
            statement.setBytes(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setBytesUnsafe(int i, ByteBuffer v) {
        for (BoundStatement statement : statements.values()) {
            statement.setBytesUnsafe(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setVarint(int i, BigInteger v) {
        for (BoundStatement statement : statements.values()) {
            statement.setVarint(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setDecimal(int i, BigDecimal v) {
        for (BoundStatement statement : statements.values()) {
            statement.setDecimal(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setUUID(int i, UUID v) {
        for (BoundStatement statement : statements.values()) {
            statement.setUUID(i, v);
        }
        return this;
    }

    @Override public QueryBinder<ENTITY> setInet(int i, InetAddress v) {
        for (BoundStatement statement : statements.values()) {
            statement.setInet(i, v);
        }
        return this;
    }

    @Override public <T> QueryBinder<ENTITY> setList(int i, List<T> v) {
        for (BoundStatement statement : statements.values()) {
            statement.setList(i, v);
        }
        return this;
    }

    @Override public <K, V> QueryBinder<ENTITY> setMap(int i, Map<K, V> v) {
        for (BoundStatement statement : statements.values()) {
            statement.setMap(i, v);
        }
        return this;
    }

    @Override public <T> QueryBinder<ENTITY> setSet(int i, Set<T> v) {
        for (BoundStatement statement : statements.values()) {
            statement.setSet(i, v);
        }
        return this;
    }

    @Override public <T> QueryBinder<ENTITY> setUDTValue(int i, UDTValue v) {
        for (BoundStatement statement : statements.values()) {
            statement.setUDTValue(i, v);
        }
        return this;
    }
}
