/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.Session;

import java.io.Closeable;
import java.util.List;

/**
 * A persistence manager instance holds the runtime model of mapped entities and composites.
 * <p>
 * Instances of this class can be created by using a {@link PersistenceManagerBuilder}
 * created using the {@link PersistenceManagerBuilder#newBuilder()} method.
 * </p>
 * <p>
 * Use a {@link PersistenceSession} via {@link #createSession()} to perform loads, inserts, updates and deletes.
 * </p>
 */
public interface PersistenceManager extends Closeable {

    @Override void close();

    /**
     * Retrieve mapped entity class descriptor for a class.
     * Does not check for superclasses of {@code type}.
     *
     * @param type class object to retrieve the mapped entity descriptor for
     * @return mapped entity descriptor or {@code null}
     */
    <E extends MappedSchemaObject> E getEntity(Class<?> type);

    /**
     * Retrieves the mapped entity descriptor for an object's class.
     * Checks superclasses of the object's class until a mapped entity descriptor if found.
     *
     * @param instance object instance - must not be {@code null}
     * @return mapped entity descriptor or {@code null}
     */
    <E extends MappedSchemaObject> E getEntityForObject(Object instance);

    /**
     * Retrieve mapped composite class descriptor for a class.
     * Does not check for superclasses of {@code type}.
     *
     * @param type          class object to retrieve the mapped composite descriptor for
     * @return mapped composite descriptor or {@code null}
     */
    <E extends MappedSchemaObject> E getComposite(Class<?> type);

    /**
     * Retrieves the mapped composite descriptor for an object's class.
     * Checks superclasses of the object's class until a mapped composite descriptor if found.
     *
     * @param instance object instance - must not be {@code null}
     * @return mapped composite descriptor or {@code null}
     */
    <E extends MappedSchemaObject> E getCompositeForObject(Object instance);

    /**
     * Generates a list of Cassandra schema objects ordered by their dependency graph.
     * Dependencies of a schema object (e.g. the user type used in an entity) will appear first.
     */
    List<MappedSchemaObject<?>> generateDependencyGraphList();

    /**
     * Creates a new schema generator instance.
     */
    SchemaGenerator createSchemaGenerator();

    /**
     * Execute the DDL required to create the table or user type represented by the given object.
     *
     * @param mappedSchemaObject schema object to create in Cassandra
     */
    void createEntitySchemaObject(MappedSchemaObject mappedSchemaObject);

    /**
     * Execute the DDL required to update the table or user type represented by the given object.
     *
     * @param mappedSchemaObject schema object to update in Cassandra
     */
    void alterEntitySchemaObject(MappedSchemaObject mappedSchemaObject);

    /**
     * Retrieves the driver session if available.
     * Will throw a {@link PersistenceRuntimeException} if the session is not available (yet).
     */
    Session driverSession();

    /**
     * Create a new persistence session to perform actions on entities without entity instance tracking.
     */
    PersistenceSession createSession();

    /**
     * Create a new persistence session to perform actions on entities with entity instance tracking.
     */
    PersistenceSession createTrackingSession();

    /**
     * Indicates whether runtime schema change detection is supported.
     * Only possible when https://datastax-oss.atlassian.net/browse/JAVA-151 and JAVA-361 are supported by Java Driver.
     * Use {@link #refreshSchema()} as a workaround.
     */
    boolean dynamicSchemaAgreementSupported();

    /**
     * Refresh schema. This is a workaround until JAVA-151 and JAVA-361 are supported by Java Driver.
     */
    @Deprecated
    void refreshSchema();
}
