/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

abstract class MappedEntityInherited<CHILD_TYPE extends MappedEntityInherited> extends MappedEntityContainer<CHILD_TYPE> {

    MappedEntityInherited(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity, Class<CHILD_TYPE> childTypeClass, CqlTable cqlTable) {
        super(persistenceManager, parseEntity, childTypeClass, cqlTable);

        if (parseEntity.entity.partitionKey().length != 0 || parseEntity.entity.clusteringKey().length != 0) {
            throw new ModelUseException("inherited entities must not define a primary key - " + type);
        }
    }

    MappedEntityInherited(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity, Class<CHILD_TYPE> childTypeClass) {
        super(persistenceManager, parseEntity, childTypeClass);

        if (parseEntity.entity.partitionKey().length != 0 || parseEntity.entity.clusteringKey().length != 0) {
            throw new ModelUseException("inherited entities must not define a primary key - " + type);
        }
    }

    @Override public void setupEntityColumns(ParseEntityBase parseEntity) {
        // nop
    }

}
