/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

abstract class AbstractDelegateFutureExt<T> implements FutureExt<T> {
    private final ResultSetFuture delegate;

    protected AbstractDelegateFutureExt(ResultSetFuture delegate) {this.delegate = delegate;}

    @Override public boolean cancel(boolean mayInterruptIfRunning) {return delegate.cancel(mayInterruptIfRunning);}

    @Override public T getUninterruptibly() {return wrapResultSet(delegate.getUninterruptibly());}

    @Override public T getUninterruptibly(long timeout, TimeUnit unit) throws TimeoutException {
        return wrapResultSet(delegate.getUninterruptibly(timeout, unit));
    }

    @Override public boolean isCancelled() {return delegate.isCancelled();}

    @Override public boolean isDone() {return delegate.isDone();}

    @Override public T get() throws InterruptedException, ExecutionException {return wrapResultSet(delegate.get());}

    @Override public T get(long timeout, TimeUnit unit)
        throws InterruptedException, ExecutionException, TimeoutException {
        return wrapResultSet(delegate.get(timeout, unit));
    }

    @Override public void addListener(Runnable listener, Executor executor) {
        delegate.addListener(listener, executor);
    }

    protected abstract T wrapResultSet(ResultSet resultSet);
}
