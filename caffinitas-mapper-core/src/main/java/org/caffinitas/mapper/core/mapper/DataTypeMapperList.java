/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.mapper;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.codec.TypeCodec;

import java.util.ArrayList;
import java.util.List;

/**
 * Internal API.
 */
public final class DataTypeMapperList<T> extends AbstractDataTypeMapperObject {
    private final TypeCodec<List<T>> codec;

    public DataTypeMapperList(TypeCodec<List<T>> codec) {
        this.codec = codec;
    }

    @SuppressWarnings("unchecked") @Override public Object toObject(Retriever retriever, CqlColumn col, Class<?> targetClass) {
        List list = retriever.getList(col, codec);
        // TODO check why retriever.getList() returns an unmodifiable list
        return list != null ? new ArrayList(list) : null;
    }

    @SuppressWarnings("unchecked") @Override public void fromObject(Binder binder, CqlColumn col, Object value) {
        if (value == null) {
            return;
        }
        binder.setList(col, (List) value, codec);
    }
}
