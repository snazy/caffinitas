/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.codec;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("UnusedDeclaration") public abstract class DynCompoSupport {
    protected final List<DataType> dataTypes = new ArrayList<DataType>();
    protected final List<ByteBuffer> components = new ArrayList<ByteBuffer>();

    private void assertType(int idx, DataType.Name typeName) {
        if (dataTypes.get(idx).getName() != typeName) {
            throw new IllegalArgumentException("not a " + typeName);
        }
    }

    public int size() {
        return dataTypes.size();
    }

    public boolean isEmpty() {
        return dataTypes.isEmpty();
    }

    public void addUnsafe(DataType dataType, ByteBuffer v) {
        dataTypes.add(dataType);
        components.add(v == null ? null : v);
    }

    public void addDouble(double v) {
        dataTypes.add(DataType.cdouble());
        components.add(TypeCodec.doubleInstance.serializeNoBoxing(v));
    }

    public void addFloat(float v) {
        dataTypes.add(DataType.cfloat());
        components.add(TypeCodec.floatInstance.serializeNoBoxing(v));
    }

    public void addLong(long v) {
        dataTypes.add(DataType.bigint());
        components.add(TypeCodec.longInstance.serializeNoBoxing(v));
    }

    public void addInt(int v) {
        dataTypes.add(DataType.cint());
        components.add(TypeCodec.intInstance.serializeNoBoxing(v));
    }

    public void addBool(boolean v) {
        dataTypes.add(DataType.cboolean());
        components.add(TypeCodec.booleanInstance.serializeNoBoxing(v));
    }

    public void addDouble(Double v) {
        dataTypes.add(DataType.cdouble());
        components.add(v == null ? null : TypeCodec.doubleInstance.serialize(v));
    }

    public void addFloat(Float v) {
        dataTypes.add(DataType.cfloat());
        components.add(v == null ? null : TypeCodec.floatInstance.serialize(v));
    }

    public void addLong(Long v) {
        dataTypes.add(DataType.bigint());
        components.add(v == null ? null : TypeCodec.longInstance.serialize(v));
    }

    public void addInt(Integer v) {
        dataTypes.add(DataType.cint());
        components.add(v == null ? null : TypeCodec.intInstance.serialize(v));
    }

    public void addBool(Boolean v) {
        dataTypes.add(DataType.cboolean());
        components.add(v == null ? null : TypeCodec.booleanInstance.serialize(v));
    }

    public void addDate(Date v) {
        dataTypes.add(DataType.timestamp());
        components.add(v == null ? null : TypeCodec.dateInstance.serialize(v));
    }

    public void addBytes(ByteBuffer v) {
        dataTypes.add(DataType.blob());
        components.add(v == null ? null : TypeCodec.bytesInstance.serialize(v));
    }

    public void addAsciiString(String v) {
        dataTypes.add(DataType.ascii());
        components.add(v == null ? null : TypeCodec.asciiInstance.serialize(v));
    }

    public void addUtf8String(String v) {
        dataTypes.add(DataType.text());
        components.add(v == null ? null : TypeCodec.utf8Instance.serialize(v));
    }

    public void addVarint(BigInteger v) {
        dataTypes.add(DataType.varint());
        components.add(v == null ? null : TypeCodec.bigIntegerInstance.serialize(v));
    }

    public void addDecimal(BigDecimal v) {
        dataTypes.add(DataType.decimal());
        components.add(v == null ? null : TypeCodec.decimalInstance.serialize(v));
    }

    public void addUUID(UUID v) {
        dataTypes.add(DataType.uuid());
        components.add(v == null ? null : TypeCodec.uuidInstance.serialize(v));
    }

    public void addTimeUUID(UUID v) {
        dataTypes.add(DataType.timeuuid());
        components.add(v == null ? null : TypeCodec.timeUUIDInstance.serialize(v));
    }

    public void addInet(InetAddress v) {
        dataTypes.add(DataType.inet());
        components.add(v == null ? null : TypeCodec.inetInstance.serialize(v));
    }

    public DataType getType(int i) {
        return dataTypes.get(i);
    }

    public double getDouble(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.DOUBLE);
        return b != null ? TypeCodec.doubleInstance.deserializeNoBoxing(b) : 0d;
    }

    public float getFloat(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.FLOAT);
        return b != null ? TypeCodec.floatInstance.deserializeNoBoxing(b) : 0f;
    }

    public Date getDate(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.TIMESTAMP);
        return b != null ? TypeCodec.dateInstance.deserialize(b) : null;
    }

    public java.sql.Date getSimpleDate(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.DATE);
        return b != null ? TypeCodec.simpleDateInstance.deserialize(b) : null;
    }

    public Time getTime(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.TIME);
        return b != null ? TypeCodec.timeInstance.deserialize(b) : null;
    }

    public long getLong(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.BIGINT);
        return b != null ? TypeCodec.longInstance.deserializeNoBoxing(b) : 0L;
    }

    public int getInt(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.INT);
        return b != null ? TypeCodec.intInstance.deserializeNoBoxing(b) : 0;
    }

    public boolean getBool(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.BOOLEAN);
        return b != null && TypeCodec.booleanInstance.deserializeNoBoxing(b);
    }

    public ByteBuffer getBytes(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.BLOB);
        return b != null ? TypeCodec.bytesInstance.deserialize(b) : null;
    }

    public String getString(int idx) {
        ByteBuffer b = components.get(idx);
        if (b == null) {
            return null;
        }
        switch (dataTypes.get(idx).getName()) {
            case ASCII:
                return TypeCodec.asciiInstance.deserialize(b);
            case TEXT:
                return TypeCodec.utf8Instance.deserialize(b);
        }
        throw new IllegalArgumentException("not a string");
    }

    public BigInteger getVarint(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.VARINT);
        return b != null ? TypeCodec.bigIntegerInstance.deserialize(b) : null;
    }

    public BigDecimal getDecimal(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.DECIMAL);
        return b != null ? TypeCodec.decimalInstance.deserialize(b) : null;
    }

    public UUID getUUID(int idx) {
        ByteBuffer b = components.get(idx);
        DataType.Name type = dataTypes.get(idx).getName();
        if (type != DataType.Name.UUID && type != DataType.Name.TIMEUUID) {
            throw new IllegalArgumentException("not a uuid/timeuuid");
        }
        return b != null ? TypeCodec.uuidInstance.deserialize(b) : null;
    }

    public InetAddress getInet(int idx) {
        ByteBuffer b = components.get(idx);
        assertType(idx, DataType.Name.INET);
        return b != null ? TypeCodec.inetInstance.deserialize(b) : null;
    }

    public boolean isNull(int idx) {
        ByteBuffer b = components.get(idx);
        return b == null;
    }

    public ByteBuffer getBytesUnsafe(int idx) {
        return components.get(idx);
    }
}
