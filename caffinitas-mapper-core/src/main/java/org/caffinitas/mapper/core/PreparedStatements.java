/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.google.common.util.concurrent.SettableFuture;
import com.google.common.util.concurrent.Uninterruptibles;
import org.caffinitas.mapper.core.codec.Binder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Per-entity cache for prepared statements.
 */
final class PreparedStatements {
    static final Logger LOGGER = LoggerFactory.getLogger(PreparedStatements.class);

    private final Map<String, PreparedStatement> globalPstmtCache;
    private final ConcurrentMap<StatementKey, StatementHolder> statements = new ConcurrentHashMap<StatementKey, StatementHolder>();
    private boolean noUpdates;
    private final Class<?> type;
    private final CqlTable cqlTable;

    PreparedStatements(Map<String, PreparedStatement> preparedStatementCache, Class<?> type, CqlTable cqlTable) {
        this.globalPstmtCache = preparedStatementCache;
        this.type = type;
        this.cqlTable = cqlTable;
    }

    PreparedStatement statementFor(Session session, StatementType statementType, Binder binder, PersistOption... options) {
        if (options == null) {
            options = PersistOption.NO_OPTIONS;
        }

        CqlColumn[] cols = binder.getColumnsForStatement();
        CqlColumn[] colsPk = binder.getColumnsPk();

        StatementKey key = new StatementKey(statementType, cols, colsPk, null, options);
        StatementHolder holder = statements.get(key);
        if (holder != null) {
            return holder.get();
        }

        String cql = statementType.buildCQL(cqlTable, cols, colsPk, options);

        return statementForInt(session, key, cql);
    }

    PreparedStatement statementFor(Session session, StatementType statementType, CqlColumn[] columns, String condition, PersistOption... options) {
        if (options == null) {
            options = PersistOption.NO_OPTIONS;
        }
        StatementKey key = new StatementKey(statementType, columns, null, condition, options);
        StatementHolder holder = statements.get(key);
        if (holder != null) {
            return holder.get();
        }

        String cql = statementType.buildCQL(cqlTable, columns, condition, options);

        return statementForInt(session, key, cql);
    }

    private PreparedStatement statementForInt(Session session, StatementKey key, String cql) {
        PreparedStatement ps = globalPstmtCache.get(cql);
        if (ps != null) {
            return ps;
        }

        SettableFuture<PreparedStatement> future = SettableFuture.create();
        StatementHolder holder;
        StatementHolder existing = statements.putIfAbsent(key, holder = new StatementHolder(cql, future));
        if (existing != null) {
            return existing.get();
        }

        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Preparing {} CQL for {} : {}", key.statementType, type, cql);
            }

            ps = session.prepare(cql);
            globalPstmtCache.put(cql, ps);
            future.set(ps);
            holder.pstmt = ps;
            holder.future = null;
            return ps;
        } catch (Throwable t) {
            future.setException(t);
            throw new PersistenceRuntimeException("Failed to prepare statement " + cql, t);
        }
    }

    void invalidate() {
        statements.clear();
    }

    boolean noUpdates() {
        return noUpdates;
    }

    public void enableUpdate(boolean enable) {
        noUpdates = !enable;
    }

    static final class StatementKey {
        private static final PersistOption[] NO_OPTIONS = {};
        final StatementType statementType;
        final CqlColumn[] columns;
        final CqlColumn[] columns2;
        final String condition;
        final PersistOption[] persistOptions;
        final int _h;

        StatementKey(StatementType statementType, CqlColumn[] columns, CqlColumn[] columns2, String condition, PersistOption[] persistOptions) {
            this.statementType = statementType;
            this.columns = columns != null ? columns : CqlColumn.NO_COLUMNS;
            this.columns2 = columns2 != null ? columns2 : CqlColumn.NO_COLUMNS;
            this.condition = condition != null ? condition : "";
            this.persistOptions = persistOptions != null ? persistOptions : NO_OPTIONS;
            this._h = calcHash();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            StatementKey that = (StatementKey) o;

            return statementType == that.statementType &&
                Arrays.equals(columns, that.columns) &&
                Arrays.equals(columns2, that.columns2) &&
                condition.equals(that.condition) &&
                Arrays.equals(persistOptions, that.persistOptions);

        }

        @Override
        public int hashCode() {
            return _h;
        }

        private int calcHash() {
            int result = statementType.hashCode();
            result = 31 * result + Arrays.hashCode(columns);
            result = 31 * result + Arrays.hashCode(columns2);
            result = 31 * result + condition.hashCode();
            result = 31 * result + Arrays.hashCode(persistOptions);
            return result;
        }
    }

    enum StatementType {
        INSERT {
            @Override String buildCQL(CqlTable cqlTable, CqlColumn[] columns, CqlColumn[] columnsPk, PersistOption[] options) {
                String cql = "INSERT INTO " + cqlTable +
                    " (" + cqlColumnsConcat(columns, columnsPk, null, ", ", null, true, true) +
                    ") VALUES (" + cqlColumnsReplaced(columns, columnsPk, null, ", ", "?", true) +
                    ')' +
                    ifNotExistsOption(options) +
                    usingOptions(options);

                return cql;
            }

            @Override String buildCQL(CqlTable cqlTable, CqlColumn[] columns, String condition, PersistOption[] options) {
                throw new UnsupportedOperationException();
            }
        },
        UPDATE {
            @Override String buildCQL(CqlTable cqlTable, CqlColumn[] columns, CqlColumn[] columnsPk, PersistOption[] options) {
                String cql = "UPDATE " + cqlTable +
                    usingOptions(options) +
                    " SET " + cqlColumnsConcat(columns, null, false, ", ", "=?", true, true) +
                    " WHERE " + cqlColumnsConcat(columnsPk, null, true, " AND ", "=?", true, false) +
                    ifOptions(options);

                return cql;
            }

            @Override String buildCQL(CqlTable cqlTable, CqlColumn[] columns, String condition, PersistOption[] options) {
                throw new UnsupportedOperationException();
            }
        },
        DELETE {
            @Override String buildCQL(CqlTable cqlTable, CqlColumn[] columns, CqlColumn[] columnsPk, PersistOption[] options) {
                String cql = "DELETE FROM " + cqlTable +
                    usingOptions(options) +
                    " WHERE " + cqlColumnsConcat(columnsPk, null, true, " AND ", "=?", true, false) +
                    ifOptions(options);

                return cql;
            }

            @Override String buildCQL(CqlTable cqlTable, CqlColumn[] columns, String condition, PersistOption[] options) {
                throw new UnsupportedOperationException();
            }
        },
        SELECT {
            @Override String buildCQL(CqlTable cqlTable, CqlColumn[] columns, CqlColumn[] columnsPk, PersistOption[] options) {
                String cql = "SELECT " + cqlColumnsConcat(columns, null, null, ", ", null, true, true) +
                    " FROM " + cqlTable +
                    " WHERE " + cqlColumnsConcat(columnsPk, null, null, " AND ", "=?", true, true) +
                    orderByOptions(options) +
                    limitOption(options) +
                    allowFilteringOption(options);

                return cql;
            }

            @Override String buildCQL(CqlTable cqlTable, CqlColumn[] columns, String condition, PersistOption[] options) {
                String cql = "SELECT " +
                    cqlColumnsConcat(columns, null, null, ", ", null, true, true) +
                    " FROM " + cqlTable +
                    " WHERE " + condition +
                    orderByOptions(options) +
                    limitOption(options) +
                    allowFilteringOption(options);

                return cql;
            }
        };

        private static String ifNotExistsOption(PersistOption[] options) {
            for (PersistOption option : options) {
                if (PersistOption.IfNotExistsOption.INSTANCE == option) {
                    return " IF NOT EXISTS";
                }
            }
            return "";
        }

        private static String ifOptions(PersistOption[] options) {
            StringBuilder ifClause = null;
            for (PersistOption option : options) {
                if (PersistOption.IfExistsOption.INSTANCE == option) {
                    if (ifClause == null) {
                        ifClause = new StringBuilder(" IF EXISTS ");
                    } else {
                        ifClause.insert(4, "EXISTS AND ");
                    }
                } else if (option instanceof PersistOption.IfOption) {
                    if (ifClause == null) {
                        ifClause = new StringBuilder();
                        ifClause.append(" IF ");
                    } else {
                        ifClause.append("AND ");
                    }
                    ifClause.append(((PersistOption.IfOption) option).getAttrPath()).append("=?");
                }
            }
            return ifClause == null ? "" : ifClause.toString();
        }

        private static String usingOptions(PersistOption[] options) {
            String opt = "";
            for (PersistOption option : options) {
                if (option instanceof PersistOption.UsingOption) {
                    if (opt.isEmpty()) {
                        opt = " USING " + ((PersistOption.UsingOption) option).getUsing();
                    } else {
                        opt = " AND " + ((PersistOption.UsingOption) option).getUsing();
                    }
                }
            }
            return opt;
        }

        private static String orderByOptions(PersistOption[] options) {
            // TODO implement CQL SELECT ORDER BY
            return "";
        }

        private static String limitOption(PersistOption[] options) {
            for (PersistOption option : options) {
                if (option instanceof PersistOption.LimitOption) {
                    return " LIMIT ?";
                }
            }
            return "";
        }

        private static String allowFilteringOption(PersistOption[] options) {
            for (PersistOption option : options) {
                if (PersistOption.AllowFilteringOption.INSTANCE == option) {
                    return " ALLOW FILTERING";
                }
            }
            return "";
        }

        abstract String buildCQL(CqlTable cqlTable, CqlColumn[] columns, CqlColumn[] columnsPk, PersistOption[] options);

        abstract String buildCQL(CqlTable cqlTable, CqlColumn[] columns, String condition, PersistOption[] options);
    }

    static final class StatementHolder {
        private volatile PreparedStatement pstmt;
        private final String cql;
        private Future<PreparedStatement> future;

        StatementHolder(String cql, Future<PreparedStatement> future) {
            this.cql = cql;
            this.future = future;
        }

        PreparedStatement get() {
            Future<PreparedStatement> f = future;
            PreparedStatement ps = pstmt;
            if (ps != null) {
                return ps;
            }
            try {
                return Uninterruptibles.getUninterruptibly(f);
            } catch (ExecutionException e) {
                throw new PersistenceRuntimeException("failed to get prepared statement for " + cql, e.getCause());
            }
        }
    }

    static String cqlColumnsConcat(CqlColumn[] cols, CqlColumn[] cols2, Boolean primaryKey, String separator, String suffix, boolean escapedName,
                                   boolean onlyExisting) {
        StringBuilder sb = new StringBuilder(cols.length * 15); // assume 15 as a good max column length
        for (CqlColumn col : cols) {
            if (perCqlCol(separator, onlyExisting, sb, col, primaryKey)) {
                continue;
            }
            sb.append(escapedName ? col.getEscapedName() : col.getName());
            if (suffix != null) {
                sb.append(suffix);
            }
        }
        if (cols2 != null) {
            for (CqlColumn col : cols2) {
                if (perCqlCol(separator, onlyExisting, sb, col, primaryKey)) {
                    continue;
                }
                sb.append(escapedName ? col.getEscapedName() : col.getName());
                if (suffix != null) {
                    sb.append(suffix);
                }
            }
        }
        return sb.toString();
    }

    static String cqlColumnsReplaced(CqlColumn[] cols, CqlColumn[] cols2, Boolean primaryKey, String separator, String replacement,
                                     boolean onlyExisting) {
        StringBuilder sb = new StringBuilder(cols.length * 15); // assume 15 as a good max column length
        for (CqlColumn col : cols) {
            if (perCqlCol(separator, onlyExisting, sb, col, primaryKey)) {
                continue;
            }
            sb.append(replacement);
        }
        if (cols2 != null) {
            for (CqlColumn col : cols2) {
                if (perCqlCol(separator, onlyExisting, sb, col, primaryKey)) {
                    continue;
                }
                sb.append(replacement);
            }
        }
        return sb.toString();
    }

    private static boolean perCqlCol(String separator, boolean onlyExisting, StringBuilder sb, CqlColumn col, Boolean primaryKey) {
        if (primaryKey != null && primaryKey != col.isPrimaryKey()) {
            return true;
        }
        if (onlyExisting && !col.isExists()) {
            return true;
        }
        if (sb.length() > 0) {
            sb.append(separator);
        }
        return false;
    }
}
