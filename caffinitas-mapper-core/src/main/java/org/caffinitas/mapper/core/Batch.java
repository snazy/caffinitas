/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.Statement;

import java.io.Closeable;
import java.util.Collection;

/**
 * Support for C* {@code BATCH} statements.
 */
public interface Batch extends Closeable {

    /**
     * Adds a CQL {@code INSERT} on the given entity instance to this batch.
     *
     * @param instance       object to delete
     * @param persistOptions persist options
     * @return batch part future - currently {@code null} - intended to reflect the status of conditional DML (e.g. {@code IF NOT EXIST})
     */
    <T> BatchPartFuture<T> insert(T instance, PersistOption... persistOptions);

    /**
     * Adds a CQL {@code UPDATE} on the given entity instance to this batch.
     * Note: if the entity only has primary key columns, this method delegates to {@link #insert(Object, PersistOption...)}.
     *
     * @param instance       object to delete
     * @param persistOptions persist options
     * @return batch part future - currently {@code null} - intended to reflect the status of conditional DML (e.g. {@code IF NOT EXIST})
     */
    <T> BatchPartFuture<T> update(T instance, PersistOption... persistOptions);

    /**
     * Adds a CQL {@code UPDATE} on the given entity instance to this batch.
     *
     * @param instance       object to delete
     * @param persistOptions persist options
     * @return batch part future - currently {@code null} - intended to reflect the status of conditional DML (e.g. {@code IF NOT EXIST})
     */
    <T> BatchPartFuture<T> delete(T instance, PersistOption... persistOptions);

    /**
     * Submits the batch and returns when its execution has finished.
     * Convinience method for {@link #submitBatchAsync()}{@code .}{@link FutureExt#getUninterruptibly()}.
     */
    void submitBatch();

    /**
     * Submits the batch and returns a future.
     */
    BatchFuture submitBatchAsync();

    Batch add(Statement statement);

    Batch addAll(Iterable<? extends Statement> statements);

    Collection<Statement> getStatements();
}
