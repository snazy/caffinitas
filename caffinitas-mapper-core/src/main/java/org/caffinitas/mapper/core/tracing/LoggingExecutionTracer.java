/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.tracing;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.ExecutionInfo;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.QueryTrace;
import com.datastax.driver.core.RegularStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Statement;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceManager;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.UpdateSchemaStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Logging execution tracer. Uses SLF4J {@link Logger} with log level {@code trace}.
 */
public class LoggingExecutionTracer implements ExecutionTracer {
    private final Logger logger;
    private final boolean fetchTrace;

    public LoggingExecutionTracer(Logger logger, boolean fetchTrace) {
        this.logger = logger;
        this.fetchTrace = fetchTrace;
    }

    public LoggingExecutionTracer(Class<?> forClass, boolean fetchTrace) {
        this.logger = LoggerFactory.getLogger(forClass);
        this.fetchTrace = fetchTrace;
    }

    public LoggingExecutionTracer(String forName, boolean fetchTrace) {
        this.logger = LoggerFactory.getLogger(forName);
        this.fetchTrace = fetchTrace;
    }

    @Override public void onReadResultSetBegin(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet,
                                               CqlColumn[] columns) {
        rsetBegin(entity, resultSet, "begin read");
    }

    private void rsetBegin(MappedSchemaObject<?> entity, ResultSet resultSet, String readModify) {
        StringBuilder execInfo = new StringBuilder();
        for (ExecutionInfo executionInfo : resultSet.getAllExecutionInfo()) {
            if (executionInfo == null)
                continue;
            Host qh = executionInfo.getQueriedHost();
            ConsistencyLevel cl = executionInfo.getAchievedConsistencyLevel();
            QueryTrace queryTrace = executionInfo.getQueryTrace();
            execInfo.append("\n    exeuction info: queried-host=[address=").append(qh.getAddress()).
                append(", version=").append(qh.getCassandraVersion()).
                append(", dc=").append(qh.getDatacenter()).
                append(", rac=").append(qh.getRack()).
                append("], achieved-consistency-level=").append(cl);
            if (queryTrace != null) {
                InetAddress coordinator = queryTrace.getCoordinator();
                int durationMicros = queryTrace.getDurationMicros();
                Map<String, String> parameters = queryTrace.getParameters();
                String requestType = queryTrace.getRequestType();
                long startedAt = queryTrace.getStartedAt();
                UUID traceId = queryTrace.getTraceId();
                execInfo.append("\n        query-trace: coordinator=").append(coordinator).
                    append(", duration[us]=").append(durationMicros).
                    append(", requestType=").append(requestType).
                    append(", startedAt=").append(startedAt).
                    append(", traceId=").append(traceId).
                    append(", parameters=").append(parameters);
                if (fetchTrace) {
                    List<QueryTrace.Event> events = queryTrace.getEvents();
                    if (events != null) {
                        for (QueryTrace.Event event : events) {
                            if (event == null)
                                continue;
                            InetAddress source = event.getSource();
                            String description = event.getDescription();
                            int sourceElapsed = event.getSourceElapsedMicros();
                            String threadName = event.getThreadName();
                            long timestamp = event.getTimestamp();
                            execInfo.append("\n            event: source=").append(source).
                                append(", source-elapsed[us]=").append(sourceElapsed).
                                append(", description=").append(description).
                                append(", timestamp=").append(timestamp).
                                append(", threadName=").append(threadName);
                        }
                    }
                }
            }
        }
        logger.trace("{} result set, entity={}, table={}, result-set={}{}",
            readModify, entity.getType(), entity.getCqlTable(), resultSet, execInfo);
    }

    @Override public void onReadResultSetRow(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet,
                                             CqlColumn[] columns, Row row) {
        logger.trace("read result set row, entity={}, table={}, row={}", entity.getType(), entity.getCqlTable(), row);
    }

    @Override public void onReadResultSetEnd(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet,
                                             CqlColumn[] columns) {
        logger.trace("end read result set, entity={}, table={}", entity.getType(), entity.getCqlTable());
    }

    @Override public void onModifyWrapResultSet(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, PersistMode mode,
                                                ResultSet resultSet) {
        rsetBegin(entity, resultSet, "modify");
        logger.trace("modify result set, entity={}, mode={}, table={}, result-set={}", entity.getType(), mode, entity.getCqlTable(), resultSet);
    }

    @Override public void onBeginModify(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, PersistMode mode, Statement statement) {
        onBeginX("modify", entity, statement);
    }

    @Override public void onBeginQuery(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, Statement statement) {
        onBeginX("query", entity, statement);
    }

    private void onBeginX(String x, MappedSchemaObject<?> entity, Statement statement) {
        String queryString = "";
        String queryKeyspace = "";
        if (statement instanceof BoundStatement) {
            PreparedStatement pstmt = ((BoundStatement) statement).preparedStatement();
            queryKeyspace = pstmt.getQueryKeyspace();
            queryString = pstmt.getQueryString();
        }
        else if (statement instanceof RegularStatement) {
            RegularStatement rstmt = (RegularStatement) statement;
            queryKeyspace = rstmt.getKeyspace();
            queryString = rstmt.getQueryString();
        }
        else if (statement instanceof BatchStatement) {
            BatchStatement bstmt = (BatchStatement) statement;
            bstmt.getKeyspace();
            StringBuilder qs = new StringBuilder("\n");
            for (Statement st : bstmt.getStatements()) {
                String q;
                if (st instanceof BoundStatement)
                    q = ((BoundStatement)st).preparedStatement().getQueryString();
                else if (st instanceof RegularStatement)
                    q = ((RegularStatement)st).getQueryString();
                else
                    continue;
                qs.append("    ").append(q).append(";\n");
            }
            queryString = qs.toString();
        }
        logger.trace("begin {}, entity={}, table={}, statement-type={}, query-keyspace={}, cl={}, scl={}, query-string={}",
            x, entity.getType(), entity.getCqlTable(), statement.getClass().getSimpleName(), queryKeyspace,
            statement.getConsistencyLevel(), statement.getSerialConsistencyLevel(), queryString);
    }

    @Override public void onUpdateSchema(PersistenceManager persistenceManager, KeyspaceMetadata keyspaceMetadata) {

    }

    @Override public void onUpdateSchemaUserType(PersistenceManager persistenceManager, KeyspaceMetadata keyspaceMetadata,
                                                 MappedSchemaObject<?> schemaObject, UpdateSchemaStatus status) {

    }

    @Override public void onUpdateSchemaEntity(PersistenceManager persistenceManager, KeyspaceMetadata keyspaceMetadata,
                                               MappedSchemaObject<?> schemaObject, UpdateSchemaStatus status) {

    }
}
