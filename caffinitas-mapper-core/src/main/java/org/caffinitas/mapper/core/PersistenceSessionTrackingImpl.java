/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Session;
import com.google.common.util.concurrent.Futures;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.caffinitas.mapper.core.util.ArrayUtil;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

final class PersistenceSessionTrackingImpl extends PersistenceSessionImpl {

    private final Map<CachedEntityInst, CachedEntity> entityCache = new ConcurrentHashMap<CachedEntityInst, CachedEntity>();
    private final Map<CachedEntityKey, CachedEntity> entityKeyCache = new ConcurrentHashMap<CachedEntityKey, CachedEntity>();

    PersistenceSessionTrackingImpl(PersistenceManagerImpl persistenceManager) {
        super(persistenceManager);
    }

    @Override public Future flush() {
        List<ModifyFuture<?>> modifyFutures = new ArrayList<ModifyFuture<?>>();

        validateUpdated();

        for (CachedEntity cachedEntity : entityCache.values()) {
            ModifyFuture f = cachedEntity.updateIfModified();
            if (f != null) {
                modifyFutures.add(f);
            }
        }

        return Futures.allAsList(modifyFutures);
    }

    private void validateUpdated() {
        if (persistenceManager.validatorFactory == null)
            return;

        Set<ConstraintViolation<?>> violations = new HashSet<ConstraintViolation<?>>();
        Validator v = persistenceManager.validatorFactory.getValidator();
        for (CachedEntity cachedEntity : entityCache.values()) {
            if (cachedEntity.isModified()) {
                violations.addAll(v.validate(cachedEntity.instance));
            }
        }
        if (!violations.isEmpty())
            throw new ConstraintViolationException(violations);
    }

    @SuppressWarnings("unchecked") @Override public <T> T evict(T instance) {
        CachedEntity cachedEntity = entityCache.remove(new CachedEntityInst<T>(instance));
        if (cachedEntity == null) {
            return null;
        }
        entityKeyCache.remove(cachedEntity.key);
        return instance;
    }

    @Override public void clear() {
        entityCache.clear();
    }

    @Override <T> T cachedEntity(MappedEntityBase entity, Retriever retriever) {
        ByteBuffer[] primaryKey = retriever.primaryKeyValues(entity.primaryKeyColumns);
        CachedEntityKey key = new CachedEntityKey(entity, primaryKey);
        @SuppressWarnings("unchecked") CachedEntity<T> cachedEntity = entityKeyCache.get(key);
        return cachedEntity != null ? cachedEntity.instance : null;
    }

    @Override <T> void loaded(MappedEntityBase entity, Retriever retriever, T instance) {
        ByteBuffer[] primaryKey = retriever.primaryKeyValues(entity.primaryKeyColumns);
        CachedEntityKey key = new CachedEntityKey(entity, primaryKey);
        CachedEntity<T> cachedEntity = new CachedEntity<T>(key, instance, entity);
        cachedEntity.update(retriever);
        entityKeyCache.put(key, cachedEntity);
        entityCache.put(new CachedEntityInst<T>(instance), cachedEntity);
    }

    @Override <T> boolean filterUnmodifiedForUpdate(MappedEntityBase entity, T instance, Binder binder) {
        CachedEntity cachedEntity = entityCache.get(new CachedEntityInst<T>(instance));
        return cachedEntity != null && cachedEntity.filterUnmodifiedForUpdate(binder);
    }

    @SuppressWarnings("unchecked") @Override <T> void modified(Binder binder, PersistMode mode, MappedEntityBase entity, T instance) {
        if (binder == null) {
            return;
        }

        ByteBuffer[] primaryKey = binder.getDataPk();
        CachedEntityKey key = new CachedEntityKey(entity, primaryKey);
        CachedEntity cachedEntity = entityKeyCache.get(key);

        switch (mode) {
            case INSERT:
            case UPDATE:
                if (cachedEntity != null) {
                    cachedEntity.update(binder);
                } else {
                    cachedEntity = new CachedEntity<T>(key, instance, entity);
                    cachedEntity.setup(binder);
                    entityKeyCache.put(key, cachedEntity);
                    entityCache.put(new CachedEntityInst<T>(instance), cachedEntity);
                }
                break;
            case DELETE:
                cachedEntity = entityKeyCache.remove(key);
                if (cachedEntity != null) {
                    entityCache.remove(new CachedEntityInst<T>(instance));
                }
                break;
        }
    }

    private final class CachedEntity<T> {
        final CachedEntityKey key;
        final T instance;
        final MappedEntityBase entity;
        private CqlColumn[] columns;
        private ByteBuffer[] values;

        private CachedEntity(CachedEntityKey key, T instance, MappedEntityBase entity) {
            if (instance == null || key == null || entity == null) {
                throw new NullPointerException();
            }
            this.key = key;
            this.entity = entity;
            this.instance = instance;
        }

        void setup(Binder binder) {
            CqlColumn[] cols = entity.writeDataColumns;
            ByteBuffer[] vals = new ByteBuffer[cols.length];
            for (int i = 0; i < vals.length; i++) {
                vals[i] = binder.getBound(cols[i]);
            }
            this.columns = cols;
            this.values = vals;
        }

        void update(Retriever retriever) {
            // copy loaded state of entity instance
            CqlColumn[] cols = entity.writeDataColumns;
            ByteBuffer[] vals = new ByteBuffer[cols.length];
            for (int i = 0; i < vals.length; i++) {
                vals[i] = retriever.getBytesUnsafe(cols[i]);
            }
            this.columns = cols;
            this.values = vals;
        }

        void update(Binder binder) {
            for (CqlColumn col : binder.getColumnsForStatement()) {
                int i = ArrayUtil.indexOfSame(columns, col);
                if (i != -1) {
                    values[i] = binder.getBound(col);
                }
            }
        }

        @SuppressWarnings("ResultOfObjectAllocationIgnored") boolean isModified() {
            Binder binder = new Binder(entity.writeDataColumns, true, entity.primaryKeyColumns, PersistMode.UPDATE, true);
            entity.buildModifyBindColumns(instance, binder);

            for (int i = 0; i < columns.length; i++) {
                CqlColumn col = columns[i];
                ByteBuffer val = binder.getBound(col);
                if (val == null) {
                    continue;
                }
                val = val.duplicate();
                ByteBuffer loaded = values[i];
                if (!valueEquals(loaded, val)) {
                    return true;
                }
            }

            return false;
        }

        @SuppressWarnings("ResultOfObjectAllocationIgnored") ModifyFuture<T> updateIfModified() {
            Binder binder = new Binder(entity.writeDataColumns, true, entity.primaryKeyColumns, PersistMode.UPDATE, true);
            entity.buildModifyBindColumns(instance, binder);

            boolean changed = false;
            for (int i = 0; i < columns.length; i++) {
                CqlColumn col = columns[i];
                ByteBuffer val = binder.getBound(col);
                if (val == null) {
                    continue;
                }
                val = val.duplicate();
                ByteBuffer loaded = values[i];
                if (!valueEquals(loaded, val)) {
                    changed = true;
                }
            }

            if (!changed) {
                return null;
            }

            Session session = driverSession();
            BoundStatement bStmt = entity.updateBoundStatement(statementOptions, session, instance, binder);
            ExecutionTracer tracer = executionTracer(PersistenceSessionTrackingImpl.this, entity);
            return new ModifyFutureImpl<T>(PersistenceSessionTrackingImpl.this, tracer, null, session.executeAsync(bStmt), entity, instance,
                PersistMode.UPDATE);
        }

        boolean filterUnmodifiedForUpdate(Binder binder) {
            for (CqlColumn col : binder.getColumnsForStatement()) {
                int i = ArrayUtil.indexOfSame(columns, col);
                if (i == -1) {
                    continue;
                }
                if (valueEquals(values[i], binder.getBound(col))) {
                    binder.unuse(i);
                }
            }

            return binder.noChanges();
        }

    }

    private class CachedEntityKey {
        final MappedEntityBase entity;
        final ByteBuffer[] primaryKey;
        private final int _h;

        CachedEntityKey(MappedEntityBase entity, ByteBuffer[] primaryKey) {
            if (primaryKey == null || entity == null) {
                throw new NullPointerException();
            }
            this.entity = entity;
            this.primaryKey = primaryKey;

            int h = 0;
            for (ByteBuffer byteBuffer : primaryKey) {
                h = 31 * h + byteBuffer.hashCode();
            }
            _h = h;
        }

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass") @Override
        public boolean equals(Object o) {
            CachedEntityKey that = (CachedEntityKey) o;
            if (that == this) {
                return true;
            }
            if (that.entity != entity) {
                return false;
            }
            for (int i = 0; i < primaryKey.length; i++) {
                if (!valueEquals(primaryKey[i], that.primaryKey[i])) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public int hashCode() {
            return _h;
        }
    }

    private final class CachedEntityInst<T> {
        private final T instance;

        private CachedEntityInst(T instance) {
            if (instance == null) {
                throw new NullPointerException();
            }
            this.instance = instance;
        }

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass") @Override
        public boolean equals(Object o) {
            CachedEntityInst that = (CachedEntityInst) o;
            return that.instance == instance;
        }

        @Override
        public int hashCode() {
            return System.identityHashCode(instance);
        }
    }

    static boolean valueEquals(ByteBuffer value, ByteBuffer bound) {
        return (value == null && bound == null) ||
            (value != null && bound != null && value.compareTo(bound) == 0);
    }
}
