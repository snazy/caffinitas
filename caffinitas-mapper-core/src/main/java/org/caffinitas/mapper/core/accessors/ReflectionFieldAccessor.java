/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.accessors;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * Internal API.
 */
final class ReflectionFieldAccessor extends AbstractAttributeAccessor {
    private final Field field;

    public ReflectionFieldAccessor(Field field) {
        if (!Modifier.isPublic(field.getModifiers())) {
            field.setAccessible(true);
        }
        this.field = field;
    }

    @Override public AccessibleObject accessible() {
        return field;
    }

    @Override public Class<?> type() {
        return field.getType();
    }

    @Override public boolean getBoolean(Object instance) {
        try {
            return field.getBoolean(instance);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public byte getByte(Object instance) {
        try {
            return field.getByte(instance);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public char getChar(Object instance) {
        try {
            return field.getChar(instance);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public short getShort(Object instance) {
        try {
            return field.getShort(instance);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public int getInt(Object instance) {
        try {
            return field.getInt(instance);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public long getLong(Object instance) {
        try {
            return field.getLong(instance);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public float getFloat(Object instance) {
        try {
            return field.getFloat(instance);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public double getDouble(Object instance) {
        try {
            return field.getDouble(instance);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked") @Override public <T> T getObject(Object instance) {
        try {
            return (T) field.get(instance);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void setBoolean(Object instance, boolean value) {
        try {
            field.setBoolean(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void setByte(Object instance, byte value) {
        try {
            field.setByte(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void setChar(Object instance, char value) {
        try {
            field.setChar(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void setShort(Object instance, short value) {
        try {
            field.setShort(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void setInt(Object instance, int value) {
        try {
            field.setInt(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void setLong(Object instance, long value) {
        try {
            field.setLong(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void setFloat(Object instance, float value) {
        try {
            field.setFloat(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void setDouble(Object instance, double value) {
        try {
            field.setDouble(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public boolean setObject(Object instance, Object value) {
        try {
            field.set(instance, value);
            return value == null;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
