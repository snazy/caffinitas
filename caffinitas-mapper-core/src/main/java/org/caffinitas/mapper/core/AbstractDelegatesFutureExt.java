/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.caffinitas.mapper.core.util.ArrayUtil;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

abstract class AbstractDelegatesFutureExt<T> implements FutureExt<T> {
    private static final EntityResultSetFuture[] NO_DELEGATES = {};

    protected EntityResultSetFuture[] delegates = NO_DELEGATES;
    protected final PersistenceSessionImpl session;
    protected final ExecutionTracer tracer;

    protected ListenableFuture combined;

    protected AbstractDelegatesFutureExt(PersistenceSessionImpl session, ExecutionTracer tracer) {
        this.session = session;
        this.tracer = tracer;
    }

    @SuppressWarnings("SuspiciousToArrayCall") @Override public void addListener(Runnable listener, Executor executor) {
        if (combined == null) {
            combined = Futures.allAsList(delegates);
        }
        combined.addListener(listener, executor);
    }

    void addResultSetFuture(MappedEntityBase entity, ResultSetFuture resultSetFuture, CqlColumn[] columns) {
        delegates = ArrayUtil.add2(delegates, new EntityResultSetFuture(entity, resultSetFuture, columns));
    }

    static class EntityResultSetFuture implements ListenableFuture<ResultSet> {
        final MappedEntityBase entity;
        final ResultSetFuture resultSetFuture;
        final CqlColumn[] columns;

        private EntityResultSetFuture(MappedEntityBase entity, ResultSetFuture resultSetFuture, CqlColumn[] columns) {
            if (entity == null || resultSetFuture == null || columns == null) {
                throw new NullPointerException();
            }
            this.entity = entity;
            this.resultSetFuture = resultSetFuture;
            this.columns = columns;
        }

        public ResultSet getUninterruptibly() {
            return resultSetFuture.getUninterruptibly();
        }

        public ResultSet getUninterruptibly(long timeout, TimeUnit unit) throws TimeoutException {
            return resultSetFuture.getUninterruptibly(timeout, unit);
        }

        @Override public boolean cancel(boolean mayInterruptIfRunning) {
            return resultSetFuture.cancel(mayInterruptIfRunning);
        }

        @Override public void addListener(Runnable listener, Executor executor) {
            resultSetFuture.addListener(listener, executor);
        }

        @Override public boolean isCancelled() {
            return resultSetFuture.isCancelled();
        }

        @Override public boolean isDone() {
            return resultSetFuture.isDone();
        }

        @Override public ResultSet get() throws InterruptedException, ExecutionException {
            return resultSetFuture.get();
        }

        @Override public ResultSet get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return resultSetFuture.get(timeout, unit);
        }
    }
}
