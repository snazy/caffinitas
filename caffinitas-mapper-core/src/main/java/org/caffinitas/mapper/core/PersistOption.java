/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.SettableData;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.policies.RetryPolicy;
import org.caffinitas.mapper.core.util.ArrayUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Persistence and query options base class.
 */
public abstract class PersistOption {
    public static final PersistOption[] NO_OPTIONS = {};

    private PersistOption() {
    }

    static void forBoundStatement(PersistOption[] persistOptions, BoundStatement bStmt) {
        if (persistOptions != null) {
            for (PersistOption persistOption : persistOptions) {
                if (persistOption instanceof BoundStatementOption) {
                    ((BoundStatementOption) persistOption).configure(bStmt);
                }
            }
        }
    }

    static abstract class BoundStatementOption extends PersistOption {
        abstract void configure(BoundStatement boundStatement);
    }

    /**
     * Option to set retry policy.
     */
    public static final class RetryPolicyOption extends BoundStatementOption {
        private final RetryPolicy retryPolicy;

        public RetryPolicyOption(RetryPolicy retryPolicy) {
            this.retryPolicy = retryPolicy;
        }

        @Override void configure(BoundStatement boundStatement) {
            boundStatement.setRetryPolicy(retryPolicy);
        }
    }

    /**
     * Option to set consistency level.
     */
    public static final class ConsistencyLevelOption extends BoundStatementOption {
        private final ConsistencyLevel consistencyLevel;

        public ConsistencyLevelOption(ConsistencyLevel consistencyLevel) {
            this.consistencyLevel = consistencyLevel;
        }

        @Override void configure(BoundStatement boundStatement) {
            boundStatement.setConsistencyLevel(consistencyLevel);
        }

        public static boolean apply(Statement statement, PersistOption[] persistOptions) {
            if (persistOptions != null) {
                for (PersistOption persistOption : persistOptions) {
                    if (persistOption instanceof ConsistencyLevelOption) {
                        statement.setConsistencyLevel(((ConsistencyLevelOption) persistOption).consistencyLevel);
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /**
     * Option to set serial consistency level.
     */
    public static final class SerialConsistencyLevelOption extends BoundStatementOption {
        private final ConsistencyLevel consistencyLevel;

        public SerialConsistencyLevelOption(ConsistencyLevel consistencyLevel) {
            this.consistencyLevel = consistencyLevel;
        }

        @Override void configure(BoundStatement boundStatement) {
            boundStatement.setSerialConsistencyLevel(consistencyLevel);
        }

        public static boolean apply(Statement statement, PersistOption[] persistOptions) {
            if (persistOptions != null) {
                for (PersistOption persistOption : persistOptions) {
                    if (persistOption instanceof SerialConsistencyLevelOption) {
                        statement.setSerialConsistencyLevel(((SerialConsistencyLevelOption) persistOption).consistencyLevel);
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /**
     * Option to set fetch size.
     */
    public static final class FetchSizeOption extends BoundStatementOption {
        private final int fetchSize;

        public FetchSizeOption(int fetchSize) {
            this.fetchSize = fetchSize;
        }

        @Override void configure(BoundStatement boundStatement) {
            boundStatement.setFetchSize(fetchSize);
        }
    }

    static abstract class UsingOption extends PersistOption {
        private final String using;

        protected UsingOption(String using) {
            if (using == null) {
                throw new NullPointerException();
            }
            this.using = using;
        }

        public String getUsing() {
            return using;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            UsingOption that = (UsingOption) o;

            return using.equals(that.using);
        }

        @Override
        public int hashCode() {
            return using.hashCode();
        }

        public abstract void bind(SettableData binder, int idx);
    }

    /**
     * Allows embedding of {@code USING TTL} in CQL {@code INSERT} and {@code UPDATE} statements.
     */
    public static final class TTLOption extends UsingOption {
        private final int seconds;

        public TTLOption(int seconds) {
            super("TTL ?");
            if (seconds < 1) {
                throw new IllegalArgumentException();
            }
            this.seconds = seconds;
        }

        public int getSeconds() {
            return seconds;
        }

        @Override public void bind(SettableData binder, int idx) {
            binder.setInt(idx, seconds);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            if (!super.equals(o)) {
                return false;
            }

            TTLOption that = (TTLOption) o;

            return seconds == that.seconds;
        }

        @Override
        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + seconds;
            return result;
        }
    }

    /**
     * Allows embedding of {@code USING WRITETIME} in CQL {@code INSERT} and {@code UPDATE} statements.
     */
    public static final class WritetimeOption extends UsingOption {
        private final long microseconds;

        public WritetimeOption(long microseconds) {
            super("WRITETIME ?");
            if (microseconds < 1) {
                throw new IllegalArgumentException();
            }
            this.microseconds = microseconds;
        }

        public long getMicroseconds() {
            return microseconds;
        }

        @Override public void bind(SettableData binder, int idx) {
            binder.setLong(idx, microseconds);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            if (!super.equals(o)) {
                return false;
            }

            WritetimeOption that = (WritetimeOption) o;

            return microseconds == that.microseconds;
        }

        @Override
        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + (int) (microseconds ^ (microseconds >>> 32));
            return result;
        }
    }

    /**
     * Allows embedding of {@code LIMIT} in CQL {@code SELECT} statements.
     */
    public static final class LimitOption extends PersistOption {
        private final int limit;

        public LimitOption(int limit) {
            this.limit = limit;
        }

        public int getLimit() {
            return limit;
        }

        @Override
        public boolean equals(Object o) {
            return this == o || !(o == null || getClass() != o.getClass());
        }

        @Override
        public int hashCode() {
            return getClass().hashCode();
        }

        public static int apply(BoundStatement bStmt, PersistOption[] persistOptions, int idx) {
            for (PersistOption option : persistOptions) {
                if (option instanceof PersistOption.LimitOption) {
                    PersistOption.LimitOption limitOption = (PersistOption.LimitOption) option;
                    bStmt.setInt(idx++, limitOption.limit);
                }
            }
            return idx;
        }
    }

    static abstract class CasOption extends PersistOption {
        protected CasOption() {
        }
    }

    /**
     * Allows embedding of {@code IF NOT EXISTS} in CQL {@code INSERT} statements.
     */
    public static final class IfNotExistsOption extends CasOption {
        public static final IfNotExistsOption INSTANCE = new IfNotExistsOption();

        private IfNotExistsOption() {
        }

        @Override
        public boolean equals(Object o) {
            return this == o || !(o == null || getClass() != o.getClass());
        }

        @Override
        public int hashCode() {
            return getClass().hashCode();
        }
    }

    /**
     * Allows embedding of {@code IF EXISTS} in CQL {@code DELETE} statements.
     */
    public static final class IfExistsOption extends CasOption {
        public static final IfExistsOption INSTANCE = new IfExistsOption();

        private IfExistsOption() {
        }

        @Override
        public boolean equals(Object o) {
            return this == o || !(o == null || getClass() != o.getClass());
        }

        @Override
        public int hashCode() {
            return getClass().hashCode();
        }
    }

    /**
     * Allows embedding of {@code IF attr=value} in CQL {@code UPDATE} statements.
     */
    public static final class IfOption<T> extends CasOption {
        private final String attrPath;
        private final T value;

        private IfOption(String attrPath, T value) {
            if (attrPath == null || value == null) {
                throw new NullPointerException();
            }
            this.attrPath = attrPath;
            this.value = value;
        }

        public String getAttrPath() {
            return attrPath;
        }

        public T getValue() {
            return value;
        }

        public static <T> IfOption create(String attrPath, T value) {
            return new IfOption<T>(attrPath, value);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            IfOption ifOption = (IfOption) o;

            return attrPath.equals(ifOption.attrPath);
        }

        @Override
        public int hashCode() {
            return attrPath.hashCode();
        }
    }

    /**
     * Allows embedding of {@code ALLOW FILTERING} in CQL {@code SELECT} statements.
     */
    public static final class AllowFilteringOption extends PersistOption {
        public static final AllowFilteringOption INSTANCE = new AllowFilteringOption();

        private AllowFilteringOption() {
        }

        @Override
        public boolean equals(Object o) {
            return this == o || !(o == null || getClass() != o.getClass());
        }

        @Override
        public int hashCode() {
            return getClass().hashCode();
        }
    }

    /**
     * Load all lazy columns eagerly.
     */
    public static final class LoadEagerOption extends PersistOption {
        public static final LoadEagerOption INSTANCE = new LoadEagerOption();

        private LoadEagerOption() {
        }

        @Override
        public boolean equals(Object o) {
            return this == o || !(o == null || getClass() != o.getClass());
        }

        @Override
        public int hashCode() {
            return getClass().hashCode();
        }
    }

    /**
     * Write {@code null} (default) values.
     */
    public static final class WriteDefaultValueOption extends PersistOption {
        public final boolean writeDefaultValues;

        public static final WriteDefaultValueOption DO = new WriteDefaultValueOption(true);
        public static final WriteDefaultValueOption DONT = new WriteDefaultValueOption(false);

        private WriteDefaultValueOption(boolean writeDefaultValues) {
            this.writeDefaultValues = writeDefaultValues;
        }

        @Override
        public boolean equals(Object o) {
            return this == o;
        }

        @Override
        public int hashCode() {
            return (writeDefaultValues ? 1 : 0);
        }

        public static boolean get(PersistOption[] persistOptions, boolean writeNullOnInsert) {
            if (persistOptions != null) {
                for (PersistOption persistOption : persistOptions) {
                    if (persistOption == WriteDefaultValueOption.DO) {
                        return true;
                    }
                    if (persistOption == WriteDefaultValueOption.DONT) {
                        return false;
                    }
                }
            }
            return writeNullOnInsert;
        }
    }

    /**
     * Restricts operations to specific column names. This restriction does not apply to primary key columns.
     */
    public static class ColumnRestrictionOption extends PersistOption {
        private final String[] columnNames;

        public ColumnRestrictionOption(String[] columnNames) {
            this.columnNames = columnNames;
        }

        public String[] getColumnNames() {
            return columnNames;
        }

        public static CqlColumn[] filter(PersistOption[] options, CqlColumn[] columns) {
            if (options == null) {
                return columns;
            }
            for (PersistOption opt : options) {
                if (opt instanceof ColumnRestrictionOption) {
                    ColumnRestrictionOption cro = (ColumnRestrictionOption) opt;
                    List<CqlColumn> r = null;
                    for (int i = 0; i < columns.length; i++) {
                        CqlColumn column = columns[i];
                        if (ArrayUtil.contains(cro.columnNames, column.getName())) {
                            if (r != null) {
                                r.add(column);
                            }
                        } else {
                            if (r == null) {
                                r = new ArrayList<CqlColumn>(cro.columnNames.length);
                                for (int j = 0; j < i; j++) {
                                    r.add(columns[i]);
                                }
                            }
                        }
                    }
                    return r == null ? columns : r.toArray(new CqlColumn[r.size()]);
                }
            }
            return columns;
        }
    }

    /**
     * Allows embedding of {@code ALLOW FILTERING} in CQL {@code SELECT} statements.
     */
    public static AllowFilteringOption allowFiltering() {
        return AllowFilteringOption.INSTANCE;
    }

    /**
     * Allows embedding of {@code LIMIT} in CQL {@code SELECT} statements.
     *
     * @param limit
     */
    public static LimitOption limit(int limit) {
        return new LimitOption(limit);
    }

    /**
     * Allows embedding of {@code IF EXISTS} in CQL {@code DELETE} statements.
     */
    public static IfExistsOption ifExists() {
        return IfExistsOption.INSTANCE;
    }

    /**
     * Allows embedding of {@code IF NOT EXISTS} in CQL {@code INSERT} statements.
     */
    public static IfNotExistsOption ifNotExists() {
        return IfNotExistsOption.INSTANCE;
    }

    /**
     * Allows embedding of {@code USING TTL} in CQL {@code INSERT} and {@code UPDATE} statements.
     */
    public static TTLOption ttl(int seconds) {
        return new TTLOption(seconds);
    }

    /**
     * Allows embedding of {@code USING WRITETIME} in CQL {@code INSERT} and {@code UPDATE} statements.
     */
    public static WritetimeOption writetime(long microSeconds) {
        return new WritetimeOption(microSeconds);
    }

    /**
     * Allows embedding of {@code IF attr=value} in CQL {@code UPDATE} statements.
     */
    public static <T> IfOption<T> ifValue(String attrPath, T value) {
        return new IfOption<T>(attrPath, value);
    }

    /**
     * Configures fetch size for {@code SELECT} statements.
     */
    public static FetchSizeOption fetchSize(int fetchSize) {
        return new FetchSizeOption(fetchSize);
    }

    /**
     * Configures retry policy.
     */
    public static RetryPolicyOption retryPolicy(RetryPolicy retryPolicy) {
        return new RetryPolicyOption(retryPolicy);
    }

    /**
     * Configures consistency level.
     */
    public static ConsistencyLevelOption consistencyLevel(ConsistencyLevel consistencyLevel) {
        return new ConsistencyLevelOption(consistencyLevel);
    }

    /**
     * Configures serial consistency level.
     */
    public static SerialConsistencyLevelOption serialConsistencyLevel(ConsistencyLevel consistencyLevel) {
        return new SerialConsistencyLevelOption(consistencyLevel);
    }

    /**
     * Load all lazy columns eagerly.
     */
    public static LoadEagerOption loadEager() {
        return LoadEagerOption.INSTANCE;
    }

    /**
     * Do not write default values of entity instances - e.g. {@code null} (default values) are not included in the column list for {@code INSERT}.
     */
    public static WriteDefaultValueOption dontWriteDefaultValues() {
        return WriteDefaultValueOption.DONT;
    }

    /**
     * Write default values of entity instances.
     */
    public static WriteDefaultValueOption writeDefaultValues() {
        return WriteDefaultValueOption.DO;
    }

    /**
     * Restricts load and modify operations to given column names. This restriction does not apply to primary key columns.
     */
    public static ColumnRestrictionOption columnRestriction(String... columnNames) {
        return new ColumnRestrictionOption(columnNames);
    }

    /**
     * Wraps a single persist option in an array.
     */
    public static PersistOption[] single(PersistOption option) {
        return new PersistOption[]{option};
    }
}
