/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.mapper;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;

import java.net.InetAddress;

/**
 * Internal API.
 */
public final class DataTypeMapperInet extends AbstractDataTypeMapperObject {

    public static final DataTypeMapperInet INSTANCE = new DataTypeMapperInet();

    private DataTypeMapperInet() {
    }

    @Override public Object toObject(Retriever retriever, CqlColumn col, Class<?> targetClass) {
        if (retriever.isNull(col)) {
            return null;
        }
        // TODO convert to targetClass
        return retriever.getInet(col);
    }

    @Override public void fromObject(Binder binder, CqlColumn col, Object value) {
        if (value == null) {
            return;
        }
        // TODO convert to InetAddress
        binder.setInet(col, (InetAddress) value);
    }
}
