/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.mapper.DataTypeMapperBytesUnsafe;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Represents an attribute that references a {@link org.caffinitas.mapper.annotations.CComposite} of type
 * {@link org.caffinitas.mapper.annotations.CompositeType#COMPOSITE}.
 */
final class MappedAttributeDynamic extends MappedAttributeSingle {

    MappedAttributeDynamic(PersistenceManagerImpl persistenceManager, ParseAttribute parseAttribute) {
        super(persistenceManager, parseAttribute, true);
    }

    @Override void setupSingleColumns(PersistenceManagerImpl persistenceManager, DataModel model,
                                      ParseAttribute parseAttribute,
                                      AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix) {

        //

        setupSingleColumn(this, parseAttribute, attributeOverrides, nameMapper, columnNamePrefix);

        singleColumn().dataType = DataType.dynamic(Collections.<Byte, DataType>emptyMap());
        singleColumn().dataTypeMapper = new DataTypeMapperBytesUnsafe();
    }

    @Override void setupClassColumns(PersistenceManagerImpl persistenceManager, MappedObject container, List<CqlColumn> cqlColumns, DataModel model,
                                     ParseAttribute parseAttribute,
                                     AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix) {

        super.setupClassColumns(persistenceManager, container, cqlColumns, model, parseAttribute, attributeOverrides, nameMapper, columnNamePrefix);
    }

    @Override void bindToStatement(Object instance, Binder binder) {
        CqlColumn col = singleColumn();
        if (!binder.contains(col)) {
            return;
        }

        Object denormInstance = accessor.getObject(instance);
        if (denormInstance != null) {
            DynamicComposite dynComp = (DynamicComposite) denormInstance;
            ByteBuffer compValue = dynComp.serialize();
            binder.setBytesUnsafe(col, compValue);
        } else {
            binder.setBytesUnsafe(col, null);
        }
    }

    @Override boolean fromRow(PersistenceSessionImpl session, Object rootInstance, Object instance, Retriever retriever) {
        CqlColumn col = singleColumn();
        if (!retriever.contains(col)) {
            return true;
        }

        ByteBuffer compValue = retriever.getBytesUnsafe(col);
        if (compValue == null) {
            return true;
        }

        // TODO parse DynamicCompositeType aliases
        DataType colType = retriever.getType(col);
        Map<Byte, DataType> aliases = Collections.emptyMap();

        DynamicComposite denormInstance = new DynamicComposite(session.persistenceManager.protocolVersion, aliases, compValue);
        accessor.setObject(instance, denormInstance);

        return false;
    }
}
