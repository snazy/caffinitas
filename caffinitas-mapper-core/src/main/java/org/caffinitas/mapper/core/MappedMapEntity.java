/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SettableData;
import org.caffinitas.mapper.annotations.CMapEntity;
import org.caffinitas.mapper.annotations.CNamedQuery;
import org.caffinitas.mapper.annotations.DataTypeName;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.accessors.ObjectFactory;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.codec.GettableRetriever;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Member;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Represents a Java class mapped to a Cassandra table.
 */
final class MappedMapEntity extends MappedEntityBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(MappedMapEntity.class);

    MappedAttribute[] partitionAttributes = MappedAttribute.NONE;
    MappedAttribute[] clusteringKeyAttributes = MappedAttribute.NONE;
    MappedAttribute[] valueAttributes = MappedAttribute.NONE;

    final Class<?> clusteringType;
    final DataType clusteringPrimitive;
    final CqlColumn clusteringColumn;

    final Class<?> valueType;
    final DataType valuePrimitive;
    final CqlColumn valueColumn;

    MappedMapEntity(PersistenceManagerImpl persistenceManager, ParseMapEntity parseMapEntity) {
        this(persistenceManager, parseMapEntity, createCqlTable(persistenceManager, parseMapEntity));
    }

    MappedMapEntity(PersistenceManagerImpl persistenceManager, ParseMapEntity parseMapEntity, CqlTable cqlTable) {
        super(persistenceManager, parseMapEntity, cqlTable,
            parseMapEntity.entity.defaultTTL() > 0 ? PersistOption.ttl(parseMapEntity.entity.defaultTTL()) : null,
            parseMapEntity.entity.writeNullOnInsert());

        clusteringType = parseMapEntity.entity.clusteringType();
        clusteringPrimitive =
            parseMapEntity.entity.clusteringDataType() != DataTypeName.GUESS ? parseMapEntity.entity.clusteringDataType().getDataType() : null;
        clusteringColumn = clusteringPrimitive != null ? new CqlColumnSimpleStd("column1", clusteringPrimitive) : null;

        valueType = parseMapEntity.entity.valueType();
        valuePrimitive = parseMapEntity.entity.valueDataType() != DataTypeName.GUESS ? parseMapEntity.entity.valueDataType().getDataType() : null;
        valueColumn = valuePrimitive != null ? new CqlColumnSimpleStd("value", valuePrimitive) : null;

        for (CNamedQuery namedQuery : parseMapEntity.entity.namedQueries()) {
            namedQueries.put(namedQuery.name(), namedQuery.condition());
        }
    }

    @Override Logger logger() {
        return LOGGER;
    }

    @Override void setupAttributes(PersistenceManagerImpl persistenceManager, DataModel model, MappedClassObject rootClassObject, String attrPrefix,
                                   Set<Class<?>> parseSet, ParseClass parseClass) {
        ParseMapEntity parseMapEntity = (ParseMapEntity) parseClass;
        setupAttributes(persistenceManager, model, rootClassObject, attrPrefix, parseSet, parseClass.parseAttributeMap);
        // TODO handle static column attributes
        partitionAttributes = allAttributes;
        setupAttributes(persistenceManager, model, rootClassObject, attrPrefix, parseSet, parseMapEntity.clusteringAttributeMap);
        clusteringKeyAttributes = Arrays.copyOfRange(allAttributes,
            partitionAttributes.length,
            allAttributes.length);
        setupAttributes(persistenceManager, model, rootClassObject, attrPrefix, parseSet, parseMapEntity.valueAttributeMap);
        valueAttributes = Arrays.copyOfRange(allAttributes,
            partitionAttributes.length + clusteringKeyAttributes.length,
            allAttributes.length);
    }

    @Override protected List<CqlColumn> setupEntityColumnsInner(ParseEntityBase parseEntity, List<CqlColumn> partitionKey,
                                                                List<CqlColumn> clusteringKey) {
        for (String attrPath : parseEntity.partitionKey) {
            partitionKey.add(validAttributeColumnByPath(attrPath, "partition key", type));
        }
        if (partitionKey.isEmpty()) {
            throw new ModelUseException("partition key of " + type + " must not be empty");
        }

        if (clusteringColumn != null) {
            clusteringKey.add(clusteringColumn);
        } else {
            for (String attrPath : parseEntity.clusteringKey) {
                clusteringKey.add(validAttributeColumnByPath(attrPath, "clustering key", clusteringType));
            }
        }

        if (clusteringKey.isEmpty()) {
            throw new ModelUseException("clustering key must not be empty for @" + CMapEntity.class.getSimpleName() + " of " + type);
        }

        List<CqlColumn> l = new ArrayList<CqlColumn>();
        if (valueColumn != null) {
            l.add(valueColumn);
        }

        return l;
    }

    @Override void postInitialize(PersistenceManagerImpl persistenceManager) {
        if (allDataColumns.length == 0 && (valueType != Void.class || valueType != void.class)) {
            throw new ModelUseException(
                "value must not be empty for @" + CMapEntity.class.getSimpleName() + " unless valueType is set to void.class in " + type);
        }

        super.postInitialize(persistenceManager);
    }

    CqlColumn validAttributeColumnByPath(String attrPath, String attrType, Class<?> attrOwner) {
        MappedAttribute attr = attributePaths.get(attrPath);

        if (attr instanceof MappedAttributeSingle) {

            Class<?> decl = ((Member) attr.accessor.accessible()).getDeclaringClass();
            if (!attrOwner.isAssignableFrom(decl)) {
                throw new ModelUseException("attribute path '" + attrPath + "' (" + attr.accessor.accessible() + ") does not belong to " + attrOwner);
            }

            return ((MappedAttributeSingle) attr).singleColumn();
        } else if (attr == null) {
            throw new ModelUseException(
                "attribute path '" + attrPath + "' expected in " + attrType + ' ' + attrOwner + " does not map to a an attribute");
        } else {
            throw new ModelUseException(
                "last part in attribute path '" + attrPath + "' of " + attrType + ' ' + attrOwner + " must not be a composite");
        }
    }

    @Override protected void setupEntityColumnsOuter() {

    }

    @Override @SuppressWarnings("unchecked") <T> T fromRow(PersistenceSessionImpl session, Retriever retriever, CqlColumn[] columns, T last) {
        // TODO allow map-entities in tracking sessions
        //T instance = session.cachedEntity(this, retriever);
        //if (instance == null) {
        //  instance = (T) session.persistenceManager.objectFactory.newInstance(type);
        //}
        //
        //session.loaded(this, retriever, instance);

        ObjectFactory objectFactory = session.persistenceManager.objectFactory;

        int protocolVersion = session.persistenceManager.protocolVersion;

        MapEntity instance = null;
        if (last != null) {
            boolean same = true;
            for (MappedAttribute partitionAttribute : partitionAttributes) {
                CqlColumn col = ((MappedAttributeSingle) partitionAttribute).singleColumn();
                ByteBuffer ser = col.dataType.serialize(partitionAttribute.accessor.getObject(last), protocolVersion);
                if (!ser.equals(retriever.getBytesUnsafe(col))) {
                    same = false;
                    break;
                }
            }
            if (same) {
                instance = (MapEntity) last;
            }
        }
        if (instance == null) {
            instance = (MapEntity) objectFactory.newInstance(type);
            for (MappedAttribute attr : partitionAttributes) {
                attr.fromRow(session, instance, instance, retriever);
            }
        }

        Object key;
        if (clusteringColumn != null) {
            key = clusteringPrimitive.deserialize(retriever.getBytesUnsafe(clusteringColumn), protocolVersion);
        } else {
            key = objectFactory.newInstance(clusteringType);
            for (MappedAttribute attr : clusteringKeyAttributes) {
                attr.fromRow(session, key, key, retriever);
            }
        }

        Object value;
        if (valueColumn != null) {
            value = valuePrimitive.deserialize(retriever.getBytesUnsafe(valueColumn), protocolVersion);
        } else {
            value = objectFactory.newInstance(valueType);
            for (MappedAttribute attr : valueAttributes) {
                attr.fromRow(session, value, value, retriever);
            }
        }

        instance.getCluster().put(key, value);

        if (postLoad != null) {
            postLoad.invoke(instance, this, retriever);
        }

        return (T) instance;
    }

    @Override protected BinderAndStatement buildInsertStatement(StatementOptions statementOptions, PersistenceSessionImpl persistenceSession,
                                                                Object raw,
                                                                PersistOption... persistOptions) {
        if (!(raw instanceof MapEntity)) {
            throw new ClassCastException("only instances of " + MapEntity.class + " can be persisted as a map-entity");
        }
        MapEntity<?, ?> instance = (MapEntity) raw;

        persistOptions = withTTL(persistOptions);

        Session session = persistenceSession.driverSession();

        BatchStatement batch = new BatchStatement();

        //TODO adding a DELETE...WHERE partitionKey=? in front of INSERTs for the same partition key results in no insert at all *SICK*
        //Binder binder = new Binder(null, true, partitionKeyColumns, PersistMode.DELETE);
        //buildModifyBindColumns(instance, binder, partitionAttributes);
        //BoundStatement bStmt = buildModifyInitial(session, persistOptions, PreparedStatements.StatementType.DELETE, binder);
        //batch.add(bStmt);

        // INSERT INTO keyspace_name.table_name
        // ( column_name, column_name...)
        // VALUES ( value, value ... )
        // IF NOT EXISTS
        // USING option AND option
        boolean serializeDefault = PersistOption.WriteDefaultValueOption.get(persistOptions, writeNullOnInsert);
        CqlColumn[] columns = PersistOption.ColumnRestrictionOption.filter(persistOptions, writeDataColumns);
        Binder binder = new Binder(columns, true, primaryKeyColumns, PersistMode.INSERT, serializeDefault);

        int protocolVersion = persistenceSession.persistenceManager.protocolVersion;

        for (Map.Entry entry : instance.getCluster().entrySet()) {
            binder.reset();

            buildModifyBindColumns(instance, binder, partitionAttributes);
            if (clusteringColumn != null) {
                binder.setBytesUnsafe(clusteringColumn, clusteringPrimitive.serialize(entry.getKey(), protocolVersion));
            } else {
                buildModifyBindColumns(entry.getKey(), binder, clusteringKeyAttributes);
            }
            if (valueColumn != null) {
                binder.setBytesUnsafe(valueColumn, valuePrimitive.serialize(entry.getValue(), protocolVersion));
            } else {
                buildModifyBindColumns(entry.getValue(), binder, valueAttributes);
            }

            BoundStatement bStmt = buildModifyInitial(session, persistOptions, PreparedStatements.StatementType.INSERT, binder);
            int idx = binder.bindColumns(0, bStmt);
            bindUsingOptions(bStmt, persistOptions, idx);
            statementOptions.applyWrite(bStmt, writeConsistencyLevel, serialConsistencyLevel, persistOptions);

            if (prePersist != null) {
                prePersist.invoke(instance, this, type, bStmt);
            }

            batch.add(bStmt);
        }

        // TODO reset MapEntity only if batch was successful
        instance.reset();

        return new BinderAndStatement(batch, binder);
    }

    @Override protected BinderAndStatement buildUpdateStatement(StatementOptions statementOptions, PersistenceSessionImpl persistenceSession,
                                                                Object raw, PersistOption... persistOptions) {
        if (preparedStatements.noUpdates()) {
            // entities without any data columns (just primary key columns) have no updateable columns - so just do an insert
            return buildInsertStatement(statementOptions, persistenceSession, raw, persistOptions);
        }

        if (!(raw instanceof MapEntity)) {
            throw new ClassCastException("only instances of " + MapEntity.class + " can be persisted as a map-entity");
        }
        MapEntity<?, ?> instance = (MapEntity) raw;

        persistOptions = withTTL(persistOptions);

        Session session = persistenceSession.driverSession();

        BatchStatement batch = new BatchStatement();

        Set removed = instance.getRemoved();
        if (!removed.isEmpty()) {
            int protocolVersion = persistenceSession.persistenceManager.protocolVersion;

            Binder binder = new Binder(null, true, primaryKeyColumns, PersistMode.DELETE, true);
            for (Object key : removed) {
                binder.reset();

                buildModifyBindColumns(instance, binder, partitionAttributes);
                if (clusteringColumn != null) {
                    binder.setBytesUnsafe(clusteringColumn, clusteringPrimitive.serialize(key, protocolVersion));
                } else {
                    buildModifyBindColumns(key, binder, clusteringKeyAttributes);
                }
            }

            BoundStatement bStmt = buildModifyInitial(session, persistOptions, PreparedStatements.StatementType.DELETE, binder);
            int idx = binder.bindColumns(0, bStmt);
            bindUsingOptions(bStmt, persistOptions, idx);
            statementOptions.applyWrite(bStmt, writeConsistencyLevel, serialConsistencyLevel, persistOptions);

            if (prePersist != null) {
                prePersist.invoke(instance, this, type, bStmt);
            }

            batch.add(bStmt);
        }

        // UPDATE keyspace_name.table_name
        // USING option AND option
        // SET assignment , assignment, ...
        // WHERE row_specification
        // IF column_name = literal AND column_name = literal . . .

        int protocolVersion = persistenceSession.persistenceManager.protocolVersion;

        CqlColumn[] columns = PersistOption.ColumnRestrictionOption.filter(persistOptions, writeDataColumns);
        Binder binder = new Binder(columns, true, primaryKeyColumns, PersistMode.UPDATE, true);
        for (Map.Entry entry : instance.getCluster().entrySet()) {
            binder.reset();

            buildModifyBindColumns(instance, binder, partitionAttributes);
            if (clusteringColumn != null) {
                binder.setBytesUnsafe(clusteringColumn, clusteringPrimitive.serialize(entry.getKey(), protocolVersion));
            } else {
                buildModifyBindColumns(entry.getKey(), binder, clusteringKeyAttributes);
            }
            if (valueColumn != null) {
                binder.setBytesUnsafe(valueColumn, valuePrimitive.serialize(entry.getValue(), protocolVersion));
            } else {
                buildModifyBindColumns(entry.getValue(), binder, valueAttributes);
            }

            BoundStatement bStmt = buildModifyInitial(session, persistOptions, PreparedStatements.StatementType.UPDATE, binder);
            int idx = bindUsingOptions(bStmt, persistOptions, 0);
            idx = binder.bindColumns(idx, bStmt);
            bindIfOptions(binder, bStmt, persistOptions, idx);
            statementOptions.applyWrite(bStmt, writeConsistencyLevel, serialConsistencyLevel, persistOptions);

            if (prePersist != null) {
                prePersist.invoke(instance, this, type, bStmt);
            }

            batch.add(bStmt);
        }

        // TODO reset MapEntity only if batch was successful
        instance.reset();

        // TODO allow map-entities in tracking sessions
        //if (persistenceSession.filterUnmodifiedForUpdate(this, instance, binder)) {
        //  // return null to indicate that the update will not be executed
        //  return null;
        //}

        return new BinderAndStatement(batch, binder);
    }

    // used by buildUpdateStatement and PersistenceSesss
    @Override BoundStatement updateBoundStatement(StatementOptions statementOptions, Session session, Object raw, Binder binder,
                                                  PersistOption... persistOptions) {
        throw new UnsupportedOperationException("not supported for map-entities");
    }

    @Override protected BinderAndStatement buildDeleteStatement(StatementOptions statementOptions, PersistenceSessionImpl persistenceSession,
                                                                Object instance,
                                                                PersistOption... persistOptions) {

        // DELETE column_name, ... | ( column_name term )
        // FROM keyspace_name. table_name
        // USING TIMESTAMP integer
        // WHERE row_specification
        // ( IF ( EXISTS | (column_name = literal ) ) AND ( column_name = literal ) . . . )

        persistOptions = withTTL(persistOptions);

        Session session = persistenceSession.driverSession();

        Binder binder = new Binder(null, true, partitionKeyColumns, PersistMode.DELETE, true);
        buildModifyBindColumns(instance, binder, partitionAttributes);
        BoundStatement bStmt = buildModifyInitial(session, persistOptions, PreparedStatements.StatementType.DELETE, binder);

        int idx = bindUsingOptions(bStmt, persistOptions, 0);

        binder.bindColumns(idx, bStmt);

        bindIfOptions(binder, bStmt, persistOptions, idx);

        statementOptions.applyWrite(bStmt, writeConsistencyLevel, serialConsistencyLevel, persistOptions);

        if (preDelete != null) {
            preDelete.invoke(instance, this, type, bStmt);
        }

        return new BinderAndStatement(bStmt, binder);
    }

    private int bindUsingOptions(SettableData<?> settableData, PersistOption[] persistOptions, int idx) {
        for (PersistOption option : persistOptions) {
            if (option instanceof PersistOption.UsingOption) {
                ((PersistOption.UsingOption) option).bind(settableData, idx++);
            }
        }
        return idx;
    }

    private void bindIfOptions(Binder binder, BoundStatement bStmt, PersistOption[] persistOptions, int idx) {
        for (PersistOption option : persistOptions) {
            if (option instanceof PersistOption.IfOption) {
                PersistOption.IfOption ifOption = (PersistOption.IfOption) option;
                MappedAttribute attr = getAttributeByPath(ifOption.getAttrPath());
                if (!(attr instanceof MappedAttributeSingle)) {
                    throw new PersistenceRuntimeException(
                        "attribute path '" + ifOption + "' specified in IfOption does not resolve to a single-column attribute in type " +
                            type.getName());
                }
                MappedAttributeSingle singleAttr = (MappedAttributeSingle) attr;
                CqlColumn col = singleAttr.singleColumn();
                DataTypeMapper dataTypeMapper = col.dataTypeMapper;
                binder.bindSpecial(bStmt, col, idx);
                dataTypeMapper.fromObject(binder, col, ifOption.getValue());
            }
        }
        binder.clearSpecial();
    }

    BoundStatement buildModifyInitial(Session session, PersistOption[] persistOptions, PreparedStatements.StatementType statementType,
                                      Binder binder) {
        PreparedStatement pStmt = preparedStatements.statementFor(session, statementType, binder, persistOptions);
        BoundStatement bStmt = pStmt.bind();

        binder.bindColumns(0, bStmt);

        PersistOption.forBoundStatement(persistOptions, bStmt);
        return bStmt;
    }

    @Override void buildModifyBindColumns(Object instance, Binder binder) {
        throw new UnsupportedOperationException("not supported for map-entities");
    }

    @Override void executeLoadBoundStatements(ExecutionTracer tracer, Session session, Object[] primaryKey, AbstractDelegatesFutureExt<?> futures,
                                              StatementOptions statementOptions, PersistOption... persistOptions) {
        if (primaryKey.length != partitionKeyColumns.length) {
            throw new ModelRuntimeException("must load map-entities only by partition key for " + type + " - expected:" +
                Arrays.toString(partitionKeyColumns) + " - got " + Arrays.toString(primaryKey));
        }

        CqlColumn[] readColumns = readColumns(persistOptions);
        CqlColumn[] pkColumns = partitionKeyColumns;

        Binder binder = new Binder(readColumns, false, pkColumns, null, true);

        int idx = 0;
        try {
            for (CqlColumn col : pkColumns) {
                col.dataTypeMapper.fromObject(binder, col, primaryKey[idx++]);
            }
        } catch (Throwable t) {
            throw new RuntimeException("Invalid primary key element "+Arrays.toString(primaryKey), t);
        }

        PreparedStatement pstmt = preparedStatements.statementFor(session, PreparedStatements.StatementType.SELECT, binder, persistOptions);
        BoundStatement bStmt = pstmt.bind();
        statementOptions.applyRead(bStmt, readConsistencyLevel, persistOptions);
        PersistOption.forBoundStatement(persistOptions, bStmt);

        idx = binder.bindColumns(0, bStmt);

        PersistOption.LimitOption.apply(bStmt, persistOptions, idx);

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Submitting load for {} : {}", type, bStmt.preparedStatement().getQueryString());
        }
        if (tracer != null) {
            tracer.onBeginQuery(futures.session, this, bStmt);
        }
        futures.addResultSetFuture(this, session.executeAsync(bStmt), readColumns);
    }

    @Override <T> boolean executeLoadLazy(PersistenceSessionImpl persistenceSession, StatementOptions statementOptions, T container,
                                          MappedAttribute[] attrs,
                                          PersistOption[] persistOptions) {
        if (attrs == null || attrs.length == 0) {
            // load all lazy attributes

            List<MappedAttribute> lazyAttr = null;
            for (MappedAttribute attr : attributePaths.values()) {
                if (attr.isLazy()) {
                    if (lazyAttr == null)
                        lazyAttr = new ArrayList<MappedAttribute>();
                    lazyAttr.add(attr);
                }
            }
            attrs = lazyAttr == null ? MappedAttribute.NONE : lazyAttr.toArray(new MappedAttribute[lazyAttr.size()]);
        }

        if (attrs.length == 0) {
            throw new PersistenceRuntimeException("no lazy attributes to load");
        }

        // concat all attribute columns in a single array
        int colCount = 0;
        for (MappedAttribute attr : attrs) {
            colCount += attr.allColumns.length;
        }
        CqlColumn[] loadColumns = new CqlColumn[colCount];
        int iCol = 0;
        for (MappedAttribute attr : attrs) {
            CqlColumn[] aCols = attr.allColumns;
            int aColCnt = aCols.length;
            if (aColCnt == 1)
                loadColumns[iCol++] = aCols[0];
            else if (aColCnt > 0) {
                System.arraycopy(aCols, 0, loadColumns, iCol, aColCnt);
                iCol += aColCnt;
            }
        }

        Binder binder = new Binder(loadColumns, false, primaryKeyColumns, null, true);

        buildModifyBindColumns(container, binder);

        Session session = persistenceSession.driverSession();

        PreparedStatement pstmt = preparedStatements
            .statementFor(session, PreparedStatements.StatementType.SELECT, binder, persistOptions);
        BoundStatement bStmt = pstmt.bind();
        statementOptions.applyRead(bStmt, readConsistencyLevel, persistOptions);
        PersistOption.forBoundStatement(persistOptions, bStmt);

        int idx = binder.bindColumns(0, bStmt);

        PersistOption.LimitOption.apply(bStmt, persistOptions, idx);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Lazy load of {} attributes for {}", attrs.length, type);
        }

        ResultSet resultSet = session.execute(bStmt);
        Row row = resultSet.one();
        if (row != null) {
            Retriever retriever = new GettableRetriever(persistenceSession.persistenceManager.protocolVersion, row, loadColumns);
            for (MappedAttribute attr : attrs) {
                Object instance = resolveParentInstance(container, attr);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Applying attribute {} to {} for {}", attr.name, instance.getClass(), type);
                }
                attr.fromRow(persistenceSession, container, instance, retriever);
            }
            return true;
        }

        LOGGER.warn("Lazy load of {} attributes returned no results for {}", attrs.length, type);
        return false;
    }

    @Override @SuppressWarnings("unchecked") <T> void setupQueryBinder(QueryBinderImpl<T> queryBinder, PersistOption[] persistOptions,
                                                                       String queryName,
                                                                       StatementOptions statementOptions) {
        if (queryName != null) {
            String namedQuery = namedQueries.get(queryName);
            if (namedQuery == null) {
                throw new PersistenceRuntimeException("no named query '" + queryName + "' for entity type " + type);
            }
            queryBinder.addCondition((Class<? extends T>) type, namedQuery);
        }

        if (queryBinder.statementFor(type) != null) {
            return;
        }

        CqlColumn[] readColumns = readColumns(persistOptions);

        PreparedStatement pstmt = preparedStatements.statementFor(queryBinder.session, PreparedStatements.StatementType.SELECT,
            readColumns, queryBinder.conditionFor((Class<? extends T>) type), persistOptions);
        BoundStatement bStmt = pstmt.bind();
        statementOptions.applyRead(bStmt, readConsistencyLevel, persistOptions);
        PersistOption.forBoundStatement(persistOptions, bStmt);
        queryBinder.addStatement((Class<? extends T>) type, bStmt, readColumns);
    }

    @Override <T> void executeQuery(ExecutionTracer tracer, QueryBinderImpl<T> queryBinder, ReadFutureImpl<T> futures) {
        BoundStatement bStmt = queryBinder.statementFor(type);
        if (bStmt != null) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("Submitting query for {} : {}", type, bStmt.preparedStatement().getQueryString());
            }
            if (tracer != null) {
                tracer.onBeginQuery(futures.session, this, bStmt);
            }
            futures.addResultSetFuture(this, queryBinder.session.executeAsync(bStmt), queryBinder.readColumnsFor(type));
        }
    }

    void verifyType() {
        if (Modifier.isAbstract(type.getModifiers())) {
            throw new ModelUseException("type " + type.getName() + " is abstract");
        }
    }
}
