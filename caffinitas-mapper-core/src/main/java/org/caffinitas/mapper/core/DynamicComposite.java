/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.utils.Bytes;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.CassandraTypeParser;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.codec.DynCompoSupport;
import org.caffinitas.mapper.core.codec.TypeCodec;

import java.nio.ByteBuffer;
import java.util.Map;

/**
 * Represents a dynamic composite.
 */
public class DynamicComposite extends DynCompoSupport {

    public DynamicComposite() {}

    public DynamicComposite(int protocolVersion, Map<Byte, DataType> aliases, ByteBuffer src) {
        ByteBuffer s = src.duplicate();
        for (int i = 0; s.remaining() > 0; i++) {
            if (s.remaining() < 2) {
                throw new IllegalArgumentException("illegally serialized DynamicCompositeType for component #" + i +
                    ": missing start-of-component-type for " + Bytes.toHexString(src));
            }

            // read component type
            int alias = s.getShort() & 0xffff;
            DataType dataType;
            if (alias < 0) {
                dataType = aliases.get((byte) alias);
            } else if (alias == 0) {
                throw new IllegalArgumentException("illegally serialized DynamicCompositeType for component #" + i +
                    ": illegal component-type length 0 for " + Bytes.toHexString(src));
            } else {
                if (s.remaining() < alias) {
                    throw new IllegalArgumentException("illegally serialized DynamicCompositeType for component #" + i +
                        ": illegal component-type length for " + Bytes.toHexString(src));
                }
                ByteBuffer name = s.duplicate();
                int e = s.position() + alias;
                name.limit(e);
                s.position(e);
                TypeCodec codec = TypeCodec.utf8Instance;
                String typeStr = (String) codec.deserialize(name);
                dataType = CassandraTypeParser.parseOne(protocolVersion, typeStr);
            }

            // read component value
            if (s.remaining() < 2) {
                throw new IllegalArgumentException("illegally serialized DynamicCompositeType for component #" + i +
                    ": length field not present for " + Bytes.toHexString(src));
            }
            int l = s.getShort();
            l &= 0xffff;
            if (s.remaining() < l) {
                throw new IllegalArgumentException(
                    "illegally serialized DynamicCompositeType at component #" + i + ": indicated length " + l + "(0x" + Integer.toHexString(l) +
                        " does not match remaining buffer length " + s.remaining() + " for " + Bytes.toHexString(src));
            }
            ByteBuffer b = s.duplicate();
            int e = b.position() + l;
            b.limit(e);
            s.position(e + 1); // skip 'end of component' byte

            // add to list
            dataTypes.add(dataType);
            components.add(b);
        }
    }

    @SuppressWarnings("unchecked") public ByteBuffer serialize() {
        // see Javadoc of org.apache.cassandra.db.marshal.DynamicCompositeType for protocol description
        int l = 0;

        TypeCodec codec = TypeCodec.utf8Instance;

        ByteBuffer[] types = new ByteBuffer[dataTypes.size()];

        for (int i = 0; i < dataTypes.size(); i++) {
            DataType dataType = dataTypes.get(i);
            ByteBuffer arg = components.get(i);

            // serialize type
            String typeStr = dataType.cassandraType();
            // TODO support type aliases for DynamicComposite
            ByteBuffer typeBin = types[i] = codec.serialize(typeStr);
            int sz = typeBin.remaining();
            if (sz > 65535)
                throw new IllegalArgumentException("Component at index #"+i+" in DynamicCompositeType too long ("+sz+") - max 65535 bytes in serialized form");
            l += 2 + sz;

            // serialize data;
            if (arg != null) {
                l += arg.remaining() + 3;
            } else {
                l += 3;
            }
        }

        ByteBuffer ser = ByteBuffer.allocate(l);

        for (int i = 0; i < components.size(); i++) {
            ByteBuffer arg = components.get(i);

            // serialize type
            // TODO support type aliases for DynamicComposite
            ByteBuffer typeBin = types[i];
            ser.putShort((short) typeBin.remaining());
            ser.put(typeBin);

            // serialize data;
            Binder.bindCompositeArg(ser, arg);
        }

        ser.flip();

        return ser;
    }

}
