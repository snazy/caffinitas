/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.google.common.util.concurrent.AbstractFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.Uninterruptibles;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

final class BatchImpl implements Batch {

    private static final Logger LOGGER = LoggerFactory.getLogger(BatchImpl.class);

    private final PersistenceSessionImpl persistenceSession;
    private final BatchStatement batchStatement;
    private volatile boolean submitted;
    private final List<BatchPartFutureImpl> futures = new ArrayList<BatchPartFutureImpl>();

    public BatchImpl(PersistenceSessionImpl persistenceSession, BatchStatement.Type type) {
        this.persistenceSession = persistenceSession;
        batchStatement = new BatchStatement(type);
        persistenceSession.statementOptions.applyWrite(batchStatement, null, null);
    }

    /**
     * Calls {@link #submitBatch()}.
     */
    @Override public void close() {
        submitBatch();
    }

    @Override public void submitBatch() {
        submitBatchAsync().getUninterruptibly();
    }

    @Override public BatchFuture submitBatchAsync() {
        submitted = true;
        persistenceSession.statementOptions.applyWrite(batchStatement, null, null);
        Session session = persistenceSession.driverSession();
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Submitting batch bound statement {}", batchStatement);
        }

        for (BatchPartFutureImpl future : futures) {
            Interceptors preModify = future.mode == PersistMode.DELETE ? future.entity.preDelete : future.entity.prePersist;
            if (preModify != null) {
                preModify.invoke(future.instance, future.entity, future.mode);
            }
        }

        BatchFutureImpl future = new BatchFutureImpl(session.executeAsync(batchStatement));
        return future;
    }

    @Override public <T> BatchPartFuture<T> insert(T instance, PersistOption... persistOptions) {
        MappedEntityBase entity = persistenceSession.modifyBatch(this, instance, PersistMode.INSERT, persistOptions);
        BatchPartFutureImpl<T> f = new BatchPartFutureImpl<T>(instance, entity, PersistMode.INSERT);
        futures.add(f);
        return f;
    }

    @Override public <T> BatchPartFuture<T> update(T instance, PersistOption... persistOptions) {
        MappedEntityBase entity = persistenceSession.modifyBatch(this, instance, PersistMode.UPDATE, persistOptions);
        BatchPartFutureImpl<T> f = new BatchPartFutureImpl<T>(instance, entity, PersistMode.UPDATE);
        futures.add(f);
        return f;
    }

    @Override public <T> BatchPartFuture<T> delete(T instance, PersistOption... persistOptions) {
        MappedEntityBase entity = persistenceSession.modifyBatch(this, instance, PersistMode.DELETE, persistOptions);
        BatchPartFutureImpl<T> f = new BatchPartFutureImpl<T>(instance, entity, PersistMode.DELETE);
        futures.add(f);
        return f;
    }

    @Override public Batch add(Statement statement) {
        addBatch(statement);
        return this;
    }

    @Override public Batch addAll(Iterable<? extends Statement> statements) {
        for (Statement st : statements) {
            addBatch(st);
        }
        return this;
    }

    @Override public Collection<Statement> getStatements() {
        return batchStatement.getStatements();
    }

    //
    //
    //

    void addBatch(Statement bStmt) {
        if (submitted) {
            throw new IllegalStateException("already submitted");
        }
        batchStatement.add(bStmt);
        // TODO apply persist options to batch statement and check that persist options match
    }

    private class BatchFutureImpl extends AbstractFuture implements BatchFuture, Runnable {

        private final ResultSetFuture delegate;

        BatchFutureImpl(ResultSetFuture resultSetFuture) {
            this.delegate = resultSetFuture;
            resultSetFuture.addListener(this, MoreExecutors.sameThreadExecutor());
        }

        @Override public Object getUninterruptibly(long timeout, TimeUnit unit) throws TimeoutException {
            try {
                Uninterruptibles.getUninterruptibly(this, timeout, unit);
            } catch (ExecutionException e) {
                Exceptions.throwImmediate(e);
            }
            return null;
        }

        @Override public Object getUninterruptibly() {
            try {
                Uninterruptibles.getUninterruptibly(this);
            } catch (ExecutionException e) {
                Exceptions.throwImmediate(e);
            }
            return null;
        }

        @SuppressWarnings("unchecked") @Override public void run() {
            try {
                ResultSet rs = delegate.get();
                if (!rs.isExhausted()) {
                    Row row = rs.one();
                    boolean applied = row.getBool("[applied]");
                    if (!applied) {
                        for (BatchPartFutureImpl future : futures) {
                            future.setException(new CasNotAppliedException(future.entity, future.instance, future.mode, row));
                        }
                        setException(new CasNotAppliedException(null, null, null, row));
                        return;
                    }
                }

                for (BatchPartFutureImpl future : futures) {
                    future.set(null);

                    Interceptors postModify = future.mode == PersistMode.DELETE ? future.entity.postDelete : future.entity.postPersist;
                    if (postModify != null) {
                        postModify.invoke(future.instance, future.entity, future.mode);
                    }
                }

                set(null);

            } catch (InterruptedException e) {
                for (BatchPartFutureImpl future : futures) {
                    future.cancel(true);
                }
                cancel(true);
            } catch (ExecutionException e) {
                for (BatchPartFutureImpl future : futures) {
                    future.setException(e.getCause());
                }
                setException(e.getCause());
            }
        }
    }

    static class BatchPartFutureImpl<T> extends AbstractFuture<T> implements BatchPartFuture<T> {
        private final T instance;
        private final MappedEntityBase entity;
        private final PersistMode mode;

        public BatchPartFutureImpl(T instance, MappedEntityBase entity, PersistMode mode) {
            this.instance = instance;
            this.entity = entity;
            this.mode = mode;
        }

        @SuppressWarnings("unchecked") @Override public T getUninterruptibly() {
            try {
                return Uninterruptibles.getUninterruptibly(this);
            } catch (ExecutionException e) {
                Exceptions.throwImmediate(e);
                return null;
            }
        }

        @SuppressWarnings("unchecked") @Override public T getUninterruptibly(long timeout, TimeUnit unit) throws TimeoutException {
            try {
                return Uninterruptibles.getUninterruptibly(this, timeout, unit);
            } catch (ExecutionException e) {
                Exceptions.throwImmediate(e);
                return null;
            }
        }

        @SuppressWarnings("unchecked") @Override protected boolean set(T value) {
            return super.set(value);
        }

        @Override protected boolean setException(Throwable throwable) {
            return super.setException(throwable);
        }
    }
}
