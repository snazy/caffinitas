/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import org.caffinitas.mapper.core.codec.GettableRetriever;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

final class ReadFutureImpl<T> extends AbstractDelegatesFutureExt<List<T>> implements ReadFuture<List<T>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReadFutureImpl.class);

    ReadFutureImpl(PersistenceSessionImpl session, ExecutionTracer tracer) {
        super(session, tracer);
    }

    @Override public boolean cancel(boolean mayInterruptIfRunning) {
        boolean r = false;
        for (EntityResultSetFuture delegate : delegates) {
            r |= delegate.resultSetFuture.cancel(mayInterruptIfRunning);
        }
        return r;
    }

    @Override public boolean isCancelled() {
        for (EntityResultSetFuture delegate : delegates) {
            if (delegate.resultSetFuture.isCancelled()) {
                return true;
            }
        }
        return false;
    }

    @Override public boolean isDone() {
        for (EntityResultSetFuture delegate : delegates) {
            if (!delegate.resultSetFuture.isDone()) {
                return false;
            }
        }
        return true;
    }

    @Override public List<T> getUninterruptibly() {
        List<T> r = null;
        for (EntityResultSetFuture delegate : delegates) {
            r = wrapResultSet(delegate.entity, delegate.resultSetFuture.getUninterruptibly(), delegate.columns, r);
        }
        return r == null ? Collections.<T>emptyList() : r;
    }

    @Override public List<T> getUninterruptibly(long timeout, TimeUnit unit) throws TimeoutException {
        List<T> r = null;
        for (EntityResultSetFuture delegate : delegates) {
            // TODO handle timeout correctly !!!
            r = wrapResultSet(delegate.entity, delegate.resultSetFuture.getUninterruptibly(timeout, unit), delegate.columns, r);
        }
        return r == null ? Collections.<T>emptyList() : r;
    }

    @Override public List<T> get() throws InterruptedException, ExecutionException {
        List<T> r = null;
        for (EntityResultSetFuture delegate : delegates) {
            r = wrapResultSet(delegate.entity, delegate.resultSetFuture.get(), delegate.columns, r);
        }
        return r == null ? Collections.<T>emptyList() : r;
    }

    @Override public List<T> get(long timeout, TimeUnit unit)
        throws InterruptedException, ExecutionException, TimeoutException {
        List<T> r = null;
        for (EntityResultSetFuture delegate : delegates) {
            // TODO handle timeout correctly !!!
            r = wrapResultSet(delegate.entity, delegate.resultSetFuture.get(timeout, unit), delegate.columns, r);
        }
        return r == null ? Collections.<T>emptyList() : r;
    }

    @SuppressWarnings("unchecked") private List<T> wrapResultSet(MappedEntityBase entity, ResultSet resultSet, CqlColumn[] columns, List<T> r) {
        List<Row> all = resultSet.all();
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("got {} row(s) for read of {}", all.size(), entity.type);
        }
        if (tracer != null) {
            tracer.onReadResultSetBegin(session, entity, resultSet, columns);
        }
        T last = r == null || r.isEmpty() ? null : r.get(r.size() - 1);
        GettableRetriever retriever = new GettableRetriever(session.persistenceManager.protocolVersion, columns);
        for (Row row : all) {
            if (row != null) {
                retriever.setCurrentSource(row);
                if (tracer != null) {
                    tracer.onReadResultSetRow(session, entity, resultSet, columns, row);
                }
                T el = entity.fromRow(session, retriever, columns, last);
                if (el != null) {
                    if (r == null) {
                        r = new ArrayList<T>();
                    }
                    r.add(el);
                    last = el;
                }
            }
        }
        if (tracer != null) {
            tracer.onReadResultSetEnd(session, entity, resultSet, columns);
        }
        return r;
    }
}
