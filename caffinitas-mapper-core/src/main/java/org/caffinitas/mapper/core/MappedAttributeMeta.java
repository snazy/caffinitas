/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CMeta;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.mapper.DataTypeMapperAscii;
import org.caffinitas.mapper.core.mapper.DataTypeMapperBigint;
import org.caffinitas.mapper.core.mapper.DataTypeMapperBlob;
import org.caffinitas.mapper.core.mapper.DataTypeMapperBoolean;
import org.caffinitas.mapper.core.mapper.DataTypeMapperCounter;
import org.caffinitas.mapper.core.mapper.DataTypeMapperDate;
import org.caffinitas.mapper.core.mapper.DataTypeMapperDecimal;
import org.caffinitas.mapper.core.mapper.DataTypeMapperDouble;
import org.caffinitas.mapper.core.mapper.DataTypeMapperFloat;
import org.caffinitas.mapper.core.mapper.DataTypeMapperInet;
import org.caffinitas.mapper.core.mapper.DataTypeMapperInt;
import org.caffinitas.mapper.core.mapper.DataTypeMapperText;
import org.caffinitas.mapper.core.mapper.DataTypeMapperTime;
import org.caffinitas.mapper.core.mapper.DataTypeMapperTimestamp;
import org.caffinitas.mapper.core.mapper.DataTypeMapperTimeuuid;
import org.caffinitas.mapper.core.mapper.DataTypeMapperUuid;
import org.caffinitas.mapper.core.mapper.DataTypeMapperVarchar;
import org.caffinitas.mapper.core.mapper.DataTypeMapperVarint;

import java.util.List;

final class MappedAttributeMeta extends MappedAttributeSingle {

    MappedAttributeMeta(PersistenceManagerImpl persistenceManager, ParseAttribute parseAttribute, MappedObject parent) {
        super(persistenceManager, parseAttribute);

        // verify where the meta attribute is used
        if ((parent instanceof MappedCompositeUDT) ||
            (parent instanceof MappedAttributeCompositeUDT)) {
            throw new ModelUseException('@' + CMeta.class.getSimpleName() + " attributes must not be embedded in a non-clumn based composite");
        }
    }

    @Override void setupSingleColumns(PersistenceManagerImpl persistenceManager, DataModel model,
                                      ParseAttribute parseAttribute, AttrOverrideMap attributeOverrides, NameMapper nameMapper,
                                      String columnNamePrefix) {
        // nop
    }

    @Override void setupClassColumns(PersistenceManagerImpl persistenceManager, MappedObject container, List<CqlColumn> cqlColumns, DataModel model,
                                     ParseAttribute parseAttribute,
                                     AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix) {

        MappedAttribute baseAttribute = container.resolveAttributePath(parseAttribute.meta.attributePath());
        if (!(baseAttribute instanceof MappedAttributeSingle)) {
            throw new ModelUseException(
                "attribtue path '" + parseAttribute.meta.attributePath() + "' does not resolve to a single-column attribute for " +
                    parseAttribute.accessible);
        }
        MappedAttributeSingle baseSingleAttribute = (MappedAttributeSingle) baseAttribute;
        CqlColumn baseColumn = baseSingleAttribute.singleColumn();

        CqlColumn cqlColumn = new CqlColumnFunction(baseColumn, DataType.bigint(), parseAttribute.meta.type());
        cqlColumn.dataTypeMapper = dataTypeMapperFor(cqlColumn.getDataType());

        // TODO meta-attribute column - check if following is always correct (composite-udt, composite-columns, entity) !
        if (container != this) {
            container.addColumn(cqlColumn);
        }
        addColumn(cqlColumn);

        super.setupClassColumns(persistenceManager, container, cqlColumns, model, parseAttribute, attributeOverrides, nameMapper, columnNamePrefix);
    }

    static DataTypeMapper dataTypeMapperFor(DataType dataType) {
        switch (dataType.getName()) {
            case ASCII:
                return DataTypeMapperAscii.INSTANCE;
            case BIGINT:
                return DataTypeMapperBigint.INSTANCE;
            case BLOB:
                return DataTypeMapperBlob.INSTANCE;
            case BOOLEAN:
                return DataTypeMapperBoolean.INSTANCE;
            case COUNTER:
                return DataTypeMapperCounter.INSTANCE;
            case DECIMAL:
                return DataTypeMapperDecimal.INSTANCE;
            case DOUBLE:
                return DataTypeMapperDouble.INSTANCE;
            case FLOAT:
                return DataTypeMapperFloat.INSTANCE;
            case INET:
                return DataTypeMapperInet.INSTANCE;
            case INT:
                return DataTypeMapperInt.INSTANCE;
            case TEXT:
                return DataTypeMapperText.INSTANCE;
            case TIMESTAMP:
                return DataTypeMapperTimestamp.INSTANCE;
            case DATE:
                return DataTypeMapperDate.INSTANCE;
            case TIME:
                return DataTypeMapperTime.INSTANCE;
            case TIMEUUID:
                return DataTypeMapperTimeuuid.INSTANCE;
            case UUID:
                return DataTypeMapperUuid.INSTANCE;
            case VARCHAR:
                return DataTypeMapperVarchar.INSTANCE;
            case VARINT:
                return DataTypeMapperVarint.INSTANCE;
            default:
                throw new ModelUseException("DatType.Name." + dataType.getName() + " not allowed for single attribute");
        }
    }
}
