/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import java.util.Locale;

/**
 * Interface to provide Java identifier to C* identifier mapping.
 */
public interface NameMapper {
    /**
     * See {@link NameMapper.NoneNameMapper}.
     */
    NameMapper NONE = new NoneNameMapper();

    /**
     * See {@link NameMapper.LowerNameMapper}.
     */
    NameMapper LOWER = new LowerNameMapper();

    /**
     * See {@link NameMapper.CapitalsToUnderscoresNameMapper}.
     */
    NameMapper CAPITALS_TO_UNDERSCORES_NAME_MAPPER = new CapitalsToUnderscoresNameMapper();

    /**
     * See {@link NameMapper.RemoveEntitySuffixNameMapper}.
     */
    NameMapper REMOVE_ENTITY_SUFFIX = new RemoveEntitySuffixNameMapper();

    /**
     * See {@link NameMapper.RemoveTypeSuffixNameMapper}.
     */
    NameMapper REMOVE_TYPE_SUFFIX = new RemoveTypeSuffixNameMapper();

    /**
     * See {@link NameMapper.RemoveCompositeSuffixNameMapper}.
     */
    NameMapper REMOVE_COMPOSITE_SUFFIX = new RemoveCompositeSuffixNameMapper();

    /**
     * See {@link NameMapper.DefaultNameMapper}.
     */
    NameMapper DEFAULT = new DefaultNameMapper();

    String mapClass(String src);

    String mapAttribute(String src);

    String getColumnNameCompositeSeparator();

    /**
     * Chain of {@link #REMOVE_ENTITY_SUFFIX}, {@link #REMOVE_TYPE_SUFFIX} {@link #REMOVE_COMPOSITE_SUFFIX} and {@link #CAPITALS_TO_UNDERSCORES_NAME_MAPPER} and {@code "_"} as the attribute separator.
     */
    class DefaultNameMapper extends NameMapperChain {
        private DefaultNameMapper() {
            super("_", REMOVE_ENTITY_SUFFIX, REMOVE_TYPE_SUFFIX, REMOVE_COMPOSITE_SUFFIX, CAPITALS_TO_UNDERSCORES_NAME_MAPPER);
        }
    }

    /**
     * Chains multiple name mappers.
     * The output of any "previous" name mapper is passed to the "next" one and returns the result of the last name mapper.
     */
    class NameMapperChain implements NameMapper {
        private final NameMapper[] mappers;
        private final String columnNameCompositeSeparator;

        protected NameMapperChain(String columnNameCompositeSeparator, NameMapper... mappers) {
            this.columnNameCompositeSeparator = columnNameCompositeSeparator;
            this.mappers = mappers;
        }

        public static NameMapper chainFor(String columnNameCompositeSeparator, NameMapper... mappers) {
            return new NameMapperChain(columnNameCompositeSeparator, mappers);
        }

        @Override public String mapClass(String src) {
            for (NameMapper mapper : mappers) {
                src = mapper.mapClass(src);
            }
            return src;
        }

        @Override public String mapAttribute(String src) {
            for (NameMapper mapper : mappers) {
                src = mapper.mapAttribute(src);
            }
            return src;
        }

        @Override public String getColumnNameCompositeSeparator() {
            String sep = columnNameCompositeSeparator;
            if (sep != null) {
                return sep;
            }
            for (NameMapper mapper : mappers) {
                sep = mapper.getColumnNameCompositeSeparator();
                if (sep != null) {
                    return sep;
                }
            }
            return "_";
        }
    }

    /**
     * Performs no mapping.
     */
    class NoneNameMapper implements NameMapper {
        @Override public String mapClass(String src) {
            return src;
        }

        @Override public String mapAttribute(String src) {
            return src;
        }

        @Override public String getColumnNameCompositeSeparator() {
            return ".";
        }
    }

    /**
     * Coverts all names to be mapped to lower case.
     */
    class LowerNameMapper implements NameMapper {
        @Override public String mapClass(String src) {
            return src.toLowerCase(Locale.ROOT);
        }

        @Override public String mapAttribute(String src) {
            return src.toLowerCase(Locale.ROOT);
        }

        @Override public String getColumnNameCompositeSeparator() {
            return ".";
        }
    }

    /**
     * Inserts underscores before capitals and returns the lower case result.
     */
    class CapitalsToUnderscoresNameMapper implements NameMapper {
        @Override public String mapClass(String src) {
            return map(src);
        }

        @Override public String mapAttribute(String src) {
            return map(src);
        }

        private static String map(String src) {
            StringBuilder sb = new StringBuilder(src.length());
            int l = src.length();
            boolean wasUpper = false;
            for (int i = 0; i < l; i++) {
                char c = src.charAt(i);
                if (Character.isUpperCase(c)) {
                    if (!wasUpper && i > 0) {
                        sb.append('_');
                    }
                    sb.append(Character.toLowerCase(c));
                    wasUpper = true;
                } else {
                    wasUpper = false;
                    sb.append(c);
                }
            }
            return sb.toString();
        }

        @Override public String getColumnNameCompositeSeparator() {
            return "_";
        }
    }

    /**
     * Removes any suffix {@code Entity} from class names.
     */
    class RemoveEntitySuffixNameMapper implements NameMapper {
        @Override public String mapClass(String src) {
            if (src.endsWith("Entity")) {
                src = src.substring(0, src.length() - "Entity".length());
            }
            return src;
        }

        @Override public String mapAttribute(String src) {
            return src;
        }

        @Override public String getColumnNameCompositeSeparator() {
            return ".";
        }
    }

    /**
     * Removes any suffix {@code Type} from class names.
     */
    class RemoveTypeSuffixNameMapper implements NameMapper {
        @Override public String mapClass(String src) {
            if (src.endsWith("Type")) {
                src = src.substring(0, src.length() - "Type".length());
            }
            return src;
        }

        @Override public String mapAttribute(String src) {
            return src;
        }

        @Override public String getColumnNameCompositeSeparator() {
            return ".";
        }
    }

    /**
     * Removes any suffix {@code Composite} from class names.
     */
    class RemoveCompositeSuffixNameMapper implements NameMapper {
        @Override public String mapClass(String src) {
            if (src.endsWith("Composite")) {
                src = src.substring(0, src.length() - "Composite".length());
            }
            return src;
        }

        @Override public String mapAttribute(String src) {
            return src;
        }

        @Override public String getColumnNameCompositeSeparator() {
            return ".";
        }
    }
}
