/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;

/**
 * Internal API - do not use.
 */
public abstract class DataTypeMapper {

    protected DataTypeMapper() {}

    public abstract boolean toBoolean(Retriever retriever, CqlColumn col);

    public abstract byte toByte(Retriever retriever, CqlColumn col);

    public abstract short toShort(Retriever retriever, CqlColumn col);

    public abstract int toInt(Retriever retriever, CqlColumn col);

    public abstract long toLong(Retriever retriever, CqlColumn col);

    public abstract char toChar(Retriever retriever, CqlColumn col);

    public abstract float toFloat(Retriever retriever, CqlColumn col);

    public abstract double toDouble(Retriever retriever, CqlColumn col);

    public abstract Object toObject(Retriever retriever, CqlColumn col, Class<?> targetClass);

    public abstract void fromBoolean(Binder binder, CqlColumn col, boolean value);

    public abstract void fromByte(Binder binder, CqlColumn col, byte value);

    public abstract void fromShort(Binder binder, CqlColumn col, short value);

    public abstract void fromInt(Binder binder, CqlColumn col, int value);

    public abstract void fromLong(Binder binder, CqlColumn col, long value);

    public abstract void fromChar(Binder binder, CqlColumn col, char value);

    public abstract void fromFloat(Binder binder, CqlColumn col, float value);

    public abstract void fromDouble(Binder binder, CqlColumn col, double value);

    public abstract void fromObject(Binder binder, CqlColumn col, Object value);
}
