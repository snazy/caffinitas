/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.api.SingleColumnConverter;
import org.caffinitas.mapper.core.api.converter.NoopConverter;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.mapper.DataTypeMapperAscii;
import org.caffinitas.mapper.core.mapper.DataTypeMapperBigint;
import org.caffinitas.mapper.core.mapper.DataTypeMapperBlob;
import org.caffinitas.mapper.core.mapper.DataTypeMapperBoolean;
import org.caffinitas.mapper.core.mapper.DataTypeMapperCounter;
import org.caffinitas.mapper.core.mapper.DataTypeMapperDate;
import org.caffinitas.mapper.core.mapper.DataTypeMapperDecimal;
import org.caffinitas.mapper.core.mapper.DataTypeMapperDouble;
import org.caffinitas.mapper.core.mapper.DataTypeMapperFloat;
import org.caffinitas.mapper.core.mapper.DataTypeMapperInet;
import org.caffinitas.mapper.core.mapper.DataTypeMapperInt;
import org.caffinitas.mapper.core.mapper.DataTypeMapperText;
import org.caffinitas.mapper.core.mapper.DataTypeMapperTime;
import org.caffinitas.mapper.core.mapper.DataTypeMapperTimestamp;
import org.caffinitas.mapper.core.mapper.DataTypeMapperTimeuuid;
import org.caffinitas.mapper.core.mapper.DataTypeMapperUuid;
import org.caffinitas.mapper.core.mapper.DataTypeMapperVarchar;
import org.caffinitas.mapper.core.mapper.DataTypeMapperVarint;

import java.util.Collections;
import java.util.List;

abstract class MappedAttributeSingle extends MappedAttribute {
    final SingleColumnConverter converter;

    MappedAttributeSingle(PersistenceManagerImpl persistenceManager, ParseAttribute parseAttribute, boolean hasAttributes) {
        super(persistenceManager, parseAttribute, hasAttributes);

        this.converter = null;
    }

    MappedAttributeSingle(PersistenceManagerImpl persistenceManager, ParseAttribute parseAttribute) {
        super(persistenceManager, parseAttribute, false);

        this.converter = persistenceManager.converterFor(SingleColumnConverter.class,
            parseAttribute.column != null ? parseAttribute.column.converter() : NoopConverter.class, type);
    }

    @Override void setupSingleColumns(PersistenceManagerImpl persistenceManager, DataModel model,
                                      ParseAttribute parseAttribute,
                                      AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix) {
        setupSingleColumn(this, parseAttribute, attributeOverrides, nameMapper, columnNamePrefix);
    }

    @Override void setupClassColumns(PersistenceManagerImpl persistenceManager, MappedObject container, List<CqlColumn> cqlColumns, DataModel model,
                                     ParseAttribute parseAttribute,
                                     AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix) {
        Collections.addAll(cqlColumns, allColumns);
    }

    CqlColumn singleColumn() {
        return allColumns[0];
    }

    @Override void bindToStatement(Object instance, Binder binder) {
        CqlColumn col = singleColumn();
        if (!binder.contains(col)) {
            return;
        }
        bindSingleValueToStatement(instance, binder, col);
    }

    @SuppressWarnings("unchecked") void bindSingleValueToStatement(Object instance, Binder binder, CqlColumn col) {
        if (converter != null || !type.isPrimitive()) {
            Object val = accessor.getAny(instance);
            if (binder.skipValue(instance, val)) {
                return;
            }
            if (converter != null) {
                val = converter.fromJava(val);
            }
            col.dataTypeMapper.fromObject(binder, col, val);
        } else {
            accessor.bind(col.dataTypeMapper, binder, col, instance);
        }
    }

    @Override boolean fromRow(PersistenceSessionImpl session, Object rootInstance, Object instance, Retriever retriever) {
        CqlColumn col = singleColumn();
        if (!retriever.contains(col)) {
            return true;
        }
        if (converter != null) {
            Object data = col.dataTypeMapper.toObject(retriever, col, null);
            @SuppressWarnings("unchecked") Object conv = converter.toJava(data);
            accessor.setAny(instance, conv);
            return conv == null;
        } else {
            return accessor.fromRow(col.dataTypeMapper, retriever, col, instance);
        }
    }

    static DataTypeMapper dataTypeMapperFor(DataType dataType) {
        switch (dataType.getName()) {
            case ASCII:
                return DataTypeMapperAscii.INSTANCE;
            case BIGINT:
                return DataTypeMapperBigint.INSTANCE;
            case BLOB:
                return DataTypeMapperBlob.INSTANCE;
            case BOOLEAN:
                return DataTypeMapperBoolean.INSTANCE;
            case COUNTER:
                return DataTypeMapperCounter.INSTANCE;
            case DECIMAL:
                return DataTypeMapperDecimal.INSTANCE;
            case DOUBLE:
                return DataTypeMapperDouble.INSTANCE;
            case FLOAT:
                return DataTypeMapperFloat.INSTANCE;
            case INET:
                return DataTypeMapperInet.INSTANCE;
            case INT:
                return DataTypeMapperInt.INSTANCE;
            case TEXT:
                return DataTypeMapperText.INSTANCE;
            case TIMESTAMP:
                return DataTypeMapperTimestamp.INSTANCE;
            case DATE:
                return DataTypeMapperDate.INSTANCE;
            case TIME:
                return DataTypeMapperTime.INSTANCE;
            case TIMEUUID:
                return DataTypeMapperTimeuuid.INSTANCE;
            case UUID:
                return DataTypeMapperUuid.INSTANCE;
            case VARCHAR:
                return DataTypeMapperVarchar.INSTANCE;
            case VARINT:
                return DataTypeMapperVarint.INSTANCE;
            default:
                throw new ModelUseException("DatType.Name." + dataType.getName() + " not allowed for single attribute");
        }
    }
}
