/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.codec;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.util.ArrayUtil;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Internal API.
 */
public abstract class Retriever {

    final int protocolVersion;
    private final CqlColumn[] columns;

    protected Retriever(int protocolVersion, CqlColumn[] columns) {
        this.protocolVersion = protocolVersion;
        this.columns = columns;
    }

    public CqlColumn[] getColumns() {
        return columns;
    }

    protected int indexOf(CqlColumn col) {
        if (!col.isExists()) {
            return -1;
        }
        int i = 0;
        for (CqlColumn c : columns) {
            if (!c.isExists()) {
                continue;
            }
            if (c == col) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public boolean contains(CqlColumn col) {
        return col.isExists() && ArrayUtil.containsSame(columns, col);
    }

    public DataType getType(CqlColumn col) {
        return col.getDataType();
    }

    public <T> T getAny(CqlColumn col, TypeCodec<T> codec) {
        ByteBuffer v = getBytesUnsafe(col);
        if (v == null) {
            return null;
        }
        return codec.deserialize(v);
    }

    public <T> List<T> getList(CqlColumn col, TypeCodec<List<T>> codec) {
        return getAny(col, codec);
    }

    public <T> Set<T> getSet(CqlColumn col, TypeCodec<Set<T>> codec) {
        return getAny(col, codec);
    }

    public <K, V> Map<K, V> getMap(CqlColumn col, TypeCodec<Map<K, V>> codec) {
        return getAny(col, codec);
    }

    public CUDTValue getUDTValue(CqlColumn col, TypeCodec<CUDTValue> codec) {
        return getAny(col, codec);
    }

    //
    // simple ones
    //

    public abstract double getDouble(CqlColumn col);

    public abstract float getFloat(CqlColumn col);

    public abstract Date getDate(CqlColumn col);

    public abstract java.sql.Date getSimpleDate(CqlColumn col);

    public abstract Time getTime(CqlColumn col);

    public abstract long getLong(CqlColumn col);

    public abstract int getInt(CqlColumn col);

    public abstract boolean getBool(CqlColumn col);

    public abstract ByteBuffer getBytes(CqlColumn col);

    public abstract String getString(CqlColumn col);

    public abstract BigInteger getVarint(CqlColumn col);

    public abstract BigDecimal getDecimal(CqlColumn col);

    public abstract UUID getUUID(CqlColumn col);

    public abstract InetAddress getInet(CqlColumn col);

    public abstract boolean isNull(CqlColumn col);

    public abstract ByteBuffer getBytesUnsafe(CqlColumn col);

    public abstract ByteBuffer[] primaryKeyValues(CqlColumn[] primaryKeyColumns);
}
