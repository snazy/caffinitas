/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.accessors;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Internal API.
 */
public final class Singletons {
    private final ObjectFactory objectFactory;
    private final Map<Class<?>, Object> classInstances = new ConcurrentHashMap<Class<?>, Object>();

    public Singletons(ObjectFactory objectFactory) {this.objectFactory = objectFactory;}

    public <T> T getInstanceOf(Class<T> clazz) {
        if (clazz == null) {
            throw new NullPointerException("cannot create instance of (null)");
        }
        @SuppressWarnings("unchecked") T inst = (T) classInstances.get(clazz);
        if (inst != null) {
            return inst;
        }
        try {
            inst = objectFactory.newInstance(clazz);
        } catch (Throwable e) {
            throw new RuntimeException("Failed to create instance of " + clazz, e);
        }
        classInstances.put(clazz, inst);
        return inst;
    }
}
