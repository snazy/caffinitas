/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.UDTValue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * A query binder is used to setup the parameters for a query either by name or index.
 * <p>
 * Parameter values must be specified using Cassandra native types - not (necessarily) the coded types.
 * </p>
 * <p>
 * <b>Cassandra 1.2 notes: setting parameters by name is only possible with C* 2.0 or newer.</b>
 * </p>
 *
 * @param <ENTITY> entity type
 */
public interface QueryBinder<ENTITY> {
    QueryBinder<ENTITY> setBool(String name, boolean v);

    QueryBinder<ENTITY> setInt(String name, int v);

    QueryBinder<ENTITY> setLong(String name, long v);

    QueryBinder<ENTITY> setDate(String name, Date v);

    QueryBinder<ENTITY> setFloat(String name, float v);

    QueryBinder<ENTITY> setDouble(String name, double v);

    QueryBinder<ENTITY> setString(String name, String v);

    QueryBinder<ENTITY> setBytes(String name, ByteBuffer v);

    QueryBinder<ENTITY> setBytesUnsafe(String name, ByteBuffer v);

    QueryBinder<ENTITY> setVarint(String name, BigInteger v);

    QueryBinder<ENTITY> setDecimal(String name, BigDecimal v);

    QueryBinder<ENTITY> setUUID(String name, UUID v);

    QueryBinder<ENTITY> setInet(String name, InetAddress v);

    <T> QueryBinder<ENTITY> setList(String name, List<T> v);

    <K, V> QueryBinder<ENTITY> setMap(String name, Map<K, V> v);

    <T> QueryBinder<ENTITY> setSet(String name, Set<T> v);

    <T> QueryBinder<ENTITY> setUDTValue(String name, UDTValue v);

    QueryBinder<ENTITY> setBool(int i, boolean v);

    QueryBinder<ENTITY> setInt(int i, int v);

    QueryBinder<ENTITY> setLong(int i, long v);

    QueryBinder<ENTITY> setDate(int i, Date v);

    QueryBinder<ENTITY> setFloat(int i, float v);

    QueryBinder<ENTITY> setDouble(int i, double v);

    QueryBinder<ENTITY> setString(int i, String v);

    QueryBinder<ENTITY> setBytes(int i, ByteBuffer v);

    QueryBinder<ENTITY> setBytesUnsafe(int i, ByteBuffer v);

    QueryBinder<ENTITY> setVarint(int i, BigInteger v);

    QueryBinder<ENTITY> setDecimal(int i, BigDecimal v);

    QueryBinder<ENTITY> setUUID(int i, UUID v);

    QueryBinder<ENTITY> setInet(int i, InetAddress v);

    <T> QueryBinder<ENTITY> setList(int i, List<T> v);

    <K, V> QueryBinder<ENTITY> setMap(int i, Map<K, V> v);

    <T> QueryBinder<ENTITY> setSet(int i, Set<T> v);

    <T> QueryBinder<ENTITY> setUDTValue(int i, UDTValue v);
}
