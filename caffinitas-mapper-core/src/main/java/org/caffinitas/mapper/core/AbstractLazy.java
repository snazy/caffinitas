/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import java.io.Serializable;

/**
 * Internal API.
 * <p>
 * Provides lazy access to fields in an entity.
 * </p>
 * <p>
 * <b>Serialization support:</b> During serialization the contained element is loaded and the passed to
 * for serialization - top level implementing classes must declare the method
 * {@code Object writeReplace() throws ObjectStreamException}. See {@link java.io.Serializable}.
 * </p>
 *
 * @param <X> type of contained object
 */
public abstract class AbstractLazy<X> implements Serializable {
    private static final long serialVersionUID = -5041004165971514013L;

    private final PersistenceSessionImpl session;
    private final Object rootInstance;
    private final Object instance;
    private final MappedAttribute[] attr;
    private volatile X delegate;

    /**
     * Constructs a lazy-load object wrapper instance.
     *
     * @param session      the persistence session used to load the rootInstance object
     * @param rootInstance entity object holding this lazy object
     * @param attr         attribute to which this instance belongs to
     */
    protected AbstractLazy(PersistenceSession session, Object rootInstance, Object instance, Object attr) {
        this.session = (PersistenceSessionImpl) session;
        this.rootInstance = rootInstance;
        this.instance = instance;
        this.attr = new MappedAttribute[]{(MappedAttribute) attr};
    }

    public boolean isLoaded() {
        return delegate != null;
    }

    @SuppressWarnings("unchecked") public void load(PersistOption... persistOptions) {
        if (delegate != null) {
            return;
        }

        delegate = session.loadLazyInternal(rootInstance, attr, persistOptions) ? (X) attr[0].accessor.getObject(instance) : notFound();
    }

    protected abstract X notFound();

    protected X delegate() {
        load();
        return delegate;
    }

    public boolean containedIn(Object instance) {
        return instance == this.instance;
    }
}
