/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.tracing;

import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Statement;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceManager;
import org.caffinitas.mapper.core.PersistenceSession;
import org.caffinitas.mapper.core.UpdateSchemaStatus;

/**
 * API to get informed when execution is about to be traced.
 */
public interface ExecutionTracer
{
    /**
     * Called when a read from a result set is being (partially) performed (the {@link java.util.concurrent.Future} has been read).
     * <b>Do not read from the provided result set! Do not call any of the methods that return a row!</b>
     */
    void onReadResultSetBegin(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet, CqlColumn[] columns);

    /**
     * Called when a row is being read from a result set (the {@link java.util.concurrent.Future} has been read).
     * <b>Do not read from the provided result set! Do not call any of the methods that return a row!</b>
     */
    void onReadResultSetRow(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet, CqlColumn[] columns, Row row);

    /**
     * Called when a read from a result set has (partially) completed (the {@link java.util.concurrent.Future} has been read).
     * <b>Do not read from the provided result set! Do not call any of the methods that return a row!</b>
     */
    void onReadResultSetEnd(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, ResultSet resultSet, CqlColumn[] columns);

    /**
     * Called when a modification statement has completed (the {@link java.util.concurrent.Future} has been read).
     * <b>Do not read from the provided result set! Do not call any of the methods that return a row!</b>
     */
    void onModifyWrapResultSet(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, PersistMode mode, ResultSet resultSet);

    /**
     * Called before a {@code INSERT}/{@code UPDATE}/{@code DELETE} statement is executed.
     */
    void onBeginModify(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, PersistMode mode, Statement statement);

    /**
     * Called before a {@code SELECT} statement is executed.
     */
    void onBeginQuery(PersistenceSession persistenceSession, MappedSchemaObject<?> entity, Statement statement);

    /**
     * Called when Apache Cassandra schema is being read and applied to persistence data model.
     */
    void onUpdateSchema(PersistenceManager persistenceManager, KeyspaceMetadata keyspaceMetadata);

    /**
     * Called when Apache Cassandra schema is being applied against a concrete user type composite.
     */
    void onUpdateSchemaUserType(PersistenceManager persistenceManager, KeyspaceMetadata keyspaceMetadata, MappedSchemaObject<?> schemaObject,
                                UpdateSchemaStatus status);

    /**
     * Called when Apache Cassandra schema is being applied against a concrete entity.
     */
    void onUpdateSchemaEntity(PersistenceManager persistenceManager, KeyspaceMetadata keyspaceMetadata, MappedSchemaObject<?> schemaObject,
                              UpdateSchemaStatus status);
}
