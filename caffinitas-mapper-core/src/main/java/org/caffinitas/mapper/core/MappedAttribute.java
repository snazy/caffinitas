/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CIgnore;
import org.caffinitas.mapper.core.accessors.AttributeAccessor;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;

import java.util.List;

/**
 * Represents a single attribute of an entity or composite.
 */
abstract class MappedAttribute extends MappedObject {
    static final MappedAttribute[] NONE = {};

    final String name;
    final AttributeAccessor accessor;
    private final boolean lazy;
    String attrPath;

    MappedAttribute(PersistenceManagerImpl persistenceManager, ParseAttribute parseAttribute, boolean hasAttributes) {
        super(parseAttribute.attrType, hasAttributes);

        this.lazy = parseAttribute.lazy != null && parseAttribute.lazy.lazy();
        this.name = parseAttribute.attrName;
        this.accessor = persistenceManager.accessorFactory.accessorFor(
            parseAttribute.accessorType, parseAttribute.accessible, parseAttribute.setter);
    }

    void setupSingleColumns(PersistenceManagerImpl persistenceManager, DataModel model, ParseAttribute parseAttribute,
                            AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix) {

    }

    void setupClassColumns(PersistenceManagerImpl persistenceManager, MappedObject container, List<CqlColumn> cqlColumns, DataModel model,
                           ParseAttribute parseAttribute,
                           AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix) {

    }

    static void setupSingleColumn(MappedAttribute mappedAttribute, ParseAttribute parseAttribute,
                                  AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix) {
        CColumn column = parseAttribute.column;
        CIgnore ignore = parseAttribute.ignore;

        String columnName = mappedAttribute.makeColumnName(attributeOverrides, nameMapper, columnNamePrefix, column);

        boolean allowNotExists = ignore != null && ignore.notExists();
        boolean ignoreTypeMismatch = ignore != null && ignore.typeMismatch();
        boolean staticCol = column != null && column.isStatic();

        CqlColumn cqlColumn =
            allowNotExists || ignoreTypeMismatch || staticCol || mappedAttribute.lazy
                ? new CqlColumnSimple(columnName, parseAttribute.dataType, allowNotExists, ignoreTypeMismatch, staticCol, mappedAttribute.lazy)
                : new CqlColumnSimpleStd(columnName, parseAttribute.dataType);
        mappedAttribute.addColumn(cqlColumn);
    }

    String makeColumnName(AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix, CColumn column) {
        return makeColumnName(attributeOverrides, nameMapper, columnNamePrefix, column, name);
    }

    static String makeColumnName(AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix,
                                 CColumn column, String attrName) {
        String columnName = attributeOverrides.get(attrName);
        if (columnName == null) {
            if (column != null) {
                columnName = column.columnName();
            }
            if (columnName == null || columnName.isEmpty()) {
                columnName = nameMapper.mapAttribute(attrName);
            }
            if (!columnNamePrefix.isEmpty()) {
                columnName = columnNamePrefix + nameMapper.getColumnNameCompositeSeparator() + columnName;
            }
        }
        return columnName;
    }

    public boolean isLazy() {
        return lazy;
    }

    /**
     * Bind column value(s) from this attribute.
     *
     * @param instance object instance
     * @param binder   value binder
     */
    void bindToStatement(Object instance, Binder binder) {
        Object denormInstance = accessor.getObject(instance);
        if (denormInstance != null) {
            for (MappedAttribute attribute : allAttributes) {
                attribute.bindToStatement(denormInstance, binder);
            }
        }
    }

    /**
     * Set value on attribute
     *
     * @param session
     * @param rootInstance
     * @param instance     target object instance
     * @param retriever    row to read columns from  @return {@code true} if value from row was {@code null}.
     */
    boolean fromRow(PersistenceSessionImpl session, Object rootInstance, Object instance, Retriever retriever) {
        Object denormInstance = session.persistenceManager.objectFactory.newInstance(type);
        boolean allNull = true;
        for (MappedAttribute attribute : allAttributes) {
            allNull &= attribute.fromRow(session, rootInstance, denormInstance, retriever);
        }
        if (allNull) {
            return true;
        }
        accessor.setObject(instance, denormInstance);
        return false;
    }

    Object anyToString(Object value) {
        if (value == null) {
            return null;
        }
        if (value instanceof String) {
            return value;
        }
        if (value instanceof char[]) {
            return new String((char[]) value);
        }
        if (value instanceof byte[]) {
            return new String((byte[]) value);
        }
        return value.toString();
    }

    Object anyToInt(Object value) {
        if (value == null) {
            return null;
        }
        if (value instanceof String) {
            return Integer.parseInt((String) value);
        }
        return value.toString();
    }

    @SuppressWarnings("unchecked") Object stringToAny(String str, Class<?> targetClass) {
        if (str == null) {
            return null;
        }
        if (targetClass == String.class) {
            return str;
        }
        if (targetClass == char[].class) {
            return str.toCharArray();
        }
        if (targetClass == byte[].class) {
            return str.getBytes();
        }
        if (targetClass.isEnum()) {
            return Enum.valueOf((Class<Enum>) targetClass, str);
        }
        throw new IllegalArgumentException("unsupported stringToAny target " + targetClass);
    }

    Object intToAny(Integer val, Class<?> targetClass) {
        if (val == null) {
            return null;
        }
        if (targetClass == String.class) {
            return val.toString();
        }
        if (targetClass.isEnum()) {
            return targetClass.getEnumConstants()[val];
        }
        throw new IllegalArgumentException("unsupported intToAny target " + targetClass);
    }

    public String getName() {
        return name;
    }

    @Override public String toString() {
        return getClass().getSimpleName() + '{' +
            "name='" + name + '\'' +
            "} " + super.toString();
    }
}
