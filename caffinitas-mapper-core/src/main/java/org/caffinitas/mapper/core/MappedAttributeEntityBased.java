/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;

import java.util.ArrayList;
import java.util.List;

final class MappedAttributeEntityBased extends MappedAttribute {
    MappedAttributeEntityBased(PersistenceManagerImpl persistenceManager, ParseAttribute parseAttribute) {
        super(persistenceManager, parseAttribute, true);
    }

    @Override void setupSingleColumns(PersistenceManagerImpl persistenceManager, DataModel model,
                                      ParseAttribute parseAttribute, AttrOverrideMap attributeOverrides,
                                      NameMapper nameMapper, String columnNamePrefix) {
        ParseClass parseClass = model.entities.get(parseAttribute.attrType);
        if (parseClass == null) {
            parseClass = model.composites.get(parseAttribute.attrType);
        }

        CColumn column = parseAttribute.column;
        attributeOverrides = attributeOverrides.forPrefix(parseAttribute.attrName);
        attributeOverrides = attributeOverrides.merge(parseAttribute.attributeOverrides);
        columnNamePrefix = makeColumnName(attributeOverrides, nameMapper, columnNamePrefix, column);

        for (MappedAttribute mappedAttribute : allAttributes) {
            ParseAttribute childParseAttribute = parseClass.attributeByName(mappedAttribute.name);
            mappedAttribute.setupSingleColumns(persistenceManager, model, childParseAttribute, attributeOverrides, nameMapper, columnNamePrefix);
        }
    }

    @Override void setupClassColumns(PersistenceManagerImpl persistenceManager, MappedObject container, List<CqlColumn> cqlColumns, DataModel model,
                                     ParseAttribute parseAttribute, AttrOverrideMap attributeOverrides,
                                     NameMapper nameMapper, String columnNamePrefix) {
        ParseClass parseClass = model.entities.get(parseAttribute.attrType);
        if (parseClass == null) {
            parseClass = model.composites.get(parseAttribute.attrType);
        }

        CColumn column = parseAttribute.column;
        attributeOverrides = attributeOverrides.forPrefix(parseAttribute.attrName);
        attributeOverrides = attributeOverrides.merge(parseAttribute.attributeOverrides);
        columnNamePrefix = makeColumnName(attributeOverrides, nameMapper, columnNamePrefix, column);

        List<CqlColumn> myColumns = new ArrayList<CqlColumn>(allAttributes.length * 2);
        for (MappedAttribute attribute : allAttributes) {
            ParseAttribute childParseAttribute = parseClass.attributeByName(attribute.name);
            attribute
                .setupClassColumns(persistenceManager, this, myColumns, model, childParseAttribute, attributeOverrides, nameMapper, columnNamePrefix);
        }

        allColumns = myColumns.toArray(new CqlColumn[myColumns.size()]);

        cqlColumns.addAll(myColumns);
    }

    @Override boolean fromRow(PersistenceSessionImpl session, Object rootInstance, Object instance, Retriever retriever) {
        return super.fromRow(session, instance, instance, retriever);
    }

    @Override void bindToStatement(Object instance, Binder binder) {
        super.bindToStatement(instance, binder);
    }
}
