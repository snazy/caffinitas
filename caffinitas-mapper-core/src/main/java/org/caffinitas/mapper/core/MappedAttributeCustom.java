/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;

import java.nio.ByteBuffer;
import java.util.Set;

/**
 * Represents a {@link org.caffinitas.mapper.annotations.CColumn} with data type {@link org.caffinitas.mapper.annotations.DataTypeName#CUSTOM}.
 */
final class MappedAttributeCustom extends MappedAttributeSingle {
    MappedAttributeCustom(PersistenceManagerImpl persistenceManager, DataModel model, ParseAttribute parseAttribute, Set<Class<?>> parseSet) {
        super(persistenceManager, parseAttribute);
    }

    @SuppressWarnings("unchecked") @Override void bindToStatement(Object instance, Binder binder) {
        CqlColumn col = singleColumn();
        if (!binder.contains(col)) {
            return;
        }

        Object val = accessor.getObject(instance);
        if (converter != null) {
            val = converter.fromJava(val);
        }
        if (val != null) {
            binder.setBytesUnsafe(col, (ByteBuffer) val);
        } else {
            binder.setBytesUnsafe(col, null);
        }
    }

    @SuppressWarnings("unchecked") @Override boolean fromRow(PersistenceSessionImpl session, Object rootInstance, Object instance,
                                                             Retriever retriever) {
        CqlColumn col = singleColumn();
        if (!retriever.contains(col)) {
            return true;
        }

        ByteBuffer value = retriever.getBytesUnsafe(col);
        if (value == null || value.remaining() == 0) {
            return true;
        }
        Object val = value;
        if (converter != null) {
            val = converter.toJava(val);
        }
        accessor.setObject(instance, val);

        return false;
    }
}
