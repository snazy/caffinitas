/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.core.util.ArrayUtil;

import java.lang.reflect.Array;
import java.util.Map;

abstract class MappedEntityContainer<CHILD_TYPE extends MappedEntityInherited> extends MappedEntity {
    MappedEntityContainer parent;
    CHILD_TYPE[] directChildren;

    @SuppressWarnings("unchecked") protected MappedEntityContainer(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity,
                                                                   Class<CHILD_TYPE> childTypeClass, CqlTable cqlTable) {
        super(persistenceManager, parseEntity, cqlTable);
        directChildren = (CHILD_TYPE[]) Array.newInstance(childTypeClass, 0);
    }

    @SuppressWarnings("unchecked") MappedEntityContainer(PersistenceManagerImpl persistenceManager, ParseEntity parseEntity,
                                                         Class<CHILD_TYPE> childTypeClass) {
        super(persistenceManager, parseEntity);
        directChildren = (CHILD_TYPE[]) Array.newInstance(childTypeClass, 0);
    }

    @SuppressWarnings("unchecked") void addDirectChildEntity(CHILD_TYPE childType) {
        directChildren = ArrayUtil.add2(directChildren, childType);
        childType.parent = this;

        for (Map.Entry<String, String> namedQuery : namedQueries.entrySet()) {
            if (!childType.namedQueries.containsKey(namedQuery.getKey())) {
                childType.namedQueries.put(namedQuery.getKey(), namedQuery.getValue());
            }
        }
    }

    void pushAttributesToChildren() {
        for (MappedEntityInherited target : directChildren) {
            for (MappedAttribute mappedAttribute : allAttributes) {
                if (target.attributes.put(mappedAttribute.name, mappedAttribute) != null) {
                    throw new ModelUseException(
                        "Duplicate attribute " + mappedAttribute.name + " in single-table inheritance of " + type + " and " + target.type);
                }
                target.addAttribute(mappedAttribute);
            }
            target.attributePaths.putAll(attributePaths);

            target.pushAttributesToChildren();
        }
    }

}
