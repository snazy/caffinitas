/*
 *      Copyright (C) 2012 DataStax Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.caffinitas.mapper.core.codec;

/**
 * A value for a User Defined Type.
 */
public class CUDTValue extends AbstractData<CUDTValue> {

    private final CUDTDefinition definition;

    CUDTValue(CUDTDefinition definition) {
        // All things in a UDT are encoded with the protocol v3
        super(definition.protocolVersion, definition.size());
        this.definition = definition;
    }

    @Override protected DataType getType(int i) {
        return definition.byIdx[i].getType();
    }

    @Override protected String getName(int i) {
        return definition.byIdx[i].getName();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof CUDTValue)) {
            return false;
        }

        CUDTValue that = (CUDTValue) o;
        return definition.equals(that.definition) && super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
