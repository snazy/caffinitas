/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.mapper;

import org.caffinitas.mapper.core.CqlColumn;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;

import java.sql.Date;

/**
 * Internal API.
 */
public final class DataTypeMapperDate extends AbstractDataTypeMapperObject {
    public static final DataTypeMapperDate INSTANCE = new DataTypeMapperDate();

    private DataTypeMapperDate() {
    }

    private static Date get(Retriever retriever, CqlColumn col) {
        return retriever.getSimpleDate(col);
    }

    private static void set(Binder binder, CqlColumn col, Date value) {
        binder.setSimpleDate(col, value);
    }

    @Override public long toLong(Retriever retriever, CqlColumn col) {
        return retriever.isNull(col) ? 0L : get(retriever, col).getTime();
    }

    @Override public Object toObject(Retriever retriever, CqlColumn col, Class<?> targetClass) {
        if (retriever.isNull(col)) {
            return null;
        }
        // TODO convert to targetClass
        return get(retriever, col);
    }

    @Override public void fromLong(Binder binder, CqlColumn col, long value) {
        set(binder, col, new Date(value));
    }

    @Override public void fromObject(Binder binder, CqlColumn col, Object value) {
        if (value == null) {
            return;
        }
        // TODO convert to Date
        set(binder, col, (Date) value);
    }
}
