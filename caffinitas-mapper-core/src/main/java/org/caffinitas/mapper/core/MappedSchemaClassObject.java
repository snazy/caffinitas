/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.Metadata;
import org.caffinitas.mapper.core.codec.DataType;
import org.slf4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

abstract class MappedSchemaClassObject<DEF> extends MappedClassObject implements MappedSchemaObject<DEF> {

    final CqlTable cqlTable;
    boolean initialUpdate;

    final Lock lock = new ReentrantLock();
    final ConditionAwaitImpl existsCondition = new ConditionAwaitImpl(lock, lock.newCondition());
    final ConditionAwaitImpl existsAndValidCondition = new ConditionAwaitImpl(lock, lock.newCondition());

    public MappedSchemaClassObject(Class<?> type, CqlTable cqlTable) {
        super(type);
        this.cqlTable = cqlTable;
    }

    abstract Logger logger();

    void schemaObjectInvalid(UpdateSchemaStatus updateStatus, String pattern, Object... args) {
        String msg = String.format(pattern, args);
        updateStatus.addError(msg);
        logger().error(msg);
    }

    void fieldExists(UpdateSchemaStatus updateStatus, CqlColumn col, DataType fieldType, boolean exists) {
        if (col.isExists() == exists) {
            return;
        }

        // handle incompatible field type
        if (exists && !col.compatible(fieldType)) {
            if (col.isExists()) {
                col.setExists(false);
                updateStatus.change = true;
            } else if (col.isIgnoreTypeMismatch()) {
                logger().info("table column {}.{} for {} has incompatible type {} expected {}", cqlTable, col.getEscapedName(), type, fieldType,
                    col.getDataType());
            } else {
                schemaObjectInvalid(updateStatus, "table column %s.%s for %s has incompatible type %s expected %s", cqlTable, col.getEscapedName(),
                    type,
                    fieldType, col.getDataType());
            }
            return;
        }

        col.setExists(exists);
        updateStatus.change = true;
        if (exists) {
            if (!initialUpdate) {
                logger().info("{}.{} for {} now exists", cqlTable, col.getEscapedName(), type);
            }
        } else {
            logger().warn("{}.{} for {} no longer exists", cqlTable, col.getEscapedName(), type);
        }
    }

    void schemaObjectExists(PersistenceManagerImpl persistenceManager, UpdateSchemaStatus updateStatus, boolean exists) {
        updateStatus.exists = exists;
        if (cqlTable.isExists() == exists) {
            return;
        }
        cqlTable.setExists(exists);
        updateStatus.change = true;

        definitionChanged(persistenceManager);

        if (exists) {
            if (!initialUpdate) {
                logger().info("{} for {} now exists", cqlTable, type);
            }
        } else {
            if (!cqlTable.allowNotExists) {
                schemaObjectInvalid(updateStatus, "type %s for %s does not exist", cqlTable, type);
            }
            logger().warn("{} for {} no longer exists", cqlTable, type);
        }
    }

    UpdateSchemaStatus updateSchemaUpdateConditions(UpdateSchemaStatus updateStatus) {
        if (cqlTable.isExists()) {
            lock.lock();
            try {
                existsCondition.signalAll();
                if (cqlTable.isValid()) {
                    existsAndValidCondition.signalAll();
                } else {
                    existsAndValidCondition.clearSignalled();
                }
            } finally {
                lock.unlock();
            }
        }

        return updateStatus;
    }

    abstract String ddlType();

    @Override public Set<String> getAttributeNames() {
        return Collections.unmodifiableSet(attributes.keySet());
    }

    @Override public List<CqlColumn> getColumnsByAttributePath(String attrPath) {
        MappedObject attr = getAttributeByPath(attrPath);
        if (attr == null) {
            return null;
        }
        List<CqlColumn> columnList = new ArrayList<CqlColumn>();
        attr.collectColumns(columnList);
        return columnList;
    }

    /**
     * Retrieve a awaiter for the condition that the underlying type is known to exist and valid against the defined data model.
     */
    @Override public ConditionAwait forSchemaObjectExistsAndValid() {
        return existsAndValidCondition;
    }

    /**
     * Retrieve a awaiter for the condition that the underlying type is known to exist.
     */
    @Override public ConditionAwait forSchemaObjectExists() {
        return existsCondition;
    }

    @Override public CqlTable getCqlTable() {
        return cqlTable;
    }

    @Override public CqlStatementList getAlterDDL(Cluster cluster) {
        Metadata meta = cluster.getMetadata();
        KeyspaceMetadata keyspaceMeta = meta.getKeyspace(cqlTable.getKeyspace());
        if (keyspaceMeta == null) {
            throw new ModelRuntimeException("Keyspace " + cqlTable.getKeyspace() + " does not exist");
        }
        return getAlterDDL(keyspaceMeta);
    }

    abstract CqlStatementList getAlterDDL(KeyspaceMetadata keyspaceMeta);

    @Override public String getDropDDL() {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        pw.println("-- " + type);
        pw.println("DROP " + ddlType() + ' ' + cqlTable + ';');
        sw.flush();
        return sw.toString();
    }
}
