/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.PersistMode;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.CompositeRetriever;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.codec.TypeCodec;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Represents a Cassandra {@code org.apache.cassandra.db.marshal.CompositeType} mapped to a Java class.
 */
final class MappedCompositeC extends MappedClassObject {

    DataType dataType;
    TypeCodec codec;

    MappedCompositeC(ParseComposite parseComposite) {
        super(parseComposite.type);

        dataType = DataType.composite(Collections.<DataType>emptyList());
    }

    @Override void postInitialize(PersistenceManagerImpl persistenceManager) {
        super.postInitialize(persistenceManager);

        try {
            Arrays.sort(allColumns, CqlColumn.NAME_NUMERIC_COMPARATOR);
        } catch (Throwable t) {
            throw new ModelUseException("attributes for composites must have a column name using @" + CColumn.class.getSimpleName() +
                "(columnName()=\"0\") with an integer value representing the position of the attribute in the composite", t);
        }

        List<DataType> components = new ArrayList<DataType>();
        for (CqlColumn col : allColumns) {
            col.setExists(true);
            components.add(col.getDataType());
        }
        dataType = DataType.composite(components);

        definitionChanged(persistenceManager);
    }

    Object deserializeAny(Object o, Object rootInstance, PersistenceSessionImpl session, Class<?> javaType) {
        if (o == null) {
            return null;
        }

        ByteBuffer compValue = (ByteBuffer) o;

        Retriever udtRetriever = new CompositeRetriever(session.persistenceManager.protocolVersion, compValue, allColumns);

        Object inst = session.persistenceManager.objectFactory.newInstance(javaType);
        for (MappedAttribute attribute : allAttributes) {
            attribute.fromRow(session, rootInstance, inst, udtRetriever);
        }

        return inst;
    }

    Object serializeAny(Object o, boolean isStaticColumn, boolean serializeDefault) {
        if (o == null) {
            return null;
        }

        Binder compBinder = new Binder(allColumns, true, null, PersistMode.INSERT, serializeDefault);

        for (MappedAttribute attribute : allAttributes) {
            attribute.bindToStatement(o, compBinder);
        }

        ByteBuffer compValue = compBinder.bindComposite(isStaticColumn);

        return compValue;
    }
}
