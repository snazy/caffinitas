/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CAttributeOverride;
import org.caffinitas.mapper.annotations.CAttributeOverrides;
import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CComposite;
import org.caffinitas.mapper.annotations.CCounter;
import org.caffinitas.mapper.annotations.CDenormalized;
import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.annotations.CIgnore;
import org.caffinitas.mapper.annotations.CLazy;
import org.caffinitas.mapper.annotations.CList;
import org.caffinitas.mapper.annotations.CMap;
import org.caffinitas.mapper.annotations.CMeta;
import org.caffinitas.mapper.annotations.CMultiColumn;
import org.caffinitas.mapper.annotations.CReference;
import org.caffinitas.mapper.annotations.CSet;
import org.caffinitas.mapper.annotations.DataTypeName;
import org.caffinitas.mapper.core.api.SingleColumnConverter;
import org.caffinitas.mapper.core.api.converter.NoopConverter;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.util.GenericTypes;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.sql.Time;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

final class ParseAttribute {
    final AccessorType accessorType;
    /**
     * Contains {@link java.lang.reflect.Field} if accessorType==FIELD,
     * cContains {@link java.lang.reflect.Method} if accessorType==GETTER.
     */
    final AccessibleObject accessible;
    Method setter;

    final String attrName;
    final Class<?> attrType;
    final Type attrGenericType;
    DataTypeName dataTypeName;
    DataType dataType;
    Class<?> collectionElementTypeJava;
    DataType collectionDataType;
    Class<?> mapValueElementTypeJava;
    DataType mapValueDataType;

    CList list;
    CSet set;
    CMap map;
    CColumn column;
    CCounter counter;
    CReference reference;
    CDenormalized denormalized;
    CIgnore ignore;
    CMeta meta;
    CLazy lazy;
    CMultiColumn multiColumn;

    AttrOverrideMap attributeOverrides;

    ParseAttribute(AccessibleObject accessible, String attrName, Class<?> attrType, Type attrGenericType,
                   AccessorType accessorType) {
        this.accessible = accessible;
        this.attrName = attrName;
        this.attrType = attrType;
        this.attrGenericType = attrGenericType;
        this.accessorType = accessorType;

        list = accessible.getAnnotation(CList.class);
        set = accessible.getAnnotation(CSet.class);
        map = accessible.getAnnotation(CMap.class);
        column = accessible.getAnnotation(CColumn.class);
        multiColumn = accessible.getAnnotation(CMultiColumn.class);
        counter = accessible.getAnnotation(CCounter.class);
        reference = accessible.getAnnotation(CReference.class);
        denormalized = accessible.getAnnotation(CDenormalized.class);
        ignore = accessible.getAnnotation(CIgnore.class);
        meta = accessible.getAnnotation(CMeta.class);
        lazy = accessible.getAnnotation(CLazy.class);

        CAttributeOverrides attributeOverrides = accessible.getAnnotation(CAttributeOverrides.class);
        CAttributeOverride attributeOverride = accessible.getAnnotation(CAttributeOverride.class);

        if (column != null && multiColumn != null) {
            throw new ModelParseException(
                "must not use @" + CColumn.class.getSimpleName() + " and @" +
                    CMultiColumn.class.getSimpleName() + " together at " + accessible
            );
        }
        if (attributeOverrides != null && attributeOverride != null) {
            throw new ModelParseException(
                "must not use @" + CAttributeOverride.class.getSimpleName() + " and @" +
                    CAttributeOverrides.class.getSimpleName() + " together at " + accessible
            );
        } else if (attributeOverride != null) {
            this.attributeOverrides = new AttrOverrideMap(attributeOverride.attributePath(), attributeOverride.column());
        } else if (attributeOverrides != null) {
            this.attributeOverrides = new AttrOverrideMap(attributeOverrides.value());
        }
    }

    boolean handle(DataModel model) {
        if (list == null && set == null && map == null && column == null && multiColumn == null && counter == null && reference == null &&
            denormalized == null && meta == null) {
            if (!verifyNull(attributeOverrides)) {
                throw new ModelParseException(accessible + " has senseless annotations");
            }
            return false;
        }

        if (multiColumn != null) {
            dataTypeName = DataTypeName.MULTI_COLUMN;
            return true;
        }

        dataTypeName = column != null ? column.type() : DataTypeName.GUESS;
        if (dataTypeName == DataTypeName.GUESS && column != null && column.converter() != null && column.converter() != NoopConverter.class) {
            try {
                SingleColumnConverter conv = column.converter().getConstructor().newInstance();
                dataTypeName = conv.dataTypeName();
                if (dataTypeName == null) {
                    dataTypeName = DataTypeName.GUESS;
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        if (dataTypeName == DataTypeName.GUESS) {
            dataTypeName = guessDataTypeName(attrType, denormalized, accessible);
        }

        if (map != null) {
            if (dataTypeName != DataTypeName.MAP) {
                throw new ModelParseException(
                    '@' + CMap.class.getSimpleName() + " present on non Map attribute " + accessible);
            }
        }
        if (list != null) {
            if (dataTypeName != DataTypeName.LIST) {
                throw new ModelParseException(
                    '@' + CList.class.getSimpleName() + " present on non List attribute " + accessible);
            }
        }
        if (set != null) {
            if (dataTypeName != DataTypeName.SET) {
                throw new ModelParseException(
                    '@' + CSet.class.getSimpleName() + " present on non Set attribute " + accessible);
            }
        }

        if (dataTypeName == DataTypeName.MAP) {
            if (!verifyNull(set, list, counter, reference, denormalized, attributeOverrides)) {
                throw new ModelParseException(
                    accessible + " has non-map related annotations but attribute is a map");
            }
            handleMap(model);
            return true;
        }

        if (dataTypeName == DataTypeName.LIST) {
            if (!verifyNull(set, map, counter)) {
                throw new ModelParseException(
                    accessible + " has non-list related annotations but attribute is a list");
            }
            handleList(model);
            return true;
        }

        if (dataTypeName == DataTypeName.SET) {
            if (!verifyNull(map, list, counter)) {
                throw new ModelParseException(
                    accessible + " has non-set related annotations but attribute is a set");
            }
            handleSet(model);
            return true;
        }

        if (meta != null) {
            if (!verifyNull(reference, counter, denormalized, lazy)) {
                throw new ModelParseException(
                    accessible + " must not use @" + CReference.class.getSimpleName() + " or @" +
                        CCounter.class.getSimpleName() + " on a meta"
                );
            }
            dataTypeName = DataTypeName.META;
            dataType = DataType.bigint();
            return true;
        }

        if (dataTypeName == DataTypeName.COMPOSITE) {
            if (!verifyNull(reference, counter, denormalized, lazy)) {
                throw new ModelParseException(
                    accessible + " must not use @" + CReference.class.getSimpleName() + " or @" +
                        CCounter.class.getSimpleName() + " on a composite"
                );
            }
            dataType = null;
            return true;
        }

        if (reference != null) {
            if (dataTypeName != DataTypeName.REFERENCE) {
                throw new ModelParseException(
                    '@' + CReference.class.getSimpleName() + " present on non reference attribute " + accessible
                );
            }
        }
        if (dataTypeName == DataTypeName.REFERENCE) {
            if (!verifyNull(counter, lazy)) {
                throw new ModelParseException(
                    accessible + " must not use @" + CReference.class.getSimpleName() + " in combination with @" +
                        CCounter.class.getSimpleName()
                );
            }
            handleReference();
            return true;
        }

        if (denormalized != null) {
            if (dataTypeName != DataTypeName.DENORMALIZED) {
                throw new ModelParseException(
                    '@' + CReference.class.getSimpleName() + " present on non denormalized attribute " + accessible
                );
            }
        }
        if (dataTypeName == DataTypeName.DENORMALIZED) {
            if (!verifyNull(counter, lazy)) {
                throw new ModelParseException(
                    accessible + " must not use @" + CDenormalized.class.getSimpleName() + " in combination with @" +
                        CCounter.class.getSimpleName()
                );
            }
            handleDenormalized();
            return true;
        }

        if (counter != null || dataTypeName == DataTypeName.COUNTER) {
            handleCounter();
            return true;
        }

        if (column != null) {
            handleColumn();
            return true;
        }

        throw new ModelParseException("internal error - could not parse attribute for " + accessible);
    }

    private void handleSet(DataModel model) {
        verifyType(Set.class);

        CColumn setElement = set != null ? set.element() : null;
        DataType elemDataType = collectionElementType(model, setElement, 0, 1);

        dataType = DataType.set(elemDataType);
        collectionDataType = elemDataType;
    }

    private void handleList(DataModel model) {
        verifyType(List.class);

        CColumn collectionElement = list != null ? list.element() : null;
        DataType elemDataType = collectionElementType(model, collectionElement, 0, 1);

        dataType = DataType.list(elemDataType);
        collectionDataType = elemDataType;
    }

    private void handleMap(DataModel model) {
        verifyType(Map.class);

        CColumn keyElement = map != null ? map.key() : null;
        CColumn valueElement = map != null ? map.value() : null;
        DataType keyDataType = collectionElementType(model, keyElement, 0, 2);
        DataType valueDataType = collectionElementType(model, valueElement, 1, 2);

        dataType = DataType.map(keyDataType, valueDataType);
        collectionDataType = keyDataType;
        mapValueDataType = valueDataType;
    }

    private DataType collectionElementType(DataModel model, CColumn elemCol, int typeIndex, int typeCount) {
        DataTypeName elemDataTypeName = elemCol != null ? elemCol.type() : DataTypeName.GUESS;
        Class<?> elemType = GenericTypes.getGenericType(attrGenericType, typeIndex, typeCount);
        if (typeIndex == 0) {
            collectionElementTypeJava = elemType;
        } else if (typeIndex == 1) {
            mapValueElementTypeJava = elemType;
        }
        if (elemDataTypeName == DataTypeName.GUESS) {
            elemDataTypeName = guessSimpleType(elemType);
        }
        if (elemDataTypeName == null) {
            ParseComposite comp = model.composites.get(elemType);
            if (comp != null && comp.composite.compositeType().isSingleColumn()) {
                return DataType.custom("");
            }
        }
        if (elemDataTypeName == null) {
            throw new ModelParseException("cannot determine data type for element " + accessible);
        }
        if (!elemDataTypeName.isForCollection()) {
            throw new ModelParseException("type " + elemDataTypeName + " is not a valid data type at " + accessible);
        }
        return createSimpleDataType(elemCol, elemDataTypeName);
    }

    private void handleReference() {
        dataType = null;
    }

    private void handleDenormalized() {
        dataType = null;
    }

    private void handleCounter() {
        verifyType(Counter.class);
        dataType = DataType.counter();
    }

    private void handleColumn() {
        dataType = createSimpleDataType(column, dataTypeName);
    }

    private DataType createSimpleDataType(CColumn col, DataTypeName dataTypeName) {
        DataType elemDataType;
        switch (dataTypeName) {
            case CUSTOM:
                if (col == null || col.customClass().isEmpty()) {
                    throw new ModelParseException(
                        "mandatory attribute customClass missing for custom type element in " + accessible);
                }
                elemDataType = DataType.custom(col.customClass());
                break;
            default:
                elemDataType = dataTypeName.getDataType();
                break;
        }
        return elemDataType;
    }

    static DataTypeName guessDataTypeName(Class<?> attrType, CDenormalized denormalized, AccessibleObject accessible) {
        // TODO this functionality to guess DataTypeName into a separate class and define an API to extend it
        // TODO converter integration might be required to guess correct DataTypeName

        DataTypeName simple = guessSimpleType(attrType);
        if (simple != null) {
            return simple;
        }
        if (List.class.isAssignableFrom(attrType)) {
            return DataTypeName.LIST;
        }
        if (Set.class.isAssignableFrom(attrType)) {
            return DataTypeName.SET;
        }
        if (Map.class.isAssignableFrom(attrType)) {
            return DataTypeName.MAP;
        }
        if (attrType.getAnnotation(CComposite.class) != null) {
            return DataTypeName.COMPOSITE;
        }
        if (attrType.getAnnotation(CEntity.class) != null) {
            if (denormalized != null) {
                return DataTypeName.DENORMALIZED;
            }
            return DataTypeName.REFERENCE;
        }
        if (Counter.class.isAssignableFrom(attrType)) {
            return DataTypeName.COUNTER;
        }
        if (DynamicComposite.class.isAssignableFrom(attrType)) {
            return DataTypeName.DYNAMIC;
        }
        throw new ModelParseException("cannot guess " + DataTypeName.class.getSimpleName() + " for " + accessible);
    }

    private static DataTypeName guessSimpleType(Class<?> attrType) {
        if (attrType == byte.class || attrType == Byte.class ||
            attrType == short.class || attrType == Short.class ||
            attrType == int.class || attrType == Integer.class) {
            return DataTypeName.INT;
        }
        if (attrType == long.class || attrType == Long.class) {
            return DataTypeName.BIGINT;
        }
        if (attrType == boolean.class || attrType == Boolean.class) {
            return DataTypeName.BOOLEAN;
        }
        if (attrType == BigInteger.class) {
            return DataTypeName.VARINT;
        }
        if (attrType == float.class || attrType == Float.class) {
            return DataTypeName.FLOAT;
        }
        if (attrType == double.class || attrType == Double.class) {
            return DataTypeName.DOUBLE;
        }
        if (attrType == BigDecimal.class) {
            return DataTypeName.DECIMAL;
        }
        if (attrType == String.class || attrType == char[].class ||
            attrType == char.class || attrType == Character.class) {
            return DataTypeName.TEXT;
        }
        if (InetAddress.class.isAssignableFrom(attrType)) {
            return DataTypeName.INET;
        }
        if (attrType == byte[].class || ByteBuffer.class.isAssignableFrom(attrType)) {
            return DataTypeName.BLOB;
        }
        if (java.sql.Date.class.isAssignableFrom(attrType)) {
            return DataTypeName.DATE;
        }
        if (Time.class.isAssignableFrom(attrType)) {
            return DataTypeName.TIME;
        }
        if (Date.class.isAssignableFrom(attrType)) {
            return DataTypeName.TIMESTAMP;
        }
        if (UUID.class.isAssignableFrom(attrType)) {
            return DataTypeName.UUID;
        }
        if (attrType.isEnum()) {
            return DataTypeName.ENUM_AS_NAME;
        }
        return null;
    }

    private void verifyType(Class<?>... clazz) {
        for (Class<?> c : clazz) {
            if (c.isAssignableFrom(attrType)) {
                return;
            }
        }

        throw new ModelParseException("Attribute " + attrType + " of " + attrName + " in " + accessible +
            " is not assignable to the required types " +
            Arrays.toString(clazz));
    }

    private static boolean verifyNull(Object... check) {
        for (Object o : check) {
            if (o != null) {
                return false;
            }
        }
        return true;
    }

    public void withSetter(Method setter) {
        this.setter = setter;
    }
}
