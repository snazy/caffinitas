/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.codec;

import org.caffinitas.mapper.core.CqlColumn;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.sql.Time;
import java.util.Date;
import java.util.UUID;

/**
 * Internal API.
 */
public final class CUDTRetriever extends Retriever {

    private final CUDTValue src;

    public CUDTRetriever(CUDTValue src, CqlColumn[] columns) {
        super(src.version, columns);
        this.src = src;
    }

    @Override public double getDouble(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getDouble(i) : 0d;
    }

    @Override public float getFloat(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getFloat(i) : 0f;
    }

    @Override public Date getDate(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getDate(i) : null;
    }

    @Override public java.sql.Date getSimpleDate(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getSimpleDate(i) : null;
    }

    @Override public Time getTime(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getTime(i) : null;
    }

    @Override public long getLong(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getLong(i) : 0L;
    }

    @Override public int getInt(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getInt(i) : 0;
    }

    @Override public boolean getBool(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 && src.getBool(i);
    }

    @Override public ByteBuffer getBytes(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getBytes(i) : null;
    }

    @Override public String getString(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getString(i) : null;
    }

    @Override public BigInteger getVarint(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getVarint(i) : null;
    }

    @Override public BigDecimal getDecimal(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getDecimal(i) : null;
    }

    @Override public UUID getUUID(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getUUID(i) : null;
    }

    @Override public InetAddress getInet(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? src.getInet(i) : null;
    }

    @Override public boolean isNull(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 && src.isNull(i);
    }

    @Override public ByteBuffer getBytesUnsafe(CqlColumn col) {
        int i = indexOf(col);
        return i != -1 ? getBytesUnsafe(i) : null;
    }

    private ByteBuffer getBytesUnsafe(int i) {
        return src.getBytesUnsafe(i);
    }

    @Override public ByteBuffer[] primaryKeyValues(CqlColumn[] primaryKeyColumns) {
        throw new UnsupportedOperationException();
    }
}
