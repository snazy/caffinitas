/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core.tracing;

import org.caffinitas.mapper.core.MappedSchemaObject;
import org.caffinitas.mapper.core.PersistenceSession;

/**
 * Configurable execution tracer per thread.
 */
public class ThreadLocalExecutionTracerFactory implements ExecutionTracerFactory {
    @SuppressWarnings("ThreadLocalNotStaticFinal") private final ThreadLocal<ExecutionTracer> delegate = new ThreadLocal<ExecutionTracer>();

    public void setThreadLocalExecutionTracer(ExecutionTracer value) {
        delegate.set(value);
    }

    public void removeThreadLocalExecutionTracer() {
        delegate.remove();
    }

    @Override public ExecutionTracer getExecutionTracer(PersistenceSession persistenceSession, MappedSchemaObject<?> entity) {
        return delegate.get();
    }
}
