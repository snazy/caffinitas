/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.tracing.ExecutionTracer;
import org.caffinitas.mapper.core.util.ArrayUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the attribute reference to another entity.
 */
final class MappedAttributeEntityReference extends MappedAttribute {
    private String[] keyAttributePaths;
    private MappedEntity refEntity;

    MappedAttributeEntityReference(PersistenceManagerImpl persistenceManager, ParseAttribute parseAttribute) {
        super(persistenceManager, parseAttribute, true);
    }

    @Override void setupClassColumns(PersistenceManagerImpl persistenceManager, MappedObject container, List<CqlColumn> cqlColumns, DataModel model,
                                     ParseAttribute parseAttribute, AttrOverrideMap attributeOverrides,
                                     NameMapper nameMapper, String columnNamePrefix) {
        CColumn column = parseAttribute.column;

        attributeOverrides = attributeOverrides.forPrefix(parseAttribute.attrName);
        attributeOverrides = attributeOverrides.merge(parseAttribute.attributeOverrides);
        ParseEntity parseEntity = model.entities.get(parseAttribute.attrType);
        refEntity = persistenceManager.getEntity(parseAttribute.attrType);

        columnNamePrefix = makeColumnName(attributeOverrides, nameMapper, columnNamePrefix, column);

        this.keyAttributePaths = ArrayUtil.concat(parseEntity.entity.partitionKey(), parseEntity.entity.clusteringKey());

        List<CqlColumn> myColumns = collectEntityKeyColumns(attributeOverrides, nameMapper, columnNamePrefix, refEntity);

        allColumns = myColumns.toArray(new CqlColumn[myColumns.size()]);

        cqlColumns.addAll(myColumns);
    }

    private List<CqlColumn> collectEntityKeyColumns(AttrOverrideMap attributeOverrides, NameMapper nameMapper, String columnNamePrefix,
                                                    MappedEntity refEntity) {
        List<CqlColumn> cols = new ArrayList<CqlColumn>();
        for (String key : keyAttributePaths) {
            CqlColumn keyCol = refEntity.validAttributeColumnByPath(key, "primary key");
            keyCol = keyCol.copy();
            String override = attributeOverrides.get(key);
            if (override != null) {
                keyCol.setName(override);
            } else {
                keyCol.setName(columnNamePrefix + nameMapper.getColumnNameCompositeSeparator() + keyCol.getName());
            }
            cols.add(keyCol);
        }
        return cols;
    }

    @Override void bindToStatement(Object instance, Binder binder) {
        Object denormInstance = accessor.getObject(instance);
        if (denormInstance != null) {
            for (int i = 0; i < keyAttributePaths.length; i++) {
                String key = keyAttributePaths[i];
                CqlColumn keyCol = this.allColumns[i];
                // TODO implement CReference on entities with primary key values in composites
                if (!binder.contains(keyCol)) {
                    return;
                }
                MappedAttribute keyAttr = this.attributes.get(key);
                ((MappedAttributeSingle) keyAttr).bindSingleValueToStatement(denormInstance, binder, keyCol);
            }
        }

        // TODO implement (conditional?) persisting of referenced entities
    }

    @Override boolean fromRow(PersistenceSessionImpl session, Object rootInstance, Object instance, Retriever retriever) {
        Object[] values = new Object[this.allColumns.length];
        for (int i = 0; i < this.allColumns.length; i++) {
            CqlColumn col = this.allColumns[i];
            Object rawVal = col.dataTypeMapper.toObject(retriever, col, null);
            values[i] = rawVal;
            if (rawVal == null) {
                // if any primary key component is null, the resulting object must be null...
                return true;
            }
        }

        // now the raw primary key of the referenced object is the values[]

        ExecutionTracer tracer = session.executionTracer(session, refEntity);
        ReadOneFutureImpl refFuture = new ReadOneFutureImpl(session, tracer);
        // TODO get persistOptions somehow...
        StatementOptions statementOptions = session.statementOptions;
        PersistOption[] persistOptions = PersistOption.NO_OPTIONS;
        refEntity.executeLoadBoundStatements(tracer, session.driverSession(), values, refFuture, statementOptions, persistOptions);
        Object refInstance = refFuture.getUninterruptibly();
        if (refInstance == null) {
            return true;
        }
        accessor.setObject(instance, refInstance);
        return false;
    }
}
