/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.core;

import org.caffinitas.mapper.annotations.CompositeType;
import org.caffinitas.mapper.core.api.SingleColumnConverter;
import org.caffinitas.mapper.core.api.converter.NoopConverter;
import org.caffinitas.mapper.core.codec.Binder;
import org.caffinitas.mapper.core.codec.CUDTValue;
import org.caffinitas.mapper.core.codec.DataType;
import org.caffinitas.mapper.core.codec.Retriever;
import org.caffinitas.mapper.core.codec.TypeCodec;
import org.caffinitas.mapper.core.lazy.LazyMap;
import org.caffinitas.mapper.core.mapper.DataTypeMapperMap;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class MappedAttributeMap extends MappedAttributeSingle implements DefinitionChangedCallback {
    final Class<?> keyTypeJava;
    final Class<?> valueTypeJava;
    final Class<?> keyTypeRow;
    final Class<?> valueTypeRow;
    MappedCompositeUDT keyComposite;
    MappedCompositeUDT valueComposite;
    final boolean needsRemap;
    final SingleColumnConverter keyConverter;
    final SingleColumnConverter valueConverter;
    DataType keyDataType;
    DataType valueDataType;

    MappedAttributeMap(PersistenceManagerImpl persistenceManager, DataModel model, ParseAttribute parseAttribute) {
        super(persistenceManager, parseAttribute);

        keyTypeJava = parseAttribute.collectionElementTypeJava;
        ParseComposite keyComp = model.composites.get(keyTypeJava);
        keyTypeRow = keyComp != null && keyComp.composite.compositeType() == CompositeType.USER_TYPE ? CUDTValue.class :
            parseAttribute.collectionDataType.asJavaClass();
        keyConverter = persistenceManager.converterFor(SingleColumnConverter.class,
            parseAttribute.map != null ? parseAttribute.map.key().converter() : NoopConverter.class,
            keyTypeJava);

        valueTypeJava = parseAttribute.mapValueElementTypeJava;
        ParseComposite valueComp = model.composites.get(valueTypeJava);
        valueTypeRow = valueComp != null && valueComp.composite.compositeType() == CompositeType.USER_TYPE ? CUDTValue.class :
            parseAttribute.mapValueDataType.asJavaClass();
        valueConverter = persistenceManager.converterFor(SingleColumnConverter.class,
            parseAttribute.map != null ? parseAttribute.map.value().converter() : NoopConverter.class,
            valueTypeJava);

        needsRemap = keyTypeJava != keyTypeRow || valueTypeJava != valueTypeRow;
    }

    @SuppressWarnings("unchecked") @Override void setupClassColumns(PersistenceManagerImpl persistenceManager, MappedObject container,
                                                                    List<CqlColumn> cqlColumns, DataModel model,
                                                                    ParseAttribute parseAttribute, AttrOverrideMap attributeOverrides,
                                                                    NameMapper nameMapper,
                                                                    String columnNamePrefix) {
        super.setupClassColumns(persistenceManager, container, cqlColumns, model, parseAttribute, attributeOverrides, nameMapper, columnNamePrefix);

        keyComposite = persistenceManager.getComposite(keyTypeJava, MappedCompositeUDT.class);
        if (keyComposite != null) {
            keyComposite.addDefinitionChangedCallback(this);
            keyDataType = keyComposite.dataType;
        } else {
            keyDataType = parseAttribute.collectionDataType;
        }

        valueComposite = persistenceManager.getComposite(valueTypeJava, MappedCompositeUDT.class);
        if (valueComposite != null) {
            valueComposite.addDefinitionChangedCallback(this);
            valueDataType = valueComposite.dataType;
        } else {
            valueDataType = parseAttribute.mapValueDataType;
        }

        singleColumn().dataType = DataType.map(keyDataType, valueDataType);
        singleColumn().dataTypeMapper = new DataTypeMapperMap(TypeCodec.mapOf(keyDataType, valueDataType, persistenceManager.protocolVersion));
    }

    @SuppressWarnings("unchecked") @Override public void definitionChanged(PersistenceManager persistenceManager, MappedClassObject classObject) {
        if (keyComposite != null) {
            keyDataType = keyComposite.dataType;
        }
        if (valueComposite != null) {
            valueDataType = valueComposite.dataType;
        }

        CqlColumn col = singleColumn();
        col.dataType = DataType.map(keyDataType, valueDataType);
        col.dataTypeMapper =
            new DataTypeMapperMap(TypeCodec.mapOf(keyDataType, valueDataType, ((PersistenceManagerImpl) persistenceManager).protocolVersion));
    }

    @Override void bindSingleValueToStatement(Object instance, Binder binder, CqlColumn col) {
        if (needsRemap) {
            Object val = accessor.getAny(instance);
            if (binder.skipValue(instance, val)) {
                return;
            }
            if (val != null) {
                val = remapToRow((Map) val);
            }
            col.dataTypeMapper.fromObject(binder, col, val);
        } else {
            super.bindSingleValueToStatement(instance, binder, col);
        }
    }

    @Override boolean fromRow(PersistenceSessionImpl session, Object rootInstance, Object instance, Retriever retriever) {
        CqlColumn col = singleColumn();
        boolean missing = !retriever.contains(col);
        if (isLazy() && missing) {
            accessor.setAny(instance, new LazyMap(session, rootInstance, instance, this));
            return false;
        }
        if (needsRemap) {
            if (missing) {
                return true;
            }
            Object data = col.dataTypeMapper.toObject(retriever, col, null);
            @SuppressWarnings("unchecked") Object conv =
                data != null ? remapFromRow((Map) data, rootInstance, session) : null;
            accessor.setAny(instance, conv);
            return conv == null;
        } else {
            return super.fromRow(session, rootInstance, instance, retriever);
        }
    }

    @SuppressWarnings("unchecked") private Map remapToRow(Map<?, ?> src) {
        if (src.isEmpty()) {
            return Collections.emptyMap();
        }
        Map conv = new HashMap(src.size() * 3 / 4);
        for (Map.Entry e : src.entrySet()) {
            Object k = e.getKey();
            Object v = e.getValue();
            k = remapMaybeConvert(k, keyConverter, keyComposite, keyTypeJava, keyTypeRow);
            v = remapMaybeConvert(v, valueConverter, valueComposite, valueTypeJava, valueTypeRow);
            conv.put(k, v);
        }
        return conv;
    }

    @SuppressWarnings("unchecked") private Object remapMaybeConvert(Object o, SingleColumnConverter converter, MappedCompositeUDT composite, Class<?> typeJava,
                                                                    Class<?> typeRow) {
        if (converter != null) {
            o = converter.fromJava(o);
        }
        else if (typeJava != typeRow) {
            if (composite != null) {
                o = composite.serializeAny(o);
            } else if (typeRow == String.class) {
                o = anyToString(o);
            } else if (typeRow == Integer.class) {
                o = anyToInt(o);
            } else {
                throw new ModelUseException("unsupported collection element types : " + typeRow + " vs. " + typeJava);
            }
        }
        return o;
    }

    @SuppressWarnings("unchecked") private Map remapFromRow(Map<?, ?> src, Object rootInstance, PersistenceSessionImpl session) {
        // do not return a Collections.empty*() to make the returned object modifyable by the application
        Map conv = new HashMap(src.size() * 3 / 4);
        for (Map.Entry e : src.entrySet()) {
            Object k = e.getKey();
            Object v = e.getValue();
            if (keyConverter != null) {
                k = keyConverter.toJava(k);
            }
            else if (keyTypeJava != keyTypeRow) {
                if (keyComposite != null) {
                    k = keyComposite.deserializeAny(k, rootInstance, session, keyTypeJava);
                } else if (keyTypeRow == String.class) {
                    k = stringToAny((String) k, keyTypeJava);
                } else if (keyTypeRow == Integer.class) {
                    k = intToAny((Integer) k, keyTypeJava);
                } else {
                    throw new ModelUseException("unsupported collection element types : " + keyTypeRow + " vs. " + keyTypeJava);
                }
            }
            if (valueConverter != null) {
                v = valueConverter.toJava(v);
            }
            else if (valueTypeJava != valueTypeRow) {
                if (valueComposite != null) {
                    v = valueComposite.deserializeAny(v, rootInstance, session, valueTypeJava);
                } else if (valueTypeRow == String.class) {
                    v = stringToAny((String) v, valueTypeJava);
                } else if (valueTypeRow == Integer.class) {
                    v = stringToAny((String) v, valueTypeJava);
                } else {
                    throw new ModelUseException("unsupported collection element types : " + valueTypeRow + " vs. " + valueTypeJava);
                }
            }
            conv.put(k, v);
        }
        return conv;
    }

    @Override public Set<MappedSchemaObject<?>> collectSchemaDependencies() {
        Set<MappedSchemaObject<?>> r = null;
        if (keyComposite != null) {
            r = new HashSet<MappedSchemaObject<?>>();
            r.add(keyComposite);
        }
        if (valueComposite != null) {
            if (r == null) {
                r = new HashSet<MappedSchemaObject<?>>();
            }
            r.add(valueComposite);
        }
        return r != null ? r : Collections.<MappedSchemaObject<?>>emptySet();
    }
}
