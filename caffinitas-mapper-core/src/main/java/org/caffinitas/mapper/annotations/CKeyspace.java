/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines the (default) keyspace for {@link CEntity}s in the package.
 * <p>
 * If no {@link CKeyspace} annotation can be found, a default value
 * declared with the {@link org.caffinitas.mapper.core.PersistenceManagerBuilder#withDefaultKeyspace(String) Builder.withDefaultKeyspace} is used.
 * </p>
 * <p>
 * It is possible to use so called 'coded names' that are mapped to physical names during runtime. If the keyspace name
 * starts with an {@code @} character, the physical keyspace is looked up from the values provided using
 * {@link org.caffinitas.mapper.core.PersistenceManagerBuilder#withMappedKeyspace(String, String) Builder.withMappedKeyspace}.
 * </p>
 */
@Target(ElementType.PACKAGE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CKeyspace {
    /**
     * Name of the keyspace or a mapped value starting with {@code '@'} that is looked up from the key-value pairs
     * configured with {@link org.caffinitas.mapper.core.PersistenceManagerBuilder#withMappedKeyspace(String, String) Builder.withMappedKeyspace}.
     *
     * @return keyspace name
     */
    String keyspace();
}
