/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import org.caffinitas.mapper.core.api.SingleColumnConverter;
import org.caffinitas.mapper.core.api.converter.NoopConverter;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares a Java field or getter method to be used for persistence.
 * <p>
 * When declared on a Java field, the value will be read from and written to using direct access to that field.
 * </p>
 * <p>
 * WHen declared on a Java getter method, the value will be read by invoking that getter method and written using the corresponding setter method.
 * </p>
 * <p>
 * Fields and methods do not need to be {@code public}.
 * </p>
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CColumn {
    /**
     * The column name. Uses the configured {@link org.caffinitas.mapper.core.NameMapper} to generate a column name.
     * Note that declared column names are concatenated when using nested structures like an entity containing a composite of type
     * {@link CompositeType#COLUMNS}. In such a case use the
     * {@link CAttributeOverrides} annotation on an attribute to override default column name generation.
     *
     * @return column name
     */
    String columnName() default "";

    /**
     * Declares the type of the attribute.
     *
     * @return data type name - defaults to {@link DataTypeName#GUESS}
     */
    DataTypeName type() default DataTypeName.GUESS;

    /**
     * work in progress - no doc yet
     *
     * @return no doc yet
     */
    String customClass() default "";

    /**
     * Defines a an attribute's column to be a CQL static column.
     *
     * @return {@code true} if static - defaults to {@code false}
     */
    boolean isStatic() default false;

    /**
     * Specify the converter to use for this column to convert between Cassandra type and Java type.
     * Defaults to no conversion.
     *
     * @see CMap#key()
     * @see CMap#value()
     * @see CSet#element()
     * @see CList#element()
     */
    Class<? extends SingleColumnConverter> converter() default NoopConverter.class;
}
