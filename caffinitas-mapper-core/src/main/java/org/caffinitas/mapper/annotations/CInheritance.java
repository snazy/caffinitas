/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Can be used on the root of an entity inheritance tree to declare that entity to be either of type 'single-table' or 'table-per-class'.
 * Partition and clustering key values are not permitted in {@link CEntity} annotations on inherited entities except the root
 * of an inheritance tree.
 */
@Target(ElementType.TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CInheritance {
    /**
     * Defines the inheritance type.
     *
     * @return inheritance type - defaults to {@link InheritanceType#TABLE_PER_CLASS}.
     */
    InheritanceType type() default InheritanceType.TABLE_PER_CLASS;
}
