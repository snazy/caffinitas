/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Optional annotation to ignore a table / type / column.
 * By default all declared entities, user types and columns must be present when the {@link org.caffinitas.mapper.core.PersistenceManager}
 * implementation is initialized.
 * By adding this annotation the persistence manager ignores non-existing or non-matching user typs and columns.
 * The persistence manager listens for schema changes and allows use entities, user types and columns when they appear or are compatible.
 */
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CIgnore {
    /**
     * Ignore if column / type / table does not exist in C* schema.
     *
     * @return {@code true} to ignore attribute if column does not exists - defaults to {@code true}
     */
    boolean notExists() default true;

    /**
     * Ignore if there is a column type mismatch.
     *
     * @return {@code true} to ignore attribute if column type does not match - defaults to {@code true}
     */
    boolean typeMismatch() default true;
}
