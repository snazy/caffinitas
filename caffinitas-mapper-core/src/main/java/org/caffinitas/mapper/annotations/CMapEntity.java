/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import com.datastax.driver.core.TableMetadata;
import org.caffinitas.mapper.core.NameMapper;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares a map-entity (requires Cassandra 2.0).
 * <p>
 * A map entity is identitfied by its partition key (= <i>Thrift key validation class</i>).
 * It contains a key-value map where the key is the clustering key (= <i>Thrift comparator type) and
 * the value is modeled by either a simple type (if only a single value column is present - (= <i>Thrift validation class</i>))
 * or a modelled class (if multiple value columns are present)</i>
 * </p>
 * <p>
 * Such mapped entities consist of a <i>partition entity</i> and a <i>clustering entity</i>. This annotation is placed on the
 * <i>partition entity</i> and this annotation declares the <i>clustering entity</i> via the {@link #clusteringType()} annotation.
 * </p>
 * <p>
 * Inheritance structures are not allowed on map-entities.
 * </p>
 * <p>
 * <i>Partition entities</i> <b>must</b> extend {@link org.caffinitas.mapper.core.MapEntity}.
 * </p>
 * <p>
 * <b><u>IMPORTANT RESTRICTION</u></b> There must be no attributes with the same name. No fields of the same name in any of the three classes!
 * </p>
 * <p>
 * Thrift examples in CQL:<br>
 * First one with <i>simple</i> types - second one with composites.
 * </p>
 * <pre><code>
 *    -- with key validation class Int32Type
 *    -- with comparator type UTF8Type
 *    -- with default validation class BytesType
 *    CREATE TABLE thrift.thrift_int_int (
 *       key int,
 *       column1 text,
 *       value blob,
 *       PRIMARY KEY (key, column1)
 *    );
 *    -- with key validation class CompositeType(UTF8Type,Int32Type)
 *    -- with comparator type CompositeType(LongType,BooleanType)
 *    -- with default validation class CompositeType(UTF8Type,TimeUUIDType)
 *    CREATE TABLE thrift.thrift_text_int_long_bool (
 *       key text,
 *       key2 int,
 *       column1 bigint,
 *       column2 boolean,
 *       value 'org.apache.cassandra.db.marshal.CompositeType(org.apache.cassandra.db.marshal.UTF8Type,org.apache.cassandra.db.marshal.TimeUUIDType)',
 *       PRIMARY KEY ((key, key2), column1, column2)
 *    );
 * </code></pre>
 *
 * @see org.caffinitas.mapper.core.DataModel
 * @see org.caffinitas.mapper.core.scan.DataModelScanner
 */
@Target(ElementType.TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CMapEntity {
    /**
     * Defines the keyspace for this entity.
     * <p>
     * If the value of this attribute is empty, the value of the {@link org.caffinitas.mapper.annotations.CKeyspace} annotation
     * on the Java package is used. If even no {@link org.caffinitas.mapper.annotations.CKeyspace} annotation can be found, a default value
     * declared with the {@link org.caffinitas.mapper.core.PersistenceManagerBuilder#withDefaultKeyspace(String) Builder.withDefaultKeyspace} is used.
     * </p>
     * <p>
     * It is possible to use so called 'coded names' that are mapped to physical names during runtime. If the keyspace name
     * starts with an {@code @} character, the physical keyspace is looked up from the values provided using
     * {@link org.caffinitas.mapper.core.PersistenceManagerBuilder#withMappedKeyspace(String, String) Builder.withMappedKeyspace}.
     * </p>
     *
     * @return keyspace name or name indirection
     */
    String keyspace() default "";

    /**
     * Name of the CQL table.
     *
     * @return table name - defaults to the name-mapped value of the entity's {@link Class#getSimpleName()}.
     */
    String table() default "";

    /**
     * Class of the name mapper to use.
     *
     * @return name mapper class - defaults to {@code NameMapper.DefaultNameMapper.class}.
     */
    Class<? extends NameMapper> nameMapper() default NameMapper.DefaultNameMapper.class;

    /**
     * Array containing the attribute path (dotted notation of attribute names) of single-column attributes that make the
     * entity's partition key.
     *
     * @return attribute paths that make the partition key
     */
    String[] partitionKey();

    /**
     * Declares the type of the child entity key.
     */
    Class<?> clusteringType();

    /**
     * If set to any primitive data type it allows to use primitive classes like {@code String}, {@code UUID} or {@code Long} as value type.
     */
    DataTypeName valueDataType() default DataTypeName.GUESS;

    /**
     * Declares the type of the child entity value.
     */
    Class<?> valueType();

    /**
     * If set to any primitive data type it allows to use primitive classes like {@code String}, {@code UUID} or {@code Long} as clustering key.
     */
    DataTypeName clusteringDataType() default DataTypeName.GUESS;

    /**
     * Array containing the attribute path (dotted notation of attribute names) of single-column attributes that make the
     * entity's clustering key (if any) in the {@link #clusteringType()}.
     *
     * @return attribute path s that make the clustering key
     */
    String[] clusteringKey();

    /**
     * Optional value to declare the ordering of the clustering key elements. Default ordering is ascending.
     *
     * @return clustering key column ordering
     */
    TableMetadata.Order[] clusteringKeyOrder() default {};

    /**
     * Named queried for this entity.
     * <p>
     * In a {@link InheritanceType#TABLE_PER_CLASS} inheritance structure, different
     * entities may define named queries using the same name but different queries to allow to query each distinct table using
     * its distinct query. If one inherited entity does not define its own named query, the variant of its superclass is used.
     * </p>
     *
     * @return named queries for this entity
     */
    CNamedQuery[] namedQueries() default {};

    /**
     * Configures default TTL for inserts in seconds. Must be greater than 0.
     */
    int defaultTTL() default -1;

    /**
     * Write null (default) column values upon {@link org.caffinitas.mapper.core.PersistenceSession#insert(Object, org.caffinitas.mapper.core.PersistOption...)}
     */
    boolean writeNullOnInsert() default true;
}
