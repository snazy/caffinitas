/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares a single attribute path to column name mapping for a denormalized, embedded
 * entity ({@link CDenormalized}) or column composite ({@link CComposite} of
 * type {@link CompositeType#COLUMNS}).
 *
 * @see CAttributeOverrides to use multiple instances of this annotation
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CAttributeOverride {
    /**
     * Path to the single-column attribute. Separate Java class attribute names using a dot {@code '.'}.
     *
     * @return attribute path
     */
    String attributePath();

    /**
     * Name of the column to use.
     *
     * @return column name
     */
    String column();
}
