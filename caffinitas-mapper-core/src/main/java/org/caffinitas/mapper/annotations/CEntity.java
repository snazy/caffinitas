/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import com.datastax.driver.core.TableMetadata;
import org.caffinitas.mapper.core.NameMapper;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares a Java class to be mapped as an entity.
 *
 * @see CInheritance
 * @see org.caffinitas.mapper.core.DataModel
 * @see org.caffinitas.mapper.core.scan.DataModelScanner
 */
@Target(ElementType.TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CEntity {
    /**
     * Defines the keyspace for this entity.
     * <p>
     * If the value of this attribute is empty, the value of the {@link CKeyspace} annotation
     * on the Java package is used. If even no {@link CKeyspace} annotation can be found, a default value
     * declared with the {@link org.caffinitas.mapper.core.PersistenceManagerBuilder#withDefaultKeyspace(String) Builder.withDefaultKeyspace} is used.
     * </p>
     * <p>
     * It is possible to use so called 'coded names' that are mapped to physical names during runtime. If the keyspace name
     * starts with an {@code @} character, the physical keyspace is looked up from the values provided using
     * {@link org.caffinitas.mapper.core.PersistenceManagerBuilder#withMappedKeyspace(String, String) Builder.withMappedKeyspace}.
     * </p>
     *
     * @return keyspace name or name indirection
     */
    String keyspace() default "";

    /**
     * Name of the CQL table.
     *
     * @return table name - defaults to the name-mapped value of the entity's {@link Class#getSimpleName()}.
     */
    String table() default "";

    /**
     * Class of the name mapper to use.
     *
     * @return name mapper class - defaults to {@code NameMapper.DefaultNameMapper.class}.
     */
    Class<? extends NameMapper> nameMapper() default NameMapper.DefaultNameMapper.class;

    /**
     * Array containing the attribute path (dotted notation of attribute names) of single-column attributes that make the
     * entity's partition key.
     * Not permitted on inherited entities except the root of an inheritance tree (that one with the
     * {@link CInheritance} annotation).
     * Note: partition key is the <i>Thrift key validation class</i>.
     *
     * @return attribute paths that make the partition key
     */
    String[] partitionKey() default {};

    /**
     * Array containing the attribute path (dotted notation of attribute names) of single-column attributes that make the
     * entity's clustering key (if any).
     * Not permitted on inherited entities except the root of an inheritance tree (that one with the
     * {@link CInheritance} annotation).
     * Note: clustering key is the <i>Thrift comparator type</i>.
     *
     * @return attribute path s that make the clustering key
     */
    String[] clusteringKey() default {};

    /**
     * Optional value to declare the ordering of the clustering key elements. Default ordering is ascending.
     *
     * @return clustering key column ordering
     */
    TableMetadata.Order[] clusteringKeyOrder() default {};

    /**
     * Named queried for this entity.
     * <p>
     * In a {@link org.caffinitas.mapper.annotations.InheritanceType#TABLE_PER_CLASS} inheritance structure, different
     * entities may define named queries using the same name but different queries to allow to query each distinct table using
     * its distinct query. If one inherited entity does not define its own named query, the variant of its superclass is used.
     * </p>
     *
     * @return named queries for this entity
     */
    CNamedQuery[] namedQueries() default {};

    /**
     * Configures default TTL for inserts in seconds. Must be greater than 0.
     */
    int defaultTTL() default -1;

    /**
     * Specifies whether all lazy attributes should be loaded if one lazy attribute is resolved.
     */
    boolean lazyLoadAll() default false;

    /**
     * Write null (default) column values upon {@link org.caffinitas.mapper.core.PersistenceSession#insert(Object, org.caffinitas.mapper.core.PersistOption...)}
     */
    boolean writeNullOnInsert() default true;
}
