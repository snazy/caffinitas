/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import org.caffinitas.mapper.core.NameMapper;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares a Java class to be a mapped composite.
 * For a description of the available different composite types see {@link CompositeType}
 */
@Target(ElementType.TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CComposite {
    /**
     * Key space for Cassandra 2.1 user defined types.
     *
     * @return keyspace name - {@link CEntity#keyspace()} for details}
     */
    String keyspace() default "";

    /**
     * Name of the CQL user type.
     * Defaults to the name-mapped value of the entity's {@link Class#getSimpleName()}.
     *
     * @return user type name in Cassandra schema
     */
    String type() default "";

    /**
     * Class of the name mapper to use.
     *
     * @return name mapper class - defaults to {@code NameMapper.DefaultNameMapper.class}.
     */
    Class<? extends NameMapper> nameMapper() default NameMapper.DefaultNameMapper.class;

    /**
     * Type of the composite.
     *
     * @return composite type - defaults to {@link CompositeType#COLUMNS}.
     */
    CompositeType compositeType() default CompositeType.COLUMNS;
}
