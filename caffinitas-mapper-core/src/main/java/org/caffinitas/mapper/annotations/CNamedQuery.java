/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares a named query for an entity.
 * <p>
 * In a {@link org.caffinitas.mapper.annotations.InheritanceType#TABLE_PER_CLASS} inheritance structure, different
 * entities may define named queries using the same name but different queries to allow to query each distinct table using
 * its distinct query. If one inherited entity does not define its own named query, the variant of its superclass is used.
 * </p>
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CNamedQuery {
    /**
     * Name of the query.
     */
    String name();

    /**
     * The WHERE clause (without the {@code WHERE} keyword) of this query.
     */
    String condition();
}
