/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares the discriminator column for an entity type inheritance of type {@code @CInheritance(type = InheritanceType.SINGLE_TABLE)}.
 * Must be present on the root entity of a {@link InheritanceType#SINGLE_TABLE} inheritance tree.
 * <p>
 * To use the default behaviour, use this snippet: {@code @CDiscriminatorColumn(column = @CColumn)}.
 * The default discriminator column name is {@code discr} of type {@code TEXT}.
 * </p>
 *
 * @see CInheritance inheritance annotation
 * @see CDiscriminatorValue discriminator value
 */
@Target(ElementType.TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CDiscriminatorColumn {
    /**
     * Discriminator column definition - use the code snippet {@code @CDiscriminatorColumn(column = @CColumn)} to use default values.
     *
     * @return discriminator column definition
     */
    CColumn column();
}
