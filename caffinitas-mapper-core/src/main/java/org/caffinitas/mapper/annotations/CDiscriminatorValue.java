/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares the discriminator value for each entity of type {@code @CInheritance(type = InheritanceType.SINGLE_TABLE)}.
 * Might be present on each entity of a {@link InheritanceType#SINGLE_TABLE} inheritance tree.
 * Uses the name-mapped value of the entity's class {@link Class#getSimpleName()}.
 *
 * @see CInheritance inheritance annotation
 * @see CDiscriminatorColumn discriminator column
 * @see org.caffinitas.mapper.core.NameMapper name mapping
 */
@Target(ElementType.TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CDiscriminatorValue {
    /**
     * Discriminator value.
     *
     * @return discriminator value
     */
    String value();
}
