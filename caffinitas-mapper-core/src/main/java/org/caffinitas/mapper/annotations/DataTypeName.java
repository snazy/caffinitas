/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

import org.caffinitas.mapper.core.codec.DataType;

/**
 * The default setting for the data type of attributes is {@link #GUESS} which means that the data type is chosen by the Java data type
 * (including generic collections). If another mapping (for example a Java {@code int} mapped to a CQL {@code bigint} or a custom
 * {@link org.caffinitas.mapper.core.api.SingleColumnConverter} has been configured, it is required to set the data type defined by this enum.
 */
public enum DataTypeName {

    /**
     * Use CQL type {@code ascii}, which represents a Java {@link String}.
     */
    ASCII(DataType.ascii(), true, true),
    /**
     * Use CQL type {@code bigint}, which represents a Java {@code long} or {@link Long}.
     */
    BIGINT(DataType.bigint(), true, true),
    /**
     * Use CQL type {@code blob}, which represents a Java {@link java.nio.ByteBuffer} or {@code byte[]}.
     */
    BLOB(DataType.blob(), true, true),
    /**
     * Use CQL type {@code boolean}, which represents a Java {@code boolean} or {@link java.lang.Boolean}.
     */
    BOOLEAN(DataType.cboolean(), true, true),
    COUNTER(DataType.counter(), false, true),
    /**
     * Use CQL type {@code decimal}, which represents a Java {@link java.math.BigDecimal}.
     */
    DECIMAL(DataType.decimal(), true, true),
    /**
     * Use CQL type {@code bigint}, which represents a Java {@code double} or {@link Double}.
     */
    DOUBLE(DataType.cdouble(), true, true),
    /**
     * Use CQL type {@code bigint}, which represents a Java {@code float} or {@link Float}.
     */
    FLOAT(DataType.cfloat(), true, true),
    /**
     * Use CQL type {@code inet}, which represents a Java {@link java.net.InetAddress}.
     */
    INET(DataType.inet(), true, true),
    /**
     * Use CQL type {@code bigint}, which represents a Java {@code int} or {@link Integer}.
     */
    INT(DataType.cint(), true, true),
    /**
     * Use CQL type {@code text}, which represents a Java {@link String}.
     */
    TEXT(DataType.text(), true, true),
    /**
     * Use CQL type {@code timestamp}, which represents a Java {@link java.util.Date}.
     */
    TIMESTAMP(DataType.timestamp(), true, true),
    /**
     * Use CQL type {@code date}, which represents a Java {@link java.sql.Date}.
     */
    DATE(DataType.date(), true, true),
    /**
     * Use CQL type {@code time}, which represents a Java {@link java.sql.Time}.
     */
    TIME(DataType.time(), true, true),
    /**
     * Use CQL type {@code uuid}, which represents a Java {@link java.util.UUID}.
     */
    UUID(DataType.uuid(), true, true),
    /**
     * @deprecated use {@link #TEXT} instead
     */
    @Deprecated
    VARCHAR(DataType.text(), true, true),
    /**
     * Use CQL type {@code varint}, which represents a Java {@link java.math.BigInteger}.
     */
    VARINT(DataType.varint(), true, true),
    /**
     * Use CQL type {@code timeuuid}, which represents a Java {@link java.util.UUID}.
     */
    TIMEUUID(DataType.timeuuid(), true, true),
    /**
     * Use custom CQL type {@code bigint}, which represents a Java {@link java.nio.ByteBuffer}.
     */
    CUSTOM(null, false, false),
    /**
     * Use CQL type {@code list}, which represents a Java {@link java.util.List}.
     */
    LIST(null, false, false),
    /**
     * Use CQL type {@code set}, which represents a Java {@link java.util.Set}.
     */
    SET(null, false, false),
    /**
     * Use CQL type {@code map}, which represents a Java {@link java.util.Map}.
     */
    MAP(null, false, false),
    /**
     * Cassandra dynamic composite type.
     */
    DYNAMIC(null, true, false),

    /**
     * Let Caffinitas guess the type of an attribute - this is suitable for most cases even {@link java.util.Set},
     * {@link java.util.List},  {@link java.util.Map}, Java enumerations,
     * meta fields (annotated with {@link org.caffinitas.mapper.annotations.CMeta}) and composites.
     */
    GUESS(null, false, false),

    /**
     * Use Java enumeration stored using the enumeration's {@link Enum#ordinal()} as an {@code int}.
     */
    ENUM_AS_ORDINAL(DataType.cint(), true, false),
    /**
     * Use Java enumeration stored using the enumerations's {@link Enum#name()} as a {@code String}.
     */
    ENUM_AS_NAME(DataType.text(), true, false),
    /**
     * Declares column meta attribute like {@code WRITETIME} or {@code TTL}.
     *
     * @see CMeta meta annotation
     */
    META(DataType.timestamp(), false, false),
    /**
     * Declares an attribute to be a composite.
     *
     * @see CComposite composite type definition
     */
    COMPOSITE(null, false, false),
    /**
     * Store the primary key of the attribute (which must be an entity instance) and load the referenced entity when the
     * containing entity is being loaded.
     *
     * @see CReference reference form
     */
    REFERENCE(null, false, false),
    /**
     * Store the full contents of the attribute (which must be an entity instance) in the containing entity or composite.
     *
     * @see CDenormalized denormalized form
     */
    DENORMALIZED(null, false, false),
    /**
     * Represents a multi-column converted Java type.
     *
     * @see org.caffinitas.mapper.annotations.CMultiColumn multi column conversion
     */
    MULTI_COLUMN(null, false, false);

    private final DataType dataType;
    private final boolean forCollection;
    private final boolean primitive;

    DataTypeName(DataType dataType, boolean forCollection, boolean primitive) {
        this.dataType = dataType;
        this.forCollection = forCollection;
        this.primitive = primitive;
    }

    public DataType getDataType() {
        return dataType;
    }

    public boolean isForCollection() {
        return forCollection;
    }

    public boolean isPrimitive() {
        return primitive;
    }
}
