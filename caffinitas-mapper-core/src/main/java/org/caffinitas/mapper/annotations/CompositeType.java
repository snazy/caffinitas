/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.annotations;

/**
 * Defines the type of a {@link CComposite}.
 */
public enum CompositeType {
    /**
     * A {@code COLUMNS} composite is "denormalized" into distinct columns.
     * An attribute that uses such a composite is null, if all its contained attributes are null.
     */
    COLUMNS(false),

    /**
     * A {@code COMPOSITE} composite uses Cassandra {@code org.apache.cassandra.db.marshal.CompositeType}.
     * An attribute that uses such a composite is null, if all its contained attributes are null.
     * Attributes in a {@code COMPOSITE} composite are index based.
     */
    COMPOSITE(true),

    /**
     * A {@code USER_TYPE} composite uses Cassandra 2.1 user types.
     * An attribute that uses such a composite is null, if the composite attribute is null.
     */
    USER_TYPE(true),

    /**
     * A {@code TUPLE} composite uses Cassandra {@code typle} type.
     * An attribute that uses such a composite is null, if all its contained attributes are null.
     * Attributes in a {@code TUPLE} composite are index based.
     */
    TUPLE(true);

    private final boolean singleColumn;

    CompositeType(boolean singleColumn) {this.singleColumn = singleColumn;}

    public boolean isSingleColumn() {
        return singleColumn;
    }
}
