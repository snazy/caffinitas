/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.converter.javatime;

import org.caffinitas.mapper.core.api.MultiColumnConverter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * Multi-column converter for {@link java.time.ZonedDateTime}.
 * <p>
 *     Types are: {@link Date}, {@link String}<br>
 *     Prop names are: {@code dt}, {@code tz}
 * </p>
 */
public class ZonedDateTimeConverter implements MultiColumnConverter<ZonedDateTime> {
    private static final Class<?>[] TYPES = {Date.class, String.class};
    private static final String[] NAMES = {"dt", "tz"};

    @Override public Class<ZonedDateTime> javaType() {
        return ZonedDateTime.class;
    }

    @Override public Class<?>[] cassandraTypes() {
        return TYPES;
    }

    @Override public String[] propertyNames() {
        return NAMES;
    }

    @Override public ZonedDateTime toJava(Object... value) {
        Date dt = (Date) value[0];
        if (dt == null) {
            return null;
        }
        ZoneId zone = ZoneId.of((String) value[1]);
        LocalDateTime ldt = LocalDateTime.from(dt.toInstant());
        return ZonedDateTime.of(ldt, zone);
    }

    @Override public Object[] fromJava(ZonedDateTime value) {
        if (value == null) {
            return null;
        }
        return new Object[]{
            new Date(value.toInstant().toEpochMilli()),
            value.getZone().getId()
        };
    }
}
