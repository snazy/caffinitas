/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.converter.javatime;

import org.caffinitas.mapper.annotations.DataTypeName;
import org.caffinitas.mapper.core.api.SingleColumnConverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

/**
 * Single-column converter for {@link java.time.LocalDate}.
 */
public class LocalDateConverter implements SingleColumnConverter<Date, LocalDate> {
    @Override public DataTypeName dataTypeName() {
        return DataTypeName.TIMESTAMP;
    }

    @Override public Class<LocalDate> javaType() {
        return LocalDate.class;
    }

    @Override public LocalDate toJava(Date value) {
        return value == null ? null : LocalDateTime.ofInstant(value.toInstant(), ZoneId.of("UTC")).toLocalDate();
    }

    @Override public Date fromJava(LocalDate value) {
        return value == null ? null : new Date(LocalDateTime.of(value, LocalTime.MIDNIGHT).toInstant(ZoneOffset.UTC).toEpochMilli());
    }
}
