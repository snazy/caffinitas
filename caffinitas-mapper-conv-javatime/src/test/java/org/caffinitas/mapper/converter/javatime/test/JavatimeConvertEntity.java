/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.caffinitas.mapper.converter.javatime.test;

import org.caffinitas.mapper.annotations.CColumn;
import org.caffinitas.mapper.annotations.CEntity;
import org.caffinitas.mapper.annotations.CMultiColumn;
import org.caffinitas.mapper.converter.javatime.OffsetDateTimeConverter;
import org.caffinitas.mapper.converter.javatime.LocalDateConverter;
import org.caffinitas.mapper.converter.javatime.LocalDateTimeConverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@CEntity(partitionKey = "id")
public class JavatimeConvertEntity {
    @CColumn private int id;

    @CMultiColumn(converter = OffsetDateTimeConverter.class) private OffsetDateTime dateTime;

    @CColumn(converter = LocalDateConverter.class) private LocalDate localDate;

    @CColumn(converter = LocalDateTimeConverter.class) private LocalDateTime localDateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public OffsetDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(OffsetDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}
